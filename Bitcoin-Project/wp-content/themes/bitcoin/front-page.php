<?php 

get_header(); ?>
<!-- <?php //echo do_shortcode('[tp_search_shortcodes slug="1c754292"]'); ?> -->
<!-- <iframe scrolling="no" width="620" height="305" frameborder="0" src="//www.travelpayouts.com/widgets/09cc645d32cf618deca829f57d37d9e5.html?v=1430"></iframe> -->
<!-- <script charset="utf-8" src="//www.travelpayouts.com/widgets/09cc645d32cf618deca829f57d37d9e5.js?v=1430" async></script>
 --><section class="work-sec" id="about" style="background-image: url(<?php the_field('work_background_image','option'); ?>);">
    <div class="container">

        <div class="works-content-sec" id="1about">
            <h1 class="text-center">HOW IT WORKS</h1>
           	
           		<?php if( have_rows('section2','option') ): ?>
                
                    <?php while ( have_rows('section2', 'option') ) : the_row(); ?>
                        <div class="row main-content">
			                <div class="">
			                    <div class="col-md-2 col-sm-2 col-xs-12">
			                        <div class="img-area-sec">
			                            <img src="<?php the_sub_field('work_image'); ?>" class="img-responsive">
			                        </div>
			                    </div>
			                    <div class="col-md-10 col-sm-10 col-xs-12">
			                        <div class="works-info">
			                            <h2><?php the_sub_field('work_title'); ?></h2>
			                            <p><?php the_sub_field('work_tagline'); ?></p>
			                        </div>
			                    </div>
			                </div>
			            </div>
                           
                    <?php  endwhile; ?>

			    <?php endif; ?>
			</div>
		</div>
	</section>



	            

           <!--  <div class="row main-content">
                <div class="">
                    <div class="col-md-2">
                        <div class="img-area-sec">
                            <img src="/bitcoin/wp-content/uploads/2018/07/second.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="works-info">
                            <h2>Easy Booking</h2>
                            <p>Search,select and save-the fastest way to book your trip</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row main-content">
                <div class="">
                    <div class="col-md-2">
                        <div class="img-area-sec">
                            <img src="/bitcoin/wp-content/uploads/2018/07/third.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="works-info">
                            <h2>24/7 Customer Care</h2>
                            <p>Get award-winning service and special deals by calling</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row main-content">
                <div class="">
                    <div class="col-md-2">
                        <div class="img-area-sec">
                            <img src="/bitcoin/wp-content/uploads/2018/07/forth.png" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="works-info">
                            <h2>Travel Protection</h2>
                            <p>Protect your travel investment, your belongings and most importantly</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section> -->
<section class="company-do" id="company" style="background-image: url(<?php the_field('about_background_image','option'); ?> );">
    <div class="container">
        <div class="">
            <div class="company-do-inner">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h1 class="text-center"><?php the_field('mission_title','option'); ?></h1>
                    <hr class="bar">
                    <p class="text-center"><?php the_field('mission_content','option'); ?> </p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h1 class="text-center"><?php the_field('what_we_do_title','option'); ?></h1>
                    <hr class="bar">
                    <p class="text-center"><?php the_field('what_we_do_content','option'); ?> </p>
                </div>
            </div>
        </div>
    </div>
</section>
										  
										  
<section class="cities-sec" id="cities">
    <div class="container">
        <div class="row">
            <?php $wpb_all_query = new WP_Query(array('post_type'=>'business_class', 'posts_per_page'=>6)); ?> 
                      <?php if ( $wpb_all_query->have_posts() ) : ?>
                          <?php 
                  while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?> 
            <div class="col-md-4 america">
                <div class="img-box">
                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive">
                </div>
                <div class="cities-content">
                    <h1 class="text-center"><?php the_title(); ?></h1>
                    <h3 class="text-center">50-70%*OFF</h3>
                    <h6 class="text-center">BUSINESS CLASS FLIGHT TO</h6>
                    <h2 class="text-center"><b>$2,642</b></h2>
                    <a href="" class="btn btn-default btn-rounded mb-4 search" data-toggle="modal" data-target="#modalRegisterForm">Search Flights >></a>
                    <!-- <a href="<?php the_permalink(); ?>"><button class="search">Search Flights >></button></a> -->
                </div>
            </div>
            <?php endwhile; ?>
                  <?php wp_reset_postdata(); ?>
                     
                    <?php else : ?>
                        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
            <!-- <div class="col-md-4">
                <div class="img-box">
                    <img src="/wp-content/uploads/2018/07/iphill-tower.png" class="img-responsive">
                </div>
                <div class="cities-content">
                    <h1 class="text-center">HONG KONG</h1>
                    <h3 class="text-center">50-70%*OFF</h3>
                    <h6 class="text-center">BUSINESS CLASS FLIGHT TO</h6>
                    <h2 class="text-center"><b>$2,642</b></h2>
                    <button class="search">Search Flights >></button>
                </div>
            </div>
            <div class="col-md-4">
                <div class="img-box">
                    <img src="/wp-content/uploads/2018/07/statue.png" class="img-responsive">
                </div>
                <div class="cities-content">
                    <h1 class="text-center">NEW YORK</h1>
                    <h3 class="text-center">50-70%*OFF</h3>
                    <h6 class="text-center">BUSINESS CLASS FLIGHT TO</h6>
                    <h2 class="text-center"><b>$2,642</b></h2>
                    <button class="search">Search Flights >></button>
                </div>
            </div> -->
        </div>
    </div>
</section>


<section class="Customer-reviews" id="customer">
    <div class="container">

    	<div class="row">
    		<div class="col-md-12">
    			<h1>CUSTOMERS REVIEW</h1>
                <p class="text-center sub-heading">Lorium ipsome is a simply dummy text</p>
    		</div>
    	</div>

        <div class="row">
			<div class='col-xs-12 col-sm-8 col-md-8 content-center'>
				<div class="carousel slide" data-ride="carousel" id="quote-carousel">
					<!-- Bottom Carousel Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#quote-carousel" data-slide-to="1"></li>
						<li data-target="#quote-carousel" data-slide-to="2"></li>
					</ol>
					<!-- Carousel Slides / Quotes -->
					<div class="carousel-inner">
						<?php 
						$wpb_all_query = new WP_Query(array('post_type'=>'testimonial','posts_per_page' => 3)); ?> 
					<?php if ( $wpb_all_query->have_posts() ) : ?>
						<?php $count = 0; 
						while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
					<!-- Quote 1 -->
					<div class="item <?php if($count == 0){ echo "active";} ?>">
					<blockquote>
					  <div class="row">
					  <div class="col-sm-3 text-center">
						<?php the_post_thumbnail('thumbnail', array( 'class' => 'img-circle img-thumbnail' )); ?>
						<!-- <img class="img-circle" src="http://www.reactiongifs.com/r/overbite.gif" style="width: 100px;height:100px;"> -->
						<!--<img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg" style="width: 100px;height:100px;">-->
					  </div>
					  <div class="col-sm-9 text-sean">
						<p class="niche"><?php the_content(); ?></p>
						<span><img src="<?php the_field('signature_image'); ?>" class="img-responsive"></span>
						<a href="#"><?php the_field('testimonial_link'); ?></a>

					  </div>
					  </div>
					</blockquote>
					</div>
					<?php $count++; endwhile; ?>
						<?php wp_reset_postdata(); ?>
						<?php else : ?>
						<p class="niche"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
						<?php endif; ?>
					<!-- Quote 2 -->
					<!--<div class="item">
					<blockquote>
					  <div class="row">
					  <div class="col-sm-3 text-center">
						<img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/mijustin/128.jpg" style="width: 100px;height:100px;">
					  </div>
					  <div class="col-sm-9">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit, eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris</p>
						<span><img src="/bitcoin/wp-content/uploads/2018/07/sign.png" class="img-responsive"></span>
						<a href="#">richardroe.com</a>

					  </div>
					  </div>
					</blockquote>
					</div>

					<div class="item">
					<blockquote>
					  <div class="row">
					  <div class="col-sm-3 text-center">
						<img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/keizgoesboom/128.jpg" style="width: 100px;height:100px;">
					  </div>
					  <div class="col-sm-9">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit, eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.</p>
						<span><img src="/bitcoin/wp-content/uploads/2018/07/sign.png" class="img-responsive"></span>
						<a href="#">richardroe.com</a>

					  </div>
					  </div>
					</blockquote>
					</div>-->
				</div>

					<!-- Carousel Buttons Next/Prev -->
					<!-- <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
<a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a> -->
				</div>
			</div>
		</div>
    </div><!--./container-->
</section><!--./Customer-reviews-->


<section class="logo-section">
    <div class="container">
        <!--   <ul id="flexiselDemo3">
    <li><img src="images/1.jpg" /></li>
    <li><img src="images/2.jpg" /></li>
    <li><img src="images/3.jpg" /></li>
    <li><img src="images/4.jpg" /></li>                                                 
  </ul> -->
        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:150px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:50px;left:0px;width:100%;height:50%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
            </div>
            <?php if( have_rows('sliders','option') ): ?>
                <div data-u="slides" style="cursor:default;position:relative;top:22px;left:0px;width:1170px;height:100px;overflow:hidden;">
                    <?php 
          while ( have_rows('sliders', 'option') ) : the_row(); 
            //$image  = get_sub_field('slider_logo');           
           // echo "〈pre〉"; print_r(the_row());
            ?>

                        <div>
                            <img data-u="image" src="<?php the_sub_field('logo_slider'); ?>" />
                        </div>
                        <?php  endwhile; ?>

                </div>
                <?php endif; ?>
        </div>
        <!-- <div>
          <img data-u="image" src="/bitcoin/wp-content/uploads/2018/07/asiana.png" />
        </div>
        <div>
          <img data-u="image" src="/bitcoin/wp-content/uploads/2018/07/korean-air.png" />
        </div>
        <div>
          <img data-u="image" src="/bitcoin/wp-content/uploads/2018/07/pecific.png" />
        </div>
        <div>
          <img data-u="image" src="/bitcoin/wp-content/uploads/2018/07/qatar.png" />
        </div>
        <div>
          <img data-u="image" src="/bitcoin/wp-content/uploads/2018/07/singapore.png" />
        </div>
        <div>
          <img data-u="image" src="/bitcoin/wp-content/uploads/2018/07/asiana.png" />
        </div>
        <div>
          <img data-u="image" src="/bitcoin/wp-content/uploads/2018/07/korean-air.png" />
        </div>
        <div>
          <img data-u="image" src="/bitcoin/wp-content/uploads/2018/07/singapore.png" />
        </div>
        <div>
          <img data-u="image" src="/bitcoin/wp-content/uploads/2018/07/qatar.png" />
        </div>
        <div>
          <img data-u="image" src="/bitcoin/wp-content/uploads/2018/07/singapore.png" /> -->
    </div>
    </div>
    </div>
    </div>
</section>
<?php get_footer();?>