  <!DOCTYPE html>
  <html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
    
    <title>bitcoin cash</title>

    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
      
      <!-- <script charset="utf-8" type="text/javascript" src="http://http://ontap8.mobilytedev.com//iframe.js"></script> -->
     <script>var ajaxurl= '<?php echo admin_url( 'admin-ajax.php' ) ?>'</script>
     <style type="text/css">
.btn.btn-lg.btn-primary.getfareprices {
	border-radius: 0;
	border-top-left-radius: 35px;
	border-bottom-right-radius: 35px;
	background-color: #463373;
	color: #fff;
	padding: 17px 40px;
	border: 0;
	position: absolute;
	top: 6px;
	right: 6px;
}
.btn-style{width: 20%;}	 

        #sel2 {
        width: 74px;
        float: left;
        height: 70px;
        /*border-right: 1px solid #808080 !important;*/
        -webkit-appearance: none;
        border-right: none;
        border-left: 0;
        -moz-appearance: none;
        appearance: none;
        }
.drop-errow1{ position: relative; padding: 0 0 0 20px;}		 
.drop-errow2::before {
	color: #333;
	content: "\f183";
	font-family: "FontAwesome";
	font-size: 18px;
	position: absolute;
	pointer-events: none;
	right: 70px;
	top: 20px;
	z-index: 1;
}	 
        /*#myModalNorm {
         padding-right: 45%;
         margin-top: 5% !important;
        }*/
        #myModalNorm .destination {
          display: flex !important;
          padding: 10px !important;
          font-size: 24px !important;
          justify-content: center !important;
          align-items: center !important;
      }
      #myModalNorm .price-wrapper {
          display: flex !important;
          padding: 30px 0 10px !important;
          font-size: 24px !important;
          justify-content: center !important;
          align-items: center !important;
      }
      #myModalNorm .destination i {
        margin: 0 10px;
        }
      #myModalNorm .price-wrapper .price {
        font-size: 60px;
        font-weight: bold;
        color: #d53c4f;
        }
      #myModalNorm .bottom {
        margin-top: 175px;
        padding-top: 10px;
        border-top: dashed 1px #dfdfdf;
      }

      /* Modal Form  CSS*/
      #myModalNorm .get-a-quote-second-form {
       width: auto;
        }
      .get-a-quote-second-form .form-messages {
        display: none;
        flex-direction: column;
        width: 100%;
        text-align: center;
        padding: 20px;
        justify-content: center;
        align-items: center;
        min-height: 320px;
        background-color: rgba(255,255,255,0.9);
      }

      .get-a-quote-second-form .form-messages p {
        margin: 10px 0;
        font-size: 15px;
      }

      .b-btn {
        display: inline-flex;
        padding: 0 15px;
        height: 40px;
        justify-content: center;
        align-items: center;
        border-width: 0;
        outline: none;
        border-radius: 2px;
        box-shadow: 0 1px 4px rgba(0,0,0,0.6);
        background-color: #3e62d2; 
        color: #ecf0f1;
        transition: background-color .3s;
        position: relative;
        font-size: 14px;
        font-weight: bold;
        cursor: pointer;
    }
    .get-a-quote-second-form .form-container {
      display: flex;
      flex-direction: column;
      width: 100%;
    }
    .get-a-quote-second-form .pseudo-radio {
      display: flex;
      width: 100%;
      padding: 0 10px 10px;
      max-width: 400px;
      background-color: none !important;
    }

    .get-a-quote-second-form .pseudo-radio li {
      flex-grow: 1;
      height: 36px;
    }


  .get-a-quote-second-form .pseudo-radio li>label {
      height: 36px;
      display: inline-flex;
      align-items: center;
      justify-content: center;
      width: 100%;
      position: relative;
      overflow: hidden;
  }

  .get-a-quote-second-form .form-body {
    display: flex;
    width: 97%;
    flex-wrap: wrap;
    background-color: #fff;
    padding: 15px 0 0;
    }
  .get-a-quote-second-form [class^="col-md-"] {
    padding-left: 10px;
    padding-right: 10px;
  }


  .get-a-quote-second-form .pseudo-radio li>label input {
    opacity: 0;
    height: 30px;
    margin: 0;
    position: absolute;
    width: 100%;
    /*opacity: 0;
    width: 0;
    height: 0;
    margin: 0;
    position: absolute;
    right: 1000%; */
  }

  .get-a-quote-second-form .form-item.required {
    position: relative;
  }
  .get-a-quote-second-form .form-item {
      width: 100%;
      margin-bottom: 15px;
      font-size: 13px;
      height: 40px;
      border: solid 1px #dfdfdf;
      border-radius: 3px;
  }
  .get-a-quote-second-form .form-item .easy-autocomplete{width: 100%!important;}
  .get-a-quote-second-form .form-item input {
    border: 0;
    width: 100%;
    height: 100%;
    line-height: 100%;
    padding: 10px;
    font-size: 15px;
    font-weight: bold;
    background: 0;
}
  .get-a-quote-second-form .form-item select {
    border: 0;
    width: 100%;
    height: 100%;
    line-height: 100%;
    padding: 0 10px;
    background: #fff;
    font-size: 15px;
    font-weight: bold;
}
.get-a-quote-second-form .pseudo-radio li>label input:checked+label {
    color: #fff;
    background-color: #233467;
    pointer-events: none;
}
.get-a-quote-second-form .form-actions button {
    width: 85%;
    height: 60px;
    border-bottom-left-radius: 20px;
    border-top-right-radius: 20px;
    line-height: 60px;
    font-weight: bold;
    font-size: 20px;
    color: #fff;
    background-color: #d33d3d;
    border: 0;
    cursor: pointer;
    transition: 0.2s,box-shadow 0.28s cubic-bezier(0.4, 0, 0.2, 1);
}
  ol, ul {
    list-style: none;
}
.get-a-quote-second-form .pseudo-radio li>label label {
    color: #233467;
    font-size: 14px;
    background-color: #cbcdd2;
    height: 36px;
    width: 100%;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    transition: 0.2s;
}
#myModalNorm .modal-open {
    overflow: scroll !important;
}
#myModalNorm .modal-dialog {
    width: 400px !important;
    margin: 45px auto 0 !important;
    height: 500px !important;
    
}

.flight-info .error {
    color: #d25353;
}
.form-item .error {
    color: #d25353;
}
.pac-container:after {
    /* Disclaimer: not needed to show 'powered by Google' if also a Google Map is shown */

    background-image: none !important;
    height: 0px;
}


		 
/*Css for booking form*/		 
         #modalRegisterForm {top: 60px;}
		 #modalRegisterForm .modal-header{ background: #f2f2f2; }
		 #modalRegisterForm .modal-header .close{margin-top: -30px;}
		 .flight_booking .btn{ background: #d33d3d; color: #fff;}
		 .flight_booking .btn:hover{ background: #463373;}
		 
		 

	

		 
@media (min-width: 768px){
    #myModalNorm .modal-dialog {
    width: 1024px !important;
    margin: 45px auto !important;
}
}
		 
@media (max-width: 767px){
  #myModalNorm .modal-dialog{width:320px !important;margin:10px auto 0 !important;height:500px !important;overflow-y:scroll !important;}
  #myModalNorm .bottom {    margin-top: 20px !important; }
}
		 

	
	#myModalNorm {
    /* padding-right: 0% !important; */
    margin-top: 5% !important;
}
/*.modal{transform:translate(0%,12%) !important;}*/
}
@media (max-width: 479px) {
	
	#myModalNorm {
    /* padding-right: 0% !important; */
    margin-top: 5% !important;
}
/*.modal{transform:translate(0%,12%) !important;}*/
}
      </style>
      
     <!--  <script charset="utf-8" src="//www.travelpayouts.com/widgets/09cc645d32cf618deca829f57d37d9e5.js?v=1430" async></script> -->
     <?php wp_head(); ?>
    </head>
    <body>
  <header>
  
  



  <nav class="navbar navbar-default navbar-fixed-top " data-spy="affix" data-offset-top="197">
    <div class="container-fluid">
      <a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="/wp-content/uploads/2018/07/bitcoin-flight-white.png" class="img-responsive"></a>
      <div id="mySidenav" class="sidenav">        
        <a class="menu-list menus" data-scroll href="#about">How it works</a>
        <a class="menu-list menus" data-scroll href="#cities">Offers</a>
        <a class="menu-list menus" data-scroll href="#company">About</a>
        <a class="menu-list menus" data-scroll href="#customer">Reviews</a>
      </div>
      
      <div id="nav-icon4" class="open_menu">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </nav>
    <div class="background-top" style="background-image: url(<?php the_field('background_image','option'); ?>);">
    <div class="container">
  <div class="banner-content">
  <h1><?php the_field('header_title','option'); ?></h1>
  <P><?php the_field('header_content','option'); ?></P>
  </div>
    <form name="myform" id="get_a_quotes" class="get-a-quote-second-form">
  <div class="round-trip">

    <div class="form-head">
                <ul class="pseudo-radio">
                    <li>
                        <label>
                            <input id="round_trip_radios" type="radio" name="trip_type" value="round_trip" checked="">
                            <label for="round_trip_radio">Round trip</label>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input id="one_way_radios" value="one_way" type="radio" name="trip_type">
                            <label for="one_way_radio">One way</label>
                        </label>
                    </li>                   
                </ul>
            </div>

    <!-- <button id="hide">Round Trip</button>
    <button id="show">One Way</button> -->
  <!-- <p>Round Trip  <span>One Way</span></p> -->
  </div>
  <div class="flight-info">
    <table class="table-responsive">
      <tr>
        <td><input name="startfrom" type="text" placeholder="From" class="Location" id="Location" required></td>
        <input type="hidden" id="OriginAirport" />
        <td><input name="endto" type="text" placeholder="To" class="Location" id="Location2" required></td>
        <td><input name="departdate" type="text" id="datepicker" class="date1" placeholder="Depart" readonly /></td>
        <td><input name="returndate" type="text" id="datepicker2" class="date2" placeholder="Return" readonly /></td>
        <td class="drop-errow1">	
		  <span class="drop-errow2"></span> 	
          <select class="form-control" id="sel2" name="no_seat">			
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </td>
        <td>
            <select class="form-control angle" id="sel1" name="seat_type">
   
    <option value="business">Business Class</option>
    <option value="first">First Class</option>
   <!--  <option value="business">BUSSINESS</option>
    <option value="economy">Economy</option> -->
    </select>
        </td>
        <td class="btn-style"><a class="btn btn-lg btn-primary getfareprices">SEARCH FLIGHT</a></td>
      </tr>
    </table>
 

  
    
  <!-- <div class="input-daterange" id="datepicker">
    
  <input type="text" name="departdate" placeholder="Depart" id="js-date">
  <input type="text" name="returndate" placeholder="Return" id="js-dates">
  </div> -->
    
    
  

   <!--  <button CLASS="SEARCH-FLIGHT" data-toggle="modal" data-target="#myModalNorm">
    Launch Normal Form
    </button> -->
    
     <!-- <button CLASS="SEARCH-FLIGHT getfareprice">SEARCH FLIGHT</button> --> 
  </div>
  </form>
</div>
  </div>

  </header>