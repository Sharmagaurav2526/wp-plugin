<?php 

//  To register a logo

$inc_path = TEMPLATEPATH . '/inc/';

function theme_prefix_setup() {
  
  add_theme_support( 'custom-logo', array(
    'height'      => 100,
    'max-width'       => 300,
    'flex-width' => true,
  ) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );

//  To register nav-menus in Wordpress 
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'top-menu' => __( 'Top Menu' ),
      'Header-sidebar-menu'    => __( 'Header-sidebar-menu', 'bitcoin' ),
      'footer-menu' => (' Footer Menu ')
    )
  );
}
add_action( 'init', 'register_my_menus' );



add_filter( 'nav_menu_link_attributes', 'wpse156165_menu_add_class', 10, 3 );

function wpse156165_menu_add_class( $atts, $item, $args ) {
    $class = 'nav-link'; // or something based on $item
    $atts['class'] = $class;
    return $atts;
}



// To register a widget in wordpress
add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget'  => '</li>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Sidebar 2', 'theme-slug' ),
        'id' => 'sidebar-2', 
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget'  => '</li>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Sidebar 3', 'theme-slug' ),
        'id' => 'sidebar-3',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<ul class="menu pages"><li id="%1$s" class="menu-item menu-item-type-post_type menu-item-object-page">',
    'after_widget'  => '</li></ul>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Recent Sidebar', 'theme-slug' ),
        'id' => 'sidebar-4',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}  
//  End register custom widgets in Wordpress
//   For register  feature image in Wordpress 
add_theme_support( 'post-thumbnails' ); 
// End function here



/*--function to add theme options start--*/
function my_acf_options_page_settings( $settings )
{
    $settings['title'] = 'Theme Options';
    $settings['pages'] = array('Header','Footer','Client Logo','Section2');
    return $settings;    
}
add_filter('acf/options_page/settings', 'my_acf_options_page_settings');
/*--//function to add theme options end--*/



/*-- Get fair Detail start here--*/

add_action( 'wp_ajax_get_flight_fare_detail', 'get_flight_fare_detail' );
add_action( 'wp_ajax_nopriv_get_flight_fare_detail', 'get_flight_fare_detail' );

function get_flight_fare_detail() {
    global $wpdb;
    extract($_POST); //die;
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
    $yname = $_REQUEST['yname'];
    $yemail = $_REQUEST['yemail'];
    $yphone = $_REQUEST['yphone'];

    $new_post1 = array(
    'post_title' => $yname,
    'post_status'   =>  'pending',
    'post_type' =>  'flight_booking'
    //'post_type' =>  'flight'
    );


    $pid = wp_insert_post($new_post1);
    add_post_meta($pid,'yemail',$yemail);
    add_post_meta($pid,'yphone',$yphone);
    add_post_meta($pid,'yname',$yname);
 
        $to              = $yemail;                                                         
        $subject         = "Bitcoin Cash";
        $from            = "sunpreet@mobilyte.com";  //lo)o)pN1Uwe      
        $headers         = "MIME-Version: 1.0" . "\n";
        $headers        .= "Content-type:text/html;charset=iso-8859-1" . "\n";
        $headers        .= 'Cc: gaurav@mobilyte.com' . "\r\n";       
        $headers        .= "From: Bitcoin Cash"."<".$from.">\nReply-To: ".$from."";        
        
        $mailContent1="<table width='500' border='1' cellpadding='0' cellspacing='0' bordercolor='#CCCCCC'>  <tr>    <td><table width='500' border='0' cellspacing='0' cellpadding='8'><tr><td colspan='2' bgcolor='#CDD9F5'><strong>APPLICATION FOR FlIGHT BOOKING</strong></td></tr><tr><td bgcolor='#FFFFDD'><strong>APPLICANT NAME: </strong></td><td bgcolor='#FFFFDD'>$yname</td></tr><tr><td bgcolor='#FFFFEC'><strong>APPLICANT PHONE: </strong></td><td bgcolor='#FFFFEC'> $yphone </td></tr><tr><td bgcolor='#FFFFEC'><strong>APPLICANT EMAIL: </strong></td><td bgcolor='#FFFFEC'> $yemail </td></tr>";


        $success = wp_mail($to,$subject,$mailContent1,$headers);  

        if($success){
              echo "Sent"; exit;
          } else {
              echo "Not Sent"; exit;
          }

 die();
}

/*-- Get fair Detail end here--*/




/*--function start for get_flight_fare_price filter--*/
add_action( 'wp_ajax_get_flight_fare_price', 'get_flight_fare_price' );
add_action( 'wp_ajax_nopriv_get_flight_fare_price', 'get_flight_fare_price' );
function get_flight_fare_price() {
    global $wpdb;
    extract($_POST);
    echo $price = $_REQUEST['price'];
    echo $trip_type = $_REQUEST['trip_type'];
    echo $location_from = $_REQUEST['location_from'];
    echo $location_to = $_REQUEST['location_to'];
    echo $depart_date = $_REQUEST['depart_date'];
    echo $return_date = $_REQUEST['return_date'];
    echo $username = $_REQUEST['username'];
    echo $phone = $_REQUEST['phone'];
    echo $email = $_REQUEST['email'];
    echo $passengers = $_REQUEST['passengers'];
    echo $flight_class = $_REQUEST['flight_class'];

    /*$insert_array = array('price' => $price,
        'startfrom' => $startfrom,
        'endto' => $endto, 
        'departdate'=>$departdate,
        'returndate' => $returndate,
        'seat_type' => $seat_type
        
         );*/
    $a = 1;
    $b = $a++;     
    $new_post = array(
    'post_title' => $username,
    'post_status'   =>  'pending',
    'post_type' =>  'flight_booking'
    //'post_type' =>  'flight'
    );
    //echo "<pre>"; print_r($new_post); echo "</pre>"; 
    $pid = wp_insert_post($new_post);
    add_post_meta($pid,'price',$price);
    add_post_meta($pid,'trip_type',$trip_type);
    add_post_meta($pid,'location_from',$location_from);
    add_post_meta($pid,'location_to',$location_to);
    add_post_meta($pid,'depart_date',$depart_date);
    add_post_meta($pid,'return_date',$return_date);
    add_post_meta($pid,'username',$username);
    add_post_meta($pid,'phone',$phone);
    add_post_meta($pid,'email',$email);
    add_post_meta($pid,'passengers',$passengers);
    add_post_meta($pid,'flight_class',$flight_class);

        $to              = $email;                                                         
        $subject         = "Bitcoin Cash";
        $from            = "sunpreet@mobilyte.com";  //lo)o)pN1Uwe      
        $headers         = "MIME-Version: 1.0" . "\n";
        $headers        .= "Content-type:text/html;charset=iso-8859-1" . "\n";
        $headers        .= 'Cc: gaurav@mobilyte.com' . "\r\n";       
        $headers        .= "From: Bitcoin Cash"."<".$from.">\nReply-To: ".$from."";        
        
        $mailContent="<table width='500' border='1' cellpadding='0' cellspacing='0' bordercolor='#CCCCCC'>  <tr>    <td><table width='500' border='0' cellspacing='0' cellpadding='8'><tr><td colspan='2' bgcolor='#CDD9F5'><strong>APPLICATION FOR FlIGHT BOOKING</strong></td></tr><tr><td bgcolor='#FFFFEC'><strong>TYPE OF TRIP: </strong></td><td bgcolor='#FFFFEC'> $trip_type </td></tr><tr><td bgcolor='#FFFFDD'><strong>APPLICANT NAME: </strong></td><td bgcolor='#FFFFDD'>$username</td></tr>
        <tr><td bgcolor='#FFFFEC'><strong>APPLICANT EMAIL: </strong></td><td bgcolor='#FFFFEC'> $email </td></tr><tr><td bgcolor='#FFFFDD'><strong>APPLICANT PHONE: </strong></td><td bgcolor='#FFFFDD'>  $phone</td> </tr><tr><td bgcolor='#FFFFEC'><strong>LOCATION FROM: </strong></td><td bgcolor='#FFFFEC'> $location_from </td></tr><tr><td bgcolor='#FFFFDD'><strong>LOCATION TO: </strong></td><td bgcolor='#FFFFDD'> $location_to </td></tr><tr><td bgcolor='#FFFFEC'><strong>DEPART DATE: </strong></td><td bgcolor='#FFFFEC'> $depart_date </td></tr><tr><td bgcolor='#FFFFDD'><strong>RETURN DATE: </strong></td><td bgcolor='#FFFFDD'> $return_date </td></tr><tr><td bgcolor='#FFFFEC'><strong>Total PASSANGERS: </strong></td><td bgcolor='#FFFFEC'> $passengers </td></tr><tr><td bgcolor='#FFFFEC'><strong>TOTAL PRICE: </strong></td><td bgcolor='#FFFFEC'> $price </td></tr><tr><td bgcolor='#FFFFDD'>&nbsp;</td><td bgcolor='#FFFFDD'> ----- </td></tr></table></td></tr></table>";




        /*$mailContent   = '<html><body>';
        $mailContent  .= '<h3> sent you an Engagement Request Form. </h3>';
        
        $mailContent  .= '<p>Company Name : ..</p>'."\r\n";    

        $mailContent  .= '<p>Thank You,</p>';
        $mailContent  .= '<p>Bitcoin Team</p>';        
        $mailContent  .= '</body></html>';*/ 

        $success = wp_mail($to,$subject,$mailContent,$headers);  

        if($success){
              echo "Sent"; exit;
          } else {
              echo "Not Sent"; exit;
          }

/*--function start to get flight price--*/
/*add_action( 'wp_ajax_get_flight_price', 'get_flight_price' );
add_action( 'wp_ajax_nopriv_get_flight_price', 'get_flight_price' );
function get_flight_price() {
    global $wpdb;
    extract($_POST);
    $test = $_REQUEST['test'];




    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://dev-sandbox-api.airhob.com/sandboxapi/flights/v1.3/search",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{ \"TripType\": \"O\", \"NoOfAdults\": 1, \"NoOfChilds\": 0, \"NoOfInfants\": 0, \"ClassType\": \"Economy\", \"OriginDestination\": [ { \"Origin\": \"SFO\", \"Destination\": \"LAX\", \"TravelDate\": \"08/23/2018\" } ], \"Currency\": \"USD\" }",
      CURLOPT_HTTPHEADER => array(
        "apikey: 65bba6d2-6e92-4",
        "cache-control: no-cache",
        "content-type: application/json",
        "mode: sandbox",
        "postman-token: 4d6c7dc5-a48e-6549-9f72-266d54f2fe29"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      $response;

    $searchflights = json_decode($response, true);

    /*echo '<pre>';
    print_r($searchflights);
    echo '</pre>';*/


  die();
}
/*--//function end to get flight price--*/