<?php
get_header();
/*if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		the_title();
		the_post_thumbnail('small');
		the_content();
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;*/
//get_sidebar();
//get_footer();
?>
<style>
.table-destination-list h2 {
    text-align: center;
}
#destination_page h2 {
    margin: 40px 0;
    text-align: center;
}
.container {
    max-width: 1140px;
}
.marg-b-40 {
    margin-bottom: 40px !important;
}
.destination-table .table-body .list-item {
    height: 45px;
    background-color: #233467;
    color: #fff;
    font-size: 15px;
    font-weight: bold;
    padding: 0 30px;
    border-bottom-left-radius: 15px;
    border-top-right-radius: 15px;
    margin-bottom: 3px;
    transition: 0.15s;
}
.destination-table .list-item {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-between;
}
.destination-table .list-item .departutre {
    width: 40%;
}
.destination-table .list-item .destination {
    width: 25%;
}
.destination-table .list-item .price {
    width: 20%;
}

</style>
<div class="container">

        <div class="table-destination-list">

            <h2>Suggested Flights to <b><?php the_title(); ?></b></h2>
            
            <div class="container">
                <div class="destination-table marg-b-40">
            <div class="table-body">
        <div class="row">
            <?php if( have_rows('business_class_flights') ): ?>
                
                    <?php while ( have_rows('business_class_flights') ) : the_row(); ?>
            <div class="col-md-6">
                    <div class="list-item">

                        <div class="departutre">
                            <?php the_sub_field('business_from'); ?>
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination"><?php the_sub_field('business_class_to'); ?></div>
                            
                        <div class="price"><?php the_sub_field('business_class_price'); ?></div>
                    </div>
                </div>

                <?php  endwhile; ?>

                <?php endif; ?>

                <!--<div class="col-md-6">
                    <div class="list-item" data-city-from="Boston" data-id-from="8303" data-airport-from="BOS" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            Boston
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,824
                            *
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="list-item" data-city-from="Chicago" data-id-from="11401" data-airport-from="CHI" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            Chicago
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,879
                            *
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="list-item" data-city-from="Dallas" data-id-from="8523" data-airport-from="DFW" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            Dallas
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,877
                            *
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="list-item" data-city-from="Houston" data-id-from="8405" data-airport-from="IAH" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            Houston
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,860
                            *
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="list-item" data-city-from="Los Angeles" data-id-from="8339" data-airport-from="LAX" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            Los Angeles
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,504
                            *
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="list-item" data-city-from="Miami" data-id-from="8431" data-airport-from="MIA" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            Miami
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,918
                            *
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="list-item" data-city-from="Orlando" data-id-from="8271" data-airport-from="ORL" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            Orlando
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,946
                            *
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="list-item" data-city-from="Philadelphia" data-id-from="8603" data-airport-from="PHL" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            Philadelphia
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,846
                            *
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="list-item" data-city-from="San Francisco" data-id-from="8324" data-airport-from="SFO" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            San Francisco
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,643
                            *
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="list-item" data-city-from="Seattle" data-id-from="8432" data-airport-from="SEA" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            Seattle
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,726
                            *
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="list-item" data-city-from="Washington" data-id-from="11406" data-airport-from="WAS" data-city-to="Sydney" data-id-to="8222" data-airport-to="SYD">

                        <div class="departutre">
                            Washington
                        </div>

                        <i class="fas fa-exchange-alt"></i>
                        <div class="destination">Sydney</div>
                                                <div class="price">$2,860
                            *
                        </div>
                    </div>
                </div>-->

                    </div>
    </div>
</div>


<script>

    var $table = $('.destination-table .table-body');
    var $item = $('.list-item', $table);

    $item.on('click', function () {
        // $(document).scrollTop(0);
        var $this = $(this);
        // var $from = $('.departutre',$this);
        // var $to = $('.destination',$this);

        console.log($this);
        console.log($this.attr('data-city-from'));

        var city_from = $this.attr('data-city-from');
        var city_from_id = $this.attr('data-id-from');
        var city_from_airport = $this.attr('data-airport-from');

        var city_to = $this.attr('data-city-to');
        var city_to_id = $this.attr('data-id-to');
        var city_to_airport = $this.attr('data-airport-to');


        $('#location_from').val(city_from + '[' + city_from_airport + ']');
        $('#location_from').attr('data-airport-id', city_from_id);
        $('#location_from').attr('data-city', $this.attr('data-city-from'));
        $('#location_to').val(city_to + '[' + city_to_airport + ']');
        $('#location_to').attr('data-airport-id', city_to_id);
        $('#location_to').attr('data-city', city_to);

    })

</script>            </div>


        </div>
    </div>
<?php  get_footer(); ?>