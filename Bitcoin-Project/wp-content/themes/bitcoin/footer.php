<footer>
    <div class="container">
    <div class="col-md-6 col-sm-6 col-xs-12">
    <p><?php the_field('copyright' , 'option'); ?></a></p>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="social-icons">
    <ul>
      <?php
        if( have_rows('social_icons' , 'option' ) ):
        while ( have_rows('social_icons' , 'option' ) ) : the_row(); ?>
      <li>
        <a href="#">
          <img src="<?php the_sub_field('social_image'); ?>">
        </a>
      </li>
        
      <?php 
       endwhile;
       else :
       endif;
         ?> 
    <!-- <li><a href="#"><img src="images/google+.png"></a></li>
    <li><a href="#"><img src="images/tweeter.png"></a></li>
    <li><a href="#"><img src="images/facebook.png"></a></li> -->
    </ul>
    </div>
    </div>
    
    </div>
    
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.js"></script>
   <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=get_template_directory_uri()?>/js/bootstrap.min.js"></script>
    <script src="<?=get_template_directory_uri()?>/js/jssor.slider-27.4.0.min.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGB9KruySvaRWEPbNuqeLoGdMd-DMh-Qk&libraries=places&language=en"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
 <!-- <script charset="utf-8" src="//www.travelpayouts.com/widgets/09cc645d32cf618deca829f57d37d9e5.js?v=1430" async></script> -->
 
<script type="text/javascript">
 jQuery("#get_a_quotes").validate({
      errorClass   : "error-message",
      validClass   : "success-message",
      rules: {
        startfrom: {
          required: true,
        },
        endto: {
          required: true,
        },
        departdate: {
          required: true,
        },        
        no_seat: {
          required: true,
        },
        seat_type: {
          required: true
        }
      },
      messages: {
        startfrom: {
          required: 'Fill Out!'
        },
        endto: {
          required: 'Fill Out!'
        },
        departdate: {
          required: 'Fill Out!'
        }

      }
    });
</script>
<script type="text/javascript">
  jQuery("#get_a_quote").validate({
  errorClass   : "error-message",
  validClass   : "success-message",
  rules: {
    location_from: {
          required: true
        },
    //location_from: "required",
    location_to:{
      required : true,
    },
    depart:{
      required : true,
    },
    return:{
      required : true,
    },
    username:{
      required : true,
    },
    phone:{
      required : true,
    },
    email:{
      required : true,
    },
    passengers:{
      required : true,
    },   
    flight_class:{
      required : true
    }     
  }
});
</script>

<script type="text/javascript">
 jQuery("#get_a_quote2").validate({
      errorClass   : "error-message",
      validClass   : "success-message",
      rules: {
        yname: {
          required: true,
        },
        yemail: {
          required: true,
        },
        yphone: {
          required: true,
        }
      },
      messages: {
        yname: {
          required: 'Fill Out!'
        },
        yemail: {
          required: 'Fill Out!'
        },
        yphone: {
          required: 'Fill Out!'
        }

      }
    });
</script>
<!--  <script>
      var input = document.getElementById('autocomplete');
      var autocomplete = new google.maps.places.Autocomplete(input);
    </script>

      <script>
      var input = document.getElementById('autocompletes');
      var autocomplete = new google.maps.places.Autocomplete(input);
    </script> -->
   
    <script type="text/javascript">
      $(function () {
        $("#datepicker").datepicker({ 
              autoclose: true,
              startDate: new Date()  
              //todayHighlight: true
        });
      });
    </script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		$("#one_way_radio").click(function(){
    			$("#return_back").hide();
    		});
    		$("#round_trip_radio").click(function(){
    			$("#return_back").show();
    		});
    	});
    </script>
    <script type="text/javascript">
      jQuery(document).ready(function(){
      	//alert("2342");
        jQuery(".pseudo-radio li input#one_way_radios").on("click",function(){
        	//alert("1231");
          jQuery("#datepicker2").hide();
        });
        jQuery(".pseudo-radio li input#round_trip_radios").click(function(){
        	//alert("sdfjskdghk");
          jQuery("#datepicker2").show();
        });
      });
    </script>
    <script>
     /* $(document).ready(function(){
          $("#hide").click(function(){
          	alert('yes');
              $("#datepicker2").hide();
          });
          $("#show").click(function(){
              $("#datepicker2").show();
          });
      });*/
    </script>
    <script type="text/javascript">
      $(function () {
        $("#datepicker2").datepicker({ 
              autoclose: true,
              startDate: new Date()  
              //todayHighlight: true
        });
      });
    </script>
     <script type="text/javascript">
      $(function () {
        $("#depart_date").datepicker({ 
              autoclose: true,
              startDate: new Date()  
              //todayHighlight: true
        });
      });
    </script>
     <script type="text/javascript">
      $(function () {
        $("#return_date").datepicker({ 
              autoclose: true,
              startDate: new Date()  
              //todayHighlight: true
        });
      });
    </script>
  <script type="text/javascript">
    jssor_1_slider_init = function() {

      var jssor_1_options = {
        $AutoPlay: 1,
        $Idle: 0,
        $SlideDuration: 5000,
        $SlideEasing: $Jease$.$Linear,
        $PauseOnHover: 4,
        $SlideWidth: 140,
        $Align: 0
      };

      var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

      /*#region responsive code begin*/

      var MAX_WIDTH = 1170;

      function ScaleSlider() {
        var containerElement = jssor_1_slider.$Elmt.parentNode;
        var containerWidth = containerElement.clientWidth;

        if (containerWidth) {

          var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

          jssor_1_slider.$ScaleWidth(expectedWidth);
        }
        else {
          window.setTimeout(ScaleSlider, 30);
        }
      }

      ScaleSlider();

      $Jssor$.$AddEvent(window, "load", ScaleSlider);
      $Jssor$.$AddEvent(window, "resize", ScaleSlider);
      $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
      /*#endregion responsive code end*/
    };
  </script>

<script type="text/javascript">jssor_1_slider_init();</script>
<script>
$(document).ready(function() {
//Set the carousel options
$('#quote-carousel').carousel({
pause: true,
interval: 4000,
});
});
</script>
<script>
  $(document).ready(function(){
  
  function scrollToSection(event) {
  event.preventDefault();
  var $section = $($(this).attr('href')); 
  $('html, body').animate({
    scrollTop: $section.offset().top
  }, 1000);
  }

  $('[data-scroll]').on('click', scrollToSection);
});
</script>
<script>
function openNav() {
    // document.getElementById("mySidenav").style.width = "30%";
	$(".sidenav").addClass("intro");
}
function closeNav() {
    // document.getElementById("mySidenav").style.width = "0";      
	$(".sidenav").removeClass("intro");
}

</script>

<script type="text/javascript">
  jQuery(".menus").on("click",function(event){
    event.preventDefault();
    $("#nav-icon4").trigger( 'click' );
    // closeNav();
    // $(this).removeClass('open');
});
</script>


<script>
$(document).ready(function(){
  $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function( event ){
  if($(this).hasClass('open')){  
    closeNav();
    $(this).removeClass('open');
  }else{    
    $(this).toggleClass('open');
    openNav();
    }
  });
});
</script>

<script type="text/javascript">
jQuery(document).ready(function(){
  jQuery(".getflightdata").on("click",function(){
    jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {
              'action':'get_flight_price',
              'test' : '123'
          },
          success:function(response) {
              console.log(response);
              //alert(response);
              if(response !== ''){
                 }else{
                 }
          },
          error: function(errorThrown){
              //console.log(errorThrown);
              //alert("fail");
          }
    });
  });
});
</script>



<script type="text/javascript">
  jQuery(".getfareprices").on("click",function(event){// Click to only happen on announce links
     //$("#cafeId").val($(this).data('id'));
      if(jQuery("#get_a_quotes").valid()){
        //alert("valid");
      event.preventDefault();
      //var random = Math.floor(100 + Math.random() * 900);
      jQuery('#myModalNorm').modal('show');
      var getTripType = $("input[name='trip_type']:checked").val();
      $("input:radio[name='trip_type_pop']").each(function(i) {
           this.checked = false;
      });
      if(getTripType == "one_way"){
        $("input:radio[name='trip_type_pop']").each(function(i) {
        if(i==1){
           this.checked = true;
        } 
          
      });

      }else{
       $("input:radio[name='trip_type_pop']").each(function(i) {
       if(i==0) {
         this.checked = true;
       }
           
      });
        
      }

      var startfrom = jQuery("#Location").val();
        var endto = jQuery("#Location2").val();
        var departdate = jQuery("#datepicker").val();
        var returndate = jQuery("#datepicker2").val();
        var no_seat = jQuery("#sel2").val();
        var seat_type = jQuery("#sel1").val();

        jQuery("#trip_type").attr("value",getTripType);
        jQuery("#location_from").attr("value",startfrom);
        jQuery("#location_to").attr("value",endto);
        jQuery("#depart_date").attr("value",departdate);
        jQuery("#return_date").attr("value",returndate);
        jQuery("#passengers").attr("value",no_seat);
        jQuery("#flight_class").attr("value",seat_type);


        /*var startfromFirst= startfrom.substring(0,startfrom.indexOf(","));
        var endfromFirst= endto.substring(0,endto.indexOf(","));

        jQuery(".destination #from_location_head").html(startfromFirst);
        jQuery(".destination #to_location_head").html(endfromFirst);*/


        /*var startfromFirst= startfrom.substring(0,startfrom.indexOf(","));
        var endfromFirst= endto.substring(0,endto.indexOf(","));*/

        jQuery(".destination #from_location_head").html(startfrom);
        jQuery(".destination #to_location_head").html(endto);
        //jQuery("#new_price").html("$"+random);

        //var price = "213412";
        console.log(startfrom);
        console.log(endto);
        console.log(departdate);
        console.log(returndate);
      }/*else{
        alert("not valid");
      }*/
   });

</script>


<script type="text/javascript">
var options = {

  url: "/airports.json",

  getValue: "name",

    template: {
        type: "description",
        fields: {
            description: "code"
        }
    },

  list: { 
    match: {
      enabled: true
    }
  },

  theme: "square"
};

$(".Location").easyAutocomplete(options);
</script>


<script type="text/javascript">
/*var options = {

  url: "/airports.json",

  getValue: "name",

    template: {
        type: "description",
        fields: {
            description: "code"
        }
    },

  list: { 
    match: {
      enabled: true
    }
  },

  theme: "square"
};

$("#Location2").easyAutocomplete(options);*/
</script>


<script type="text/javascript">
    jQuery(document).on("click",".getfareprice",function(event){
      //alert('here');
        event.preventDefault();
        if(jQuery("#get_a_quote").valid()){
        var trip_type = jQuery("#trip_type").val();
        var location_from = jQuery("#location_from").val();
        var location_to = jQuery("#location_to").val();
        var depart_date = jQuery("#depart_date").val();
        var return_date = jQuery("#return_date").val();
        var username = jQuery("#username").val();
        var phone = jQuery("#phone").val();
        var email = jQuery("#email").val();
        var passengers = jQuery("#passengers").val();
        var flight_class = jQuery("#flight_class").val();
        //var price = jQuery("#new_price").html();
        var price = "$$$";
		// and the formula is:
		//var price = Math.floor(Math.random() * (max - min + 1)) + min;

        jQuery.ajax({
          type: 'POST',
          url: ajaxurl,
          data: {
                  'action':'get_flight_fare_price',
                  'price' : price,
                  'trip_type' : trip_type,
                  'location_from' :location_from,
                  'location_to' : location_to,
                  'depart_date' : depart_date,
                  'return_date' : return_date,
                  'username' : username,
                  'phone' : phone,
                  'email' : email,
                  'passengers' : passengers,
                  'flight_class' : flight_class
              },
              success:function(response) {
                    if(response !== ''){
                  	swal("Thank You for your request!", "we will contact you shortly.!", "success");
                  	setTimeout(function(){// wait for 5 secs(2)
                     location.reload(true); // then reload the page.(3)
                }, 2000); 
                   }else{
                   }
              },
              error: function(errorThrown){
                  console.log(errorThrown);
                  //alert("fail");
              }
        });
      }
    });

</script>



<script type="text/javascript">
    jQuery(document).on("click",".getfaredetail",function(event){
      //alert('here');
      if(jQuery("#get_a_quotes").valid()){
        event.preventDefault();
        //if(jQuery("#get_a_quote").valid()){
        var yname = jQuery("#orangeForm-name").val();
        var yemail = jQuery("#orangeForm-email").val();
        var yphone = jQuery("#orangeForm-phone").val();
        jQuery.ajax({
          type: 'POST',
          url: ajaxurl,
          data: {
                  'action':'get_flight_fare_detail',
                  'yname' : yname,
                  'yemail' : yemail,
                  'yphone' :yphone
              },
              success:function(response) {
                    if(response !== ''){
                    swal("Thank You for your request!", "we will contact you shortly.!", "success");
                    // location.reload(true);
                   setTimeout(function(){// wait for 5 secs(2)
                     location.reload(true); // then reload the page.(3)
                }, 2000); 
                   }else{
                   }
              },
              error: function(errorThrown){
                  console.log(errorThrown);
                  //alert("fail");
              }
        });

      }
    });

</script>




<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    	<div class="">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Bitcoin Cash Flights
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
               
                <div class="col-md-6">
                <div class="form-info">
                    <p>In order to secure this low fare, simply enter your information on the request form below and one
                        of
                        our specialists will contact you shortly.</p>

                    <p>Need your low fare locked in now? Call us directly to reserve your business class savings
                        instantly.</p>

                    <b>Need immediate assistance? Call us now</b>
                    <a href="tel:888-315-7838" class="phone">
                                                888-315-7838</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="destination">
                    <span id="from_location_head">Chandigarh</span>
                    <i class="fas fa-plane"></i>
                    <br> <span id="to_location_head">Delhi</span>
                </div>
                <div class="price-wrapper">
                    <div class="price" id="new_price"><span>$$$</span><!-- <sup>*</sup> --></div>
                </div>
            </div>

        <div class="bottom">
           <div class="">
                <form id="get_a_quote" class="get-a-quote-second-form" method="post">

			        <div class="form-messages">
			            <div class="succes-message">
			                <div class="close-success-message">
			                    <h3>Thank you!</h3>
			                    <p class="">One of our travel experts
			                        will contact you momentarily
			                        to follow up with your
			                        travel itinerary</p>

			                    <div class="col-md-12">
			                        <span class="b-btn close-success-message">OK</span>
			                    </div>


			                </div>
			            </div>
			        </div>
        <div class="form-container">

            <div class="form-head">
                <ul class="pseudo-radio">
                    <li>
                        <label>
                            <input id="round_trip_radio" type="radio" name="trip_type_pop" value="round_trip" checked="">
                            <label for="round_trip_radio">Round trip</label>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input id="one_way_radio" value="one_way" type="radio" name="trip_type_pop">
                            <label for="one_way_radio">One way</label>
                        </label>
                    </li>
                    <!-- <li>
                        <label>
                            <input id="multi_city_radio" value="multicity" type="radio" required="" name="trip_type">
                            <label for="multi_city_radio">Multi City</label>
                        </label>
                    </li> -->
                </ul>
            </div>

            <div class="form-body">

                <div class="col-md-3 col-xs-12">
                    <div class="form-item required">
                        <input type="text" name="location_from" placeholder="From" id="location_from" class="autocomplete_custom Location">
                    </div>
                </div>

              <script type="text/javascript">
              var options = {

                url: "/airports.json",

                getValue: "name",

                  template: {
                      type: "description",
                      fields: {
                          description: "code"
                      }
                  },

                list: { 
                  match: {
                    enabled: true
                  }
                },

                theme: "square"
              };

              $(".Location").easyAutocomplete(options);
              </script>
                 <!-- <script>
			      var input = document.getElementById('location_from');
			      console.log(input);
			      var autocomplete = new google.maps.places.Autocomplete(input);
			    </script> -->

                <div class="col-md-3 col-xs-12">
                    <div class="form-item required">
                        <input type="text" id="location_to" name="location_to" placeholder="To" class="autocomplete_custom Location">
                    </div>
                </div>

                
                  <script type="text/javascript">
                  var options = {

                    url: "/airports.json",

                    getValue: "name",

                      template: {
                          type: "description",
                          fields: {
                              description: "code"
                          }
                      },

                    list: { 
                      match: {
                        enabled: true
                      }
                    },

                    theme: "square"
                  };

                  $(".Location").easyAutocomplete(options);
                  </script>
                <!-- <script>
			      var input = document.getElementById('location_to');
			      console.log(input);
			      var autocomplete = new google.maps.places.Autocomplete(input);
			    </script> -->

                <div class="col-md-3 col-xs-12">
                    <div class="form-item required">
                        <input type="text" required="" name="depart" autocomplete="off" placeholder="Depart" id="depart_date" class="depart date-picker">
                    </div>
                </div>

                <div class="col-md-3 col-xs-12" id="return_back">
                    <div class="form-item required">
                        <input type="text" name="return" autocomplete="off" placeholder="Return" id="return_date" class="to" style="">
                    </div>
                </div>


                <div class="col-md-3 col-xs-12">
                    <div class="form-item required">
                        <input autocomplete="off" type="text" name="username" id="username" required="" placeholder="Enter Your Name">
                    </div>
                </div>

                <div class="col-md-3 col-xs-12">

                    <div class="form-item required">
                    	<input autocomplete="off" type="text" name="phone" id="phone" required="" placeholder="Enter Phone">
                        </div>
                    </div>
                

                <div class="col-md-3 col-xs-12">


                    <div class="form-item required">
                        <input type="email" autocomplete="off" required="" name="email" id="email" placeholder="E-Mail">
                    </div>
                </div>

                <div class="col-md-1 pad-r-0">
                    <div class="form-item required">
                        <select name="passengers" id="passengers">
                          <span class="drop-errow2"></span>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2 pad-l-0">
                    <div class="form-item required">
                        <select name="flight_class" id="flight_class">
                            <option value="b">Business Class</option>
                            <option value="f">First Class</option>
                        </select>
                    </div>
                </div>



                <div class="multicity-block" style="display: none;">
                    <div class="multicity-list"></div>
                    <div class="multi-city-add">
                        <span class="add-button b-btn">Add Flight</span>
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <div class="col-md-6 pad-t-10 pad-b-10">
                    <span class="info-text">
                        <span>*By submitting my details I agree to be contacted to discuss my travel plans.
                         <a target="_blank" href="/privacy">Privacy Policy</a>.
                        </span>
                    </span>
                </div>


                <div class="col-md-2">

                </div>

                <div class="col-md-4">
                    <button id="quote_form_submit" class="getfareprice">Continue</button>
                </div>
            </div>
        </div>


    </form>
            	</div>


            </div>		


               
             
                
            </div>
            
            <!-- Modal Footer -->
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Close
                </button>
                <button type="button" class="btn btn-primary">
                    Save changes
                </button>
            </div> -->
        </div>
    </div>
    </div>
</div>





<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
			
            <div class="modal-header text-center">
                <h4>Flight Booking</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body">
			  <form id="get_a_quote2" class="flight_booking" method="post">	
                <div class="form-group">
                    <i class="fa fa-user"></i>
                    <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                    <input type="text" name="yname" id="orangeForm-name" class="form-control validate" required="">
                </div>
                <div class="form-group">
                    <i class="fa fa-envelope"></i>
                    <label data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
                    <input type="email" name="yemail" id="orangeForm-email" class="form-control validate" required="">
                </div>
                <div class="form-group">
                    <i class="fa fa-lock"></i>
                    <label data-error="wrong" data-success="right" for="orangeForm-pass">Your Phone</label>
                    <input type="text" name="yphone" id="orangeForm-phone" class="form-control validate" required="">
                </div>
				 
				<div class="form-group text-center">
					<button class="btn getfaredetail">Submit</button>
				</div>	
				  
              </form> 
            </div>

        </div>
    </div>
</div>

<!-- <div class="text-center">
    <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalRegisterForm">Launch Modal Register Form</a>
</div> -->


<!-- Post Info -->

    <!-- <script charset="utf-8" type="text/javascript" src="http://http://ontap8.mobilytedev.com//iframe.js"></script> -->
    <style type="text/css">
    	
    	.pac-container { z-index: 100000; }
    </style>
    <?php wp_footer(); ?>
    </body>
  </html>