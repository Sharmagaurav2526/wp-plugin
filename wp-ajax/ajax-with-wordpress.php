<?php
------------------------+-------
@author : Harish Chauhan
Ajax in WordPress using jQuery - JSON response
------------------------+-------

Step 1. put this code in header.php

	<script type="text/javascript">
  	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
  </script>
+----------

Step 2. put this link on your template file where you want to use ajax

	<a class="call_to_ajax" href="">Call to Ajax</a>

  if you are using a form then add add this hidden field on your form
    <input type='hidden' name="action" value='my_function'> //ny _function is the ajax call function
+----------

Step 3. put this code on your functions.php file to register ajax scripts file

/**
 * Register and enque ajax scripts a unique function, put this code on functions.php
 */
add_action( 'init', 'my_script_enqueuer' );
function my_script_enqueuer() {

	wp_register_script( "my_voter_script", get_template_directory_uri() . '/js/my_ajax_scripts.js', array('jquery') );
   	wp_enqueue_script( 'my_voter_script' );
}
+----------

Step 4. Now create a function that you want on ajax call

/**
 * callback function for ajax call
 */
function my_function() {
	
  $formInput = $_POST['input_field_name'];

	$data['name'] = 'harish';

	$result = json_encode($data);
    echo $result;

	die();
}

+----------

Step 5. Now activate the ajax functions from admin panel bu putting these lines of code into start of your functions.php file

add_action("wp_ajax_my_function", "my_function");
add_action("wp_ajax_nopriv_my_function", "my_function");
+----------

Step 6. Put this code on my_ajax_scripts.js file to handle out the ajax call by jQuery.

jQuery(document).ready( function() {

   jQuery(".call_to_ajax").click( function(event) {

      event.preventDefault();

      post_id = 20;

      //if using the form the serialize the form data
      var serializedData = jQuery("#bt_create_tx_escrow").serialize();

      jQuery.ajax({
         type : "post",
         dataType : "json",
         url : ajaxurl,
         statusCode: {
              500: function() {
                  alert(" 500 data still loading");
                  console.log('500 ');
              }
          },
         
         data : {action: "my_function", post_id : post_id},
         
         //if using the form then use this
         //data : serializedData,

         error: function(xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            alert(err.Message);
        },
         success: function(response) {

            alert(response.name);
            
        },
      }); 

   });

});
+----------

------------------------+-------
Ajax in WordPress using jQuery - HTML response
------------------------+-------

Step 1. put this code in header.php

	<script type="text/javascript">
  	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
  	</script>
+----------

Step 2. put this link on your template file where you want to use ajax

	<a class="call_to_ajax" href="">Ajax</a>
+----------

Step 3. put this code on your functions.php file to register ajax scripts file

/**
 * Register and enque ajax scripts a unique function, put this code on functions.php
 */
add_action( 'init', 'my_script_enqueuer' );
function my_script_enqueuer() {

	wp_register_script( "my_voter_script", get_template_directory_uri() . '/js/my_ajax_scripts.js', array('jquery') );
   	wp_enqueue_script( 'my_voter_script' );
}
+----------

Step 4. Now create a function that you want on ajax call

/**
 * callback function for ajax call
 */
function my_function() {
	
	echo "Put The HTML Data here";

	die();
}
+----------

Step 5. Now activate the ajax functions from admin panel bu putting these lines of code into start of your functions.php file

add_action("wp_ajax_my_function", "my_function");
add_action("wp_ajax_nopriv_my_function", "my_function");
+----------

Step 6. Put this code on my_ajax_scripts.js file to handle out the ajax call by jQuery.

jQuery(document).ready( function() {

   jQuery(".call_to_ajax").click( function(event) {

      event.preventDefault();

      alert(ajaxurl);

      jQuery.ajax({
         type : "post",
         url : ajaxurl,
         statusCode: {
              500: function() {
                  alert(" 500 data still loading");
                  console.log('500 ');
              }
         },
         data : {action: "my_function", post_id : post_id},
         success: function(response) {

            $('.responseDiv').html(response);
            
         },
      }); 

   });

});

+----------
//CSRF_TOKEN global form token
<script>
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
</script>

//Passenger Actions
  $( document ).on( "click", ".passangerAzAction", function(){
  //$(".passangerAzAction").click(function(event){
    event.preventDefault();
    
    id = $(this).attr('azId');
    azAction = $(this).attr('azAction');

      //if using the form the serialize the form data
      //var serializedData = jQuery("#form").serialize();

      jQuery.ajax({
          type : "post",
          dataType : "json",
          url : pasangerActionURL,
          
          data : { _token: CSRF_TOKEN, azId : id, azAction : azAction },
           
          //if using the form then use this
          //data : serializedData,

          success: function(response) {
              
              if (response.azStatus == 'success') { 
                alert(response.azStatus); 
              }
              else { alert(response.azStatus); }
          },
          failure: function(errMsg) {
            alert(errMsg);
        }
      });

  });

  +----------
  //Passenger Actions
  $('#credit_form').on('submit', function(e) { //use on if jQuery 1.7+
      e.preventDefault();  //prevent form from submitting
      var data = $("#credit_form").serialize();
      alert('check console');
      console.log(data); //use the console for debugging, F12 in Chrome, not alerts
  });