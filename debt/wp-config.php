<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'debtsite');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'toor');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mC TmyA+s$W>gjgw;a)Whi2bhQPO$Jy{?C:5vM?oWO,?m%vSTBu4R4DC;>Z%A)$5');
define('SECURE_AUTH_KEY',  'MTVj,Oe*tthJC|V{!1NmL@VmnMzQGiiA -C_/0Qe@Kv.=H@ aKWYP+Yk>:Sw%^9&');
define('LOGGED_IN_KEY',    ';l)0I f/n}j#6Eno_<1;7XRDZW>4Tf0.o+v+`(/_Dfp$;S3|<g&m-<2P8^g{,sB#');
define('NONCE_KEY',        '%?ZRquF#Y!}B|P#_@^.R}$`/E3cIahf d`y#+{jKsZ_B54^Aev.U6OX_k?ff.o$1');
define('AUTH_SALT',        '-{-x:#6+O3Fx,j|9YTShfQq-wW[|^_fi}Nv;aRXbN*d1L7rrFL8V#.w3@lUS2B8O');
define('SECURE_AUTH_SALT', '8UZ|-0txB`muLoT3L!doJ56Ye]BKvX1N=?ff3Zs?r.KFe+Q$SB.)C/3y]n` N?*F');
define('LOGGED_IN_SALT',   'jZDDP<>RB ,(Sb_GR;wpOp0bba)4ce$[N@RH}FFCb*q`_ZK,t9}%Od%]d!$Om@Wr');
define('NONCE_SALT',       'W<H5FfQRCT{K`2k%eivs,yKC/1WH{C$5tBLlvr>rE|Bo3{O4B8W5@YP4fmtkY#cW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
