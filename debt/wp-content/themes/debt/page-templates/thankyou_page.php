<?php 
// Template Name: Thank-you Page
get_header();
?>
<!-- <section class="thank_sec">
    <div class="container">
        <div class="title_subtitle_holder text-center">           
            <h1>Thank You</h1>  
        </div>
    </div>
</section> -->
<section class="thank_sec_01">
    <div class="container">
        <div class="title_subtitle_holder_01 text-center">
			<h1>Thank you for filling out our form, Please expect a call or a text from an eDebt help Expert with in 24 hours. </h1>
        </div>
    </div>
</section>
<?php 
get_footer();
?>
