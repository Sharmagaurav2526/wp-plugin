<?php 
// Template Name: Debt Problem
get_header();
?>
<div id="content" class="site-content">
	<div class="education container">
		<div class="row">
			<div class="col-lg-12">
				<div class="title">
					<h1 class="text-center"><?php the_field('dept_heading');?></h1>
					<div class="hr3"></div>
				    <div class="hr4"></div>
				</div>
				<div id="identify" class="section_pt">
					<h3>Do you have savings?</h3>
					<div class="align-r"><img src="<?php the_field('dept_image');?>" /></div>
					<?php the_field('dept_content');?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
get_footer();
?>
