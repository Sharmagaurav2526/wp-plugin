<?php 
// Template Name: Contact Page Template
get_header();
?>

<div class="breadcrumbs-container">
	<div class="container-fluid">
		<div id="breadcrumbs"><span><span><a href="https://www.debt.com/"></a> <span class="breadcrumb_last">Contact Us</span></span></span></div> 
 </div>
</div>
<div id="content" class="site-content">
	<div class="contact container">
		<div class="row">
			<div class="col-lg-1"></div>
			<div class="title col-lg-5 col-xl-5">
				<h1>Contact</h1>
				<div class="social-icons">
					<ul>
						<li> 
							<a class="sm-icon fb" href="#" target="_blank" property="sameAs"> 

						<span class="icon fb">
							<i class="fa fa-facebook" aria-hidden="true" title="Facebook Icon"></i>
						</span> <span class="sr-only">Facebook Icon linking to Facebook Page</span> </a>
						</li>
						<li> 
                                     <a class="sm-icon twitter" href="#" target="_blank" property="sameAs">
          							 <span class="icon twitter">


          							 	<i aria-hidden="true" class="fa fa-twitter" title="Twitter Icon"></i></span> 
          							 <span class="sr-only">Twitter Icon linking to Twitter Page</span> </a>
          						  </li>
          							<li> 
          							  <a class="sm-icon youtube" href="#" target="_blank" property="sameAs"> 
                            <span class="icon youtube"><i aria-hidden="true" class="fa fa-youtube" title="Youtube Icon"></i></span>
          							   <span class="sr-only">Youtube Icon linking to Youtube Page</span> </a>
          							 </li>
          							<li>
          								 <a href="#" target="_blank" property="sameAs" class="sm-icon in"> 
          								  <span class="icon in"><i aria-hidden="true" title="Instagram Icon" class="fa fa-instagram"></i></span> 
          								  <span class="sr-only">Instagram Icon linking to Instagram Page</span> </a>
          							</li>
                            <li> 
                              <a class="sm-icon pinterest" href="#" target="_blank" property="sameAs"> 
                                <span class="icon pinterest"><i aria-hidden="true" class="fa fa-pinterest" title="Pinterest Icon"></i></span> 
                                <span class="sr-only">Pinterest Icon linking to Pinterest Page</span> </a>
                              </li>

					</ul>
				</div>
			</div>
			<div class="col-lg-5 col-xl-5">
				<div id="contact-form" class="contact-form" data-form="main-div"> 
					<span class="success-message" style="display: none;">Thank you for contacting. We'll get back to you as soon as possible!</span>
					<form id="contact-vtform1" class="dc_forms" novalidate="novalidate">
						<div class="form-row">
							<div class="col">
								<div class="form-group"> 
									<label for="first_name">First Name</label> 
									<input class="form-control" name="first_name" data-var-name="first_name" id="first_name" required="" minlength="2" data-ng-pattern="/^[A-Za-z]+$/" maxlength="50" aria-required="true" type="text">
								</div>
							</div>
							<div class="col">
								<div class="form-group"> 
									<label for="last_name">Last Name</label>
									 <input class="form-control" name="last_name" data-var-name="last_name" id="last_name" required="" minlength="2" aria-required="true" type="text">
									</div>
								</div>
							</div>
							<div class="form-group">
							 <label for="email">Phone Number</label>
							 <input type="tel" class="form-control" id="phone_number" name="phone_number" data-mask="(000) 000-0000" data-pattern="^(\+1)?1?[ \-\.]*\(?([2-9][0-8][0-9])\)?[ \-\.]*([2-9][0-9]{2})[ \-\.]*([0-9]{4})$" data-var-name="phone_number" required="" aria-required="true">
							</div>
							<div class="form-group">
							 <label for="email">Email Address</label>
							  <input class="form-control" data-var-name="email" id="email" placeholder="email@example.com" required="" aria-required="true" type="email"> 
							  <input name="send_email" data-var-name="send_email" value="qa@debt.com" type="hidden">
							   <input name="subject" data-var-name="subject" value="Contact form submission" type="hidden">
							</div>
							<div class="form-group"> 
								<label for="message">Message</label>
								<textarea class="form-control" data-var-name="message" id="message" name="message" required="" rows="3" aria-required="true"></textarea>
							</div>
								<div class="form-check"> <label class="form-check-label"> <input class="form-check-input" type="checkbox"> I request to be contacted at the email or by any other information provided in the comment box. </label>
								</div>
								 <button id="contact-button1" class="btn btn-primary">Submit<span class="ng-hide" style="display: none;">&nbsp;<img src="https://www.debt.com/ang/images/ajax-loader.gif" alt="ajax loader">
								 </span>
								 </button>
								 <input name="xxTrustedFormToken" id="xxTrustedFormToken_1" value="https://cert.trustedform.com/d54cf6348ffb8fd92eefb651a1700347c0ae39cd" type="hidden">
								 <input name="xxTrustedFormCertUrl" id="xxTrustedFormCertUrl_1" value="https://cert.trustedform.com/d54cf6348ffb8fd92eefb651a1700347c0ae39cd" type="hidden">
								</form>
							</div>
						</div>
						<div class="col-lg-1"></div>
					</div>
				</div>
				  </div>

<?php get_footer(); ?>
<script type="text/javascript">
    //var ajaxurl = "<?php //echo admin_url('admin-ajax.php'); ?>";
    jQuery(document).ready(function(){
      jQuery('#phone_number').mask('(000) 000-0000');
    });
</script>