<?php 
// Template Name: Debt Calculator
get_header();
?>
<?php 
// Template Name: Debt Calculator
get_header();
?>
<div id="content" class="site-content">
            <div class="container">
                <div class="other-block" id="debtcom-calculator-v3">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <div id="profile">
                                    <div class="title">
                                        <h1>Credit Card Payoff Calculator</h1></div>
                                    <form id="minimum_payment_form" name="minimum_payment_form" class="needs-validation" novalidate="" role="form" onsubmit="return getSuccessOutput();">
                                        <div class="form-group">
                                            <label for="current_balance" class="control-label h5"> Current Balance:
                                                <button type="button" class="calculator-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Enter the current balance from each of your credit cards one at a time. You can find the current balance on your most recent credit card statement.">?</button>
                                            </label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">$</span></div>
                                                <input type="number" class="form-control calculator-form required" name="current_balance" id="current_balance" placeholder="10000" required="required">
                                            </div>
                                            <div class="invalid-feedback 2feed"></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="interest_rate" class="control-label h5"> Interest Rate (APR):
                                                <button type="button" class="calculator-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Enter the current APR for purchases from that card; this information will also be listed on your most current credit card statement.">?</button>
                                            </label>
                                            <div class="input-group">
                                                <input type="number" class="form-control calculator-form" name="interest_rate" id="interest_rate" placeholder="18" required="required">
                                                <div class="input-group-append"><span class="input-group-text">%</span></div>
                                            </div>
                                            <div class="invalid-feedback 2feed"></div>
                                        </div>
                                        <label class="control-label h5"> How are your minimum payments calculated? </label>
                                        <fieldset>
                                            <div class="form-group">
                                                <label for="percentage_of_balance" class="col-xs-10 offset-xs-2 control-label h5 mt-2"> Percentage of Balance:
                                                    <button type="button" class="calculator-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimum payment requirements are calculated as a percentage of what you owe. The calculation should be listed on your original credit card agreement or you can simply use the default option of 3%, which is standard for most cards">?</button>
                                                </label>
                                                <div class="input-group">
                                                    <select class="form-control calculator-form" name="percentage_of_balance" id="percentage_of_balance" required="false">
                                                        <option value="1">Interest + 1%</option>
                                                        <option value="2">2</option>
                                                        <option value="2.5">2.5</option>
                                                        <option value="3" selected="">3</option>
                                                        <option value="3.5">3.5</option>
                                                        <option value="4">4</option>
                                                        <option value="4.5">4.5</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                    <div class="input-group-append"><span class="input-group-text">%</span></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="minimum_dollar_amount" class="col-xs-12 offset-xs-2 control-label h5 mt-2" style="display: none;"> Your Next Minimum Payment is:
                                                    <button type="button" class="calculator-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimum payments increase or decrease based on your balance. So, as you pay off credit card balances, the minimum payment requirement will gradually decrease over time.">?</button>
                                                </label>
                                                <p class="lead" id="minimum_payment"></p>
                                                <input type="hidden" value="25" name="minimum_dollar_amount" id="minimum_dollar_amount">
                                            </div>
                                        </fieldset>
                                        <button id="btn12" class="btn btn-lg btn-primary" type="submit"> Calculate</button>
                                        <div id="output_error" class="invalid-feedback"></div>
                                        <input type="hidden" name="xxTrustedFormToken" id="xxTrustedFormToken_1" value="https://cert.trustedform.com/e81af722f444f38bc1f7e361d876f82c98fca1ce">
                                        <input type="hidden" name="xxTrustedFormCertUrl" id="xxTrustedFormCertUrl_1" value="https://cert.trustedform.com/e81af722f444f38bc1f7e361d876f82c98fca1ce">
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6 text-center" id="message" style="display: none;">
                                <div class="debt-free-date animated fadeIn">
                                    <p>You'll be debt-free by <span class="debt-free-m-y js-completion-year"></span> or <span class="js-total-years"></span> years and <span class="js-months-remainder"></span> (<span class="js-month"></span> payments)</p>
                                </div>
                                <div class="graph-container clearfix">
                                    <div class="stacked-bar-graph"> <span class="segment-container js-principle-percentage-width"> <span class="bar bar-1 js-principle-percentage"></span>
                                        <p>Principle:
                                            <br> <span class="js-total-balance"></span></p>
                                        </span> <span class="segment segment-container js-interest-percentage-width"> <span class="bar bar-2 js-interest-percentage"></span>
                                        <p>Interest:
                                            <br> <span class="js-total-interest"></span></p>
                                        </span>
                                    </div>
                                </div>
                                <div class="total-balance">
                                    <p>Total amount you’ll pay (including interest): <span class="js-total-total">fixed</span></p>
                                </div>
                                <div class="sales-pitch-container">
                                    <h3 class="line-through-title"><span>Did You Know?</span></h3>
                                    <p>Minimum payment schedules are not designed to pay off credit card balances quickly. In fact, they're really designed to keep you in debt as long as possible.</p>
                                    <p>Don't spend another sleepless night stressing about your balances. Let Debt.com help you find a solution that works for you and your budget.</p> <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#ccFormModal">Let Us Help You</a>
                                    <br> or call <span class="phone"><a class="tracking-phone-click" href="tel:+18555564383"><span itemprop="telephone" class="tracking-phone">(855) 556-4383</span></a>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div id="table" style="display: none;">
                                    <div class="col-lg-12">
                                        <h3 class="mt-0 mb-3">Payment Schedule</h3>
                                        <div class="table-responsive scrolling-table"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="ccFormModal" tabindex="-1" role="dialog" aria-labelledby="ccFormModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-wrapper clearfix" id="bib-wrapper">
                                    <div class="success-message" style="display: none;">
                                        <h2>Congratulations, <span class="js-name">Tiffany</span>!</h2>
                                        <p>We’ve received your request and have matched you with a Trusted Provider that specializes in Credit Card Debt Solutions. You should receive a call within the next few minutes so you can get connected. If you are unavailable, a confirmation text will be sent, so connecting at your convenience is quick and easy. We look forward to assisting you!</p>
                                        <input type="hidden" name="res_email" id="res_email" value="">
                                        <input type="hidden" name="res_leadguid" id="res_leadguid" value="">
                                    </div>
                                    <div id="debtform-app">
                                        <form role="form" id="msform" class="vt-form hp-bt lead-form" name="formItem" novalidate="novalidate" autocomplete="off">
                                            <fieldset class="step-1 amount-owed">
                                                <div class="how-much-you-owe" data-next="1">
                                                    <h3>Tell us how much you owe.</h3>
                                                    <input class="debt-slider" type="text" min="1,000" max="100,000" value="25" name="points" step="1" data-var-name="debtFieldName" style="display: none;">
                                                    <div class="asRange">
                                                        <div class="asRange-bar"></div>
                                                        <div class="asRange-pointer asRange-pointer-1" tabindex="0" style="left: 24.2424%;"><span class="asRange-tip">$25,000</span></div><span class="asRange-selected" style="left: 0px; width: 24.2424%;"></span>
                                                        <div class="asRange-scale">
                                                            <ul class="asRange-scale-lines">
                                                                <li class="asRange-scale-grid" style="left: 0%;"></li>
                                                                <li style="left: 5%;"></li>
                                                                <li style="left: 10%;"></li>
                                                                <li style="left: 15%;"></li>
                                                                <li style="left: 20%;"></li>
                                                                <li class="asRange-scale-inlineGrid" style="left: 25%;"></li>
                                                                <li style="left: 30%;"></li>
                                                                <li style="left: 35%;"></li>
                                                                <li style="left: 40%;"></li>
                                                                <li style="left: 45%;"></li>
                                                                <li class="asRange-scale-grid" style="left: 50%;"></li>
                                                                <li style="left: 55%;"></li>
                                                                <li style="left: 60%;"></li>
                                                                <li style="left: 65%;"></li>
                                                                <li style="left: 70%;"></li>
                                                                <li class="asRange-scale-inlineGrid" style="left: 75%;"></li>
                                                                <li style="left: 80%;"></li>
                                                                <li style="left: 85%;"></li>
                                                                <li style="left: 90%;"></li>
                                                                <li style="left: 95%;"></li>
                                                                <li class="asRange-scale-grid" style="left: 100%;"></li>
                                                            </ul>
                                                            <ul class="asRange-scale-values">
                                                                <li style="left: 0%;"><span>1</span></li>
                                                                <li style="left: 50%;"><span>49.5</span></li>
                                                                <li style="left: 100%;"><span>100</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="button" name="next" class="next action-button btn btn-primary" value="Get Started">
                                            </fieldset>
                                            <fieldset class="step-3 qualifying-questions-1 required" aria-required="true">
                                                <div class="cc-debt-questions" data-next="1">
                                                    <h3>What is the status of your payments?</h3>
                                                    <div class="tax-debt-types">
                                                        <label>
                                                            <input type="radio" name="radio_debt" data-var-name="payment_status" value="Current">
                                                            <div class="current-cc box"> <span>Current and/or struggling</span></div>
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="radio_debt" data-var-name="payment_status" value="Behind">
                                                            <div class="behind-less-cc box"> <span>Behind less than 6 months</span></div>
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="radio_debt" data-var-name="payment_status" value="Collections">
                                                            <div class="behind-more-cc box"> <span>More than 6 months behind</span></div>
                                                        </label>
                                                    </div>
                                                </div> <span class="showerror" style="display: none;">Please select an option</span>
                                                <input type="reset" name="reset" class="reset action-button btn btn-light" value="Start Over">
                                                <input type="button" name="next" class="next action-button btn btn-primary" value="Next ">
                                            </fieldset>
                                            <fieldset class="step-4 zip-code required" aria-required="true">
                                                <h3>Enter your zip code.</h3>
                                                <div class="form-group" data-next="1">
                                                    <label for="zip" class="sr-only">Zip Code</label>
                                                    <input id="zip" name="zip" data-var-name="zip" class="form-control required" placeholder="Zip Code" required="required" type="text" maxlength="5" autocomplete="nope" aria-required="true"> <span id="spanEmail" class="required hideError" aria-required="true">Please Enter a valid Zip Code</span>
                                                    <p class="zip-error">Not a real zip code</p>
                                                </div> <span class="showerror" style="display: none;">Please select an option</span>
                                                <input type="reset" name="reset" class="reset action-button btn btn-light" value="Start Over">
                                                <input type="button" name="next" class="next action-button btn btn-primary" value="Next">
                                            </fieldset>
                                            <fieldset class="processing-step">
                                                <h3>Searching...</h3>
                                                <h4>Searching for availability in <span class="js-city"></span></h4>
                                                <div class="spinner"><img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/loading.gif" alt="Loading..."></div>
                                                <h4>Please wait while we find the best solution for you.</h4></fieldset>
                                            <fieldset class="step-5">
                                                <h3>Good news! We can help!</h3>
                                                <h4>Provide a few details about yourself so we can set you up with a debt specialist.</h4>
                                                <div class="form-row clientInfo">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="first_name" class="sr-only">First Name</label>
                                                            <input id="first_name" name="first_name" data-var-name="first_name" class="form-control" placeholder="First Name" required="" type="text" minlength="2" data-ng-pattern="/^[A-Za-z]+$/" maxlength="50" aria-required="true">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="last_name" class="sr-only">Last Name</label>
                                                            <input id="last_name" name="last_name" data-var-name="last_name" class="form-control" placeholder="Last Name" required="" type="text" minlength="2" aria-required="true">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email" class="sr-only">Email</label>
                                                            <input id="email" name="email" data-var-name="email" class="form-control" placeholder="Email Address" required="" type="email" aria-required="true">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="primary_phone" class="sr-only">Phone Number</label>
                                                            <input id="primary_phone" name="primary_phone" data-var-name="primary_phone" class="form-control phone" data-mask="(999) 999-9999" data-pattern="^(\+1)?1?[ \-\.]*\(?([2-9][0-8][0-9])\)?[ \-\.]*([2-9][0-9]{2})[ \-\.]*([0-9]{4})$" placeholder="Phone Number" required="" type="tel" maxlength="14" autocomplete="off" aria-required="true">
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="reset" name="reset" class="reset action-button btn btn-light" value="Start Over">
                                                <button type="submit" class="submit action-button btn btn-primary" id="vt-bib-button-2">Contact Me<span>&nbsp;<i class="far fa-spinner fa-spin"></i></span></button>
                                                <div class="form-disclaimer" id="form-disclaimer">
                                                    <br>
                                                    <p class="general-disclaimer">By clicking on the "Contact Me" button above, you consent, acknowledge, and agree to the following: Our <a href="https://www.debt.com/terms-conditions/" target="_blank">Terms of Use</a> and <a href="https://www.debt.com/privacy-policy/" target="_blank">Privacy Policy</a>. That you are providing express "written" consent for Debt.com or appropriate <a href="https://www.debt.com/privacy-policy/partners/" target="_blank">service provider(s)</a> to call you (autodialing, text and pre-recorded messaging for convenience) via telephone, mobile device (including SMS and MMS - charges may apply depending on your carrier, not by us), even if your telephone number is currently listed on any internal, corporate, state or federal Do-Not-Call list. We take your privacy seriously and you may receive electronic communications, including periodic emails with important news, financial tips, tools and more. You can always unsubscribe at any time. Consent is not required as a condition to utilize Debt.com services and you are under no obligation to purchase anything.</p>
                                                </div>
                                            </fieldset>
                                            <input type="hidden" name="pid" value="95124" data-var-name="pid">
                                            <input type="hidden" name="vid" value="106" data-var-name="vid">
                                            <input type="hidden" name="xxTrustedFormToken" id="xxTrustedFormToken_2" value="https://cert.trustedform.com/e81af722f444f38bc1f7e361d876f82c98fca1ce">
                                            <input type="hidden" name="xxTrustedFormCertUrl" id="xxTrustedFormCertUrl_2" value="https://cert.trustedform.com/e81af722f444f38bc1f7e361d876f82c98fca1ce">
                                        </form>
                                        <div class="credibility-logos clearfix">
                                            <a href="https://www.bbb.org/south-east-florida/business-reviews/credit-monitoring-protection/debtcom-in-plantation-fl-90080459" target="_blank"> <img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/bbb-aplus-logo.png" class="bbb-logo" alt="BBB Logo"> </a> <img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/United-Way-logo.png" alt="United Way">
                                            <a target="_blank" href="https://seal.godaddy.com/verifySeal?sealID=VttdGalb6RNnHGweH1ZPwYmmYt3XSbYeXM2Jk90v88Poa2JxPmkvASdRAOnA"> <img class="security-logo" src="https://www.debt.com/ang/images/godaddySecure.gif" alt="Go Daddy Secure" border="0"> </a>
                                        </div>
                                    </div>
                                </div>
                                <script src="https://keycdn.debt.com/wp-content/themes/Dcom/js/forms/jquery.mask.min.js" defer="defer"></script>
                                <script>
                                    var _getLangCode = 'en';
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
               <!--  <h2>Finding better ways to pay off your credit cards</h2>
                <p>Minimum payment schedules are not designed to pay off credit card balances quickly. In fact, they’re really designed to keep you in debt as long as possible. Interest charges are how credit card companies make profits. So, it’s in their best interest to keep you in debt as long as possible. But it’s definitely not in your best interest!</p>
                <h3>Why minimum payments don’t work</h3>
                <p>Minimum payments only pay off a small percentage of what you owe each month – usually about 2-5%. At the same time, you have high APR that’s eating up a big portion of each minimum payment you make.</p>
                <p>At 15% APR, about half of every minimum payment gets used to cover accrued interest charges. At 20% APR, that jumps to about two-thirds of the payment. This is why you can make payments month after month, but you never seem to get anywhere – especially if you’re still making charges each month to the card.</p>
                <p>So, if you really want to effectively pay off your credit card balances, you need to find solutions that are more effective than minimum payments.</p>
                <h2>What’s the best way to pay off credit cards?</h2>
                <p>There are a number of solutions that reduce or eliminate interest charges so you can pay off credit card balances faster. The finding the best solution to use in your situation depends on three things:</p>
                <ol>
                    <li>How much you owe, in total</li>
                    <li>Your credit score</li>
                    <li>How much free cash flow you have available</li>
                </ol>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th>Solution</th>
                                <th>Recommended Credit Score</th>
                                <th>Recommended Debt Amount</th>
                                <th>Interest Rate Adjustment</th>
                                <th>Monthly Payment</th>
                                <th>Works for…</th>
                                <th>Be aware of…</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Credit Card Balance Transfer</td>
                                <td>Excellent</td>
                                <td>Less than $5,000</td>
                                <td>0% APR during introductory period</td>
                                <td>Could be higher, as you work to eliminate the balance in the intro period</td>
                                <td>Credit cards only</td>
                                <td>Transfer fees (range from $3 to 3% of the balance transferred)</td>
                            </tr>
                            <tr>
                                <td>Personal Debt Consolidation Loan</td>
                                <td>Good-Excellent</td>
                                <td>Less than $25,000</td>
                                <td>Target 5-10% APR</td>
                                <td>Can be less than what you pay now</td>
                                <td>Credit cards, medical bills, unsecured loans, back taxes</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Home Equity Loan</td>
                                <td>Fair-Excellent</td>
                                <td>Up to 80% of the equity available in your home</td>
                                <td>Typically around 5%</td>
                                <td>Can be less than what you pay now</td>
                                <td>Credit cards, medical bills, back taxes, other financial goals</td>
                                <td>Increased risk of foreclosure</td>
                            </tr>
                            <tr>
                                <td>HELOC</td>
                                <td>Fair-Excellent</td>
                                <td>Up to 80% of the equity available in your home</td>
                                <td>Typically around 5%</td>
                                <td>Interest-only for 10 years, then payments increase with principal + interest</td>
                                <td>Credit cards, medical bills, back taxes, other financial goals</td>
                                <td>Increased risk of foreclosure</td>
                            </tr>
                            <tr>
                                <td>Debt management program</td>
                                <td>Any, including bad</td>
                                <td>$10,000 and up (works for over $100K)</td>
                                <td>Negotiated 0-11%, on average</td>
                                <td>Total payments reduced by up to 30-50%</td>
                                <td>Credit cards, medical bills, payday loans</td>
                                <td>Freezes any accounts you include in the program</td>
                            </tr>
                            <tr>
                                <td>Debt settlement program</td>
                                <td>Any, including bad</td>
                                <td>Some (not all) programs limit to $100,000</td>
                                <td>n/a (interest charges do not apply)</td>
                                <td>Monthly set aside; significantly less than other options</td>
                                <td>Any debts already in collections</td>
                                <td>Credit damage lasting seven years from the date of discharge</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="short-cta">
                    <div>
                        <h4>Not sure which solution is right for you? Let Debt.com match you with an accredited debt relief specialist for a free, confidential debt and budget analysis.</h4></div><a target="_blank" href="https://www.debt.com/land/credit-counseling-1/?kw=credit+consolidation&amp;utm_campaign=inlinecta" class="btn btn-primary">Get Help Now</a></div>
                <h3>Here’s why a debt management program may be your best option…</h3>
                <p>Let’s be honest. Most people would rather solve debt problems on their own. But unfortunately, do-it-yourself doesn’t always work. <a href="/how-to/transfer-credit-card-balance/" rel="noopener">Balance transfers</a> and <a href="/credit-card-debt/consolidation-loan/" rel="noopener">consolidation loans</a> can effectively consolidate debt at a lower rate. But what they can’t do is help you balance your budget, so you stop charging. If you consolidate existing debt, then run up new balances, you make the situation worse instead of better.</p>
                <p>If you work with a credit counseling agency to enroll in a <a href="/credit-card-debt/management-program/" rel="noopener">debt management program</a>, they help you create a balanced budget. At the same time, your creditors freeze any accounts you include in the program. This allows you to break your credit dependence, so you can stop charging and learn how to live without relying on credit to get by.</p>
                <p>This option also won’t damage your credit when done correctly. Since the <a href="/credit-card-debt/counseling/" rel="noopener">credit counseling</a> team negotiates with creditors and they agree to the program, you build a positive credit history on all your accounts with every monthly payment you make. Since credit history is the biggest factor in calculating consumer credit scores, making debt management program payments on time is good for your credit.</p>
                <p>That being said, if you’re already behind on many of your accounts, <a href="/credit-card-debt/settlement/" rel="noopener">debt settlement</a> can be a good option. In this case, you’ve probably already taken some damage to your credit. So, your goal is usually just to eliminate the debt as quickly as possible. Settlement can get you out of debt for a portion of what you owe.</p>
                <p>Finally, most experts agree you should avoid home equity loans and HELOCs to pay off unsecured debt. If you default on these loans, you could lose your home to foreclosure. It’s typically not worth the increased risk to pay off your credit cards – especially when there are so many other solutions available.</p>
                <h3>How long should it take to pay off credit card balances?</h3>
                <p>A good rule of thumb is that any solution you use should be able to pay off your debt in 3-5 years. Solutions that take longer than that would mean that you’re still throwing money away on interest charges. If you play around with the credit card payoff calculator above, you can see the problem that arises with lengthy repayment plans, like minimum payments. At a certain point, the total interest charges actually become more than the original amount charged.</p>
                <p>Let’s say you have a $2,000 credit card balance to pay off at 22% APR. On a standard 3% payment schedule, it takes almost 11 years to pay off the balance. The total interest charges are $2,299.67. So, you charged $2,000, but you end up paying more than double that amount to pay off your balances.</p>
                <p>This shows why credit card debt is such a burden on your budget. People often think of credit cards like free money, but interest charges often eat up income that you needed to cover other things. As a result, you end up juggling bills and putting off necessary expenses like doctor’s visits and car repairs. High credit card balances could be the reason you’re having so much trouble making ends meet.</p>
                <div class="short-cta">
                    <div>
                        <h4>Don’t spend another sleepless night stressing about your balances. Let Debt.com help you find a solution that works for you and your budget.</h4></div><a target="_blank" href="https://www.debt.com/land/credit-counseling-1/?kw=credit+consolidation&amp;utm_campaign=inlinecta" class="btn btn-primary">Find Relief Today</a></div>
            </div> -->
            <script type="application/ld+json">{ "@context": "http://schema.org", "@type": "Organization", "name": "Debt.com", "legalName": "Debt.com, LLC.", "url": "https://www.debt.com", "logo": "https://keycdn.debt.com/wp-content/themes/Dcom/images/logo4.png", "telephone": "1-800-810-0989", "email": "editor(at)debt.com", "contactPoint": [ { "@type": "ContactPoint", "telephone": "+1-800-810-0989", "contactType": "customer service", "areaServed": [ "US" ] } ], "address": { "@type": "PostalAddress", "streetAddress": "5769 W. Sunrise Blvd.", "addressLocality": "Plantation", "addressRegion": "FL", "postalCode": "33313", "addressCountry": "US" }, "sameAs": [ "https://www.facebook.com/debtcom", "https://twitter.com/debtcom", "https://www.linkedin.com/company/debt-com", "https://www.youtube.com/c/DebtDotCom", "https://plus.google.com/114028290748823715361/posts", "https://www.pinterest.com/debtcom/", "https://flipboard.com/@debtcom", "https://www.debt.com/feed/" ] }
            </script>
        </div>
      <!--   <div class="footer-disclaimer container-fluid">
            <p>This website is intended for informational purposes and as a reference tool to match consumers with companies that may be able to assist them. <a href="/advertising-disclosures/">View our Advertising Disclosures here</a>.</p>
        </div> -->

<?php 
get_footer();
?>
