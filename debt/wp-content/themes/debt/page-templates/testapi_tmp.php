<?php 
// Template Name: TestApi Page Template
get_header();
?>


<script type="text/javascript">
  //var company_slug = 'demo-b10';
  var company_slug = 'plumbtastic';
  var createCORSRequest = function(method, url) {
  var xhr = new XMLHttpRequest();
  if ("withCredentials" in xhr) {
    // Most browsers.
    xhr.open(method, url, true);
  } else if (typeof XDomainRequest != "undefined") {
    // IE8 & IE9
    xhr = new XDomainRequest();
    xhr.open(method, url);
  } else {
    // CORS not supported.
    xhr = null;
  }
  return xhr;
};

var url = 'https://www.reviewchamp.net/login/reviews?company_slug='+company_slug;
var method = 'GET';
var xhr = createCORSRequest(method, url);

xhr.onload = function() {
  alert(xhr.response);
  console.log(xhr.response);
  // Success code goes here.
};

xhr.onerror = function() {
  alert("error");
  // Error code goes here.
};

xhr.send();
</script>

<?php get_footer(); ?>