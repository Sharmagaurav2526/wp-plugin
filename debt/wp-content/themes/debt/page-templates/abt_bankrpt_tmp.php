<?php 
// Template Name: About Bankruptcy
get_header();
?>
<div id="content" class="site-content">
	<div class="education container">
		<div class="row">
			<div class="col-lg-12">
				<div class="title">
					<h1 class="text-center"><?php the_field('about_title'); ?></h1>
					
					<div id="identify" class="section_rt">
						<div class="image_l"><img class="img-responsive" src="<?php the_field('about_images'); ?>"/></div>
						<?php the_field('about_content'); ?></div>

						
						<div id="identify" class="section_rt">
							<div class="image_l"><img src="<?php the_field('about_image_2'); ?>" /></div>
						<?php the_field('bankruptcy_content'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
get_footer();
?>
