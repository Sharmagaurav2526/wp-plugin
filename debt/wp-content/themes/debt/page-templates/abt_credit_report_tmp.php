<?php 
// Template Name: About Credit Reporting
get_header();
?>
<div id="content" class="site-content">
	<div class="education container">
		<div class="row">
			<div class="col-lg-12">
				<div class="title">
					<h1 class="text-center">About Credit Reporting</h1>
				</div>
				<div id="identify" class="section_crt">
					<p class="text-center"><strong><?php the_field('credit_title');?></strong></p>
					<div class="text-center"><img src="<?php the_field('credit_image');?>" /></div>
					<?php the_field('credit_content');?>
				</div>
				<div class="credit-report">
				    <h3 class="text-center report"><strong><?php the_field('improving_title');?></strong></h3>
				    <div class="hr"></div>
				    <div class="hr2"></div>
				    <div class="report-content">
				        <div class="image_l"><img src="<?php the_field('improving_image');?>" /></div>
				        <?php the_field('improving_content');?>
				    </div>
				</div>

				<div class="credit-act">
				    <h3 class="text-center"><strong><?php the_field('fair_act_heading');?></strong></h3>
				    <div class="hr3"></div>
				    <div class="hr4"></div>
				    <div class="fair-act-content">
				        <?php the_field('fair_act_content');?>
				    </div>
				</div>
				<div class="other-consumer">
                    <strong><?php the_field('consumer_heading');?></strong>
                    <div class="hr0"></div>
				    <div class="hr1"></div>
					<?php the_field('bankrupt_content');?>
					<div class="dispute">
						<h3><strong><?php the_field('disputes_title');?></strong></h3>
					<div class="image_l"><img src="<?php the_field('disputes_image');?>" /></div>
					<?php the_field('disputes_content');?>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
get_footer();
?>
