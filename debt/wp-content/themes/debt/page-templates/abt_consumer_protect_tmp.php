<?php 
// Template Name: About Consumer Protection Laws
get_header();
?>
<div id="content" class="site-content">
	<div class="education container">
		<div class="row">
			<div class="col-lg-12">
			<h1 class="text-center">About Consumer Protection Laws</h1>
				<div id="identify" class="section_abt">
					<?php the_field('consumer_content'); ?>
						
				</div>

					<div class="consumer-law">
					<img src="<?php the_field('consumer_image'); ?>" />
					<?php the_field('consumer_content_2'); ?>
				</div>
				   
			     
		    </div>
	   </div>
    </div>
</div>

<?php 
get_footer();
?>
