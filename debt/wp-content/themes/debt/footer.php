<footer id="colophon" class="site-footer" role="contentinfo">
   <?php ?>
   <div id="footer-organization-wrapper">
      <div class="footer-main">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-9 text-center m-auto">
                  <div class="nav-column">
                     <div class="menu-company-container">
                        <?php if(!is_front_page()): ?>
                        <?php 
                           wp_nav_menu(array(
                            'theme_location' => 'primary',
                            'menu' => 'main-menu',
                            'container' => 'ul',
                            'menu_class' => 'footer-menu',
                            'echo' => true,
                            'before' => '',
                            'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'depth' => 0
                            )
                           );
                           ?>
                        <!-- <ul id="menu-company" class="footer-menu">
                           <li class="menu-item"><a href="#">Home</a></li>
                           <li class="menu-item"><a href="#">Solutions</a></li>
                           <li class="menu-item"><a href="#">Education</a></li>
                           <li class="menu-item"><a href="#">News</a></li>
                           <li class="menu-item"><a href="http://debt.mobilytedev.com/contact-page/">Contact us</a></li>
                                                  </ul> -->
                        <?php endif; ?>
                     </div>
                  </div>
                  <div class="social-icons">
                     <ul>
                        <?php 
                           if( have_rows('footer_icons','option') ):
                                   while ( have_rows('footer_icons','option') ) : the_row();
                            ?>
                        <li> 
                           <a class="sm-icon <?php the_sub_field('main_class'); ?>" href="<?php the_sub_field('icon_link'); ?>" target="_blank" property="sameAs"> 
                           <span class="icon <?php the_sub_field('main_class'); ?>"><i aria-hidden="true" class="<?php the_sub_field('icon_class'); ?>" title="<?php the_sub_field('icon_title'); ?>"></i></span> 
                           <span class="sr-only"><?php the_sub_field('icon_tagline'); ?></span> </a>
                        </li>
                        <?php 
                           endwhile;
                           endif;
                           ?>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php ?>
      <div class="footer-bottom">
         <div class="container-fluid text-center">
            <div class="row">
               <div class="col-lg-12"> © 2018 <span property="legalName">
                  <span property="name">eDebt</span>® </span> All Rights Reserved.
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>
<!--/#footer-->
<?php wp_footer(); ?>
<!-- <script>
   jQuery(document).ready(function(){

    setTimeout( function() {

      jQuery('#myModal').modal('toggle'); },10000);
      //script for onload page show popup
     

     jQuery('.debt-types .student-loans').click(function(){
       jQuery('.debt-types label').removeClass('active');
   
     });
     jQuery('.debt-types .taxes').click(function(){
       jQuery('.debt-types label').removeClass('active');
       
     });
      jQuery('.credit-repair .taxes').click(function(){
       jQuery('.debt-types label').removeClass('active');
       
     });
   });
</script> -->
<!---------------------------------POP-UP SCRIPT---------------------------------------------->
 <script>
  jQuery(document).ready(function(){
    jQuery('.form_class').on('click',function(event){
      event.preventDefault(); 


                    var debt_amount_1 = jQuery('#bx-element_1').val();
                    var debt_amount_2 = jQuery('#bx-element_2').val();
                    var debt_amount_3 = jQuery('#bx-element_3').val();
                    
                    var payment_status = jQuery('#bx-payment').val();
                    var credit_card = jQuery('#selected_debt_option').val();
                  
                    var loan_amount = jQuery('#bx-loan').val();
                    var status_type = jQuery('#bx-status').val();
                    var debt_tax    = jQuery('#bx-debt-tax').val();
                    var credit_payment = jQuery('#bx-credit').val();

                    //console.log(interest_about);
                    //return false;
                    var firstName_1 = jQuery('#fname_1').val();
                    var firstName_2 = jQuery('#fname_2').val();
                    var firstName_3 = jQuery('#fname_3').val();
                    var firstName_4 = jQuery('#fname_4').val();

                    var lastname_1  = jQuery('#lname_1').val();
                    var lastname_2 = jQuery('#lname_2').val();
                    var lastname_3  = jQuery('#lname_3').val();
                    var lastname_4  = jQuery('#lname_4').val();

                    var emailAddress_1 = jQuery('#email_1').val();
                    var emailAddress_2= jQuery('#email_2').val();
                    var emailAddress_3= jQuery('#email_3').val();
                    var emailAddress_4 = jQuery('#email_4').val();

                    var zip_code_1     = jQuery('#zip_code_1').val();
                    var zip_code_2     = jQuery('#zip_code_2').val();
                    var zip_code_3     = jQuery('#zip_code_3').val();
                    var zip_code_4     = jQuery('#zip_code_4').val();

                    var phoneNumber_1 = jQuery('#phone_1').val();
                    var phoneNumber_2 = jQuery('#phone_2').val();
                    var phoneNumber_3 = jQuery('#phone_3').val();
                    var phoneNumber_4 = jQuery('#phone_4').val();
                    
                    


                     //alert(top_for_vanue); 
                     var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
                    jQuery.ajax({
                        type : 'POST',
                        url : ajaxurl,
                        //action: 'multi_step_ajax_handler',
                        data : {'action': 'multi_step_form_submit',
                        debt_amount_1: debt_amount_1,
                        debt_amount_2: debt_amount_2,
                        debt_amount_3: debt_amount_3,
                         credit_card: credit_card, 
                          payment_status: payment_status,
                         loan_amount: loan_amount,
                         status_type: status_type,
                         firstName_1: firstName_1,
                         firstName_2: firstName_2,
                         firstName_3: firstName_3,
                         firstName_4: firstName_4,
                         lastname_1: lastname_1, 
                         lastname_2: lastname_2, 
                         lastname_3: lastname_3, 
                         lastname_4: lastname_4, 
                         emailAddress_1: emailAddress_1, 
                         emailAddress_2: emailAddress_2, 
                         emailAddress_3: emailAddress_3, 
                         emailAddress_4: emailAddress_4, 
                         zip_code_1: zip_code_1,
                         zip_code_2: zip_code_2,
                         zip_code_3: zip_code_3,
                         zip_code_4: zip_code_4,
                         debt_tax: debt_tax,
                         credit_payment: credit_payment, 
                         phoneNumber_1: phoneNumber_1, 
                         phoneNumber_2: phoneNumber_2, 
                         phoneNumber_3: phoneNumber_3, 
                         phoneNumber_4: phoneNumber_4, 
                       },

                       success: function(result){
                              alert(result);
                             //location.reload();
                            //console.log(result);
                       },
                       error: function(result){
                            console.log('error');
                       } 
                  });
            //jquery clode popup
     });
  });
</script>
<script>
jQuery( "#c-button--push-right" ).click(function() {
  jQuery( "#header-nav" ).toggle( "slow")
});
</script>
<script type="text/javascript">
    jQuery(document).on('click', '.hide_type-of-debt', function(e) {
    // body...
    e.preventDefault();
   if(jQuery(this).hasClass('hide_type-of-debt')) $('#myModal').modal('toggle');

  });
</script>
<!---------------------------------END POP-UP SCRIPT---------------------------------------------->
<!--  -->
<script type="text/javascript">

  
  jQuery(document).on('click', '.debt_options', function(e) {
    e.preventDefault();
    var selected_debt_option = $(this).attr('its-value');
    $('#selected_debt_option').val(selected_debt_option);

    $('.text-center.bar').show();
    $('div.progress').show();
    jQuery(this).closest('fieldset').hide(400);
    var target_class = $(this).attr('target-class');
    if(target_class=='.taxes-debt-steps') var initial_width=70;
    else var initial_width=25;
    if(target_class=='.credit-repair-debt-steps') var initial_width=70;
     else var initial_width=25;
    jQuery(target_class+':first').show(400);

    var width = 0;
    var id = setInterval(frame, 10);
    function frame() {
      if (width == initial_width) {
        clearInterval(id);
      } else {
        width++; 
        jQuery('.progress-bar').css('width',width + '%');
        jQuery('.bar i').text(width+' % complete')
      }
    }

  });




  jQuery(document).on('click', 'fieldset .next2.action-button2', function(e) {
    e.preventDefault();
    if(jQuery(this).hasClass('final')) $('#myModal').modal('toggle');
    var step = jQuery(this).closest('fieldset');
    jQuery(this).closest('fieldset').hide();
    step.next().show(400);
    if(step.next().data('step')=='4'){
      $('.text-center.bar').hide();
      $('div.progress').hide();
    }
    var start_width = step.data('progress');
    var next_width = step.next().data('progress');
    var width = start_width;
    var id = setInterval(frame, 10);
    function frame() {
      if (width == next_width) {
        clearInterval(id);
      } else {
        width++; 
        jQuery('.progress-bar').css('width',width + '%')
        jQuery('.bar i').text(width+' % complete')
      }
    }

  });
</script>
</body>
</html>