<div class="container register-form">
	 <div class="row">
	    <div class="col-md-6 col-sm-12 col-lg-6 col-md-offset-3">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<!-- <h2>Work Order Form</h2> -->
					<!-- Work Order Form For Owner -->
					<h2>Service Request</h2>
				</div>
				<div class="panel-body">
	               
	                    <!-- Work Order Form For Resident -->
	                       <form name="resident_myform" method="post" class="service_request" id="service_request_resident">
	                       	<input id="user_type" name="user_type" class="form-control" type="hidden" value="<?php echo $role[0]; ?>">
	                       	<input id="user_id" name="user_id" class="form-control" type="hidden" value="<?php echo $user_id; ?>">
						<div class="form-group">
							<label for="resident_myName">First Name</label>
							<input id="resident_myName" name="resident_myName" class="form-control" type="text" data-validation="required">
							<span id="error_name" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="resident_lastname">Last Name</label>
							<input id="resident_lastname" name="resident_lastname" class="form-control" type="text" data-validation="email">
							<span id="error_lastname" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="resident_email">Email</label>
							<input id="resident_email" name="resident_email" class="form-control" type="email" data-validation="email">
							<span id="error_email" class="text-danger"></span>
						</div>
						
						<div class="form-group">
							<label for="resident_phone">Phone Number</label>
							<input type="text" id="resident_phone" name="resident_phone" class="form-control">
							<span id="error_phone" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="resident_address">Address</label>
							<textarea class="form-control" rows="3" name="resident_address" id="resident_address"></textarea>
						</div>
						<div class="form-group">
							<label for="phone">Your Unit</label>
							<input type="text" id="resident_unit" name="resident_unit" class="form-control" >
							<span id="error_phone" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="resident_city">Your City</label>
							<input type="text" id="resident_city" name="resident_city" class="form-control" >
							<span id="error_phone" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="disc">Discription Of Problem</label>
							<textarea class="form-control" rows="3" name="resident_description" id="resident_description"></textarea>
						</div>

						<input type="hidden" name="action" value="service_request_ajax_handler">
						<input type="hidden" name="service_request" value="resident">
						<button id="submit" type="submit" value="submit" class="btn btn-primary center" name="resident_submit">Submit</button>
				
					</form>


	                
				
				</div>
			</div>
		</div>
	</div>
</div>