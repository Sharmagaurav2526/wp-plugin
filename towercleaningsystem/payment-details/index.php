<?php 
include( '../wp-load.php' ); // loads WordPress Environment
get_header(); 
$formId = $_REQUEST['form-id'];
?>
  <!-- link to the SqPaymentForm library -->
  <script type="text/javascript" src="paymentform.js"></script>

  <!-- link to the local SqPaymentForm initialization -->
  <script type="text/javascript" src="sqpaymentform.js"></script>

  <!-- link to the custom styles for SqPaymentForm -->
  <link rel="stylesheet" type="text/css" href="sqpaymentform.css">

  
<div class="form-wrapper">
<div class="container register-form">
  <div id="sq-ccbox">
    <!--
      You should replace the action attribute of the form with the path of
      the URL you want to POST the nonce to (for example, "/process-card")
    -->
    <form id="nonce-form" novalidate action="process-card.php" method="post">
      Pay with a Credit Card
      <table>
      <tbody>
        <tr>
          <td>Card Number:</td>
          <td><div id="sq-card-number"></div></td>
        </tr>
        <tr>
          <td>CVV:</td>
          <td><div id="sq-cvv"></div></td>
        </tr>
        <tr>
          <td>Expiration Date: </td>
          <td><div id="sq-expiration-date"></div></td>
        </tr>
        <tr>
          <td>Postal Code:</td>
          <td><div id="sq-postal-code"></div></td>
        </tr>
        <tr>
          <td colspan="2">
            <button id="sq-creditcard" class="button-credit-card" onclick="requestCardNonce(event)">
              Pay with card
            </button>
          </td>
        </tr>
      </tbody>
      </table>

      <!--
        After a nonce is generated it will be assigned to this hidden input field.
      -->
      <input type="hidden" id="card-nonce" name="nonce">
      <input type="hidden" id="sq-amount" name="sq-amount" value="25">
      <input type="hidden" id="sq-form" name="sq-form" value="<?php echo $formId; ?>">
    </form>
  </div>

  <div id="sq-walletbox" style="display:none">
    Pay with a Digital Wallet
    <div id="sq-apple-pay-label" class="wallet-not-enabled">Apple Pay for Web not enabled</div>
    <!-- Placeholder for Apple Pay for Web button -->
    <button id="sq-apple-pay" class="button-apple-pay"></button>

    <div id="sq-masterpass-label" class="wallet-not-enabled">Masterpass not enabled</div>
    <!-- Placeholder for Masterpass button -->
    <button id="sq-masterpass" class="button-masterpass"></button>
  </div>
</div>
</div>
<?php get_footer(); ?>