<?php
// v2014.09.23

require_once(dirname(__FILE__) . '/ip_in_range.php');


/*
 * Return true if user is coming from a recognized IP address, false otherwise
 */
function ucp_whitelist_isOfficeIP(){
	if(defined('UCP_WHITELIST_IS_OFFICE_IP')){
		if(UCP_WHITELIST_IS_OFFICE_IP) return true;
		else return false;
	}
	
    $allowed_ips = array(
        '127.0.0.1',   // localhost
        '::1',   // IPv6 localhost
        '64.91.250.206',   // Liquid Web server
        '50.79.9.169',   // Comcast
    );
    $allowed_range = '10.254.4.0/22';   // Firehost VPN
    
    $user_ip = $_SERVER['REMOTE_ADDR'];
    
    if(in_array($user_ip, $allowed_ips)){
		define('UCP_WHITELIST_IS_OFFICE_IP', true);
		return true;
	}
    elseif(ip_in_range($user_ip, $allowed_range)){
		define('UCP_WHITELIST_IS_OFFICE_IP', true);
		return true;
	}
    else{
		define('UCP_WHITELIST_IS_OFFICE_IP', false);
		return false;
	}
}

?>