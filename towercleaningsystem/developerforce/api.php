<?php

/*
 * This is the main file for the Salesforce PHP API.
 * It provides a single function that serves up Salesforce login credentials,
 * so that alternate versions can be created to log in to sandboxes
 * while sharing the same library of functions.
 */



/*
 * A standard function to provide login credentials. Can be customized for sandboxes etc.
 */
function sfConnectionCredentials(){
	$credentials['sf_username'] = "salesforceapi@ucp.org";   // add .sandboxname to the end for sandboxes
	$credentials['sf_password'] = "Wg<omX6'%`|4^CIK\*ou";
	$credentials['wsdl_file_name'] = "partner.wsdl";   // Use partner.sandbox.wsdl for all sandboxes, partner.wsdl for production
	
	return $credentials;
}

require_once(dirname(__FILE__).'/api_functions.php');

?>
