<?php 
// Template Name: Dashboard Template
get_header();
?>
<style type="text/css">
  
#example_wrapper {

    margin-top: 5%;

}
.modal {    
      top: 100px;
        }
 .modal-backdrop.in {
    height: auto !important;
}
.modal-backdrop.in {
    filter: alpha(opacity=50);
    opacity: 0;
}       
</style>
<?php 
// if (is_user_logged_in()):
    global $wpdb;
    $user_id = get_current_user_id();
    $appTable = $wpdb->prefix . "service_request";
    $userTable = $wpdb->prefix . "users";    
    $applications = $wpdb->get_results('SELECT * FROM  `as_service_request` AS p INNER JOIN `as_users` AS pm ON p.user_id = pm.id where p.user_id = "'.$user_id.'"'); ?>  

<div class="daashboard-wrapper">
<div class="container">
<div class="section-header">
			             			     <h2 class="section-title text-center wow fadeInDown animated animated animated" style="visibility: visible; animation-name: fadeInDown;">Hello <?php $user = get_user_by( 'id', $user_id); echo $user->data->display_name; ?>, Your Service Requests</h2>           
			               
            			</div>
  
  <div class="panel-group">
    <?php 
          //$counter = 0;
          foreach($applications as $key=>$application){ 
    ?>
     <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#collapse<?php echo $key; ?>">Service Request ID: <?php echo $application->id; ?></a>  
        </h4>
      </div>
      <div id="collapse<?php echo $key; ?>" class="panel-collapse collapse  <?php echo ($key+1 == 1 ? 'in' : NULL); ?>">
        <div class="panel-body">
          <div class="row">            
            <div class="col-sm-6 form-group"><label for="Name">Name:</label> <?php echo $application->first_name." ". $application->last_name; ?></div> 
            <div class="col-sm-6 form-group"><label for="Email">Email:</label> <?php echo $application->email; ?></div>
            <div class="col-sm-6 form-group"><label for="Phone">Phone:</label> <?php echo $application->phone_number; ?></div>
            <div class="col-sm-6 form-group"><label for="Address">Address:</label> <?php echo $application->address; ?></div>
          </div>
          <div class="row">          
            <div class="col-sm-6 form-group"><label for="City">City:</label> <?php echo $application->city; ?></div>
            <div class="col-sm-6 form-group"><label for="Unit">Unit:</label> <?php echo $application->unit; ?></div>
            <div class="col-sm-6 form-group"><label for="Type">User Type:</label> <?php echo $application->user_type; ?></div>   
            <div class="col-sm-6 form-group"><label for="Status">Status:</label> <?php echo $application->status; ?></div>
          </div>  
          <div class="row">
            <div class="col-sm-12 form-group"><label for="Description">Description:</label> <?php echo $application->description; ?></div>
          </div>
          <div class="row">
            <div class="col-sm-12 form-group">
              <h3>Requested Services List:</h3> 
              <div>
                <table class="table table-striped">
                  <thead class="alert alert-info">
                      <tr>
                          <th>Sr. No.</th>                          
                          <th>Service Name</th>
                          <th>Service Price</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php                       
                      $requestedServices = $wpdb->get_results('SELECT * FROM  `as_services` where `service_request_id` = "'.$application->id.'"');
                      //echo "<pre>"; print_r($requestedServices);
                      if(count($requestedServices)>0){
                      foreach($requestedServices as $key=>$service){  
                    ?>
                      <tr>
                          <td><?php echo $key+1; ?></td>                          
                          <td><?php echo $service->service_name; ?></td>
                          <td><?php echo $service->service_price; ?></td>
                      </tr> 
                      <?php  }} else { ?>  
                      <tr>
                          <td class="text-center"><h3>There is no service added.</h3></td>                          
                      </tr> 
                      <?php  } ?>                                         
                  </tbody>
                </table>
              </div>
            </div>
          </div>          
        </div>        
      </div>
    </div>
    <?php  } ?>   
  </div>
</div>
</div>




<?php get_footer(); ?>

