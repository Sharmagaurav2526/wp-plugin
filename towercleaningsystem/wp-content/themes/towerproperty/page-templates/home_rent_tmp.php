<?php  
// Template Name: Home Rent Page
get_header();
?>

<?php 
$gallery_post_id = $_GET['id'];

//print_r($gallery_post_id); die; 
global $wpdb;
$as_rental_table = $wpdb->prefix . "rental_apps";
if($_POST['action'] == 'rental_apps_info'){
        $app_name = $_POST['app_name'];
        $app_middle_name = $_POST['app_middle_name'];
        $app_last_name = $_POST['app_last_name'];
        $app_dob = $_POST['app_dob'];
        $app_social_security = $_POST['app_social_security'];
        $app_email  = $_POST['app_email']; 
        $app_home_phone = $_POST['app_home_phone'];
        $app_cell_phone = $_POST['app_cell_phone'];
        $app_driver_licence = $_POST['app_driver_licence']; 
        $app_other_occupants = $_POST['app_other_occupants'];
        $app_occupant_dob = $_POST['app_occupant_dob'];
        $app_relationship = $_POST['app_relationship'];
        $app_other_occupant1 = $_POST['app_other_occupant1'];
        $app_occupant_dob1 = $_POST['app_occupant_dob1'];
        $app_relationship1 = $_POST['app_relationship1'];
        $app_other_occupant2 = $_POST['app_other_occupant2'];
        $app_occupant_dob2 = $_POST['app_occupant_dob2'];
        $app_relationship2 = $_POST['app_relationship2'];
        $applicant_info_array = array('app_name'=>$app_name,'app_middle_name'=>$app_middle_name,'app_last_name'=>$app_last_name,'app_dob'=>$app_dob,'app_social_security'=>$app_social_security,'app_email'=>$app_email,'app_home_phone'=>$app_home_phone,'app_cell_phone'=>$app_cell_phone,'app_driver_licence'=>$app_driver_licence,'app_other_occupants'=>$app_other_occupants,'app_occupant_dob'=>$app_occupant_dob,'app_relationship'=>$app_relationship,'app_other_occupant1'=>$app_other_occupant1,'app_occupant_dob1'=>$app_occupant_dob1,'app_relationship1'=>$app_relationship1,'app_other_occupant2'=>$app_other_occupant2,'app_occupant_dob2'=>$app_occupant_dob2,'app_relationship2'=>$app_relationship2);
        $applicant_info_json = json_encode($applicant_info_array);

        /* Current residence and Previous residence Start Here */

        $rental_current_address =  $_POST['rental_current_address'];
        $rental_current_city = $_POST['rental_current_city'];
        $rental_current_state = $_POST['rental_current_state'];
        $rental_current_zip = $_POST['rental_current_zip'];
        $rental_current_monthly_rent = $_POST['rental_current_monthly_rent'];
        $rental_current_residency_date = $_POST['rental_current_residency_date'];
        $rental_current_reason_moving = $_POST['rental_current_reason_moving'];
        $rental_current_owner_name = $_POST['rental_current_owner_name'];
        $rental_current_phone = $_POST['rental_current_phone'];
        $rental_previous_address = $_POST['rental_previous_address'];
        $rental_previous_city = $_POST['rental_previous_city'];
        $rental_previous_state = $_POST['rental_previous_state'];
        $rental_previous_zip = $_POST['rental_previous_zip'];
        $rental_previous_monthly_rent = $_POST['rental_previous_monthly_rent'];
        $rental_previous_date_residency = $_POST['rental_previous_date_residency'];
        $rental_previous_reason_moving = $_POST['rental_previous_reason_moving'];
        $rental_previous_owner_name = $_POST['rental_previous_owner_name'];
        $rental_previous_phone = $_POST['rental_previous_phone'];
        $rental_history_array = array('rental_current_address'=>$rental_current_address,'rental_current_city'=>$rental_current_city,'rental_current_state'=>$rental_current_state,'rental_current_zip'=>$rental_current_zip,'rental_current_monthly_rent'=>$rental_current_monthly_rent,'rental_current_residency_date'=>$rental_current_residency_date,'rental_current_reason_moving'=>$rental_current_reason_moving,'rental_current_owner_name'=>$rental_current_owner_name,'rental_current_phone'=>$rental_current_phone,'rental_previous_address'=>$rental_previous_address,'rental_previous_city'=>$rental_previous_city,'rental_previous_state'=>$rental_previous_state,'rental_previous_zip'=>$rental_previous_zip,'rental_previous_monthly_rent'=>$rental_previous_monthly_rent,'rental_previous_date_residency'=>$rental_previous_date_residency,'rental_previous_reason_moving'=>$rental_previous_reason_moving,'rental_previous_owner_name'=>$rental_previous_owner_name,'rental_previous_phone'=>$rental_previous_phone);
        $rental_history_json = json_encode($rental_history_array);
        /* Current Address and Previous Address End Here */

        /* Current Address and Previous Address Start Here */

        $emp_history_current_employer =  $_POST['emp_history_current_employer'];
        $emp_history_current_occupation = $_POST['emp_history_current_occupation'];
        $emp_history_current_address = $_POST['emp_history_current_address'];
        $emp_history_current_phone = $_POST['emp_history_current_phone'];
        $emp_history_current_date = $_POST['emp_history_current_date'];
        $emp_history_current_supervisor = $_POST['emp_history_current_supervisor'];
        $emp_history_current_pay = $_POST['emp_history_current_pay'];
        $emp_history_previous_employer = $_POST['emp_history_previous_employer'];
        $emp_history_previous_occupation = $_POST['emp_history_previous_occupation'];
        $emp_history_previous_address = $_POST['emp_history_previous_address'];
        $emp_history_previous_phone = $_POST['emp_history_previous_phone'];
        $emp_history_previous_date = $_POST['emp_history_previous_date'];
        $emp_history_previous_supervisor = $_POST['emp_history_previous_supervisor'];
        $emp_history_previous_pay = $_POST['emp_history_previous_pay'];
        $emp_history_info_array = array('emp_history_current_employer'=>$emp_history_current_employer,'emp_history_current_occupation'=>$emp_history_current_occupation,'emp_history_current_address'=>$emp_history_current_address,'emp_history_current_phone'=>$emp_history_current_phone,'emp_history_current_date'=>$emp_history_current_date,'emp_history_current_supervisor'=>$emp_history_current_supervisor,'emp_history_current_pay'=>$emp_history_current_pay,'emp_history_previous_employer'=>$emp_history_previous_employer,'emp_history_previous_occupation'=>$emp_history_previous_occupation,'emp_history_previous_address'=>$emp_history_previous_address,'emp_history_previous_phone'=>$emp_history_previous_phone,'emp_history_previous_date'=>$emp_history_previous_date,'emp_history_previous_supervisor'=>$emp_history_previous_supervisor,'emp_history_previous_pay'=>$emp_history_previous_pay);
        $emp_history_info_json = json_encode($emp_history_info_array);
        /* Current Address and Previous Address End Here */


        /* Credit History Start Here */
        $credit_history_bank = $_POST['credit_history_bank'];
        $credit_history_balance = $_POST['credit_history_balance'];
        $credit_history_checking_account = $_POST['credit_history_checking_account'];
        $credit_history_saving_account = $_POST['credit_history_saving_account'];
        $credit_history_credit_card = $_POST['credit_history_credit_card'];
        $credit_history_auto_loan = $_POST['credit_history_auto_loan'];
        $credit_history_additional_debt = $_POST['credit_history_additional_debt'];
        $credit_history_info_array = array('credit_history_bank'=>$credit_history_bank,'credit_history_balance'=>$credit_history_balance,'credit_history_checking_account'=>$credit_history_checking_account,'credit_history_saving_account'=>$credit_history_saving_account,'credit_history_credit_card'=>$credit_history_credit_card,'credit_history_auto_loan'=>$credit_history_auto_loan,'credit_history_additional_debt'=>$credit_history_additional_debt);
        $credit_history_info_json = json_encode($credit_history_info_array);

        /* Credit History End Here */

        /* Reference Start Here */

        $refrence_name = $_POST['refrence_name'];
        $refrence_phone = $_POST['refrence_phone'];
        $refrence_relationship = $_POST['refrence_relationship'];
        $refrence_name1 = $_POST['refrence_name1'];
        $refrence_phone1 = $_POST['refrence_phone1'];
        $refrence_relationship1 = $_POST['refrence_relationship1']; 


         $reference_info_array = array('refrence_name'=>$refrence_name,'refrence_phone'=>$refrence_phone,'refrence_relationship'=>$refrence_relationship,'refrence_name1'=>$refrence_name1,'refrence_phone1'=>$refrence_phone1,'refrence_relationship1'=>$refrence_relationship1);
        $reference_info_json = json_encode($reference_info_array);
        /* Reference End Here */

        /* general Information Start Here */

        $general_info_delinquent_rent = $_POST['general_info_delinquent_rent'];
        $general_info_lawsuit = $_POST['general_info_lawsuit'];
        $general_info_smoke = $_POST['general_info_smoke'];
        $general_info_pets = $_POST['general_info_pets'];
        $general_info_breed = $_POST['general_info_breed'];
        $general_info_explain = $_POST['general_info_explain'];
        $general_info_explain1 = $_POST['general_info_explain1'];
        $general_info_explain2 = $_POST['general_info_explain2'];      



        $general_info_current_address = $_POST['general_info_current_address'];
        $general_info_current_address2 = $_POST['general_info_current_address2'];
        $general_info_negative_credit = $_POST['general_info_negative_credit'];
        $general_info_negative_credit2 = $_POST['general_info_negative_credit2'];

        $general_info_rental_fee = $_POST['general_info_rental_fee'];


        $general_info_array = array('general_info_delinquent_rent'=>$general_info_delinquent_rent,'general_info_lawsuit'=>$general_info_lawsuit,'general_info_smoke'=>$general_info_smoke,'general_info_pets'=>$general_info_pets,'general_info_breed'=>$general_info_breed,'general_info_explain'=>$general_info_explain,'general_info_explain1'=>$general_info_explain1,'general_info_explain2'=>$general_info_explain2,'general_info_current_address'=>$general_info_current_address,'general_info_current_address2'=>$general_info_current_address2,'general_info_negative_credit'=>$general_info_negative_credit,'general_info_negative_credit2'=>$general_info_negative_credit2,'general_info_rental_fee'=>$general_info_rental_fee);
        $general_info_json = json_encode($general_info_array);


        /* general Information End Here */

        /* Additional Information Start Here */

        $additional_question = $_POST['additional_question'];
        $additional_question2 = $_POST['additional_question2'];
        $additional_question3 = $_POST['additional_question3'];

        $additional_info_array = array('additional_question'=>$additional_question,'additional_question2'=>$additional_question2,'additional_question3'=>$additional_question3);
        $additional_info_json = json_encode($additional_info_array);

        /* Additional Information End Here */
        


        /* Applicant Signature And Date Start Here */

        $applicant_signature = $_POST['applicant_signature'];
        $applicant_date = $_POST['applicant_date'];

        $agreement_info_array = array('applicant_signature'=>$applicant_signature,'applicant_date'=>$applicant_date);
        $agreement_info_json = json_encode($agreement_info_array);

        /* Applicant Signature And Date End Here */

        $wpdb->insert($as_rental_table, 
            array( 
                'applicant_info_json'     => $applicant_info_json,
                'rental_history_json'     => $rental_history_json,
                'employment_history_json' => $emp_history_info_json,
                'credit_history' => $credit_history_info_json,
                'reference_json' => $reference_info_json,
                'general_json'  => $general_info_json,
                'additional_json' => $additional_info_json,
                'aggrement_json'  => $agreement_info_json,
                'gallery_post_id' => $gallery_post_id        
            ), 
            array( 
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',    
                '%s'    
            ) 
        );
        $lastid = $wpdb->insert_id;
//die;
                $to = 'info@towerpropertyservices.com';
                //$to = 'sachin.gupta@mobilyte.com';
                $cc = 'sunpreet@mobilyte.com,sachin.gupta@mobilyte.com';
                $subject = 'Service Request Confirmation';
                $from = 'info@towerpropertyservices.com';
                // To send HTML mail, the Content-type header must be set
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                // Create email headers
                $headers .= 'From: '.$from."\r\n".
                    'Reply-To: '.$from."\r\n" . 'Cc: '.$cc."\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                // Compose a simple HTML email message
                $message1 = '<html><body><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><style>
                        table {
                            border-collapse: collapse;
                            border-spacing: 0;
                            width: 100%;
                            border: 1px solid #ddd;
                        }

                        th, td {
                            text-align: left;
                            padding: 5px;
                        }

                        th {
                            font-weight: bold;
                        }

                        tr:nth-child(even) {
                            background-color: #f2f2f2
                        }

                        th:first-child, td:first-child {
                            text-align: left;
                        }
                        .fa-check {
                            color: green;
                        }

                        .fa-remove {
                            color: red;
                        }
                        .column {
                            float: left;
                            width: 50%;
                        }

                        /* Clear floats after the columns */
                        .row:after {
                            content: "";
                            display: table;
                            clear: both;
                        }
                        </style>';
                $message1 .= '<div style="border: 1px solid black;padding: 0;background: #337ab7;border-radius: 0;text-align: center;font-size: 19px;color: white;">Applicant Information</div>
<table style="border: 1px solid black;border-collapse: collapse;width:100%;background-color: lemonchiffon;">
  <tr>
    <th>First Name</th>
    <th>Middle Name</th>
    <th>Last Name</th>
  </tr>
  <tr>
    <td>'.$app_name.'</td>
    <td>'.$app_middle_name.'</td>
    <td>'.$app_last_name.'</td>
  </tr>
  <tr>
    <th>Birth Date</th>
    <th>Social Security</th>
    <th>Email Address</th>
  </tr>
  <tr>
    <td>'.$app_dob.'</td>
    <td>'.$app_social_security.'</td>
    <td>'.$app_email.'</td>
  </tr>
  <tr>
    <th>Home Phone</th>
    <th>Cell Phone</th>
    <th>Driver\'s Licence#</th>
  </tr>
  <tr>
    <td>'.$app_home_phone.'</td>
    <td>'.$app_cell_phone.'</td>
    <td>'.$app_driver_licence.'</td>
  </tr>
 <tr>
    <td colspan="3"><table>
<tr>
    <th style="border: 1px solid goldenrod;border-collapse: collapse;background-color: goldenrod;color: white;">All Other occupants (under 18)</th>
    <th style="border: 1px solid goldenrod;border-collapse: collapse;background-color: goldenrod;color: white;">Birth Date</th>
    <th style="border: 1px solid goldenrod;border-collapse: collapse;background-color: goldenrod;color: white;">Relationship to Applicant</th>
  </tr>

  <tr style="">
    <td style="border: 1px solid goldenrod;border-collapse: collapse;text-align:center;">'.$app_other_occupants.'</td>
    <td style="border: 1px solid goldenrod;border-collapse: collapse;">'.$app_occupant_dob.'</td>
    <td style="border: 1px solid goldenrod;border-collapse: collapse;text-align:center;">'.$app_relationship.'</td>
  </tr>
  <tr style="">
    <td style="border: 1px solid goldenrod;border-collapse: collapse;text-align:center;">'.$app_other_occupant1.'</td>
    <td style="border: 1px solid goldenrod;border-collapse: collapse;">'.$app_occupant_dob1.'</td>
    <td style="border: 1px solid goldenrod;border-collapse: collapse;text-align:center;">'.$app_relationship1.'</td>
  </tr>
  <tr style="">
    <td style="border: 1px solid goldenrod;border-collapse: collapse;text-align:center;">'.$app_other_occupant2.'</td>
    <td style="border: 1px solid goldenrod;border-collapse: collapse;">'.$app_occupant_dob2.'</td>
    <td style="border: 1px solid goldenrod;border-collapse: collapse;text-align:center;">'.$app_relationship2.'</td>
  </tr>
  </table></td>
  </tr>
  </table>

<div style="border: 1px solid black;padding: 0;background: #337ab7;border-radius: 0;text-align: center;font-size: 19px;color: white;">Current residence Rental History</div>
<table style="border: 1px solid black;border-collapse: collapse;width:100%;background-color: lemonchiffon;">  
  
  <tr>
    <th>Address</th>
    <th>City</th>
    <th>State</th>
  </tr>
  <tr>
    <td>'.$_POST['rental_current_address'].'</td>
    <td>'.$_POST['rental_current_city'].'</td>
    <td>'.$_POST['rental_current_state'].'</td>
  </tr>
  <tr>
    <th>Zip</th>
    <th>Monthly Rent</th>
    <th>Date of Residency</th>
  </tr>
  <tr>
    <td>'.$_POST['rental_current_zip'].'</td>
    <td>'.$_POST['rental_current_monthly_rent'].'</td>
    <td>'.$_POST['rental_current_residency_date'].'</td>
  </tr>
  <tr>
    <th>Reason for moving</th>
    <th>Owner/Manager name</th>
    <th>Phone Number</th>
  </tr>
  <tr>
    <td>'.$_POST['rental_current_reason_moving'].'</td>
    <td>'.$_POST['rental_current_owner_name'].'</td>
    <td>'.$_POST['rental_current_phone'].'</td>
  </tr> 
  </table>

<div style="border: 1px solid black;padding: 0;background: #337ab7;border-radius: 0;text-align: center;font-size: 19px;color: white;">Previous residence Rental History</div>
<table style="border: 1px solid black;border-collapse: collapse;width:100%;background-color: lemonchiffon;">    
  <tr>
    <th>Address</th>
    <th>City</th>
    <th>State</th>
  </tr>
  <tr>
    <td>'.$_POST['rental_previous_address'].'</td>
    <td>'.$_POST['rental_previous_city'].'</td>
    <td>'.$_POST['rental_previous_state'].'</td>
  </tr>
  <tr>
    <th>Zip</th>
    <th>Monthly Rent</th>
    <th>Date of Residency</th>
  </tr>
  <tr>
    <td>'.$_POST['rental_previous_zip'].'</td>
    <td>'.$_POST['rental_previous_monthly_rent'].'</td>
    <td>'.$_POST['rental_previous_date_residency'].'</td>
  </tr>
  <tr>
    <th>Reason for moving</th>
    <th>Owner/Manager name</th>
    <th>Phone Number</th>
  </tr>
  <tr>
    <td>'.$_POST['rental_previous_reason_moving'].'</td>
    <td>'.$_POST['rental_previous_owner_name'].'</td>
    <td>'.$_POST['rental_previous_phone'].'</td>
  </tr> 
  </table>

  <div style="border: 1px solid black;padding: 0;background: #337ab7;border-radius: 0;text-align: center;font-size: 19px;color: white;">Current employer Employment History</div>
<table style="border: 1px solid black;border-collapse: collapse;width:100%;background-color: lemonchiffon;">
  <tr>
    <th>Current employer</th>
    <th>Occuption</th>
    <th></th>
  </tr>
  <tr>
    <td>'.$emp_history_current_employer.'</td>
    <td>'.$emp_history_current_occupation.'</td>
    <td></td>
  </tr>
  <tr>
    <th>Employer address</th>
    <th>Employer\'s phone</th>
    <th>Date of employment</th>
  </tr>
  <tr>
    <td>'.$emp_history_current_address.'</td>
    <td>'.$emp_history_current_phone.'</td>
    <td>'.$emp_history_current_date.'</td>
  </tr>
  <tr>
    <th>Name of supervisor</th>
    <th>Monthly pay</th>
    <th></th>
  </tr>
  <tr>
    <td>'.$emp_history_current_supervisor.'</td>
    <td>'.$emp_history_current_pay.'</td>
    <td></td>
  </tr> 
  </table>
  <div style="border: 1px solid black;padding: 0;background: #337ab7;border-radius: 0;text-align: center;font-size: 19px;color: white;">Previous employer Employment History</div>
  <table style="border: 1px solid black;border-collapse: collapse;width:100%;background-color: lemonchiffon;">
  <tr>
    <th>Current employer</th>
    <th>Occuption</th>
    <th></th>
  </tr>
  <tr>
    <td>'.$emp_history_previous_employer.'</td>
    <td>'.$emp_history_previous_occupation.'</td>
    <td></td>
  </tr>
  <tr>
    <th>Employer address</th>
    <th>Employer\'s phone</th>
    <th>Date of employment</th>
  </tr>
  <tr>
    <td>'.$emp_history_previous_address.'</td>
    <td>'.$emp_history_previous_phone.'</td>
    <td>'.$emp_history_previous_date.'</td>
  </tr>
  <tr>
    <th>Name of supervisor</th>
    <th>Monthly pay</th>
    <th></th>
  </tr>
  <tr>
    <td>'.$emp_history_previous_supervisor.'</td>
    <td>'.$emp_history_previous_pay.'</td>
    <td></td>
  </tr> 
  </table>
  <div style="border: 1px solid black;padding: 0;background: #337ab7;border-radius: 0;text-align: center;font-size: 19px;color: white;">Credit history</div>
  <table style="border: 1px solid black;border-collapse: collapse;width:100%;background-color: lemonchiffon;">
  <tr>
    <th>Bank/Institution</th>
    <th>Balance on Deposit</th>
    <th></th>
  </tr>
  <tr>
    <td>'.$credit_history_bank.'</td>
    <td>'.$credit_history_balance.'</td>
    <td></td>
  </tr>
  <tr>
    <th>Checking Account</th>
    <th>Saving Account</th>
    <th>Credit Card</th>
  </tr>
  <tr>
    <td>'.$credit_history_checking_account.'</td>
    <td>'.$credit_history_saving_account.'</td>
    <td>'.$credit_history_credit_card.'</td>
  </tr>
  <tr>
    <th>Auto loan</th>
    <th>Additional debt</th>
    <th></th>
  </tr>
  <tr>
    <td>'.$credit_history_auto_loan.'</td>
    <td>'.$credit_history_additional_debt.'</td>
    <td></td>
  </tr> 
  </table>
  <div style="border: 1px solid black;padding: 0;background: #337ab7;border-radius: 0;text-align: center;font-size: 19px;color: white;">Refrence</div>
  <table>
<tr>
    <th style="border: 1px solid goldenrod;border-collapse: collapse;background-color: goldenrod;color: white;">Name</th>
    <th style="border: 1px solid goldenrod;border-collapse: collapse;background-color: goldenrod;color: white;">Phone Number</th>
    <th style="border: 1px solid goldenrod;border-collapse: collapse;background-color: goldenrod;color: white;">Relationship</th>
  </tr>

  <tr style="">
    <td style="border: 1px solid goldenrod;border-collapse: collapse;text-align:center;">'.$refrence_name.'</td>
    <td style="border: 1px solid goldenrod;border-collapse: collapse;">'.$refrence_phone.'</td>
    <td style="border: 1px solid goldenrod;border-collapse: collapse;text-align:center;">'.$refrence_relationship.'</td>
  </tr>
  <tr style="">
    <td style="border: 1px solid goldenrod;border-collapse: collapse;text-align:center;">'.$refrence_name1.'</td>
    <td style="border: 1px solid goldenrod;border-collapse: collapse;">'.$refrence_phone1.'</td>
    <td style="border: 1px solid goldenrod;border-collapse: collapse;text-align:center;">'.$refrence_relationship1.'</td>
  </tr>

  </table>
<div style="border: 1px solid black;padding: 0;background: #337ab7;border-radius: 0;text-align: center;font-size: 19px;color: white;">General Information</div>
  <table> 
  <tr>
    <td>Have you ever been late or delinquent on rent?</td>
    <td>'.$_POST['general_info_delinquent_rent'].'</td>
    
  </tr>
  <tr>
    <td>Have you ever been party to a lawsuit?</td>
    <td>'.$_POST['general_info_lawsuit'].'</td>
    
  </tr>
  <tr>
    <td>Do you smoke?</td>
    <td>'.$_POST['general_info_smoke'].'</td>
   
  </tr>
  <tr>
    <td>Do you have any pets?</td>
    <td>'.$_POST['general_info_pets'].'</td>
    
  </tr>
</table>
<div style="font-weight: bold;">If yes,list type, breed, weight, and age.</div>'.$_POST['general_info_breed'].'
<div style="font-weight: bold;">If yes to any of the above,Please explain why.</div>'.$_POST['general_info_explain'].'<br/>'.$_POST['general_info_explain1'].'<br/>'.$_POST['general_info_explain2'].'
<div style="font-weight: bold;">Why are you moving from your current address?</div>'.$_POST['general_info_current_address'].'<br/>'.$_POST['general_info_current_address2'].'
<div style="font-weight: bold;">Is there anything negative in your credit or background check you want to comment on ?</div>'.$_POST['general_info_negative_credit'].'<br/>'.$_POST['general_info_negative_credit2'].'
<div class="row">          
    <div class="col-sm-6 column" style="font-weight: bold;width: 50%">Rental aplication fee:</div>'.$_POST['aplication_fee'].'<br/>
    <div class="col-sm-6 column" style="font-weight: bold;width: 50%">Paid</div>'.((!empty($_POST['general_info_rental_fee']))  ? "Yes" : "No").'           
</div>
<div style="font-weight: bold;">Additional questions:</div>'.$_POST['additional_question'].'<br/>'.$_POST['additional_question2'].'<br/>'.$_POST['additional_question3'].'
<div style="font-weight: bold;">Agreement & Authorization:</div>
<span>By signing this application, I verify that the statements in this application are true and correct.I authorize the use of the information and contacts provided to complete a credit, refrence, and/or background check.I understand that false or lack of information my result in the rejection of this application.</span>
<div class="row">          
    <div class="col-sm-6 column" style="font-weight: bold;width: 50%">Signature of applicant: '.$applicant_signature.'</div>
    <div class="col-sm-6 column" style="font-weight: bold;width: 50%">Date: '.$applicant_date.'</div>            
</div>';
                //$message1 .= '<a href="http://towercleaningsystems.mobilytedev.com/register-user?id='.$user_id.'">Approve User</a>';
                $message1 .= '</body></html>';

         
                wp_mail($to, $subject, $message1, $headers);
                //wp_redirect( admin_url('?page=all-service-requests') );
                wp_redirect( site_url('payment-details/?form-id='.$lastid) );
                //echo '<script type="text/javascript">window.location = "http://towercleaningsystems.mobilytedev.com/payment-details/?id="'.$_REQUEST["id"].';</script>';
                //echo '<script type="text/javascript">swal("Success!", "Your Application has been Recieved Successfully", "success");</script>';


/*$applicant_info_array = array('app_name' => $_POST['app_name'],
        'app_middle_name' => $_POST['app_middle_name'],
        'app_last_name' => $_POST['app_last_name'], 
        'app_dob'=> $_POST['app_dob'],
        'app_social_security' => $_POST['app_social_security'],
        'app_email' => $_POST['app_email'], 
        'app_home_phone' => $_POST['app_home_phone'], 
        'app_cell_phone' => $_POST['app_cell_phone'], 
        'app_driver_licence' => $_POST['app_driver_licence'], 
        'app_other_occupants' => $_POST['app_other_occupants'],
        'app_occupant_dob'=> $_POST['app_occupant_dob'],
        'app_relationship'=> $_POST['app_relationship'],
        'app_occupant_detail'=> $_POST['app_occupant_detail'],
        'app_occupant_detail1'=> $_POST['app_occupant_detail1']
         );
  //$applicant_out = array_values($applicant_info_array);
  $app_data = json_encode($applicant_info_array);
  echo "<pre>"; print_r($app_data);

  $rental_info_array = array('rental_current_address' => $_POST['rental_current_address'],
        'rental_current_city' => $_POST['rental_current_city'],
        'rental_current_state' => $_POST['rental_current_state'], 
        'rental_current_zip'=> $_POST['rental_current_zip'],
        'rental_current_monthly_rent' => $_POST['rental_current_monthly_rent'],
        'rental_current_residency_date' => $_POST['rental_current_residency_date'], 
        'rental_current_reason_moving' => $_POST['rental_current_reason_moving'], 
        'rental_current_owner_name' => $_POST['rental_current_owner_name'], 
        'rental_current_phone' => $_POST['rental_current_phone'], 
        'rental_previous_address' => $_POST['rental_previous_address'],
        'rental_previous_city'=> $_POST['rental_previous_city'],
        'rental_previous_state'=> $_POST['rental_previous_state'],
        'rental_previous_zip'=> $_POST['rental_previous_zip'],
        'rental_previous_monthly_rent'=> $_POST['rental_previous_monthly_rent'],
        'rental_previous_date_residency'=> $_POST['rental_previous_date_residency'],
        'rental_previous_reason_moving'=> $_POST['rental_previous_reason_moving'],
        'rental_previous_owner_name'=> $_POST['rental_previous_owner_name'],
        'rental_previous_phone'=> $_POST['rental_previous_phone']
         );
  //$rental_out = array_values($rental_info_array);
  $rental_data = json_encode($rental_info_array);
  echo "<pre>"; print_r($app_data); die();



  $rental_id = "INSERT INTO `as_service_request`( `applicant_info_json`,`rental_history_json`) VALUES ('$app_data','$rental_data')";
    $wpdb->query($rental_id);*/

  //echo "<pre>"; print_r($app_data); die;
//$table_name = $wpdb->prefix."service_request";
  //$insert_id = $wpdb->insert($table_name, $app_data );
    /*$insert_id = "INSERT INTO `as_service_request`( `user_id`, `first_name`, `last_name`, `email`, `phone_number`, `address`, `unit`, `user_type`, `description`, `services`, `status` , `city`) VALUES ('$user_id','$first_name','$last_name','$email','$phone','$address','$unit','$user_type','$description','avaliable','pending' , '$city')";
    $wpdb->query($insert_id);*/

//die('shaan');

}


?>


<?php  
/*echo "here";
if(isset($_POST["rental_submit"])) {

echo "here123"; die
die('here');
}*/
  ?>

<div class="page-wrapper">
<div class="hmgal-heading">
<div class="container">
<div class="section-header">
	 <h1 class="section-title text-center wow fadeInDown animated animated animated" style="visibility: visible; animation-name: fadeInDown;">Rental Application</h1>     			</div>
</div>
</div>
<div class="container">

    
<form name="rental_apps" method="post" class="rental_info" id="rental_info" >
  <input type="hidden" name="gallery_post_id" id="gallery_post_id" value="<?php echo $gallery_post_id; ?>">  
<div class="row homerent-wrapper">
<h3>Applicant Information</h3>
<div class="formborder">
<div class="col-xs-6 col-sm-6 col-sm-3">
<label>Name:First</label>
<input class="first" name="app_name" id="app_name" type="text" required/>
</div>  
<div class="col-xs-6 col-sm-3">
<label>Middle</label>
<input type="text" name="app_middle_name" id="app_middle_name" />
</div>
<div class="col-xs-6 col-sm-2">
<label>Last</label>
<input type="text" name="app_last_name" id="app_last_name" required/>
</div>
<div class="col-xs-6 col-sm-2">
<label>Birth Date</label>
<input type="text" name="app_dob" id="app_dob" required/>
</div>
<div class="col-xs-6 col-sm-2">
<label>Social Security</label>
<input class="last" type="text" name="app_social_security" id="app_social_security" />
</div>
</div>  
<!--row text bar -->
<div class="formborder">
<div class="col-xs-6 col-sm-4">
<label>Email Address:</label>
<input class="first" type="text" name="app_email" id="app_email" required/>
</div>

<div class="col-xs-6 col-sm-2">
<label>Home Phone</label>
<input type="text" name="app_home_phone" id="app_home_phone" required/>
</div>
<div class="col-xs-6 col-sm-3">
<label>Cell Phone</label>
<input type="text" name="app_cell_phone" id="app_cell_phone" />
</div>
<div class="col-xs-6 col-sm-3">
<label>Driver's Licence# </label>
<input class="last" type="text" name="app_driver_licence" id="app_driver_licence" />
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-xs-6 col-sm-5">
<label>All Other occupants (under 18):</label>
<input class="first" type="text" name="app_other_occupants" id="app_other_occupants" />
</div>

<div class="col-xs-6 col-sm-3">
<label>Birth Date</label>
<input type="text" name="app_occupant_dob" id="app_occupant_dob" />
</div>

<div class="col-xs-6 col-sm-4">
<label>Relationship to Applicant</label>
<input class="last" type="text" name="app_relationship" id="app_relationship" />
</div>
</div>
<!--row text bar -->
<div class="formborder">

<div class="col-xs-6 col-sm-5">
<input class="first" type="text" name="app_other_occupant1" id="app_other_occupant1" />
</div>

<div class="col-xs-6 col-sm-3">
<input type="text" name="app_occupant_dob1" id="app_occupant_dob1" />
</div>

<div class="col-xs-6 col-sm-4">
<input class="last" type="text" name="app_relationship1" id="app_relationship1" />
</div>
<!-- <div class="col-sm-12">
<input class="first-last" type="text" name="app_occupant_detail" id="app_occupant_detail" />
</div> -->
</div>
<!--row text bar -->
<div class="formborder">

<div class="col-xs-6 col-sm-5">
<input class="first" type="text" name="app_other_occupant2" id="app_other_occupant2" />
</div>

<div class="col-xs-6 col-sm-3">
<input type="text" name="app_occupant_dob2" id="app_occupant_dob2" />
</div>

<div class="col-xs-6 col-sm-4">
<input class="last" type="text" name="app_relationship2" id="app_relationship2" />
</div>
<!-- <div class="col-sm-12">
<input class="first-last" type="text" name="app_occupant_detail1" id="app_occupant_detail1" />
</div> -->
</div>
<!--row text bar -->
<h3>Rental History</h3>
<h4>Current residence</h4>
<div class="formborder">
<div class="col-xs-6 col-sm-4">
<label>Address</label>
<input class="first" type="text" name="rental_current_address" id="rental_current_address" required/>
</div>  
<div class="col-xs-6 col-sm-2">
<label>City</label>
<input type="text" name="rental_current_city" id="rental_current_city" required/>
</div>
<div class="col-xs-6 col-sm-3">
<label>State</label>
<input type="text" name="rental_current_state" id="rental_current_state" required/>
</div>

<div class="col-xs-6 col-sm-3">
<label>Zip</label>
<input class="last" type="text" name="rental_current_zip" id="rental_current_zip" required/>
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-xs-6 col-sm-5">
<label>Monthly Rent</label>
<input class="first" type="text" name="rental_current_monthly_rent" id="rental_current_monthly_rent" required>
</div>

<div class="col-xs-6 col-sm-3">
<label>Date of Residency</label>
<input type="text" name="rental_current_residency_date" id="rental_current_residency_date">
</div>

<div class="col-xs-6 col-sm-4">
<label>Reason for moving</label>
<input class="last" type="text" name="rental_current_reason_moving" id="rental_current_reason_moving" required>
</div>
</div>
  
  <!--row text bar -->
<div class="formborder">
<div class="col-xs-6 col-sm-6">
<label>Owner/Manager name</label>
<input class="first" type="text" name="rental_current_owner_name" id="rental_current_owner_name" required>
</div>

<div class="col-xs-6 col-sm-6">
<label>Phone Number</label>
<input class="last" type="text" name="rental_current_phone" id="rental_current_phone">
</div>
</div>
<!--row text bar -->

<h4>Pervious residence</h4>
<div class="formborder">
<div class="col-xs-6 col-sm-4">
<label>Address</label>
<input class="first" type="text" name="rental_previous_address" id="rental_previous_address" />
</div>  
<div class="col-xs-6 col-sm-2">
<label>City</label>
<input type="text" name="rental_previous_city" id="rental_previous_city" />
</div>
<div class="col-xs-6 col-sm-3">
<label>State</label>
<input type="text" name="rental_previous_state" id="rental_previous_state" />
</div>

<div class="col-xs-6 col-sm-3">
<label>Zip</label>
<input class="last"  type="text" name="rental_previous_zip" id="rental_previous_zip" />
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-xs-6 col-sm-5">
<label>Monthly Rent</label>
<input class="first" type="text" name="rental_previous_monthly_rent" id="rental_previous_monthly_rent">
</div>

<div class="col-xs-6 col-sm-3">
<label>Date of Residency</label>
<input type="text" name="rental_previous_date_residency" id="rental_previous_date_residency" />
</div>

<div class="col-xs-6 col-sm-4">
<label>Reason for moving</label>
<input class="last" type="text" name="rental_previous_reason_moving" id="rental_previous_reason_moving" >
</div>
</div>
  
  <!--row text bar -->
<div class="formborder">
<div class="col-xs-6 col-sm-6">
<label>Owner/Manager name</label>
<input class="first" type="text" name="rental_previous_owner_name" id="rental_previous_owner_name">
</div>

<div class="col-xs-6 col-sm-6">
<label>Phone Number</label>
<input class="last" type="text" name="rental_previous_phone" id="rental_previous_phone">
</div>
</div>
<!--row text bar -->

<h3>Employment History</h3>
<h3>Current employer</h3>
<div class="formborder">
<div class="col-xs-6 col-sm-6">
<label>Current employer</label>
<input class="first" type="text" name="emp_history_current_employer" id="emp_history_current_employer" />
</div>  
<div class="col-xs-6 col-sm-6">
<label>Occuption</label>
<input class="last"  type="text" name="emp_history_current_occupation" id="emp_history_current_occupation" />
</div>
</div>

<!--row text bar -->

<div class="formborder">
<div class="col-xs-6 col-sm-6">
<label>Employer address</label>
<input class="first" type="text" name="emp_history_current_address" id="emp_history_current_address" />
</div>  
<div class="col-xs-6 col-sm-3">
<label>Employer's phone</label>
<input class="" type="text" name="emp_history_current_phone" id="emp_history_current_phone" />
</div>
<div class="col-xs-6 col-sm-3">
<label>Date of employment</label>
<input class="last" type="text" name="emp_history_current_date" id="emp_history_current_date" />
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-xs-6 col-sm-6">
<label>Name of supervisor</label>
<input class="first" type="text" name="emp_history_current_supervisor" id="emp_history_current_supervisor" />
</div>  
<div class="col-xs-6 col-sm-6">
<label>Monthly pay</label>
<input class="last"  type="text" name="emp_history_current_pay" id="emp_history_current_pay" />
</div>
</div>
<!--row text bar -->


<h3>Previous employer</h3>
<div class="formborder">
<div class="col-xs-6 col-sm-6">
<label>Current employer</label>
<input class="first" type="text" name="emp_history_previous_employer" id="emp_history_previous_employer" />
</div>  
<div class="col-xs-6 col-sm-6">
<label>Occuption</label>
<input class="last"  type="text" name="emp_history_previous_occupation" id="emp_history_previous_occupation" />
</div>
</div>

<!--row text bar -->

<div class="formborder">
<div class="col-xs-6 col-sm-6">
<label>Employer address</label>
<input class="first" type="text" name="emp_history_previous_address" id="emp_history_previous_address" />
</div>  
<div class="col-xs-6 col-sm-3">
<label>Employer's phone</label>
<input class="" type="text" name="emp_history_previous_phone" id="emp_history_previous_phone" />
</div>
<div class="col-xs-6 col-sm-3">
<label>Date of employment</label>
<input class="last"  type="text" name="emp_history_previous_date" id="emp_history_previous_date" />
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-xs-6 col-sm-6">
<label>Name of supervisor</label>
<input class="first" type="text" name="emp_history_previous_supervisor" id="emp_history_previous_supervisor" />
</div>  
<div class="col-xs-6 col-sm-6">
<label>Monthly pay</label>
<input class="last"  type="text" name="emp_history_previous_pay" id="emp_history_previous_pay" />
</div>
</div>
<h3>Credit history</h3>
<!--row text bar -->
<div class="col-md-12">
<div class="row">
<div class="col-md-3">
<span>Cheking account</span>
</div>
<div class="col-md-9">
<div class="row">
<div class="formborder">
<div class="col-xs-6 col-sm-6">
<label>Bank/Institution</label>
<input class="first" type="text" name="credit_history_bank" id="credit_history_bank" />
</div>  
<div class="col-xs-6 col-sm-6">
<label>Balance on deposit or Balance owned </label>
<input class="last"  type="text" name="credit_history_balance" id="credit_history_balance" />
</div>
</div>
</div></div>
</div></div>

<!--row text bar -->
<div class="col-md-12">
<div class="row">
<div class="col-sm-3">
<span>Cheking account</span>
</div>
<div class="col-sm-9">
<div class="row">
<div class="formborder">
<div class="col-xs-12 col-sm-12">
<input class="first-last"  type="text" name="credit_history_account" id="credit_history_checking_account" />
</div>
</div>
</div></div>
</div></div>

<!--row text bar -->
<div class="col-md-12">
<div class="row">
<div class="col-sm-3">
<span>Saving account</span>
</div>
<div class="col-sm-9">
<div class="row">
<div class="formborder">
<div class="col-xs-12 col-sm-12">
<input class="first-last" type="text" name="credit_history_saving_account" id="credit_history_saving_account" />
</div>
</div>
</div></div>
</div></div>
<!--row text bar -->
<div class="col-md-12">
<div class="row">
<div class="col-sm-3">
<span>Credit card</span>
</div>
<div class="col-sm-9">
<div class="row">
<div class="formborder">
<div class="col-xs-12 col-sm-12">
<input class="first-last" type="text" name="credit_history_credit_card" id="credit_history_credit_card" />
</div>
</div>
</div></div>
</div></div>
<!--row text bar -->
<div class="col-md-12">
<div class="row">
<div class="col-sm-3">
<span>Auto loan</span>
</div>
<div class="col-sm-9">
<div class="row">
<div class="formborder">
<div class="col-xs-12 col-sm-12">
<input class="first-last" type="text" name="credit_history_auto_loan" id="credit_history_auto_loan" />
</div>
</div>
</div></div>
</div></div>
<!--row text bar -->
<div class="col-md-12">
<div class="row">
<div class="col-sm-3">
<span>Additional debt.</span>
</div>
<div class="col-sm-9">
<div class="row">
<div class="formborder">
<div class="col-xs-12 col-sm-12">
<input class="first-last" type="text" name="credit_history_additional_debt" id="credit_history_additional_debt" />
</div>
</div>
</div></div>
</div></div>
<!--row text bar -->

<div class="formborder">
<h3>Refrence </h3>
<div class="col-xs-6 col-sm-6">
<label> Name</label>
<input class="first" type="text" name="refrence_name" id="refrence_name" >
</div>  
<div class="col-xs-6 col-sm-3">
<label>Phone Number</label>
<input class="" type="text" name="refrence_phone" id="refrence_phone" >
</div>
<div class="col-xs-6 col-sm-3">
<label>Relationship</label>
<input class="last" type="text" name="refrence_relationship" id="refrence_relationship">
</div>
</div>
<!--row text bar -->
<div class="formborder">
  <div class="col-xs-6 col-sm-6">

<input class="first" type="text" name="refrence_name1" id="refrence_name1" required>
</div>  
<div class="col-xs-6 col-sm-3">

<input class="" type="text" name="refrence_phone1" id="refrence_phone1" required>
</div>
<div class="col-xs-6 col-sm-3">

<input class="last" type="text" name="refrence_relationship1" id="refrence_relationship1" required>
</div>
<!-- <div class="col-sm-12">
<input class="first-last" type="text">
</div> -->
</div>


<!--row text bar -->

<div class="formborder checkboxborder">
<h3>General Information </h3>
<div class="col-xs-6 col-sm-6">
<label> Have you ever been late or delinquent on rent?</label>

</div>  
<div class="col-xs-6 col-sm-3">
 <div class="chiller_cb">
    <input id="myCheckbox" type="radio" name="general_info_delinquent_rent" id="general_info_delinquent_rent" value="yes">
    <label for="myCheckbox">Yes</label>
    <span></span>
  </div>
</div>
<div class="col-xs-6 col-sm-3">
 <div class="chiller_cb">
    <input id="myCheckbox1" type="radio" name="general_info_delinquent_rent" id="general_info_delinquent_rent" value="no">
    <label for="myCheckbox1">No</label>
    <span></span>
  </div>
</div>
</div>
<!--row text bar -->

<div class="formborder checkboxborder">

<div class="col-xs-6 col-sm-6">
<label> Have you ever been party to a lawsuit?</label>

</div>  
<div class="col-xs-6 col-sm-3">
 <div class="chiller_cb">
    <input id="myCheckbox2" type="radio" name="general_info_lawsuit" id="general_info_lawsuit" value="yes">
    <label for="myCheckbox2">Yes</label>
    <span></span>
  </div>
</div>
<div class="col-xs-6 col-sm-3">
 <div class="chiller_cb">
    <input id="myCheckbox3" type="radio" name="general_info_lawsuit" id="general_info_lawsuit" value="no" >
    <label for="myCheckbox3">No</label>
    <span></span>
  </div>
</div>
</div>
<!--row text bar -->

<div class="formborder checkboxborder">

<div class="col-xs-6 col-sm-6">
<label> Do you smoke?</label>

</div>  
<div class="col-xs-6 col-sm-3">
 <div class="chiller_cb">
    <input id="myCheckbox4" type="radio" name="general_info_smoke" id="general_info_smoke" value="yes" >
    <label for="myCheckbox4">Yes</label>
    <span></span>
  </div>
</div>
<div class="col-xs-6 col-sm-3">
 <div class="chiller_cb">
    <input id="myCheckbox5" type="radio" name="general_info_smoke" id="general_info_smoke" value="no" >
    <label for="myCheckbox5">No</label>
    <span></span>
  </div>
</div>
</div>
<!--row text bar -->

<div class="formborder checkboxborder">

<div class="col-xs-6 col-sm-6">
<label> Do you have any pets?</label>

</div>  
<div class="col-xs-6 col-sm-3">
 <div class="chiller_cb">
    <input id="myCheckbox6" type="radio" name="general_info_pets" id="general_info_pets" value="yes" >
    <label for="myCheckbox6">Yes</label>
    <span></span>
  </div>
</div>
<div class="col-xs-6 col-sm-3">
 <div class="chiller_cb">
    <input id="myCheckbox7" type="radio" name="general_info_pets" id="general_info_pets" value="no" >
    <label for="myCheckbox7">No</label>
    <span></span>
  </div>
</div>
</div>
<!--row text bar -->
<div class="col-md-12">
<div class="row">
<div class="col-md-4">
<span>If yes,list type, breed, weight, and age.</span>
</div>
<div class="col-md-8">
<div class="row">
<div class="formborder">
<div class="col-xs-12 col-sm-12">
<input class="first-last" type="text" name="general_info_breed" id="general_info_breed">
</div>
</div>
</div></div>
</div></div>

<!--row text bar -->
<div class="formborder">
<div class="col-sm-12">
<label>If yes to any of the above,Please explain why.</label>
<input class="first-last" type="text" name="general_info_explain" id="general_info_explain" >
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-sm-12">
<input class="first-last" type="text" name="general_info_explain1" id="general_info_explain1">
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-sm-12">
<input class="first-last" type="text" name="general_info_explain2" id="general_info_explain2">
</div>
</div>


<!--row text bar -->
<div class="formborder">
<div class="col-sm-12">
<label>Why are you moving from your current address?</label>
<input class="first-last" type="text" name="general_info_current_address" id="general_info_current_address" required>
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-sm-12">
<input class="first-last" type="text" name="general_info_current_address2" id="general_info_current_address2" >
</div>
</div>

<!--row text bar -->
<div class="formborder">
<div class="col-sm-12">
<label>Is there anything negative in your credit or background check you want to comment on ?</label>
<input class="first-last" type="text" name="general_info_negative_credit" id="general_info_negative_credit">
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-sm-12">
<input class="first-last" type="text" name="general_info_negative_credit2" id="general_info_negative_credit2" >
</div>
</div>
<!--row text bar -->
<div class="formborder checkboxborder">
<div class="col-xs-12 col-sm-6">
<label>Rental aplication fee:$<input class="first-last" type="text" name="aplication_fee" id="aplication_fee" style="width: 55%;
    margin-left: 1%;border-bottom: 1px solid #ccc;" required></label>

</div>  
<div class="col-xs-12 col-sm-6">
 <div class="chiller_cb">
    <input id="myCheckbox16" type="checkbox" name="general_info_rental_fee" id="general_info_rental_fee" value="yes">
    <label for="myCheckbox16">Paid</label>
    <span></span>
  </div>
</div>
</div>

<!--row text bar -->
<h3>Additional questions:</h3>
<div class="formborder">
<div class="col-sm-12">
<input class="first-last" type="text" name="additional_question" id="additional_question" >
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-sm-12">
<input class="first-last" type="text" name="additional_question2" id="additional_question2">
</div>
</div>
<!--row text bar -->
<div class="formborder">
<div class="col-sm-12">
<input class="first-last" type="text" name="additional_question3" id="additional_question3">
</div>
</div>
<!--row text bar -->
<h3>Agreement & Authorization</h3>
<p>By signing this application, I verify that the statements in this application are true and correct.I authorize the use of the information and contacts
provided to complete a credit, refrence, and/or background check.I understand that false or lack of information my result in the rejection of this application.</p>
<!--row text bar -->
<div class="formborder">
<div class="col-sm-6">
<label>Signature of applicant:</label>
<input class="first-last" type="text" name="applicant_signature" id="applicant_signature" >
</div>
<div class="col-sm-6">
<label>Date:</label>
<input class="first-last" type="date" name="applicant_date" id="applicant_date" value="<?php echo date("Y-m-d"); ?>" min="<?php echo date("Y-m-d"); ?>" required>
</div>
</div>





<input type="hidden" name="action" value="rental_apps_info">
<input type="hidden" name="service_request_update" value="resident">

<!-- <button id="submit" type="submit" value="submit" class="btn btn-primary center" name="rental_submit">Submit</button> -->
<button id="submit" type="submit" value="submit" class="btn btn-primary center" name="rental_submit">Submit</button>



</div>  
</form>   

</div>
</div>


<?php get_footer(); ?>