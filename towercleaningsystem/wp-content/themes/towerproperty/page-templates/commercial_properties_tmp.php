<?php  
// Template Name: Commercial Properties Page
get_header();
?>
<div class="page-wrapper">
<div class="hmgal-heading">
<div class="container">
<div class="section-header">
	  <h1 class="section-title text-center wow fadeInDown animated animated animated" style="visibility: visible; animation-name: fadeInDown;"><?php the_field('commercial_properties_title'); ?></h1>
	</div>
</div>
</div>

<div class="container">
<h1 class="includhead">Included Services</h1> 

<ul class="inservice">
	<?php if( have_rows('commercial_properties_services') ):
		  while ( have_rows('commercial_properties_services') ) : the_row(); ?>
	<li><p><?php the_sub_field('commercial_properties_all_services'); ?> </p></li>
	<?php   endwhile;
			else :
			endif; ?>
	<!-- <li><p>Daily janitorial/ commercial office cleaning </p></li>
	<li><p>Recycling programs.</p></li>
	<li> <p>Capture and remove, dust and allergen elimination techniques.</p></li>
	<li><p>Residue free carpet deep cleaning.</p></li>
	<li> <p>Hard floor, ceramic tile and stone grout deep cleaning and whitening.</p></li>
	<li><p>VCT vinyl composite tile stripping sealing and deep scrub refinishing.</p></li>
	<li><p>Window cleaning </p></li>
	<li><p>Pressure washing </p></li>
	<li><p>Production area clean room cleaning </p></li>
	<li><p>Warehouse concrete, auto scrubbing, diamond polishing, densifying and sealing </p></li>
	<li><p>Landscaping </p></li>
	<li><p>Plumbing </p></li>
	<li><p>Electrical </p></li>
	<li><p>HVAC Repair </p></li>
	<li><p>Duct Cleaning </p></li>
	<li><p>Renovations </p></li>
	<li><p>Junk Removal and Recovery </p></li> -->

</ul>



</div>
</div>

<?php get_footer(); ?>