<?php 
// Template Name: Work Order Form
get_header(); ?>

<style type="text/css">
	.error {
    	color: #e78282 !important;
	}
</style>

<?php 
 $user_id = get_current_user_id();
// $all_meta_for_user = get_user_meta( $user_id );
// $user_role = $all_meta_for_user['as_capabilities'][0];
?>
<?php 
$user = wp_get_current_user();
$role = ( array ) $user->roles;
// echo "<pre>";
// print_r($role[0]); die();
$user_info = get_userdata($user_id);
$user_email = $user_info->user_email;
/*echo "<pre>";
print_r($user_email); die();*/
$user_detail =  get_user_meta($user_id);

$user_firstname = $user_detail['first_name'][0];
$user_last_name = $user_detail['last_name'][0];
$user_description = $user_detail['description'][0];
$user_phone_number = $user_detail['phone_number'][0];
//$user_firstname = $user_detail['phone_number'][0];
/*echo "<pre>";
print_r($user_firstname);
print_r($last_name); 
print_r($phone_number); 
print_r($description); die();
*/
//print_r($user_detail); die();
?>
<div class="form-wrapper">
<div class="container register-form">
	 <div class="row">
	    <div class="col-md-6 col-sm-12 col-lg-6 col-md-offset-3 alloforms">
			<div class="panel panel-primaryd">
				<div class="panel-heading">
					<!-- <h2>Work Order Form</h2> -->
					<!-- Work Order Form For Owner -->
					<h2>Service Request</h2>
				</div>
				<div class="panel-body">
	               <?php if($role[0] == 'owner'){ ?>				
					
					<form name="myform" method="post" class="service_request_owner" id="service_request_owner">
						<input id="user_type" name="user_type" class="form-control" type="hidden" value="<?php echo $role[0]; ?>">
						<input id="user_id" name="user_id" class="form-control" type="hidden" value="<?php echo $user_id; ?>">
						<div class="form-group">
							<label for="myName">First Name</label>
							<input id="myName" name="myName" class="form-control" type="text" data-validation="required" value="<?= ( !empty($user_firstname) ? $user_firstname : ''); ?>">
							<span id="error_name" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="lastname">Last Name</label>
							<input id="lastname" name="lastname" class="form-control" type="text" data-validation="lastname" value="<?= ( !empty($user_last_name) ? $user_last_name : ''); ?>">
							<span id="error_lastname" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="email">Email</label>
					<input id="email" name="email" class="form-control" type="email" data-validation="email" value="<?= ( !empty($user_email) ? $user_email : ''); ?>">
							<span id="error_email" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="phone">Phone Number</label>
							<input type="phone" id="phone" name="phone" class="form-control" value="<?= ( !empty($user_phone_number) ? $user_phone_number : ''); ?>">
							<span id="error_phone" class="text-danger"></span>
						</div>
							<div class="form-group">
							<label for="address">Address</label>
							<textarea class="form-control" rows="3" name="address"></textarea>
						</div>
							<div class="form-group">
							<label for="disc">Description Of Problem</label>
							<textarea class="form-control" rows="3" name="description"></textarea>
						</div>
						
						<input type="hidden" name="action" value="service_request_ajax_handlers">
						<input type="hidden" name="service_request" value="owner">
						<button id="submit" type="submit" value="submit" class="btn btn-primary center" name="btn_register">Submit</button>
				
					</form>
	                   <?php }
	                   else {
	                    ?>
	                    <!-- Work Order Form For Resident -->
	                       <form name="resident_myform" method="post" class="service_request" id="service_request_resident">
	                       	<input id="user_type" name="user_type" class="form-control" type="hidden" value="<?php echo $role[0]; ?>">
	                       	<input id="user_id" name="user_id" class="form-control" type="hidden" value="<?php echo $user_id; ?>">
						<div class="form-group">
							<label for="resident_myName">First Name</label>
							<input id="resident_myName" name="resident_myName" class="form-control" type="text" data-validation="required" value="<?= ( !empty($user_firstname) ? $user_firstname : ''); ?>">
							<span id="error_name" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="resident_lastname">Last Name</label>
							<input id="resident_lastname" name="resident_lastname" class="form-control" type="text" data-validation="email" value="<?= ( !empty($user_last_name) ? $user_last_name : ''); ?>">
							<span id="error_lastname" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="resident_email">Email</label>
							<input id="resident_email" name="resident_email" class="form-control" type="email" data-validation="email" value="<?= ( !empty($user_email) ? $user_email : ''); ?>">
							<span id="error_email" class="text-danger"></span>
						</div>
						
						<div class="form-group">
							<label for="resident_phone">Phone Number</label>
							<input type="text" id="resident_phone" name="resident_phone" class="form-control" value="<?= ( !empty($user_phone_number) ? $user_phone_number : ''); ?>" >
							<span id="error_phone" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="resident_address">Address</label>
							<textarea class="form-control" rows="3" name="resident_address" id="resident_address"></textarea>
						</div>
						<div class="form-group">
							<label for="phone">Your Unit</label>
							<input type="text" id="resident_unit" name="resident_unit" class="form-control" >
							<span id="error_phone" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="resident_city">Your City</label>
							<input type="text" id="resident_city" name="resident_city" class="form-control" >
							<span id="error_phone" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label for="disc">Discription Of Problem</label>
							<textarea class="form-control" rows="3" name="resident_description" id="resident_description"></textarea>
						</div>

						<input type="hidden" name="action" value="service_request_ajax_handler">
						<input type="hidden" name="service_request" value="resident">
						<button id="submit" type="submit" value="submit" class="btn btn-primary center" name="resident_submit">Submit</button>
				
					</form>


	                 <?php } ?>
				
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php get_footer(); ?>