<?php  
// Template Name: Projects Page
get_header();
?>

<style>
.add{background: #333; padding: 10%; height: 300px;}

.nav-sidebar { 
    width: 100%;
    padding: 8px 0; 
    border-right: 1px solid #ddd;
}
.nav-sidebar a {
    color: #333;
    -webkit-transition: all 0.08s linear;
    -moz-transition: all 0.08s linear;
    -o-transition: all 0.08s linear;
    transition: all 0.08s linear;
}
.nav-sidebar .active a { 
    cursor: default;
    background-color: #34ca78; 
    color: #fff; 
}
.nav-sidebar .active a:hover {
    background-color: #37D980;   
}
.nav-sidebar .text-overflow a,
.nav-sidebar .text-overflow .media-body {
    white-space: nowrap;
    overflow: hidden;
    -o-text-overflow: ellipsis;
    text-overflow: ellipsis; 
}

.btn-blog {
    color: #ffffff;
    background: #035d78;
    border-color: #035d78;
    border-radius:0;
    margin-bottom:10px
    padding: 12px 45px;
    font-size: 13px;
}
.btn-blog:hover,
.btn-blog:focus,
.btn-blog:active,
.btn-blog.active,
.open .dropdown-toggle.btn-blog {
    color: white;
    background-color:#035d78;
    border-color: #035d78;
}
 h2{color:#035d78;}
 .margin10{margin-bottom:10px; margin-right:10px;}

</style>

<div class="page-wrapper service-wrapper">
<div class="hmgal-heading">
<div class="container">
<div class="section-header">
	  <h1 class="section-title text-center wow fadeInDown animated animated animated animated" style="visibility: visible; animation-name: fadeInDown;"><?php the_title(); ?> </h1>
	</div>
</div>
</div>
  <div class="container">  
	<div id="blog" >
	 <?php


$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : '1';
$args = array (
    'nopaging'               => false,
    'paged'                  => $paged,
    'posts_per_page'         => '6',
    'post_type'              => 'projects',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {

    

    while ( $query->have_posts() ) {
        $query->the_post();
        
?><div class="row">
<div class="servicelistings">
		<div class="col-xs-12 col-sm-4 paddingTop20">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail(); ?></a>
        </div>  
                 <div class="col-xs-12 col-sm-8 ">
                     <h1><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php  the_title(); ?></a></h1>                     
                                           
                     <article><p>
                         <?php the_excerpt(); ?> 
                         </p></article>
                     <div class="blogShort"><a class="btn btn-blog pull-right marginBottom10" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">READ MORE</a> </div>

                 </div>  
              </div></div>
               <?php 
       
    }
    if ( $query->max_num_pages > 1 ) : // if there's more than one page turn on pagination
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    
?>
<div class="col-md-12 text-center">
	<ul class="pagenavd">
	<?php if($paged>1){ ?><li><?php previous_posts_link( '« Prev' ); ?></li><?php } ?>
    <?php if($paged < $query->max_num_pages){ ?><li><?php next_posts_link( 'Next »', $query->max_num_pages ); ?></li><?php } ?>
    </ul>
</div>	
<?php endif;
} else {
    // no posts found
    echo '<h1 class="page-title screen-reader-text">No Posts Found</h1>';
}

// Restore original Post Data
wp_reset_postdata();
 ?>
    </div>
</div>
</div>

<?php get_footer(); ?>