<?php 
// Template Name: Contact Page Template
get_header();
?>
<div class="form-wrapper contactform-wrapper">
<div class="hmgal-heading">
<div class="container">
<div class="section-header">
			                <h1 class="section-title text-center wow fadeInDown animated animated" style="visibility: visible; animation-name: fadeInDown;"><?php the_field('contact_us_title'); ?></h1>
										           
			               
			              
            			</div>
</div>
</div>



	<div class="container ">
	<div class="row ">
		<?php if(have_rows('contact_section')):
		while(have_rows('contact_section')) : the_row(); ?>
		<div class="col-md-4 contact-top">	
		<img src="<?php the_sub_field('contact_image') ?>">
		<p><?php the_sub_field('contact_type'); ?></p>	
		<span><?php the_sub_field('contact_description'); ?></span>
		</div>

		<?php
			endwhile;
			else:
			endif;
		 ?>
		
		<!-- <div class="col-md-4 contact-top">		
		<img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/emailus.png">
		<p>Email us on</p>
		<span>info@digital.com</span>		
		</div>
		
		<div class="col-md-4 contact-top">	
		<img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/findus.png">
		<p>Find us on</p>
		<span>123 Queen Square,Bristol, UK
		</span>		
		</div> -->

		</div>
		</div>
		
		
		<div class="container ">
		<div class="row contactbottom">
	<div class="col-md-8 col-md-offset-2 alloforms">
		<h2>Get your Quotes</h2>
		<div class="um">
		<div class="um-form contactform">
		<?php echo do_shortcode('[contact-form-7 id="141" title="Contact form 1"]'); ?>
		</div>
	</div>
	
	</div>
	
	</div>
	</div>

	<!-- <iframe src="<?php the_field('contact_map'); ?>" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe> -->
	<?php the_field('contact_map'); ?>
	<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13007073.571715705!2d-104.65411479699895!3d37.25826261177418!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1527271149679" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe> -->
<section  class="asst-logo wow fadeInRight">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown"><?php the_field('contact_associations_title'); ?></h2>                
            </div>
            <div class="carousel">                   
                <ul id="flexiselDemo3">
				<?php if( have_rows('contact_affiliations_images') ):
					 while ( have_rows('contact_affiliations_images') ) : the_row(); ?>
                    <li><img src="<?php the_sub_field('affiliation_images'); ?>" /></li>
                     <?php   endwhile;
						else :
						endif; ?>						

                  <!-- <li><img src="<?php  echo get_template_directory_uri(); ?>/images/client/client-logo-02.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-03.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-04.png" /></li> 
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-05.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-06.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-07.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-08.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-09.png" /></li> --> 

                </ul>    
            </div>
            <div class="clearout"></div>
        </div>
    </section>
<?php get_footer(); ?>