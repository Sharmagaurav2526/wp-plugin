<?php 
// Template Name: Register User Template
get_header();
?>
<?php  
if(isset($_POST["btn_register"])) {
$first_name = trim($_POST['myName']);
$last_name = trim($_POST['lastname']);
$email  = trim($_POST['email']);
$password = trim($_POST['password']);
$user_role = trim($_POST['user_role']);
$phone = trim($_POST['phone']);
   
   if ( email_exists( $email ) ) {
    	$message  = 'Email Already Exists';
	}	
	elseif ( username_exists( $email ) ) {
    	$message  = 'Username Already Exists';
	}
	else{
         $userdata = array(
		    'user_login'  =>  $email,
		    'user_email'    =>  $email,
		    'display_name'    =>  $first_name,
		    'user_pass'   =>  $password,
		    'role'   => $user_role,
		    'first_name' => $first_name,
		    'last_name' => $last_name
		);

		$user_id = wp_insert_user( $userdata ) ;
		//  Send Email to the Register User 
		        $to = $email;
				$subject = 'Approve Registration';
				$from = 'info@towerpropertyservices.com';
				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				// Create email headers
				$headers .= 'From: '.$from."\r\n".
				    'Reply-To: '.$from."\r\n" .
				    'X-Mailer: PHP/' . phpversion();
				// Compose a simple HTML email message
				$message1 = '<html><body>';
				$message1 .= '<h1 style="color:#f40;">Hi '.$first_name.'</h1>';
				$message1 .= '<p style="color:#080;font-size:18px;">Please approve to register Succesfully with .</p>';
				$message1 .= '<p>User Name : '.$email.'</p>';
				$message1 .= '<p>User First Name : '.$first_name.'</p>';
				$message1 .= '<p>Your Email is  : '.$email.'</p>';
				$message1 .= '<a href="http://towercleaningsystems.mobilytedev.com/register-user?id='.$user_id.'">Approve User</a>';
				$message1 .= '</body></html>';
                mail($to, $subject, $message1, $headers);
		//  End Email Section
		  if ( ! is_wp_error( $user_id ) ) {
           $message = "Please verify to register Succesfully.";
           }
           else{
           	$message = "Something went wrong. Please try again!";
           }
  	   }
 }

 ?>
<div class="container register-form">
 <div class="row">
    <div class="col-md-6 col-sm-12 col-lg-6 col-md-offset-3">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h2>Registration Form</h2>
				<?php if(isset($message)) echo $message ?>	
			</div>
			<div class="panel-body">
				<form name="myform" method="post" id="register_form">
					<div class="form-group">
						<label for="myName">First Name</label>
						<input id="myName" name="myName" class="form-control" type="text" data-validation="required">
						<span id="error_name" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label for="lastname">Last Name</label>
						<input id="lastname" name="lastname" class="form-control" type="text" data-validation="email">
						<span id="error_lastname" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input id="email" name="email" class="form-control" type="email" data-validation="email">
						<span id="error_email" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input id="password" name="password" class="form-control" type="password" data-validation="password">
						<span id="error_password" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label for="confirm_password">Confirm Password</label>
						<input id="confirm_password" name="confirm_password" class="form-control" type="password" data-validation="confirm_password">
						<span id="error_confirm_password" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label for="user_role">User Role</label>
						<select name="user_role" id="user_role" class="form-control">
							<option value="owner" selected>Owner</option>
							<option value="resident" >Resident</option>
						</select>
						<span id="error_gender" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label for="phone">Phone Number</label>
						<input type="phone" id="phone" name="phone" class="form-control" >
						<span id="error_phone" class="text-danger"></span>
					</div>
					
					
					<button id="submit" type="submit" value="submit" class="btn btn-primary center" name="btn_register">Sign Up</button>
			
				</form>

			</div>
		</div>
	</div>
</div>
</div>
<?php get_footer(); ?>
<script type="text/javascript">
	jQuery(function() {
	jQuery("#register_form").validate({
          rules: {
              email: {
                required: true,
                email: true
              },
               password: {
            required: true,
            minlength: 8	
             },
            confirm_password: {
            required: true,
            equalTo: "#password"
            },
            myName : "required",
            lastname : "required",
            phone : "required"
          },
          messages :{
          	email: "Please enter a valid email address",
          	password : "Please enter 8 chracter password",
          	confirm_password : "Password and Confirm Password must be same.",
          	myName : "First Name will be required",
          	lastname : "Last Name will be required",
          	phone : "Must fill Phone Number"
          },
          submitHandler: function(form) {
          form.submit();
          }
      });
   });
</script>