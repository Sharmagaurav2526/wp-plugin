<?php  
// Template Name: About Us Page
get_header();
?>
<div class="page-wrapper">
<div class="hmgal-heading">
<div class="container">
<div class="section-header">
	  <h1 class="section-title text-center wow fadeInDown animated animated animated" style="visibility: visible; animation-name: fadeInDown;">About Us</h1>
	</div>
</div>
</div>
				<section class="outhistory">
						<div class="container">
						
							<div class="row">
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="bulb_img">
										<!-- <img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/5.jpg" alt="#"> -->
										<img src="<?php the_field('company_image'); ?>" alt="#">
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="content">
										<h1><?php the_field('company_history_title'); ?></h1>
										<p><?php the_field('company_description'); ?></p>
									</div>
									
									<div class="tick_lines">
										<h3><?php the_field('our_value_title'); ?></h3>
										<?php if( have_rows('company_points') ):
					 					while ( have_rows('company_points') ) : the_row(); ?>

										<div class="tick_heading">
											<img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/tick.jpg" alt="#">
										</div>
										<div class="content tick_content">

											<p><?php the_sub_field('company_point'); ?></p>
										</div>

										<?php   endwhile;
										else :
										endif; ?>
										<!-- <div class="tick_heading">
												<img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/tick.jpg" alt="#">
										</div>
										<div class="content tick_content">
											<p>Consectetur adipiscing sed dapibus leo</p>
										</div>
										<div class="tick_heading">
												<img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/tick.jpg" alt="#">
										</div>
										<div class="content tick_content">
											<p>Duis sed dapibus leo commodo nibh ante</p>
										</div>
										<div class="tick_heading">
											<img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/tick.jpg" alt="#">
										</div>
										<div class="content tick_content">
											<p>Sed commodo nibh ante ipsum dolor</p>
										</div>
										<div class="tick_heading">
												<img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/tick.jpg" alt="#">
										</div>
										<div class="content tick_content">
											<p>Lorem ed dapibus leo commodo ipsum dolor</p>
										</div> -->
									</div>
									
								</div>
							</div>
						</div>
					</section>

					<div class="clearfix"></div>
					
					<section class="team_images">
					  <div class="container">
						<div class="section-header">
			                <h2 class="section-title text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;"><?php the_field('owner_leadership_title') ?></h2>
			                
			                <p class="text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;"><?php the_field('owner_leadership_heading'); ?></p>
			                <!-- <p class="text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">Since 1992, we have been dedicated to extending the life of your building assets and providing an exceptional<br>experience for your customers.</p> -->
            			</div>
								<div class="row">
								<?php if( have_rows('owner_leadership_member_image') ):
					 					while ( have_rows('owner_leadership_member_image') ) : the_row(); ?>
								<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
									<div class="team_img">
										<img src="<?php the_sub_field('owner_member_image'); ?>" alt="#">
										<div class="content team">
											<h4><?php the_sub_field('owner_member_name'); ?></h4>
											<h6><?php the_sub_field('owner_member_description'); ?></h6>
											<h6><?php the_sub_field('owner_member_email'); ?></h6>
										</div>
									</div>
								</div>
								<?php   endwhile;
										else :
										endif; ?>
									
								<!--<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
									<div class="team_img">
										<img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/team3.jpg" alt="#">
										<div class="content team">
											<h4>Team Name</h4>
											<h6>CPI Expert</h6>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
									<div class="team_img">
										<img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/team2.jpg" alt="#">
										<div class="content team">
											<h4>Team Name</h4>
											<h6>CPS Expert</h6>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
									<div class="team_img">
										<img src="http://towercleaningsystems.mobilytedev.com/wp-content/uploads/2018/05/team3.jpg" alt="#">
										<div class="content team">
											<h4>Team Name</h4>
											<h6>CPL Expert</h6>
										</div>
									</div>
								</div>-->
								</div>
							</div>
						</section>
						
						   

	 <section  class="asst-logo wow fadeInRight">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown"><?php the_field('membership_association_title'); ?></h2>                
            </div>
            <div class="carousel">                   
                <ul id="flexiselDemo3">
				<?php if( have_rows('membership_and_association_icons') ):
					 while ( have_rows('membership_and_association_icons') ) : the_row(); ?>
                    <li><img src="<?php the_sub_field('membership_association_image'); ?>" /></li>
                     <?php   endwhile;
						else :
						endif; ?>						

                  <!-- <li><img src="<?php  echo get_template_directory_uri(); ?>/images/client/client-logo-02.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-03.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-04.png" /></li> 
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-05.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-06.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-07.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-08.png" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/client/client-logo-09.png" /></li> --> 

                </ul>    
            </div>
            <div class="clearout"></div>
        </div>
    </section>
</div>


<?php get_footer(); ?>