<?php 
get_header(); ?>

 <section id="main-slider">
        <div class="owl-carousel">
            <?php if( have_rows('slider') ):
            while ( have_rows('slider') ) : the_row(); ?>
            <div class="item" style="background-image: url(<?php  the_sub_field('slider_image'); ?>);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <p><?php the_sub_field('slider_content'); ?></p>
                                    <h2><?php the_sub_field('slider_heading'); ?></h2>                                    
                                    <a class="btn btn-primary btn-lg border-w" href="<?php the_sub_field('slider_button_url'); ?>"><?php the_sub_field('slider_button_name'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
            <?php endwhile;
            else :
            endif; ?>
            <!--  <div class="item" style="background-image: url(<?php // echo get_template_directory_uri(); ?>/images/slider-01.png);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <p>property management company serving the</p>
                                    <h2>Residential<br>and<br>Commercial</h2>                                    
                                    <a class="btn btn-primary btn-lg" href="#">FIND out more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --><!--/.item-->

        </div><!--/.owl-carousel-->
    </section><!--/#main-slider-->

    <section id="cta" class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">                    
                    <div class="media service-box wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="pull-left">
                            <i class="fa fa-wicon"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxMi45MzEgMzEyLjkzMSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzEyLjkzMSAzMTIuOTMxOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjY0cHgiIGhlaWdodD0iNjRweCI+CjxnPgoJPGc+CgkJPGc+CgkJCTxnPgoJCQkJPHBhdGggZD0iTTI4Mi42MDMsODAuMzI0aC00LjQ2NVYxOS42MTlDMjc4LjEzNyw4Ljc5LDI2OS4zMzIsMCwyNTguNTE5LDBoLTguNjQ4bC0wLjk1MiwwLjA3MSAgICAgIEMxODMuMjQ4LDEwLjkxNiwzMC4xODEsNTYuNTc3LDI4LjY4LDU3LjAyM0MxMC43MDQsNjIuMjc3LDEwLjcwNCw3NS40MjksMTAuNzA0LDgzLjMxdjE2LjYzOHYxNjkuODI5djIzLjUzNSAgICAgIGMwLDEwLjgwNyw4LjgxMSwxOS42MTksMTkuNjE5LDE5LjYxOWgyNTIuMjhjMTAuODI5LDAsMTkuNjI0LTguODExLDE5LjYyNC0xOS42MTl2LTQ2Ljk1di03OC4yNTJWOTkuOTQ4ICAgICAgQzMwMi4yMzIsODkuMTE5LDI5My40MzgsODAuMzI0LDI4Mi42MDMsODAuMzI0eiBNMzIuMDAzLDY4LjI2NmMxLjUxOC0wLjQ2MiwxNTMuMTUzLTQ1LjcxNSwyMTguMzQ2LTU2LjU2MWg4LjE2OSAgICAgIGM0LjM2MiwwLDcuOTE5LDMuNTU3LDcuOTE5LDcuOTE0VjgwLjMzSDMwLjMyM2MtMi44MjMsMC01LjQ4OCwwLjYyNS03Ljg5OCwxLjY5N0MyMi40NDIsNzUuMzc1LDIyLjk5Niw3MC44OTMsMzIuMDAzLDY4LjI2NnogICAgICAgTTI5MC41MjIsMjM0LjY3OWgtNzcuMDA2Yy00LjM2MiwwLTcuOTE0LTMuNTczLTcuOTE0LTcuOTM2di0zOS4wMTRjMC00LjM2MiwzLjU1Mi03LjkxNCw3LjkxNC03LjkxNGg3Ny4wMDZWMjM0LjY3OXoiIGZpbGw9IiMwMzVkNzgiLz4KCQkJPC9nPgoJCTwvZz4KCQk8Zz4KCQkJPGc+CgkJCQk8Y2lyY2xlIGN4PSIyMjUuNzU5IiBjeT0iMjA2Ljk1MSIgcj0iOC42MDUiIGZpbGw9IiMwMzVkNzgiLz4KCQkJPC9nPgoJCTwvZz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" /></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><?php the_field('maintance_heading'); ?></h4>
                            <p><?php the_field('maintance_subheading'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <a class="btn btn-primary btn-lg" href="<?php the_field('button_link'); ?>"><?php the_field('button_name'); ?></a>
                </div>
            </div>
        </div>
    </section><!--/#cta-->

    <section id="services" >
        <div class="container">

            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown"><?php the_field('service_heading'); ?></h2>
                <p class="text-center wow fadeInDown"><?php the_field('service_content'); ?></p>
            </div>

            <div class="row">
                <div class="features">
                    <?php $wpb_all_query = new WP_Query(array('post_type'=>'all_services', 'posts_per_page'=>3)); ?> 
                      <?php if ( $wpb_all_query->have_posts() ) : ?>
                          <?php 
                  while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                    <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">
                        <div class="media service-box">
                            <!-- <img src="<?php //echo get_template_directory_uri(); ?>/images/service-01.png" alt="" title="" class=""> -->     
                            <?php the_post_thumbnail();  ?>                       
                            <div class="service-body">
                                <h4 class="m-t-b-30 text-center media-heading"><?php the_title(); ?></h4>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->
                     <?php endwhile; ?>
                  <?php wp_reset_postdata(); ?>
                     
                    <?php else : ?>
                        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                    <!-- <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="100ms">
                        <div class="media service-box">
                            <img src="<?php //echo get_template_directory_uri(); ?>/images/service-02.png" alt="" title="" class="">                            
                            <div class="service-body">
                                <h4 class="m-t-b-30 text-center media-heading">Interior & Exterior Painting (Custom Faux Glazing)</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="200ms">
                        <div class="media service-box">
                            <img src="<?php// echo get_template_directory_uri(); ?>/images/service-03.png" alt="" title="" class="">
                            <div class="service-body">
                                <h4 class="m-t-b-30 text-center media-heading">Drywall repair and installation</h4>
                            </div>
                        </div>
                    </div> --><!--/.col-md-4-->

                    <div class="col-md-12 col-sm-12 wow fadeInUp text-center" data-wow-duration="400ms" data-wow-delay="300ms">
                        <a class="btn btn-primary btn-lg" href="<?php the_field('service_button_url'); ?>"><?php the_field('service_button_name'); ?></a>
                    </div>

                </div>
            </div><!--/.row-->    
        </div><!--/.container-->
    </section><!--/#services-->


    <section class="module pb-0" id="works">
          <div class="page-width">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown"><?php the_field('recent_project_heading'); ?></h2>
                <p class="text-center wow fadeInDown"><?php the_field('recent_project_content'); ?></p>
            </div>
          
          
            <div class="row projects">
              
                <ul class="filter font-alt" id="filters">
                  <li><a class="current wow fadeInUp" href="#" data-filter="*">All</a></li>
                    <?php
                  $args = array(
                    'orderby' => 'name',
                    'parent' => 0,
                     'post_type' => 'projects',
                     'include' => array('8','9')
                    );
                  $category = get_categories( $args );
                  foreach ( $category as $cat ) { ?>
                  <li><a class="wow fadeInUp" href="#" data-filter=".<?php echo  $cat->name; ?>" data-wow-delay="0.2s"><?php echo  $cat->name; ?></a></li>
                   <?php } ?>
                  <!-- <li><a class="wow fadeInUp" href="#" data-filter=".commercial" data-wow-delay="0.4s">Commercial</a></li> -->
                  <!--li><a class="wow fadeInUp" href="#" data-filter=".photography" data-wow-delay="0.6s">tab-03</a></li>
                  <li><a class="wow fadeInUp" href="#" data-filter=".webdesign" data-wow-delay="0.6s">tab-040</a></li-->
                </ul>
              
            
          
          <ul class="works-grid works-grid-gut works-grid-4 works-hover-w" id="works-grid">
            <?php $wpb_all_query = new WP_Query(array('post_type'=>'projects', 'posts_per_page'=>-1));
               
             ?> 
                      <?php if ( $wpb_all_query->have_posts() ) : ?>
                          <?php 
                  while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); 
                    $get_category_id = get_the_category(get_the_ID());
                    foreach ($get_category_id as $get_category_name) {
                        $cat_name = $get_category_name->name;
                    }
                    ?> 
            <li class="work-item <?php echo  $cat_name; ?>  webdesign"><a href="<?php the_permalink(); ?>">
                <div class="work-image"><?php the_post_thumbnail(); ?><!-- <img src="<?php // echo get_template_directory_uri(); ?>/images/pro-img-01.png" alt="Portfolio Item"/> --></div>
                <div class="work-caption font-alt">
                  <h3 class="work-title"><?php the_title(); ?></h3>
                  <div class="work-descr"><?php echo $cat_name; ?></div>
                </div></a></li>
            <?php endwhile; ?>
                  <?php wp_reset_postdata(); ?>
                     
                    <?php else : ?>
                        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
            <!-- <li class="work-item commercial photography"><a href="portfolio_single_featured_image2.html">
                <div class="work-image"><img src="<?php // echo get_template_directory_uri(); ?>/images/pro-img-02.png" alt="Portfolio Item"/></div>
                <div class="work-caption font-alt">
                  <h3 class="work-title">Corporate Identity</h3>
                  <div class="work-descr">commercial</div>
                </div></a></li>
            <li class="work-item residential photography"><a href="portfolio_single_featured_slider1.html">
                <div class="work-image"><img src="<?php // echo get_template_directory_uri(); ?>/images/pro-img-03.png" alt="Portfolio Item"/></div>
                <div class="work-caption font-alt">
                  <h3 class="work-title">Corporate Identity</h3>
                  <div class="work-descr">residential</div>
                </div></a></li>
            <li class="work-item residential photography"><a href="portfolio_single_featured_slider2.htmll">
                <div class="work-image"><img src="<?php // echo get_template_directory_uri(); ?>/images/pro-img-04.png" alt="Portfolio Item"/></div>
                <div class="work-caption font-alt">
                  <h3 class="work-title">Corporate Identity</h3>
                  <div class="work-descr">residential</div>
                </div></a></li> -->
            
          </ul>
          <div class="col-md-12 col-sm-12 wow fadeInUp text-center" data-wow-duration="400ms" data-wow-delay="300ms">
                        <a class="m-t-b-20 btn btn-primary btn-lg" href="<?php the_field('recent_button_link'); ?>"><?php the_field('recent_button_name'); ?></a>
                    </div>
              </div>
          </div>
        </section>


    <section id="about">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown"><?php the_field('about_us_heading'); ?></h2>
                <p class="text-center wow fadeInDown"><?php the_field('about_us_content'); ?></p>
            </div>
            <div class="row">
                <div class="col-sm-6 wow fadeInLeft">
                    <div class="about-img">
                        <img class="img-responsive" src="<?php the_field('ceo_image'); ?>" alt="">
                    </div>
                </div>
                <div class="col-sm-6" style="position: relative;">
                    <div class="center-cont">
                       <?php if( have_rows('about_ceo') ):
                     while ( have_rows('about_ceo') ) : the_row(); ?>
                    <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="<?php the_sub_field('icon_class'); ?>"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><?php the_sub_field('heading'); ?></h4>
                            <p><?php the_sub_field('subheading'); ?></p>
                        </div>
                    </div>
                        <?php   endwhile;
                        else :
                        endif; ?>
                    <!-- <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="fa fa-cubes"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Best Materials</h4>
                            <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>
                        </div>
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="fa fa-pie-chart"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">professional standards</h4>
                            <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>
                        </div>
                    </div> -->

                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="testimonial" style="background-image: url(<?php the_field('background_image'); ?>);">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">

                    <div id="carousel-testimonial" class="carousel slide wow fadeInLeft" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner text-center" role="listbox">
                        <?php 
				       $wpb_all_query = new WP_Query(array('post_type'=>'testimonial', 'posts_per_page'=>2)); ?> 
							<?php if ( $wpb_all_query->have_posts() ) : ?>
							    <?php $count = 0; 
							    while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
							        <div class="item <?php if($count == 0){ echo "active";} ?>">                                
							                                <p><!-- <img class="img-circle img-thumbnail" src="<?php // echo get_template_directory_uri(); ?>/images/testimonial/01.jpg" alt=""> -->
							                                	 <?php the_post_thumbnail('thumbnail', array( 'class' => 'img-circle img-thumbnail' )); ?>
							                                </p>
							                                <p class="t_text"><?php the_content(); ?></p>
							                                <h4><?php the_title(); ?></h4>
							                                <small><?php the_excerpt(); ?></small>
							                            </div>
							    <?php $count++; endwhile; ?>
							    <?php wp_reset_postdata(); ?>
						    	<?php else : ?>
							    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
							    <?php endif; ?>

                           
    
                           <!--  <div class="item">
                                <p><img class="img-circle img-thumbnail" src="<?php // echo get_template_directory_uri(); ?>/images/testimonial/01.jpg" alt=""></p>
                                <p class="t_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam</p>
                                <h4>Louise S. Morgan</h4>
                                <small>Treatment, storage, and disposal (TSD) worker</small>
                            </div> -->

                        </div>

                        <!-- Controls -->
                        <div class="btns">
                            <a class="btn btn-primary btn-sm pull-left" href="#carousel-testimonial" role="button" data-slide="prev">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="btn btn-primary btn-sm pull-right" href="#carousel-testimonial" role="button" data-slide="next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#testimonial-->

    <section id="client_logo" class="wow fadeInRight">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown"><?php the_field('work_with_heading'); ?></h2>                
            </div>
            <div class="carousel">                   
                <ul id="flexiselDemo3">
					<?php if( have_rows('company_icons') ):
					 while ( have_rows('company_icons') ) : the_row(); ?>
                    <li><img src="<?php the_sub_field('images'); ?>" /></li>
                     <?php   endwhile;
						else :
						endif; ?>
                   <!--  <li><img src="<?php // echo get_template_directory_uri(); ?>/images/client/client-logo-02.png" /></li>
                    <li><img src="<?php // echo get_template_directory_uri(); ?>/images/client/client-logo-03.png" /></li>
                    <li><img src="<?php // echo get_template_directory_uri(); ?>/images/client/client-logo-04.png" /></li> 
                    <li><img src="<?php // echo get_template_directory_uri(); ?>/images/client/client-logo-05.png" /></li>
                    <li><img src="<?php // echo get_template_directory_uri(); ?>/images/client/client-logo-06.png" /></li>
                    <li><img src="<?php // echo get_template_directory_uri(); ?>/images/client/client-logo-07.png" /></li>
                    <li><img src="<?php // echo get_template_directory_uri(); ?>/images/client/client-logo-08.png" /></li>
                    <li><img src="<?php // echo get_template_directory_uri(); ?>/images/client/client-logo-09.png" /></li>  -->

                </ul>    
            </div>
            <div class="clearout"></div>
        </div>
    </section>


   


<?php get_footer();
?>