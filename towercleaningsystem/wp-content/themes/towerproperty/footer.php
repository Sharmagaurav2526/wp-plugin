 <section id="work-process">
        <div class="container">            

            <div class="row text-center">
                <div class="col-md-4 col-sm-2 col-xs-6">
                    <div class="media service-box wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="pull-left">
                            <i class="fa fa-wicon"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU3OC4xMDYgNTc4LjEwNiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTc4LjEwNiA1NzguMTA2OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjY0cHgiIGhlaWdodD0iNjRweCI+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTU3Ny44Myw0NTYuMTI4YzEuMjI1LDkuMzg1LTEuNjM1LDE3LjU0NS04LjU2OCwyNC40OGwtODEuMzk2LDgwLjc4MSAgICBjLTMuNjcyLDQuMDgtOC40NjUsNy41NTEtMTQuMzgxLDEwLjQwNGMtNS45MTYsMi44NTctMTEuNzI5LDQuNjkzLTE3LjQzOSw1LjUwOGMtMC40MDgsMC0xLjYzNSwwLjEwNS0zLjY3NiwwLjMwOSAgICBjLTIuMDM3LDAuMjAzLTQuNjg5LDAuMzA3LTcuOTUzLDAuMzA3Yy03Ljc1NCwwLTIwLjMwMS0xLjMyNi0zNy42NDEtMy45NzlzLTM4LjU1NS05LjE4Mi02My42NDUtMTkuNTg0ICAgIGMtMjUuMDk2LTEwLjQwNC01My41NTMtMjYuMDEyLTg1LjM3Ni00Ni44MThjLTMxLjgyMy0yMC44MDUtNjUuNjg4LTQ5LjM2Ny0xMDEuNTkyLTg1LjY4ICAgIGMtMjguNTYtMjguMTUyLTUyLjIyNC01NS4wOC03MC45OTItODAuNzgzYy0xOC43NjgtMjUuNzA1LTMzLjg2NC00OS40NzEtNDUuMjg4LTcxLjI5OSAgICBjLTExLjQyNS0yMS44MjgtMTkuOTkzLTQxLjYxNi0yNS43MDUtNTkuMzY0UzQuNTksMTc3LjM2MiwyLjU1LDE2NC41MXMtMi44NTYtMjIuOTUtMi40NDgtMzAuMjk0ICAgIGMwLjQwOC03LjM0NCwwLjYxMi0xMS40MjQsMC42MTItMTIuMjRjMC44MTYtNS43MTIsMi42NTItMTEuNTI2LDUuNTA4LTE3LjQ0MnM2LjMyNC0xMC43MSwxMC40MDQtMTQuMzgyTDk4LjAyMiw4Ljc1NiAgICBjNS43MTItNS43MTIsMTIuMjQtOC41NjgsMTkuNTg0LTguNTY4YzUuMzA0LDAsOS45OTYsMS41MywxNC4wNzYsNC41OXM3LjU0OCw2LjgzNCwxMC40MDQsMTEuMzIybDY1LjQ4NCwxMjQuMjM2ICAgIGMzLjY3Miw2LjUyOCw0LjY5MiwxMy42NjgsMy4wNiwyMS40MmMtMS42MzIsNy43NTItNS4xLDE0LjI4LTEwLjQwNCwxOS41ODRsLTI5Ljk4OCwyOS45ODhjLTAuODE2LDAuODE2LTEuNTMsMi4xNDItMi4xNDIsMy45NzggICAgcy0wLjkxOCwzLjM2Ni0wLjkxOCw0LjU5YzEuNjMyLDguNTY4LDUuMzA0LDE4LjM2LDExLjAxNiwyOS4zNzZjNC44OTYsOS43OTIsMTIuNDQ0LDIxLjcyNiwyMi42NDQsMzUuODAyICAgIHMyNC42ODQsMzAuMjkzLDQzLjQ1Miw0OC42NTNjMTguMzYsMTguNzcsMzQuNjgsMzMuMzU0LDQ4Ljk2LDQzLjc2YzE0LjI3NywxMC40LDI2LjIxNSwxOC4wNTMsMzUuODAzLDIyLjk0OSAgICBjOS41ODgsNC44OTYsMTYuOTMyLDcuODU0LDIyLjAzMSw4Ljg3MWw3LjY0OCwxLjUzMWMwLjgxNiwwLDIuMTQ1LTAuMzA3LDMuOTc5LTAuOTE4YzEuODM2LTAuNjEzLDMuMTYyLTEuMzI2LDMuOTc5LTIuMTQzICAgIGwzNC44ODMtMzUuNDk2YzcuMzQ4LTYuNTI3LDE1LjkxMi05Ljc5MSwyNS43MDUtOS43OTFjNi45MzgsMCwxMi40NDMsMS4yMjMsMTYuNTIzLDMuNjcyaDAuNjExbDExOC4xMTUsNjkuNzY4ICAgIEM1NzEuMDk4LDQ0MS4yMzgsNTc2LjE5Nyw0NDcuOTY4LDU3Ny44Myw0NTYuMTI4eiIgZmlsbD0iIzAzNWQ3OCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" /></i>
                        </div>
                        <div class="media-body text-left">
                            <h4 class="media-heading white-text"><?php the_field('phone','option'); ?></h4>
                            <p><?php the_field('phone_number','option'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-2 col-xs-6">
                    <div class="media service-box wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="pull-left">
                            <i class="fa fa-wicon"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDYwIDYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA2MCA2MDsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI2NHB4IiBoZWlnaHQ9IjY0cHgiPgo8Zz4KCTxwYXRoIGQ9Ik0yMS4wMjEsMjguOTc3QzE4Ljc1LDI2LjQxMiwxNS43MjIsMjUsMTIuNDkzLDI1cy02LjI1NywxLjQxMi04LjUyNywzLjk3N2MtNC42MTIsNS4yMTEtNC42MTIsMTMuNjg4LDAsMTguODk5ICAgbDguNTI3LDkuNjMzbDguNTI3LTkuNjMzQzI1LjYzMyw0Mi42NjUsMjUuNjMzLDM0LjE4OCwyMS4wMjEsMjguOTc3eiBNMTkuNTIzLDQ2LjU1bC03LjAyOSw3Ljk0MUw1LjQ2NCw0Ni41NSAgIGMtMy45NjYtNC40NzktMy45NjYtMTEuNzY4LDAtMTYuMjQ3QzcuMzQ5LDI4LjE3Myw5Ljg0NiwyNywxMi40OTMsMjdzNS4xNDUsMS4xNzMsNy4wMjksMy4zMDMgICBDMjMuNDg4LDM0Ljc4MiwyMy40ODgsNDIuMDcsMTkuNTIzLDQ2LjU1eiIgZmlsbD0iIzAzNWQ3OCIvPgoJPHBhdGggZD0iTTQ4Ljc0NywyNC41MDdsNi40NjQtNy4yODZjMy40NzgtMy45MiwzLjQ3OC0xMC4yOTYsMC0xNC4yMTZDNTMuNDkxLDEuMDY3LDUxLjE5NSwwLDQ4Ljc0NiwwICAgYy0yLjQ0OCwwLTQuNzQ0LDEuMDY3LTYuNDY0LDMuMDA1Yy0zLjQ3OCwzLjkyLTMuNDc4LDEwLjI5NiwwLDE0LjIxNkw0OC43NDcsMjQuNTA3eiBNNDMuNzc4LDQuMzMzICAgQzQ1LjExMywyLjgyOCw0Ni44NzcsMiw0OC43NDYsMmMxLjg3LDAsMy42MzQsMC44MjgsNC45NjksMi4zMzNjMi44MjgsMy4xODcsMi44MjgsOC4zNzMsMCwxMS41NmwtNC45NjgsNS42MDFsLTQuOTY5LTUuNjAxICAgQzQwLjk1LDEyLjcwNiw0MC45NSw3LjUyLDQzLjc3OCw0LjMzM3oiIGZpbGw9IiMwMzVkNzgiLz4KCTxwYXRoIGQ9Ik01Mi42MDQsNTQuNTE3Yy0wLjUzOS0wLjEyMS0xLjA3NCwwLjIxNy0xLjE5NSwwLjc1NmMtMC4xMTUsMC41MS0wLjMzOCwwLjk3OS0wLjY2MywxLjM5MiAgIGMtMC4zNDEsMC40MzQtMC4yNjcsMS4wNjMsMC4xNjgsMS40MDRjMC4xODQsMC4xNDQsMC40MDEsMC4yMTQsMC42MTcsMC4yMTRjMC4yOTcsMCwwLjU5LTAuMTMxLDAuNzg3LTAuMzgyICAgYzAuNTA0LTAuNjQsMC44NjQtMS4zOTYsMS4wNDItMi4xODhDNTMuNDgxLDU1LjE3Myw1My4xNDMsNTQuNjM4LDUyLjYwNCw1NC41MTd6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNMzEuOTk1LDMxYy0wLjQzMy0wLjAwMS0wLjg1NC0wLjA3OC0xLjI1Mi0wLjIzYy0wLjUxMi0wLjE5NS0xLjA5MywwLjA2Mi0xLjI5MSwwLjU3OCAgIGMtMC4xOTYsMC41MTYsMC4wNjMsMS4wOTQsMC41NzgsMS4yOTFjMC42MjYsMC4yMzgsMS4yODUsMC4zNiwxLjk2MywwLjM2MWgwLjM1N2MwLjU1MywwLDEtMC40NDcsMS0xcy0wLjQ0Ny0xLTEtMUgzMS45OTV6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNMjkuNjIxLDI0LjY3NmMwLjE4MSwwLDAuMzYzLTAuMDQ5LDAuNTI4LTAuMTUxYzAuNDQzLTAuMjc2LDAuOTMzLTAuNDQ1LDEuNDU0LTAuNTAzYzAuNTQ5LTAuMDYxLDAuOTQ1LTAuNTU1LDAuODg1LTEuMTA0ICAgcy0wLjU1MS0wLjk0MS0xLjEwNC0wLjg4NWMtMC44MjIsMC4wOTEtMS41OTQsMC4zNTctMi4yOTQsMC43OTRjLTAuNDY5LDAuMjkyLTAuNjExLDAuOTA5LTAuMzE5LDEuMzc4ICAgQzI4Ljk2MSwyNC41MDksMjkuMjg3LDI0LjY3NiwyOS42MjEsMjQuNjc2eiIgZmlsbD0iIzAzNWQ3OCIvPgoJPHBhdGggZD0iTTE4LjA2NCw1OGgtMmMtMC41NTMsMC0xLDAuNDQ3LTEsMXMwLjQ0NywxLDEsMWgyYzAuNTUzLDAsMS0wLjQ0NywxLTFTMTguNjE2LDU4LDE4LjA2NCw1OHoiIGZpbGw9IiMwMzVkNzgiLz4KCTxwYXRoIGQ9Ik0zNS45OTMsNDBjLTAuMDg2LDAtMC4xNzEsMC4wMDItMC4yNTYsMC4wMDZjLTAuNTUyLDAuMDIyLTAuOTgsMC40ODgtMC45NTgsMS4wNGMwLjAyMSwwLjUzOCwwLjQ2NSwwLjk1OSwwLjk5OCwwLjk1OSAgIGMwLjAxNCwwLDAuMDI4LDAsMC4wNDItMC4wMDFMMzcuNzc4LDQyYzAuNTUzLDAsMS0wLjQ0NywxLTFzLTAuNDQ3LTEtMS0xSDM1Ljk5M3oiIGZpbGw9IiMwMzVkNzgiLz4KCTxwYXRoIGQ9Ik0zNS40OTMsMjRoMmMwLjU1MywwLDEtMC40NDcsMS0xcy0wLjQ0Ny0xLTEtMWgtMmMtMC41NTMsMC0xLDAuNDQ3LTEsMVMzNC45NCwyNCwzNS40OTMsMjR6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNNDcuNzc4LDQwYy0wLjU1MywwLTEsMC40NDctMSwxczAuNDQ3LDEsMSwxaDJjMC41NTMsMCwxLTAuNDQ3LDEtMXMtMC40NDctMS0xLTFINDcuNzc4eiIgZmlsbD0iIzAzNWQ3OCIvPgoJPHBhdGggZD0iTTUxLjM1MSwzMmMwLTAuNTUzLTAuNDQ3LTEtMS0xaC0yYy0wLjU1MywwLTEsMC40NDctMSwxczAuNDQ3LDEsMSwxaDJDNTAuOTAzLDMzLDUxLjM1MSwzMi41NTMsNTEuMzUxLDMyeiIgZmlsbD0iIzAzNWQ3OCIvPgoJPHBhdGggZD0iTTQxLjc3OCw0MmgyYzAuNTUzLDAsMS0wLjQ0NywxLTFzLTAuNDQ3LTEtMS0xaC0yYy0wLjU1MywwLTEsMC40NDctMSwxUzQxLjIyNiw0Miw0MS43NzgsNDJ6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNNDMuNDkzLDI0YzAuNTUzLDAsMS0wLjQ0NywxLTFzLTAuNDQ3LTEtMS0xaC0yYy0wLjU1MywwLTEsMC40NDctMSwxczAuNDQ3LDEsMSwxSDQzLjQ5M3oiIGZpbGw9IiMwMzVkNzgiLz4KCTxwYXRoIGQ9Ik0zNi4zNTEsMzFjLTAuNTUzLDAtMSwwLjQ0Ny0xLDFzMC40NDcsMSwxLDFoMmMwLjU1MywwLDEtMC40NDcsMS0xcy0wLjQ0Ny0xLTEtMUgzNi4zNTF6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNNDUuMzUxLDMyYzAtMC41NTMtMC40NDctMS0xLTFoLTJjLTAuNTUzLDAtMSwwLjQ0Ny0xLDFzMC40NDcsMSwxLDFoMkM0NC45MDMsMzMsNDUuMzUxLDMyLjU1Myw0NS4zNTEsMzJ6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNNTkuNDI2LDM1LjYzMmMtMC4wODgtMC41NDYtMC42MDMtMC45MTktMS4xNDYtMC44MjljLTAuNTQ2LDAuMDg3LTAuOTE3LDAuNjAxLTAuODI5LDEuMTQ2ICAgYzAuMDI4LDAuMTc4LDAuMDQ0LDAuMzU5LDAuMDQyLDAuNTUyYzAsMC4zMzYtMC4wNDgsMC42NjgtMC4xNDIsMC45ODhjLTAuMTU1LDAuNTMsMC4xNDgsMS4wODYsMC42NzksMS4yNDEgICBjMC4wOTQsMC4wMjcsMC4xODgsMC4wNCwwLjI4MSwwLjA0YzAuNDMzLDAsMC44MzItMC4yODIsMC45Ni0wLjcxOWMwLjE0Ny0wLjUwMywwLjIyMi0xLjAyNCwwLjIyMi0xLjU0MyAgIEM1OS40OTUsMzYuMjIsNTkuNDczLDM1LjkyNSw1OS40MjYsMzUuNjMyeiIgZmlsbD0iIzAzNWQ3OCIvPgoJPHBhdGggZD0iTTQyLjA2NCw1OGgtMmMtMC41NTMsMC0xLDAuNDQ3LTEsMXMwLjQ0NywxLDEsMWgyYzAuNTUzLDAsMS0wLjQ0NywxLTFTNDIuNjE2LDU4LDQyLjA2NCw1OHoiIGZpbGw9IiMwMzVkNzgiLz4KCTxwYXRoIGQ9Ik0zNi4wNjQsNThoLTJjLTAuNTUzLDAtMSwwLjQ0Ny0xLDFzMC40NDcsMSwxLDFoMmMwLjU1MywwLDEtMC40NDcsMS0xUzM2LjYxNiw1OCwzNi4wNjQsNTh6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNNDcuMDY1LDUwYzAtMC41NTMtMC40NDctMS0xLTFoLTJjLTAuNTUzLDAtMSwwLjQ0Ny0xLDFzMC40NDcsMSwxLDFoMkM0Ni42MTcsNTEsNDcuMDY1LDUwLjU1Myw0Ny4wNjUsNTB6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNNDEuMDY1LDUwYzAtMC41NTMtMC40NDctMS0xLTFoLTJjLTAuNTUzLDAtMSwwLjQ0Ny0xLDFzMC40NDcsMSwxLDFoMkM0MC42MTcsNTEsNDEuMDY1LDUwLjU1Myw0MS4wNjUsNTB6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNNDcuOTkzLDU4aC0xLjkzYy0wLjU1MywwLTEsMC40NDctMSwxczAuNDQ3LDEsMSwxbDEuOTcxLTAuMDAxbDAuMDI5LTF2MWMwLjU1MywwLDAuOTY1LTAuNDQ3LDAuOTY1LTEgICBDNDkuMDI4LDU4LjQ0Nyw0OC41NDYsNTgsNDcuOTkzLDU4eiIgZmlsbD0iIzAzNWQ3OCIvPgoJPHBhdGggZD0iTTUxLjUzMSw1Mi43MTljMC4yMTYsMCwwLjQzNS0wLjA3LDAuNjE3LTAuMjE0YzAuNDM1LTAuMzQyLDAuNTA5LTAuOTcxLDAuMTY4LTEuNDA0Yy0wLjUwMy0wLjYzOS0xLjE1Mi0xLjE2Ny0xLjg3OS0xLjUyOCAgIGMtMC40OTctMC4yNDctMS4wOTUtMC4wNDQtMS4zNDEsMC40NWMtMC4yNDYsMC40OTUtMC4wNDQsMS4wOTUsMC40NSwxLjM0MWMwLjQ2MywwLjIyOSwwLjg3NywwLjU2NiwxLjE5NywwLjk3NCAgIEM1MC45NDEsNTIuNTg4LDUxLjIzNCw1Mi43MTksNTEuNTMxLDUyLjcxOXoiIGZpbGw9IiMwMzVkNzgiLz4KCTxwYXRoIGQ9Ik01NS4zNDUsMzkuNzI5QzU0LjkxNiwzOS45MDksNTQuNDYyLDQwLDUzLjk5Myw0MGgtMC4yMTVjLTAuNTUzLDAtMSwwLjQ0Ny0xLDFzMC40NDcsMSwxLDFoMC4yMTUgICBjMC43MzYsMCwxLjQ1MS0wLjE0NCwyLjEyNS0wLjQyN2MwLjUxLTAuMjE0LDAuNzQ5LTAuOCwwLjUzNS0xLjMwOUM1Ni40NCwzOS43NTUsNTUuODUsMzkuNTE1LDU1LjM0NSwzOS43Mjl6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNMjguMDI5LDI5Ljg1NWMwLjUyNC0wLjE3MywwLjgxLTAuNzM4LDAuNjM4LTEuMjYzYy0wLjExNS0wLjM1MS0wLjE3NC0wLjcxOC0wLjE3NC0xLjA4OCAgIGMwLjAwMS0wLjE1MSwwLjAxLTAuMzAyLDAuMDI4LTAuNDQ5YzAuMDY5LTAuNTQ4LTAuMzE5LTEuMDQ5LTAuODY3LTEuMTE3Yy0wLjU2NS0wLjA2My0xLjA0OSwwLjMyMS0xLjExNywwLjg2NyAgIGMtMC4wMjgsMC4yMjYtMC4wNDMsMC40NTYtMC4wNDQsMC42OTRjMCwwLjU4OCwwLjA5MiwxLjE2NiwwLjI3MywxLjcxOGMwLjEzOSwwLjQyMSwwLjUzLDAuNjg4LDAuOTUsMC42ODggICBDMjcuODIsMjkuOTA1LDI3LjkyNiwyOS44OSwyOC4wMjksMjkuODU1eiIgZmlsbD0iIzAzNWQ3OCIvPgoJPHBhdGggZD0iTTI0LjA2NCw1OGgtMmMtMC41NTMsMC0xLDAuNDQ3LTEsMXMwLjQ0NywxLDEsMWgyYzAuNTUzLDAsMS0wLjQ0NywxLTFTMjQuNjE2LDU4LDI0LjA2NCw1OHoiIGZpbGw9IiMwMzVkNzgiLz4KCTxwYXRoIGQ9Ik0zNC4xMjIsNTAuNTk0YzAuMzgsMCwwLjc0Mi0wLjIxNywwLjkxLTAuNTg0YzAuMjI5LTAuNTAzLDAuMDA5LTEuMDk2LTAuNDkzLTEuMzI1Yy0wLjQ2OS0wLjIxNS0wLjg5NC0wLjUzOC0xLjIyOC0wLjkzNiAgIGMtMC4zNTYtMC40MjMtMC45ODYtMC40NzgtMS40MDktMC4xMjJjLTAuNDIzLDAuMzU1LTAuNDc4LDAuOTg2LTAuMTIyLDEuNDA5YzAuNTI0LDAuNjIzLDEuMTksMS4xMzEsMS45MjcsMS40NjcgICBDMzMuODQyLDUwLjU2NCwzMy45ODMsNTAuNTk0LDM0LjEyMiw1MC41OTR6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNNTYuNzQ2LDMxLjczOGMtMC43MTQtMC40MTQtMS40OTQtMC42NTctMi4zMTctMC43MjJjLTAuNTQ5LTAuMDQtMS4wMzIsMC4zNjctMS4wNzUsMC45MTkgICBjLTAuMDQzLDAuNTUxLDAuMzY4LDEuMDMyLDAuOTE5LDEuMDc1YzAuNTIyLDAuMDQxLDEuMDE3LDAuMTk1LDEuNDcsMC40NThjMC4xNTgsMC4wOTIsMC4zMzEsMC4xMzUsMC41MDEsMC4xMzUgICBjMC4zNDUsMCwwLjY4MS0wLjE3OSwwLjg2Ni0wLjQ5OEM1Ny4zODcsMzIuNjI4LDU3LjIyNCwzMi4wMTYsNTYuNzQ2LDMxLjczOHoiIGZpbGw9IiMwMzVkNzgiLz4KCTxwYXRoIGQ9Ik0zMC4wNjQsNThoLTJjLTAuNTUzLDAtMSwwLjQ0Ny0xLDFzMC40NDcsMSwxLDFoMmMwLjU1MywwLDEtMC40NDcsMS0xUzMwLjYxNiw1OCwzMC4wNjQsNTh6IiBmaWxsPSIjMDM1ZDc4Ii8+Cgk8cGF0aCBkPSJNMzIuNTU1LDQ0LjgzOWMwLjA5OS0wLjUxNCwwLjMwNy0wLjk4OSwwLjYxOS0xLjQxM2MwLjMyNy0wLjQ0NCwwLjIzMi0xLjA3LTAuMjEzLTEuMzk4ICAgYy0wLjQ0NS0wLjMyNi0xLjA3LTAuMjMyLTEuMzk4LDAuMjEzYy0wLjQ4OSwwLjY2Ni0wLjgxNywxLjQxMy0wLjk3MywyLjIyMmMtMC4xMDQsMC41NDIsMC4yNTEsMS4wNjYsMC43OTQsMS4xNzEgICBjMC4wNjMsMC4wMTIsMC4xMjcsMC4wMTgsMC4xODksMC4wMThDMzIuMDQzLDQ1LjY1LDMyLjQ2Miw0NS4zMTcsMzIuNTU1LDQ0LjgzOXoiIGZpbGw9IiMwMzVkNzgiLz4KCTxwYXRoIGQ9Ik00OC45OTMsMTVjMy4wMzIsMCw1LjUtMi40NjgsNS41LTUuNVM1Mi4wMjUsNCw0OC45OTMsNHMtNS41LDIuNDY4LTUuNSw1LjVTNDUuOTYxLDE1LDQ4Ljk5MywxNXogTTQ4Ljk5Myw2ICAgYzEuOTMsMCwzLjUsMS41NywzLjUsMy41cy0xLjU3LDMuNS0zLjUsMy41cy0zLjUtMS41Ny0zLjUtMy41UzQ3LjA2NCw2LDQ4Ljk5Myw2eiIgZmlsbD0iIzAzNWQ3OCIvPgoJPHBhdGggZD0iTTEyLjQ5MywzMWMtMy4zMDksMC02LDIuNjkxLTYsNnMyLjY5MSw2LDYsNnM2LTIuNjkxLDYtNlMxNS44MDIsMzEsMTIuNDkzLDMxeiBNMTIuNDkzLDQxYy0yLjIwNiwwLTQtMS43OTQtNC00ICAgczEuNzk0LTQsNC00czQsMS43OTQsNCw0UzE0LjY5OSw0MSwxMi40OTMsNDF6IiBmaWxsPSIjMDM1ZDc4Ii8+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" /></i>
                        </div>
                        <div class="media-body text-left">
                            <h4 class="media-heading white-text"><?php the_field('main_address' , 'option'); ?></h4>
                            <p><?php the_field('full_address' , 'option'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-2 col-xs-6">
                    <div class="media service-box wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="pull-left">
                            <i class="fa fa-wicon"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDE0IDE0IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAxNCAxNDsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI2NHB4IiBoZWlnaHQ9IjY0cHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik03LDlMNS4yNjgsNy40ODRsLTQuOTUyLDQuMjQ1QzAuNDk2LDExLjg5NiwwLjczOSwxMiwxLjAwNywxMmgxMS45ODYgICAgYzAuMjY3LDAsMC41MDktMC4xMDQsMC42ODgtMC4yNzFMOC43MzIsNy40ODRMNyw5eiIgZmlsbD0iIzAzNWQ3OCIvPgoJCTxwYXRoIGQ9Ik0xMy42ODQsMi4yNzFDMTMuNTA0LDIuMTAzLDEzLjI2MiwyLDEyLjk5MywySDEuMDA3QzAuNzQsMiwwLjQ5OCwyLjEwNCwwLjMxOCwyLjI3M0w3LDggICAgTDEzLjY4NCwyLjI3MXoiIGZpbGw9IiMwMzVkNzgiLz4KCQk8cG9seWdvbiBwb2ludHM9IjAsMi44NzggMCwxMS4xODYgNC44MzMsNy4wNzkgICAiIGZpbGw9IiMwMzVkNzgiLz4KCQk8cG9seWdvbiBwb2ludHM9IjkuMTY3LDcuMDc5IDE0LDExLjE4NiAxNCwyLjg3NSAgICIgZmlsbD0iIzAzNWQ3OCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" /></i>
                        </div>
                        <div class="media-body text-left">
                            <h4 class="media-heading white-text"><?php the_field('email_heading' , 'option'); ?></h4>
                            <p><?php the_field('email_address' , 'option'); ?></p>
                        </div>
                    </div>
                </div>       
                
            </div>
        </div>
    </section><!--/#work-process-->
 <footer id="footer">
        <div class="module-small bg-dark">
          <div class="container">
            <div class="row">

              <div class="col-sm-3">
                <div class="widget">
                  <h5 class="widget-title font-alt"><?php the_field('about_us_heading','option'); ?></h5>
                  <p class="black-text"><?php the_field('about_us_content' , 'option'); ?></p>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="widget">
                  <h5 class="widget-title font-alt">Our Services</h5>
                   <ul class="service icon-list">
                    <?php $wpb_all_query = new WP_Query(array('post_type'=>'all_services', 'posts_per_page'=>6)); ?> 
                      <?php if ( $wpb_all_query->have_posts() ) : ?>
                          <?php 
                  while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?> 
                    <li><a href="<?php the_permalink(); ?>"><i class="fa fa-check-circle" aria-hidden="true"></i><?php the_title(); ?></a></li>
                    <?php endwhile; ?>
                  <?php wp_reset_postdata(); ?>
                     
                    <?php else : ?>
                        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                   <!--  <li><a href="#"><i class="fa fa-check-circle" aria-hidden="true"></i>Commercial Landscaping</a></li>
                    <li><a href="#"><i class="fa fa-check-circle" aria-hidden="true"></i>Hard Floor and Carpet Cleaning</a></li>
                    <li><a href="#"><i class="fa fa-check-circle" aria-hidden="true"></i>Carpet, VCT, Plank repair and installation</a></li>
                    <li><a href="#"><i class="fa fa-check-circle" aria-hidden="true"></i>Janitorial</a></li>
                    <li><a href="#"><i class="fa fa-check-circle" aria-hidden="true"></i>Carpentry & General Repairs</a></li> -->

                  </ul>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="widget">
                  <h5 class="widget-title font-alt">Categories</h5>
                  <ul class="categories icon-list">
                    <?php
                  $args = array(
                    'orderby' => 'name',
                    'parent' => 0,
                     'number'=>8
                    );
                  $categories = get_categories( $args );
                  foreach ( $categories as $category ) { ?>
                     <!-- <li><a href="<?php //echo  get_category_link($category->term_id) ?>"><?php echo  $category->name; ?></a></li> -->
                     <li><a href="#"><?php echo  $category->name; ?></a></li>
                      <?php } ?>

                    <!-- <li><a href="#">design</a></li>
                    <li><a href="#">flooring</a></li>
                    <li><a href="#">painting</a></li>
                    <li><a href="#">plumbing</a></li>
                    <li><a href="#">renovation</a></li>
                    <li><a href="#">Commercial</a></li>
                    <li><a href="#">Residential Properties</a></li> -->

                  </ul>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="widget">
                  <h5 class="widget-title font-alt">Lastest Posts</h5>
                  <ul class="widget-posts">
                    <?php $the_query = new WP_Query( 'posts_per_page= 2' ); ?>
                    <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                    <li class="clearfix">
                      <div class="widget-posts-image"><a href="#"><!-- <img src="<?php //echo get_template_directory_uri(); ?>/images/post-img-01.png" alt="Post Thumbnail"> --> <?php the_post_thumbnail(); ?></a></div>
                      <div class="widget-posts-body">
                        <div class="widget-posts-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                        <div class="site-text widget-posts-meta"><?php echo get_the_date(); ?></div>
                      </div>
                    </li>
                    
                    <?php 
                      endwhile;
                      wp_reset_postdata(); ?>
                    <!-- <li class="clearfix m-t-b-20">
                      <div class="widget-posts-image"><a href="#"><img src="<?php // echo get_template_directory_uri(); ?>/images/post-img-02.png" alt="Post Thumbnail"></a></div>
                      <div class="widget-posts-body">
                        <div class="widget-posts-title"><a href="#">Designer Desk Essentials</a></div>
                        <div class="site-text widget-posts-meta">April 25 2017</div>
                      </div>
                    </li> -->

                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>



        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center copyright">
                    &copy; <?php the_field('copywrite' , 'option'); ?> <a target="_blank" href="#" title=""></a>
                </div>                
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/mousescroll.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/smoothscroll.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.isotope.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.inview.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

    <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        jQuery(document).ready(function() {
        jQuery("#example").DataTable();
        } );
    </script>
    <script type="text/javascript">
    
        $(window).load(function() {
        
            $("#flexiselDemo3").flexisel({
                visibleItems: 6,
                itemsToScroll: 1,         
                autoPlay: {
                    enable: true,
                    interval: 5000,
                    pauseOnHover: true
                }        
            });

            $("#flexiselDemo4").flexisel({
                infinite: false     
            });    

        });
</script>
<script type="text/javascript">

    var worksgrid   = $('#works-grid'),
            worksgrid_mode;

        if (worksgrid.hasClass('works-grid-masonry')) {
            worksgrid_mode = 'masonry';
        } else {
            worksgrid_mode = 'fitRows';
        }

        worksgrid.imagesLoaded(function() {
            worksgrid.isotope({
                layoutMode: worksgrid_mode,
                itemSelector: '.work-item'
            });
        });
    $('#filters a').click(function() {
        
            $('#filters .current').removeClass('current');
            $(this).addClass('current');
            var selector = $(this).attr('data-filter');

            worksgrid.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });

            return false;
        });

</script>

<script> 
$(document).ready(function(){
$( "li.menu-item" ).hover(function() {  // mouse enter
    $( this ).find( " > .sub-menu" ).show(); // display immediate child

}, function(){ // mouse leave
    if ( !$(this).hasClass("current_page_item") ) {  // check if current page
        $( this ).find( ".sub-menu" ).hide(); // hide if not current page
    }
});
});
</script>

<?php wp_footer(); ?>
</body>
</html>