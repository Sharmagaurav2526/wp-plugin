<?php
get_header();
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		
		the_post_thumbnail();
		the_content();
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
get_sidebar();
get_footer();
?>