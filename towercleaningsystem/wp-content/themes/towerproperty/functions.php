<?php 

//  To register a logo

$inc_path = TEMPLATEPATH . '/inc/';

function theme_prefix_setup() {
	
	add_theme_support( 'custom-logo', array(
		'height'      => 100,
		'width'       => 400,
		'flex-width' => true,
	) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );

//  To register nav-menus in Wordpress 
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'top-menu' => __( 'Top Menu' ),
      'footer-menu' => (' Footer Menu ')
    )
  );
}
add_action( 'init', 'register_my_menus' );

// To register a widget in wordpress
add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Sidebar 2', 'theme-slug' ),
        'id' => 'sidebar-2', 
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Sidebar 3', 'theme-slug' ),
        'id' => 'sidebar-3',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Recent Sidebar', 'theme-slug' ),
        'id' => 'sidebar-4',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}  
//  End register custom widgets in Wordpress
//   For register  feature image in Wordpress 
add_theme_support( 'post-thumbnails' ); 
// End function here

function add_registration_trial( $user_id ) {
    if ($_POST['role']) {
        if($_POST['role'] == 'owner'){ 
           wp_update_user( array( 'ID' => $user_id, 'role' => 'owner' ) );
        // update_user_meta( $user_id, 'as_capabilities', 'a:1:{s:5:"owner";b:1;}');
        }
        else {
            wp_update_user( array( 'ID' => $user_id, 'role' => 'resident' ) );
        } 
        
    }
}

add_action( 'user_register', 'add_registration_trial', 10, 1 );

/*add_role(
    'custom_editor',
    __( 'Custom Editor' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
    )
);
add_menu_page('Page title', 'Top-level menu title', 'manage_options', 'my-top-level-handle', 'my_magic_function');
add_submenu_page( 'my-top-level-handle', 'Page title', 'Sub-menu title', 'manage_options', 'my-submenu-handle', 'my_magic_function');*/

// function auto_redirect_external_after_logout(){
//   wp_redirect( 'http://towercleaningsystems.mobilytedev.com/login' );
//   exit();
// }
// add_action( 'wp_logout', 'auto_redirect_external_after_logout');

//show_admin_bar( true );
/*function bs_hide_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}
add_action('after_setup_theme', 'bs_hide_admin_bar');*/


/*function service_request_ajax_handler() {
  global $wpdb;
  $user_id = $_POST['user_id'];
  $email = $_POST['email'];
  $first_name = $_POST['first_name'];
  $last_name = $_POST['last_name'];
  $phone = $_POST['phone'];
  $address = $_POST['address'];
  $description = $_POST['description'];
  $unit = $_POST['unit'];
  $city = $_POST['city'];
  $user_type = $_POST['user_type'];

  $all_data = $wpdb->insert('as_service_request',array('user_id' => $user_id,'email' => $email,'first_name' => $first_name, 'last_name'=>$last_name,'phone' => $phone,'address' => $address, 'description' => $description, 'unit' => $unit, 'city' => $city, 'user_type' => $user_type),array(‘%s’));

  //$data = $wpdb->get_row( 'SELECT * FROM wp_options WHERE option_id = ' . $id, ARRAY_A );
  echo json_encode($all_data);
  return true;
  wp_die(); // just to be safe
}
add_action( 'wp_ajax_service_request_ajax_action', 'service_request_ajax_handler' );
add_action( 'wp_ajax_nopriv_service_request_ajax_action', 'service_request_ajax_handler' );*/

/*add_action('admin_init', 'disable_dashboard');

function disable_dashboard() {
    if (!is_user_logged_in()) {
        return null;
    }
    if (!current_user_can('administrator') && is_admin()) {
        wp_redirect(home_url()));
        exit;
    }
}*/


/*add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
if (!current_user_can('Administrator') && !is_admin()) {
  show_admin_bar(false);
}
}*/


/* ##### Service Request func ##### */

add_action("wp_ajax_service_request_ajax_handler", "service_request_ajax_handler");
add_action("wp_ajax_nopriv_service_request_ajax_handler", "service_request_ajax_handler");

function service_request_ajax_handler() {
    
   // echo "<pre>"; print_r($_POST); echo "</pre>";
    global $wpdb;
    //print_r($_POST);
      $user_id = $_POST['user_id'];
      $email = $_POST['resident_email'];
      $first_name = $_POST['resident_myName'];
      $last_name = $_POST['resident_lastname'];
      $phone = $_POST['resident_phone'];
      $address = $_POST['resident_address'];
      $description = $_POST['resident_description'];
      $unit = $_POST['resident_unit'];
      $city = $_POST['resident_city'];
      $user_type = $_POST['user_type'];
    //echo "<pre>"; print_r($user_id); echo "</pre>";
    $insert_array = array('user_id' => $user_id,
        'email' => $email,
        'first_name' => $first_name, 
        'last_name'=>$last_name,
        'phone_number' => $phone,
        'address' => $address, 
        'description' => $description, 
        'unit' => $unit, 
        'city' => $city, 
        'user_type' => $user_type,
        'services'=>'available',
        'status'=>pending
         );
    // echo "<pre>"; print_r($insert_array); echo "</pre>"; 
    $table_name = $wpdb->prefix."service_request";
    // $insert_id = $wpdb->insert($table_name, $insert_array );
    $insert_id = "INSERT INTO `as_service_request`( `user_id`, `first_name`, `last_name`, `email`, `phone_number`, `address`, `unit`, `user_type`, `description`, `services`, `status` , `city`) VALUES ('$user_id','$first_name','$last_name','$email','$phone','$address','$unit','$user_type','$description','avaliable','pending' , '$city')";
    $wpdb->query($insert_id);
   
    if($insert_id)
    {
       return true;
       //die('True');
    }
    else
    {
        return false;
        //echo "False";
    }
   
    //return true;
    die();
}





/* Owner Function Start */
add_action("wp_ajax_service_request_ajax_handlers", "service_request_ajax_handlers");
add_action("wp_ajax_nopriv_service_request_ajax_handlers", "service_request_ajax_handlers");

function service_request_ajax_handlers() {
    
   // echo "<pre>"; print_r($_POST); echo "</pre>"; die;
    global $wpdb;
    //print_r($_POST);
      $user_id = $_POST['user_id'];
      $email = $_POST['email'];
      $first_name = $_POST['myName'];
      $last_name = $_POST['lastname'];
      $phone = $_POST['phone'];
      $address = $_POST['address'];
      $description = $_POST['description'];     
      $user_type = $_POST['user_type'];
    //echo "<pre>"; print_r($user_id); echo "</pre>";
    $insert_array = array('user_id' => $user_id,
        'email' => $email,
        'first_name' => $first_name, 
        'last_name'=>$last_name,
        'phone_number' => $phone,
        'address' => $address, 
        'description' => $description,       
        'user_type' => $user_type,
        'services'=>'available',
        'status'=> 'pending'
         );
    // echo "<pre>"; print_r($insert_array); echo "</pre>"; 
    $table_name = $wpdb->prefix."service_request";
    $insert_id = $wpdb->insert($table_name, $insert_array );
   // $insert_id = "INSERT INTO `as_service_request`( `user_id`, `first_name`, `last_name`, `email`, `phone_number`, `address`, `user_type`, `description`, `services`, `status`) VALUES ('$user_id','$first_name','$last_name','$email','$phone','$address','$user_type','$description','avaliable','pending' )";
    //$wpdb->query($insert_id);
   
    if($insert_id)
    {
       return true;
       //die('True');
    }
    else
    {
        return false;
        //echo "False";
    }
   
    //return true;
    die();
}

/* Owner Function End */



/* Request CSV Function Start Here */

/* ##### Service Request func ##### */

add_action("wp_ajax_csv_request_ajax_handler", "csv_request_ajax_handler");
add_action("wp_ajax_nopriv_csv_request_ajax_handler", "csv_request_ajax_handler");

function csv_request_ajax_handler() {
    alert('yes');
    //echo "<pre>"; print_r($_POST); echo "</pre>";

    $filename=$_FILES["file"]["tmp_name"];    
  
 
     if($_FILES["file"]["size"] > 0)
     {
        $file = fopen($filename, "r");
          while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
           {
 
    $insert_csv = "INSERT INTO `as_csvimport`( `firstname`, `lastname`, `username`, `password`, `email`, `phone`) VALUES ('".$getData[0]."','".$getData[1]."','".$getData[2]."','".$getData[3]."','".$getData[4]."','".$getData[5]."')";
    $wpdb->query($insert_csv);
             
    if($insert_csv)
    {
       return true;
       //die('True');
    }
    else
    {
        return false;
        //echo "False";
    }
   
    //return true;
    die();

        /*if(!isset($insert_csv))
        {
          echo "<script type=\"text/javascript\">
              alert(\"Invalid File:Please Upload CSV File.\");
              window.location = \"index.php\"
              </script>";   
        }
        else {
            echo "<script type=\"text/javascript\">
            alert(\"CSV File has been successfully Imported.\");
            window.location = \"index.php\"
          </script>";
        }*/
           }
      
           fclose($file); 
     }



   /* global $wpdb;
    //print_r($_POST);
      $user_id = $_POST['user_id'];
      $email = $_POST['resident_email'];
      $first_name = $_POST['resident_myName'];
      $last_name = $_POST['resident_lastname'];
      $phone = $_POST['resident_phone'];
      $address = $_POST['resident_address'];
      $description = $_POST['resident_description'];
      $unit = $_POST['resident_unit'];
      $city = $_POST['resident_city'];
      $user_type = $_POST['user_type'];
    //echo "<pre>"; print_r($user_id); echo "</pre>";
    $insert_array = array('user_id' => $user_id,
        'email' => $email,
        'first_name' => $first_name, 
        'last_name'=>$last_name,
        'phone_number' => $phone,
        'address' => $address, 
        'description' => $description, 
        'unit' => $unit, 
        'city' => $city, 
        'user_type' => $user_type,
        'services'=>'available',
        'status'=>pending
         );*/
    // echo "<pre>"; print_r($insert_array); echo "</pre>"; 
    //$table_name = $wpdb->prefix."service_request";
    // $insert_id = $wpdb->insert($table_name, $insert_array );
    //$insert_id = "INSERT INTO `as_service_request`( `user_id`, `first_name`, `last_name`, `email`, `phone_number`, `address`, `unit`, `user_type`, `description`, `services`, `status` , `city`) VALUES ('$user_id','$first_name','$last_name','$email','$phone','$address','$unit','$user_type','$description','avaliable','pending' , '$city')";
    //$wpdb->query($insert_id);
   
    /*if($insert_id)
    {
       return true;
       //die('True');
    }
    else
    {
        return false;
        //echo "False";
    }
   
    //return true;
    die();*/
}






/* Request CSV Function End Here */









/* Request Service Function Start */

add_action("wp_ajax_service_request_ajax_handler_update", "service_request_ajax_handler_update");
add_action("wp_ajax_nopriv_service_request_ajax_handler_update", "service_request_ajax_handler_update");

function service_request_ajax_handler_update() {
    
    echo "<pre>"; print_r($_POST); echo "</pre>"; die;
    global $wpdb;
    //print_r($_POST);
      $user_id = $_POST['user_id'];
      $email = $_POST['email'];
      $first_name = $_POST['myName'];
      $last_name = $_POST['lastname'];
      $phone = $_POST['phone'];
      $address = $_POST['address'];
      $description = $_POST['description'];     
      $user_type = $_POST['user_type'];
    //echo "<pre>"; print_r($user_id); echo "</pre>";
    $insert_array = array('user_id' => $user_id,
        'email' => $email,
        'first_name' => $first_name, 
        'last_name'=>$last_name,
        'phone_number' => $phone,
        'address' => $address, 
        'description' => $description,       
        'user_type' => $user_type,
        'services'=>'available',
        'status'=>Pending
         );
    // echo "<pre>"; print_r($insert_array); echo "</pre>"; 
    $table_name = $wpdb->prefix."service_request";
    $insert_id = $wpdb->insert($table_name, $insert_array );
   // $insert_id = "INSERT INTO `as_service_request`( `user_id`, `first_name`, `last_name`, `email`, `phone_number`, `address`, `user_type`, `description`, `services`, `status`) VALUES ('$user_id','$first_name','$last_name','$email','$phone','$address','$user_type','$description','avaliable','pending' )";
    //$wpdb->query($insert_id);
   
    if($insert_id)
    {
       return true;
       //die('True');
    }
    else
    {
        return false;
        //echo "False";
    }
   
    //return true;
    die();
}

/* Request Service Function End */






/* ##### /Service Request func ##### */

//Added By SK for Service Requests
/** Step 1. */
function all_service_requests() {
    add_menu_page( 'Service Requests', 'All Requests', 'read', 'all-service-requests', 'my_plugin_options', 'dashicons-text', '3' );
}

/** Step 2 (from text above). */
add_action( 'admin_menu', 'all_service_requests' );

/** Step 3. */
function my_plugin_options() {

  if ( isset($_REQUEST['single']) ) {
    # code...
    $service_request_id = $_REQUEST['service-request-id'];
    require_once TEMPLATEPATH . '/inc/single_service_request_view.php';
  }
  else
  {

    if (is_user_logged_in()):
      global $wpdb;
      $name = "Wayne";
      $appTable = $wpdb->prefix . "service_request";
      $query = $wpdb->prepare("SELECT * FROM $appTable WHERE %d >= '0'", $name);
      $applications = $wpdb->get_results($query);    
    echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
      $("#example").DataTable();
      } );
    </script>';
      echo "<table id=example class='table table-striped table-bordered' style='width:100%'>";
      echo "<thead>
        <tr>
          <th>ID</th>
          <th>User ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Phone Number</th>
          <th>Unit</th>
          <th>City</th>
          <th>User Type</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>";
      foreach($applications as $application){
      echo "<tr>";
      echo "<td>".$application->id."</td>";
      echo "<td>".$application->user_id."</td>";
      echo "<td>".$application->first_name."</td>";
      echo "<td>".$application->last_name."</td>";
      echo "<td>".$application->email."</td>";
      echo "<td>".$application->phone_number."</td>";
      echo "<td>".$application->unit."</td>";
      echo "<td>".$application->city."</td>";
      echo "<td>".$application->user_type."</td>";
      echo "<td>".$application->status."</td>";
      echo "<td><a href='".admin_url('?page=all-service-requests&single=true&service-request-id='.$application->id)."';><span class='glyphicon glyphicon-eye-open' title='View Service Request'></span></a></td>";
      //echo "<td><a href='#' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-eye-open' title='View Service Request'></span></a> | <a href=''><span class='glyphicon glyphicon-trash' title='Delete Service Request'></span></a></td>";
      echo "</tr>";

      ?>
  <!-- The Modal -->   
  
  <?php 
      }
      echo "</table>";
      else:
      echo "Sorry, only registered users can view this information";
      endif;
      
      echo '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>';

  }
}

// Pending Bubble
add_action( 'admin_menu', 'add_testimonial_pending_bubble' );

function add_testimonial_pending_bubble() {

    global $menu;
    global $wpdb;
    //$custom_post_count = wp_count_posts('testimonials');
    $custom_post_count = $wpdb->get_results('SELECT * FROM  `as_service_request` where `status` = "pending" ');
    $custom_post_count = count($custom_post_count);
    //print_r($custom_post_count);die;
    //$custom_post_pending_count = $custom_post_count->pending;
    $custom_post_pending_count = $custom_post_count;
    if ( $custom_post_pending_count ) {
          //echo "<pre>"; print_r($menu); echo "</pre>"; 
        foreach ( $menu as $key => $value ) {
            
            //if ( $menu[$key][2] == 'edit.php?post_type=booking' ) {

                $menu[$key+1][0] .= " <i class='update-plugins count-1'>" . $custom_post_pending_count . '</i>';

                return;
            //}
        }
    }
}

add_action('admin_head', 'admin_custom_css');

function admin_custom_css() {
  echo '<style>
    i.my-notification{
        display: inline-block;
        background-color: #006300;
        padding: 0 5px;
        border-radius: 50%;
        font-weight: bold;
    }
  </style>';
}

// For Sub Menu
add_action('wp_enqueue_scripts', 'cssmenumaker_scripts_styles' );

function cssmenumaker_scripts_styles() {  
   wp_enqueue_style( 'cssmenu-styles', get_template_directory_uri() . '/css/styles-submenu.css');
}

class CSS_Menu_Maker_Walker extends Walker {

  var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul>\n";
  }

  function end_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "$indent</ul>\n";
  }

  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    global $wp_query;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $class_names = $value = '';        
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    /* Add active class */
    if(in_array('current-menu-item', $classes)) {
      $classes[] = 'active';
      unset($classes['current-menu-item']);
    }

    /* Check for children */
    $children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
    if (!empty($children)) {
      $classes[] = 'has-sub';
    }

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $value . $class_names .'>';

    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'><span>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= '</span></a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

  function end_el( &$output, $item, $depth = 0, $args = array() ) {
    $output .= "</li>\n";
  }
}


/*--======walker menu code start========--*/

class themeslug_walker_nav_menu extends Walker_Nav_Menu {
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        if ( array_search( 'menu-item-has-children', $item->classes ) ) {
            $output .= sprintf( "\n<li class='dropdown %s'><a href='%s' class=dropdown-toggle data-toggle=dropdown-toggle role=button aria-expanded=false >%s<span class='caret'></span></a>\n", ( array_search( 'current-menu-item', $item->classes ) || array_search( 'current-page-parent', $item->classes ) ) ? 'active' : '', $item->url, $item->title );
        } else {
            $output .= sprintf( "\n<li %s><a href='%s'>%s</a><li class='divider'></li>\n", ( array_search( 'current-menu-item', $item->classes) ) ? ' class=" "' : '', $item->url, $item->title );
        }
    }

    function start_lvl( &$output, $depth ) {
        $indent = str_repeat( "\t", $depth );
        $output .= "\n$indent<ul class=\"dropdown-menu\" role=\"menu\">\n";
    }
}

/*--======end walker menu code start========--*/
?>
