
j = jQuery; 

j(document).on('submit', '.service_request_update', function(event){

  event.preventDefault();

  alert("It's working");

});

jQuery(function(event){
 
  jQuery(".service_request").validate({
    rules: {
        resident_myName: {required: true},
        resident_lastname: {required: true},
        resident_email: {required: true, email:true},
        resident_phone: {required: true},
        resident_address: {required: true},
        resident_unit: {required: true},
        resident_city: {required: true},
        resident_description: {required: true},
    },

    submitHandler: function (form) {

        var request;

        // bind to the submit event of our form

        // let's select and cache all the fields
        var $inputs = jQuery("#service_request_resident").find("input, select, button");
        // serialize the data in the form
        var serializedData = jQuery("#service_request_resident").serialize();
        
        //$inputs.prop("disabled", true);

        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            dataType : "json",
            
            data : serializedData,

            success : function( response ) {
              //alert('asd');
              if(response == 'false'){
                //alert('asd');
                        //jQuery('.form-error-register').css('display','block');
                        swal("Wrong!", "Something Went Wrong!", "error");
                        jQuery('.service_request')[0].reset();
                        //jQuery('.form-login-error').css('display','block');
                        // jQuery('.error_msg').show();
                      }else{
                        swal("Success!", "Resident Service Request Added Successfully", "success");
                        jQuery('.service_request')[0].reset();
                        //alert('false');
                        //jQuery('.form-success-register').css('display','block');
                      }
                /*swal("Good job!", "You clicked the button!", "success"); 
                console.log(response);*/

            }
        });
      }
    });
  });



    /* Service Request For Owner Role */

  jQuery(function(  ){
 
  jQuery(".service_request_owner").validate({
    rules: {
        myName: {required: true},
        lastname: {required: true},
        email: {required: true, email:true},
        phone: {required: true},
        address: {required: true},
        description: {required: true}
    },

    submitHandler: function (form) {

        var request;
        var $inputs = jQuery("#service_request_owner").find("input, select, button");
     
        var serializedData1 = jQuery("#service_request_owner").serialize();
        console.log(serializedData1);
  
        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            dataType : "json",
            action : "service_request_ajax_handlers",
            data : serializedData1,

            success : function( response ) {
            
              if(response == 'false'){
               
                        swal("Wrong!", "Something Went Wrong!", "error");
                        jQuery('.service_request')[0].reset();
                      
                      }else{
                        swal("Success!", "Owner Service Request Added Successfully", "success");
                        //swal("Good job!", "Owner Service Request Added Successfully", "success");
                        jQuery('.service_request')[0].reset();
                        
                      }

            }
        });
      }
    });
  });


/* Import CSV In Table Start Here*/

jQuery(function(event){
       
        jQuery("#csv_request").validate({
          rules: {
        csvImport: "required"
          },
          submitHandler: function (form) {
                var request;
                var $inputs = jQuery("#csv_request").find("input, select, button, textarea , radio , checkbox")
                var serializedData = jQuery("#csv_request").serialize();
                var file = jQuery("#file").val();
                console.log(file);
                 jQuery.ajax({
                        url: ajaxurl,
                        type: "POST",
                        data: {
                            'action' : 'csv_request_ajax_handler',
                            'file'      : file,
                           },
                         success : function( response ) {
                    }
                });
            }
      });
    
    });


/*jQuery(function(  ){
    alert('inside here');
    var form_data = {};
    jQuery(this).find('input').each(function(){
        form_data[this.name] = jQuery(this).val();
    });
    jQuery('#csv_request').ajaxForm({
        alert('inside form request');
        url: ajaxurl, // there on the admin side, do-it-yourself on the front-end
        data: form_data,
        type: 'POST',
        action : "csv_request_ajax_handler",
        //contentType: 'json',
        success: function(response){
            alert(response.message);
        }
    });
});*/



/* Import CSV In Table End Here*/




/* Import CSV In Table Start Here*/

/*jQuery(function(  ){
 
  jQuery(".csv_request").validate({
    rules: {
        csvImport: {required: true}
        
    },

    submitHandler: function (form) {
        alert('i am here');
        var request;
        var $inputs = jQuery("#csv_request").find("input, select, button");
        //console.log($inputs);
        var serializedDataCsv = jQuery("#csv_request").serialize();
        console.log(serializedDataCsv);
  
        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            dataType : "json",
            action : "service_request_ajax_handlers",
            data : serializedData1,

            success : function( response ) {
            
              if(response == 'false'){
               
                        swal("Wrong!", "Something Went Wrong!", "error");
                        jQuery('.service_request')[0].reset();
                      
                      }else{
                        swal("Success!", "Owner Service Request Added Successfully", "success");
                        //swal("Good job!", "Owner Service Request Added Successfully", "success");
                        jQuery('.service_request')[0].reset();
                        
                      }

            }
        });
      }
    });
  });*/



/* Import CSV In Table End Here*/





  /* Service Request For Edit/Update  */

  jQuery(function(  ){
 
  jQuery(".service_request_update").validate({
    rules: {
        name: {required: true},
        lastname: {required: true},
        email: {required: true, email:true},
        phone: {required: true},
        address: {required: true},
        unit: {required: true},
        city: {required: true},
        description: {required: true},
        request_services: {required: true},
        status: {required: true}

    },

    submitHandler: function (form) {

        var request;
        var $inputs = jQuery("#service_request_update").find("input, select, button");
     
        var serializedData1 = jQuery("#service_request_update").serialize();
        console.log(serializedData1);
  
        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            dataType : "json",
            action : "service_request_ajax_handler_update",
            data : serializedData1,

            success : function( response ) {
            
              if(response == 'false'){
               
                        swal("Wrong!", "Something Went Wrong!", "error");
                        jQuery('.service_request')[0].reset();
                      
                      }else{
                        swal("Success!", "Service Request Updated Successfully", "success");
                        //swal("Good job!", "Owner Service Request Added Successfully", "success");
                        jQuery('.service_request')[0].reset();
                        
                      }

            }
        });
      }
    });
  });