<!DOCTYPE html>
<html lang="en" style="margin-top: 0 !important;">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Tower</title>
	<!-- core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">    
    <link href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/owl.transitions.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/main.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/ico/icon.png">
    <?php wp_head(); ?>  
    <script>var ajaxurl= '<?php echo admin_url( 'admin-ajax.php' ) ?>'</script>  
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
 
</head><!--/head-->

<body id="home" class="homepage">

    
    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            
            <div class="top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 text-left">
                            <ul class="top-bar">
                                <li><a href="<?php the_field('phone_url', 'option'); ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php the_field('phone_number', 'option'); ?></a></li>
                                <li><a href="<?php the_field('email_url', 'option'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <?php the_field('email', 'option'); ?></a></li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-4 text-right">
                            <ul class="social-icons">
                                  <?php if( have_rows('social_icon', 'option') ): ?>
                                  <?php while( have_rows('social_icon', 'option') ): the_row(); ?>
                                 <li><a href="<?php the_sub_field('icon_url'); ?>"><i class="<?php the_sub_field('icon'); ?>"></i></a></li>
                                 <?php endwhile; ?>
                                 <?php endif; ?>
                                <!-- <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li> -->                            
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container m-t-40">                
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
                    $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                    ?>
                    <a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php  echo $image[0]; ?>" alt="logo"></a> 
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <!-- <ul class="nav navbar-nav">
                        <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Services</a>
                            <ul class="dropdown-menu">
                              <li><a href="#">Services 01</a></li>
                              <li><a href="#">Services 02</a></li>
                              <li><a href="#">Services 03</a></li>
                              <li><a href="#">Services 04</a></li>
                              <li><a href="#">Services 05</a></li>
                            </ul>
                          </li>                        
                        <li class="scroll"><a href="#features">homes for rent</a></li>
                        <li class="scroll"><a href="#services">about us</a></li>
                        <li class="scroll"><a href="#portfolio">contact us</a></li>                                                
                    </ul> -->
                    <?php //wp_nav_menu( array( 'theme_location' => 'header-menu' , 'container' => 'ul' , 'menu_class' => 'nav navbar-nav ') ); ?>
					<?php   wp_nav_menu(array(
                    'theme_location' => 'header-menu',
                    'container' => 'ul',
                    'menu_class' => 'nav navbar-nav ',
                    'echo' => true,
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'depth' => 0,
                    )
                  ); ?>

                    <?php  
                        //wp_nav_menu(array(  'menu' => 'Main Navigation',  'container_id' => 'cssmenu', 'walker' => new CSS_Menu_Maker_Walker(),'menu_class' => 'nav navbar-nav')); 
                    ?>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->