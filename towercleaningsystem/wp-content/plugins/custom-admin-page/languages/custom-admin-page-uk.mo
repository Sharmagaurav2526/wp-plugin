��    :      �  O   �      �  	   �                      
   (     3     @     H     c     v  	   {     �  K   �     �  x   �  I   j     �     �     �     �     �     �     �          !     $     -     3     C  9   O  ;   �     �     �  	   �     �     �     �  #   �       '   :  ,   b     �     �     �     �     �  H   �  2   	     O	     U	     [	     g	  	   p	     z	     	     �	  �  �	  !     &   -     T     [     n     �  #   �  
   �  *   �  #   �     "  %   7  8   ]  �   �  *   D  �   o  �   R     �     �  [      #   \  !   �  7   �  2   �  '     
   5     @     Z  !   i  *   �  r   �  g   )  &   �     �  #   �     �     �  
     9     @   R  E   �  I   �     #     <  (   E  "   n     �  �   �     +  
   G  
   R     ]     }     �     �     �     �     6      '                  +   
                 1                -       "       ,      !              )   	   :   #   .       0   %   4   2   5             9      8   (      *             /   $   3                       7                                 &                             Add Image Add New Page All Cancel Capabilities Capability Change Image Content Custom Admin Page Settings Delete Permanently Edit Edit page Edit permalink Enter %s to leave div.wp-menu-image empty so the icon can be added via CSS. Enter Page Title Enter a base64-encoded SVG using a data URI, which will be colored to match the color scheme. This should begin with %s. Enter the name of the Dashicons helper class to use a font icon, e.g. %s. FAQ Icon Icon URL for this menu. Level Levels No pages found in Trash. No pages were selected. No pages yet. OK Optional Order Page Attributes Page filter Page has been saved. Refresh the page to see the changes. Page has been updated. Refresh the page to see the changes. Pages per page Parent Permalink Restore Save Search Search results for &#8220;%s&#8221; Selected pages were deleted. Selected pages were moved to the trash. Selected pages were restored from the trash. Settings Slug Some errors occurred Such name already exists Support The capability level required for this menu to be displayed to the user. The order in the menu where this page will appear. Title Trash Update page in trash no parent page pages see Project-Id-Version: custom-admin-page
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-21 17:23+0200
PO-Revision-Date: 2018-03-21 17:23+0200
Last-Translator: plugin@bestwebsoft.com <plugin@bestwebsoft.com>
Language-Team: bestwebsoft.com <plugin@bestwebsoft.com>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;_n:1,2
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.8.7.1
Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n%10==1 && n%100!=11) ? 3 : ((n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20)) ? 1 : 2);
X-Poedit-SearchPath-0: .
 Додати зображення Додати нову сторінку Всі Скасувати Рівні доступу Рівень доступу Змінити зображення Зміст Налаштування Custom Admin Page Видалити остаточно Редагувати Редагувати сторінку Редагувати постійне посилання Введіть %s, щоб залишити div.wp-menu-image порожнім, таким чином іконка може бути додана за допомогою CSS. Введіть назву сторінки Введіть base64-кодоване SVG за допомогою даних URI, яке буде забарвлено відповідно до колірної схеми. Воно повинно починатися з %s. Введіть ім'я допоміжного класу Dashicons, щоб використовувати значок шрифту, наприклад, %s. FAQ Значок URL-адресу іконки, використовуваної для цього меню. Рівень користувача Рівні користувача Сторінок в кошику не знайдено. Не вибрано жодної сторінки. Сторінок не знайдено. Добре Необов'язково Порядок Атрибути сторінки Фільтр списку сторінок Дані сторінки збережені. Оновіть сторінку, щоб побачити зміни. Сторінка оновлена. Оновіть сторінку, щоб побачити зміни. Сторінок на сторінці Предок Постійне посилання Відновити Зберегти Пошук Результати пошуку для &#8220;%s&#8221; Обрані сторінки видалено назавжди. Обрані сторінки переміщено до кошика. Обрані сторінки відновлено ​​з кошика. Налаштування Слаг Виникли деякі помилки Таке ім'я вже існує Техпідтримка Рівень доступу цього меню необхідний для відображення користувачеві. Позиція в меню. Назва Кошик Оновити сторінку в кошику без предка сторінка сторінки дивись 