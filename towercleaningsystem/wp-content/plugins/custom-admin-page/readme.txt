=== Custom Admin Page by BestWebSoft ===
Contributors: bestwebsoft
Donate link: https://bestwebsoft.com/donate/
Tags: custom admin page, add admin page, add custom page, add admin menu, custom admin menu, dashboard page, custom dashboard, create custom page, create admin pages, unlimited custom pages, castom pages, cutsom pages
Requires at least: 3.9
Tested up to: 4.9.4
Stable tag: 0.1.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add unlimited custom pages to WordPress admin dashboard.

== Description ==

Simple plugin which adds unlimited custom pages to your WordPress admin dashboard. Add as many pages as you want, customize the appearance using TinyMCE, choose page position and icon.

Download, install, and add your own admin pages in a few clicks!

https://www.youtube.com/watch?v=Ecvb7F2sATs

= Features =

* Add unlimited number of custom pages
* Manage your custom page using TinyMCE editor
* Make custom pages available for certain user roles
* Add custom page to parent page
* Set custom page order
* Choose custom page icon for the sidebar menu
* Compatible with latest WordPress version
* Incredibly simple settings for fast setup without modifying code
* Detailed step-by-step documentation and videos

If you have a feature suggestion or idea you'd like to see in the plugin, we'd love to hear about it! [Suggest a Feature](https://support.bestwebsoft.com/hc/en-us/requests/new)

= Documentation & Videos =

* [[Doc] Installation](https://docs.google.com/document/d/1-hvn6WRvWnOqj5v5pLUk7Awyu87lq5B_dO-Tv-MC9JQ/)

= Help & Support =

Visit our Help Center if you have any questions, our friendly Support Team is happy to help - <https://support.bestwebsoft.com/>

= Translation =

* Russian (ru_RU)
* Ukrainian (uk)

Some of these translations are not complete. We are constantly adding new features which should be translated. If you would like to create your own language pack or update the existing one, you can send [the text of PO and MO files](https://codex.wordpress.org/Translating_WordPress) to [BestWebSoft](https://support.bestwebsoft.com/hc/en-us/requests/new) and we'll add it to the plugin. You can download the latest version of the program for work with PO and MO files [Poedit](https://www.poedit.net/download.php).

= Recommended Plugins =

* [Updater](https://bestwebsoft.com/products/wordpress/plugins/updater/?k=f33e1bb49ee6d97b299ba2d41e6ff4c2) - Automatically check and update WordPress website core with all installed plugins and themes to the latest versions.

== Installation ==

1. Upload the `custom-admin-page` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin using the 'Plugins' menu in your WordPress admin panel.
3. You can adjust the necessary settings using your WordPress admin panel in "BWS Panel" > "Custom Admin Page".

== Frequently Asked Questions ==

= I have some problems with the plugin's work. What Information should I provide to receive proper support? =

Please make sure that the problem hasn't been discussed yet on our forum (<https://support.bestwebsoft.com>). If no, please provide the following data along with your problem's description:

1. the link to the page where the problem occurs
2. the name of the plugin and its version. If you are using a pro version - your order number.
3. the version of your WordPress installation
4. copy and paste into the message your system status report. Please read more here: [Instruction on System Status](https://docs.google.com/document/d/1Wi2X8RdRGXk9kMszQy1xItJrpN0ncXgioH935MaBKtc/)

== Screenshots ==

1. Plugin`s settings page.
2. Add New Page using the plugin.
3. Custom page added by the plugin.
4. Custom child page added by the plugin.


== Changelog ==

= V0.1.5 - 23.03.2018 =
* Update : We updated all functionality for wordpress 4.9.4.

= V0.1.4 - 22.03.2018 =
* Bugfix : Bugs related to the pictures size in the admin menu and capability level have been fixed.

= V0.1.3 - 12.07.2017 =
* Update : We updated all functionality for wordpress 4.8.

= V0.1.2 - 17.04.2017 =
* Bugfix : Multiple Cross-Site Scripting (XSS) vulnerability was fixed.

= V0.1.1 - 03.10.2016 =
* NEW : An ability to set certain capability instead of capability level.

= V0.1 - 28.06.2016 =
* NEW : The Custom Admin Page by BestWebSoft plugin is ready for use.

== Upgrade Notice ==

= V0.1.5 =
* The compatibility with new WordPress version updated.

= V0.1.4 =
* Bugs fixed.
* Usability improved.

= V0.1.3 =
* The compatibility with new WordPress version updated.

= V0.1.2 =
* Bugs fixed.

= V0.1.1 =
* Functionality expanded.

= V0.1 =
The Custom Admin Page by BestWebSoft plugin is ready for use.
