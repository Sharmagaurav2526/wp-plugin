<?php

/*
 * Sends an email to someone via Gmail SMTP. Returns the number of successful sends (so 0 = failure, 1 = success)
 * You generally should not call this function directly - use ucp_iContact_sendEmail() instead for better logging, & queuing in case of initial failure
 */
function ucp_iContact_processEmail($email, $subject, $html_body = '', $text_body = '', $name=''){
	global $settings;
	$debug = $settings['debug'];
	
	require_once(dirname(__FILE__).'/swiftlib/swift_required.php');
	
	if(empty($email) or empty($subject) or (empty($html_body) and empty($text_body)) ){
		ucp_iContact_debugMsg('Error in ucp_iContact_processEmail(): not all arguments supplied!');
		return false;
	}
	
	$message = Swift_Message::newInstance()
		->setSubject($subject)
		->setFrom(array($settings['subscriptions_email_address']=>$settings['subscriptions_email_name']));
	
	if(empty($html_body)) $message->setBody($text_body, 'text/plan');
	else{
		$message->setBody($html_body, 'text/html');
		if(!empty($text_body)) $message->addPart($text_body, 'text/plain');
	}
	
	if(!empty($name)) $message->setTo(array($email => $name));
	else $message->setTo(array($email));
	
	if($debug) echo '<br />ucp_iContact_processEmail() about to initialize transport<br />';
	
	$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
		->setUsername($settings['subscriptions_email_address'])
		->setPassword($settings['subscriptions_email_password'])
	;
	
	$mailer = Swift_Mailer::newInstance($transport);
	
	if($debug) echo '<br />ucp_iContact_processEmail() done initializing transport<br />';
	
	$exception = false;
	
	try{
		$result = $mailer->send($message, $failures);
	}
	catch(Exception $e){
		$exception = true;
		ucp_iContact_debugMsg('Exception in ucp_iContact_processEmail(): '.$e->getMessage());
	}
	
	if($exception){
		ucp_iContact_debugMsg('ucp_iContact_processEmail() reports swiftmailer result: <pre>' . print_r($result, true) . '</pre>');
	}
	elseif($debug === 'verbose' and !$exception){
		echo '<br />ucp_iContact_processEmail() reports swiftmailer result: <br /><pre>';
		print_r($result);
		echo '</pre><br /><br />';
	}
	
	if(empty($result)){
		if($debug){
			$failures_text = 'Could not deliver to: ';
			foreach($failures as $failure){
				$failures_text .= $failure.'<br />';
			}
			ucp_iContact_debugMsg('Failure in ucp_iContact_processEmail(): '.$failures_text);
		}
	}
	
	return $result;
}

?>