<?php

function ucp_iContact_doSignupForm($atts){
	wp_enqueue_script('jquery');
	global $settings;
	$debug = $settings['debug'];
	
	$submission_made = false;
	$submission_validated = false;
	
	ob_start();
	
	if($settings['debug']) echo '<strong>Debug mode on.</strong><br />';
	
	if(isset($_POST['formsubmitted'])){
		$submission_made = true;
		$existing_id = false;
		
		ucp_iContact_stripPOSTslashes();
		
		if(empty($_POST['address2'])) $_POST['MailingStreet'] = $_POST['address1'];
		else $_POST['MailingStreet'] = $_POST['address1'] . "\n" . $_POST['address2'];
		
		// Validate input
		$stop_errors = ucp_iContact_validateUserInfo($_POST, array('Email', 'FirstName', 'LastName', 'newsletters'));
		
		// If no errors so far, check if there's a pending submission in the MySQL queue
		if(empty($stop_errors)) {
			$already_signed_up = ucp_iContact_isEmailInSignupTable($_POST['Email']);
			if($already_signed_up) $stop_errors[] = '<p>' . $_POST['Email'] . ' is already pending approval for newsletter signup(s). Please follow the verification steps in the email you received.</p><p>If you did not receive an email, your submission will time out within ' . $settings['submission_good_for'] . ' hours, and you can try again.</p><p>If you are having problems receiving the verification email, please contact <a href="'.$settings['subscriptions_email_address'].'">'.$settings['subscriptions_email_address'].'</a>.</p>';
		}
		
		// If no errors so far, check if there's an existing Contact in Salesforce
		if(empty($stop_errors)){
			$results = ucp_iContact_getSFContactByEmail($_POST['Email'], array('Id', 'Email', 'HasOptedOutOfEmail', 'Email_subscriptions__c'));
			if($debug === 'verbose'){
				echo 'SQL Query result:<br />';
				print_r($results);
				echo '<br />';
			}
			if(is_array($results)){
				if(count($results) > 1){
					$stop_errors[] = 'We already have more than one entry for the email address <strong>' . $_POST['Email'] . '</strong>! Please contact <a href="mailto:support@ucp.org">support@ucp.org</a> and let us know your email address and that you encountered this error.';
				}
				elseif(count($results) == 1){
					if($results[0]->fields->HasOptedOutOfEmail !== 'false'){
						$stop_errors[] = 'This address has opted out of UCP publications entirely. To reverse this you must contact <a href="mailto:'.$settings['subscriptions_email_address'].'">'.$settings['subscriptions_email_address'].'</a> personally.';
						$existing_id = true;
					}
					elseif($results[0]->fields->Email_subscriptions__c !== 'true'){
						$subscription_edit_url = ucp_iContact_getURLForShortcode('icontact_sub_edit');
						$stop_errors[] = 'This email address already subscribed to one or more UCP publications at some point. Please <a href="'.$subscription_edit_url.'">edit your subscriptions</a> instead of using this form.';
						$existing_id = true;
					}
					else $existing_id = $results[0]->Id;
				}
			}
			else $stop_errors[] = 'Error checking email address: unknown database error. Please report this error and your email address ' . $_POST['Email'] . ' to <a href="mailto:support@ucp.org">support@ucp.org</a>';
		}
		
		ucp_iContact_sanitizePOSTForHTML();
		
		// If there are errors, display them
		if(!empty($stop_errors)){
			?>
			<div class="newsletter-errors"><p><strong>There were problems submitting your request.</strong></p><ul>
			<?php
			foreach($stop_errors as $an_error) echo '<li>' . $an_error . '</li>';
			?>
			</ul></div>
			<?php
			if($debug === 'verbose'){
				echo '<br /><strong>POST</strong><br />';
				foreach($_POST as $post_name => $post_value){
					echo $post_name . ' => ' . $post_value . '<br />';
				}
				if(!empty($_POST['newsletters']) or true){
					echo '<br />';
					foreach($_POST['newsletters'] as $newsl_name => $newsl_value){
						echo $newsl_name . ' => ' . $newsl_value . '<br />';
					}
				}
			}
		}
		else{ // Everything's validated, so now let's enter it in the MySQL database
			$submission_validated = true;
			
			ucp_iContact_submitSignupForm($_POST, $existing_id);
		}
	}
	
	if(!$submission_validated){
		$char_coding = ENT_QUOTES | ENT_HTML401;
		ucp_iContact_getStylesheet();
		
		$primary_newsletters = ucp_iContact_getPrimaryMailings();
		
		if(!empty($_GET['Email'])) $email_preset = htmlspecialchars($_GET['Email']);
		
		if(!empty($_POST['Salutation'])) $salutation_options_html = ucp_iContact_outputSalutationSelectOptions($_POST['Salutation']);
		else $salutation_options_html = ucp_iContact_outputSalutationSelectOptions();
		
		if(!empty($_POST['ucp_contact_suffix__c'])) $suffix_options_html = ucp_iContact_outputSuffixSelectOptions($_POST['ucp_contact_suffix__c']);
		else $suffix_options_html = ucp_iContact_outputSuffixSelectOptions();
		?>
		
		<form name="ucp-newsletter-signup" id="ucp-newsletter-signup" class="ucp-newsletter" method="POST" action="<?php echo ucp_iContact_getCurrentURL(); ?>">
			<div class="contactfields">
				<input type="hidden" name="formsubmitted" value="true" />
				<p><span class="label"><label for="email">Email</label>*</span> <input type="text" name="Email" id="email" maxlength="80" placeholder="name@domain.com" value="<?php if(!empty($_POST['Email'])) echo $_POST['Email']; elseif(isset($email_preset)) echo $email_preset; ?>" required="required" /></p>
				<p><span class="label"><label for="salutation">Prefix</label></span> <select name="Salutation" id="salutation"><?php echo $salutation_options_html; ?></select></p>
				<p><span class="label"><label for="firstname">First Name</label>*</span> <input type="text" name="FirstName" id="firstname" maxlength="40" value="<?php if(!empty($_POST['FirstName'])) echo $_POST['FirstName']; ?>" required="required" /></p>
				<p><span class="label"><label for="middlename">Middle Name/Initial</label></span> <input type="text" name="ucp_contact_middle_name__c" id="middlename" maxlength="60" value="<?php if(!empty($_POST['ucp_contact_middle_name__c'])) echo $_POST['ucp_contact_middle_name__c']; ?>" /></p>
				<p><span class="label"><label for="lastname">Last Name</label>*</span> <input type="text" name="LastName" id="lastname" maxlength="80" value="<?php if(!empty($_POST['LastName'])) echo $_POST['LastName']; ?>" required="required" /></p>
				<p><span class="label"><label for="suffix">Suffix</label></span> <select name="ucp_contact_suffix__c" id="suffix"><?php echo $suffix_options_html; ?></select></p>
				<p><span class="label"><label for="email_sub_organization__c">Organization</label></span> <input type="text" name="email_sub_organization__c" id="email_sub_organization__c" maxlength="100" value="<?php if(!empty($_POST['email_sub_organization__c'])) echo $_POST['email_sub_organization__c']; ?>" /></p>
				<p>
					<span class="label"><label for="country">Country</label></span> <select name="MailingCountry" id="country">
					<?php if(!empty($_POST['MailingCountry'])) ucp_iContact_outputCountrySelectOptions($_POST['MailingCountry']);
					else ucp_iContact_outputCountrySelectOptions(); ?>
					</select>
				</p>
				<p><span class="label"><label for="province">State/Province</label></span> <input type="text" name="MailingState" id="province" maxlength="80" value="<?php if(!empty($_POST['MailingState'])) echo $_POST['MailingState']; ?>" /></p>
				<p><span class="label"><label for="city">City</label></span> <input type="text" name="MailingCity" id="city" maxlength="40" value="<?php if(!empty($_POST['MailingCity'])) echo $_POST['MailingCity']; ?>" /></p>
				<p><span class="label"><label for="postalcode">Zip/Postal Code</label></span> <input type="text" name="MailingPostalCode" id="postalcode" maxlength="20" value="<?php if(!empty($_POST['MailingPostalCode'])) echo $_POST['MailingPostalCode']; ?>" /></p>
				<p><span class="label"><label for="address1">Address</label></span> <input type="text" name="address1" id="address1" maxlength="255" value="<?php if(!empty($_POST['address1'])) echo $_POST['address1']; ?>" /></p>
				<p><span class="label"><label for="address2">Address Line 2</label></span> <input type="text" name="address2" id="address2" maxlength="255" value="<?php if(!empty($_POST['address2'])) echo $_POST['address2']; ?>" /></p>
				<p><span class="label"><label for="phone">Phone</label></span> <input type="tel" name="Phone" id="phone" maxlength="40" value="<?php if(!empty($_POST['Phone'])) echo $_POST['Phone']; ?>" /></p>
				<p></p><strong>*required</strong></p>
			</div>
			<div class="newslettersbox">
				<div>
					<p style="float:left;">Please sign me up for...</p>
					<p id="newsletters-toggle-container" style="float:right; display:none;"><input type="checkbox" name="newsletters-toggle" id="newsletters-toggle" value="newsletters-toggle"<?php if(!$submission_made or !empty($_POST['newsletters-toggle'])) echo ' checked="checked"'; ?> /> <label class="newsletter" for="newsletters-toggle">Everything</label></p>
				</div>
				<?php foreach($primary_newsletters as $a_newsletter):
					$friendly_name = preg_replace("/[^A-Za-z0-9]/", '', $a_newsletter->sf_name);
					if($a_newsletter->isIndie()): ?>
						<div>
							<input type="checkbox" id="indie-<?php echo $friendly_name; ?>" disabled="disabled" /> <label class="newsletter" for="indie-<?php echo $friendly_name; ?>">
							<?php if(!empty($a_newsletter->indie_link)): ?><a href="<?php echo $a_newsletter->indie_link; ?>" target="_blank"><?php endif; ?>
							<?php echo $a_newsletter->label; ?>
							<?php if(!empty($a_newsletter->indie_link)): ?></a><?php endif; ?>
							</label><br />
							<p class="newsletter-desc"><?php echo $a_newsletter->description; ?></p>
						</div>
					<?php elseif($a_newsletter->isActive()): ?>
						<div>
							<input type="checkbox" name="newsletters[]" id="newsletters-<?php echo $friendly_name; ?>" value="<?php echo $a_newsletter->sf_name; ?>"<?php if(!$submission_made or (isset($_POST['newsletters']) and in_array($a_newsletter->sf_name, $_POST['newsletters']))) echo ' checked="checked"'; ?> /> <label class="newsletter" for="newsletters-<?php echo $friendly_name; ?>"><?php echo $a_newsletter->label; ?></label><br />
							<p class="newsletter-desc"><?php echo $a_newsletter->description; ?></p>
						</div>
					<?php else: ?>
						<div style="opacity: 0.75;">
							<input type="checkbox" id="unlinkednewsletter-<?php echo $friendly_name; ?>" disabled="disabled" /> <label class="newsletter" for="unlinkednewsletter-<?php echo $friendly_name; ?>"><?php echo $a_newsletter->label; ?></label><br />
							<p class="newsletter-desc"><?php echo $a_newsletter->description; ?></p>
						</div>
				<?php endif;
				endforeach; ?>
			</div>
			<p class="submit"><input type="submit" name="newsletter-submit" id="newsletter-submit" class="submit" value="Sign me up!" /></p>
		</form>
		
		<script type="text/javascript">
			jQuery('#newsletters-toggle-container').css('display', 'block');
			
			jQuery('#newsletters-toggle').change(function() {
				if(this.checked) {
					jQuery('input[name=newsletters\\[\\]').prop('checked', true);
				}
				else{
					jQuery('input[name=newsletters\\[\\]').prop('checked', false);
				}
			});
			
			jQuery('input[name=newsletters\\[\\]').change(function(){
				if(!this.checked) {
					jQuery('#newsletters-toggle').prop('checked', false);
				}
				else if(jQuery('input[name=newsletters\\[\\]').not(':checked').length == 0) {
					jQuery('#newsletters-toggle').prop('checked', true);
				}
			});
		</script>
		<?php
	}
	
	return ob_get_clean();
}
if(function_exists('add_shortcode')){
	add_shortcode('icontact_signup', 'ucp_iContact_doSignupForm');
}


/*
 * Enters signup data into SQL.
 * Echoes results directly in function, as it is assumed this will be executed in the context of page output.
 */
function ucp_iContact_submitSignupForm($data, $existing_id){
	global $settings;
	$subscriptions_email_address = $settings['subscriptions_email_address'];
	
	$verification_code = ucp_iContact_randomString(20);
	
	$insert_result = ucp_iContact_insertSignupDataToSQL($data, $verification_code, $existing_id);
	
	if($insert_result){   // we've cached their signup in SQL, so mail them their verification stuff
		if(!empty($data['FirstName']) and !empty($data['LastName']) and !empty($data['ucp_contact_middle_name__c'])) $full_name = $data['FirstName'] . ' ' . $data['ucp_contact_middle_name__c'] . ' ' . $data['LastName'];
		elseif(!empty($data['FirstName']) and !empty($data['LastName'])) $full_name = $data['FirstName'] . ' ' . $data['LastName'];
		elseif(empty($data['FirstName']) and $empty($data['LastName'])) $full_name = '';
		elseif(!empty($data['FirstName'])) $full_name = $data['FirstName'];
		else $full_name = $data['LastName'];
		
		$verification_page_url = ucp_iContact_getURLForShortcode('icontact_signup_verification');
		if(empty($verification_page_url)) $verification_page_url = 'PLACEHOLDER';
		
		$verification_page_url_prefilled = $verification_page_url . '?Email=' . rawurlencode($data['Email']) . '&SignupVerificationCode=' . rawurlencode($verification_code);
		
		$email_template_html = <<<EOT
<html><body>
<center><img src="http://ucplabs.org/mailassets/icontact_subs/UCPLogo.png" alt="United Cerebral Palsy logo" style="margin: 5 auto;" /></center>
<p>You or someone with your email address requested to be subscribed to the following UCP newsletters:</p>
<ul>
EOT;
		foreach($data['newsletters'] as $a_newsletter) $email_template_html .= "<li>".$a_newsletter."</li>";
		$email_template_html .= <<<EOT
</ul>
<p>To confirm your request and complete your subscription, please visit <a href="$verification_page_url_prefilled">$verification_page_url_prefilled</a> - or, go to <a href="$verification_page_url">$verification_page_url</a> and input your email address and this code: <b>$verification_code</b></p>
<p>If you did not subscribe this email address, you can ignore this message and the subscription will not be finalized.</p>
<p>You may also contact us at: <a href="$subscriptions_email_address">$subscriptions_email_address</a></p>
</body></html>
EOT;
		$email_template_plain = <<<EOT
You or someone with your email address requested to be subscribed to the following UCP newsletters:
EOT;
		foreach($data['newsletters'] as $a_newsletter) $email_template_plain .= $a_newsletter."\n";
		$email_template_plain .= <<<EOT

To confirm your request and complete your subscription, please visit $verification_page_url_prefilled - or, go to $verification_page_url and input your email address and this code: $verification_code

If you did not subscribe this email address, you can ignore this message and the subscription will not be finalized.

You may also contact us at: $subscriptions_email_address
EOT;
		
		$mailing_result = ucp_iContact_sendEmail(0, $data['Email'], $subject = 'Please confirm your subscription', $email_template_html, $email_template_plain, $full_name, $insert_result);
		
		if(!empty($mailing_result)){
			echo '<p><strong>Thank you for requesting a subscription.</strong> We\'ve sent you a verification email from '.$subscriptions_email_address.'. Please follow the instructions in this email to finalize your subscription.</p>';
		}
		else{
			echo '<p><strong>We were unable to email you at this time.</strong> Thank you for requesting a subscription. We need you to confirm receipt of an email message before the subscription will be finalized. We\'ll try to reach you again later. If this continues to be a problem, you can reach us at <a href="'.$subscriptions_email_address.'">'.$subscriptions_email_address.'</a>.';
		}
	}
	else echo '<p><strong>Error: Could not submit subscription request!</strong></p>';
}

?>