<?php
/*
Plugin Name: UCP National iContact forms
Description: Implements signup and subscription-editing forms for UCP's iContact for Salesforce
Version: v2015.05.29
Author: Benjamin Voynick
Author URI: mailto:support@ucp.org

   Usage:
   Use shortcode [icontact_signup] to place the signup form inside a page.
   Use shortcode [icontact_signup_verification] to place the signup verificaton form inside a page.
   Use shortcode [icontact_sub_edit] to place the subscription editing form inside a page.
*/

if ( !defined('ABSPATH') ) die("Could not find WordPress.");

require_once(ABSPATH .'/developerforce/api.php');   // Salesforce production instance

require_once(ABSPATH .'/whitelist/whitelist.php');

require_once(dirname(__FILE__).'/settings.php');
$settings = ucp_iContact_getSettings();

require_once(dirname(__FILE__).'/databases.php');
require_once(dirname(__FILE__).'/smtp.php');



// classes
class ucp_mailingList {
	public $is_active_value = false;
	public $is_primary = false;
	public $sf_name = null;
	public $label = null;
	public $description = '';
	public $indie = false;
	public $indie_link = '';
	
	function __construct($sf_name, $label, $description = null, $primary = false, $indie = false, $indie_url = ''){
		if(!empty($sf_name)) $this->sf_name = $sf_name;
		if(!empty($label)) $this->label = $label;
		if(!empty($description)) $this->description = $description;
		if($primary) $this->setPrimary();
		if($indie){
			$this->setIndie();
			if(!empty($indie_url)) $this->setIndieLink($indie_url);
		}
		
		$newsletter_object = ucp_iContact_getSFMetadata();
		if(empty($newsletter_object)){
			if(!defined("UCP_ICONTACT_METADATA_FAILURE_LOGGED")){
				trigger_error("icontact-sub.php received a metadata failure when creating a ucp_mailingList object", E_USER_WARNING);
				define("UCP_ICONTACT_METADATA_FAILURE_LOGGED", true);
			}
		}
		else{
			foreach($newsletter_object->picklistValues as $picklist_value){
				if($picklist_value->value === $this->sf_name){
					$this->setActive();
					//break;
				}
			}
		}
	}
	
	function isPrimary(){
		return $this->is_primary;
	}
	
	function isActive(){
		return $this->is_active_value;
	}
	
	function isIndie(){
		return $this->indie;
	}
	
	function setActive(){
		$this->is_active_value = true;
	}
	
	function setPrimary(){
		$this->is_primary = true;
	}
	
	function setDescription($description){
		$this->description = $description;
	}
	
	function setIndie($boolean = true){
		if(!$boolean) $this->indie = false;
		else $this->indie = true;
	}
	
	function setIndieLink($url = ''){
		$this->indie_link = $url;
	}
}



/*
 * Trigger a PHP error of customizable level (default Warning)
 *
 * If Debug option is on, echo it too.
 */
function ucp_iContact_debugMsg($msg, $error_level = E_USER_WARNING){
	trigger_error($msg, $error_level);
	
	global $settings;
	$debug = $settings['debug'];
	
	if($debug) echo '<br>' . $msg . '<br>';
}

 

/*
 * Create a random string
 * @author XEWeb <http://www.xeweb.net> (modified from original)
 * @param $length the length of the string to create
 * @return $str the string
 */
function ucp_iContact_randomString($length) {
	global $settings;
	
	if(empty($length)) return false;
	
	$token_characters = array_merge(range('A','Z'), range('a','z'), range('0','9')); //Array of possible token characters. Remember that this has to be stored as text in Salesforce and passed by URL, so characters must be friendly to both of those things
	$str = "";
	
	$max = count($token_characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $token_characters[$rand];
	}
	return $str;
}


/*
 * Generate a new 32-character string and make sure no other Contact records have this string as their identifier in the "Email subscription token" field.
 * Will return the first unique token, or give up after 100 tries.
 */
function ucp_iContact_genNewSubToken(){
	$max_token_tries = 100;
	
	$token_is_unique = false;
	$token_tries = 0;
	
	while(!$token_is_unique){
		if($token_tries > $max_token_tries) return false;
		
		$token = ucp_iContact_randomString(32);
		
		$pre_existing = ucp_iContact_getSFContactByToken($token, array('Id', 'Email'));
		if($pre_existing === false){
			echo '<p><strong>Unknown error connecting to database!</strong></p> Please let us know this happened at <a href="mailto:support@ucp.org?subject=Subscriber Token Generation unknown database error">support@ucp.org</a>';
			trigger_error('ucp_iContact_genNewSubToken() could not contact Salesforce!', E_USER_WARNING);
			return false;
		}
		elseif(empty($pre_existing)){
			$token_is_unique = true;
		}
	}
	
	return $token;
}


function ucp_iContact_checkStringChars($string, $special_chars_okay = true){
	$special_chars_disallowed = array(
		"\\",
		"/",
		"<",
		">",
		";",
		"%",
		"[",
		"]"
	);
	
	if(ctype_alnum($string)) return true;
	elseif($special_chars_okay){
		$chars = str_split(preg_replace('/[\da-zA-Z ]/i', '', $string));
		foreach($chars as $a_char){
			if(in_array($a_char, $special_chars_disallowed)) return false;
		}
		
		return true;
	}
	else return false;
}


function ucp_iContact_validateEmail($email){
	$length = strlen($email);
	
	if($length < 6 or $length > 254) return false;
	
	require_once(dirname(__FILE__).'/swiftlib/swift_required.php');
	
	$swift_validation = Swift_Validate::email($email);
	
	if($swift_validation) return true;
	else return false;
}


function ucp_iContact_checkPhoneNumber($phone){
	if(strlen($phone) < 7) return false;
	
	$legit_chars_string = '0123456789( )-.+';
	$legit_chars = str_split($legit_chars_string);
	$phone = str_split($phone);
	
	foreach($phone as $phone_char){
		if(!in_array($phone_char, $legit_chars)){
			return false;
		}
	}
	
	return true;
}


function ucp_iContact_getSubscriptionString($newsletters){
	if(empty($newsletters)) return ';';
	
	$sub_string = '';
	
	$first_newsletter = true;
	foreach($newsletters as $a_newsletter_sub){
		if($first_newsletter) $first_newsletter = false;
		else $sub_string .= ';';
		$sub_string .= $a_newsletter_sub;
	}
	return $sub_string;
}


/*
 * Checks input in POST (assuming that POST fields have identical names to Salesforce)
 * Returns any and all issues found as an array of strings
 */
function ucp_iContact_validateUserInfo($data, $required_fields = array()){
	$errors = array();
	
	if(!empty($data) and is_array($data) and is_array($required_fields)){
		if(empty($data['Email']) and in_array('Email', $required_fields)) $errors[] = "You didn't provide an email address.";
		elseif(!empty($data['Email'])){
			if(strlen($data['Email']) > 80) $errors[] = "The email address was too long.";
			elseif(!ucp_iContact_validateEmail($data['Email'])) $errors[] = $data['Email'] . " isn't a valid email address.";
		}
		
		if(empty($data['FirstName']) and in_array('FirstName', $required_fields)) $errors[] = "You didn't provide a first name.";
		elseif(!empty($data['FirstName'])){
			if(strlen($data['FirstName']) > 40) $errors[] = "That first name is too long.";
			elseif(!ucp_iContact_checkStringChars($data['FirstName'])) $errors[] = "First name contains invalid characters.";
		}
		
		if(empty($data['LastName']) and in_array('LastName', $required_fields)) $errors[] = "You didn't provide a last name.";
		elseif(!empty($data['LastName'])){
			if(strlen($data['LastName']) > 80) $errors[] = "That last name is too long.";
			elseif(!ucp_iContact_checkStringChars($data['LastName'])) $errors[] = "Last name contains invalid characters.";
		}
		
		if(empty($data['ucp_contact_middle_name__c']) and in_array('ucp_contact_middle_name__c', $required_fields)) $errors[] = "You didn't provide a middle name.";
		elseif(!empty($data['ucp_contact_middle_name__c'])){
			if(strlen($data['ucp_contact_middle_name__c']) > 60) $errors[] = "That middle name is too long.";
		}
		
		if(empty($data['Salutation']) and in_array('Salutation', $required_fields)) $errors[] = "You didn't provide a prefix.";
		elseif(!empty($data['Salutation'])){
			$salutation_recognized = false;
			
			if(strlen($data['Salutation']) <= 40){
				$salutation_list = ucp_iContact_getSFMetadata('Salutation');
				$salutation_list = $salutation_list->picklistValues;
				foreach($salutation_list as $a_salutation){
					if($a_salutation->value === $data['Salutation']){
						$salutation_recognized = true;
						break;
					}
				}
			}
			
			if(!$salutation_recognized) $errors[] = "That prefix \"" . $data['Salutation'] . "\" is not recognized.";
		}
		
		if(empty($data['ucp_contact_suffix__c']) and in_array('ucp_contact_suffix__c', $required_fields)) $errors[] = "You didn't provide a suffix.";
		elseif(!empty($data['ucp_contact_suffix__c'])){
			$suffix_recognized = false;
			
			if(strlen($data['ucp_contact_suffix__c']) <= 40){
				$suffix_list = ucp_iContact_getSFMetadata('ucp_contact_suffix__c');
				$suffix_list = $suffix_list->picklistValues;
				foreach($suffix_list as $a_suffix){
					if($a_suffix->value === $data['ucp_contact_suffix__c']){
						$suffix_recognized = true;
						break;
					}
				}
			}
			
			if(!$suffix_recognized) $errors[] = "That suffix is not recognized.";
		}
		
		if(empty($data['email_sub_organization__c']) and in_array('email_sub_organization__c', $required_fields)) $errors[] = "You didn't provide an organization.";
		elseif(!empty($data['email_sub_organization__c'])){
			if(strlen($data['email_sub_organization__c']) > 100) $errors[] = "That organization name is too long.";
		}
		
		if(empty($data['MailingCountry']) and in_array('MailingCountry', $required_fields)) $errors[] = "You didn't provide a country.";
		elseif(!empty($data['MailingCountry'])){
			if(strlen($data['MailingCountry']) > 80) $errors[] = "That country name is too long.";
			else{
				$valid_countries = ucp_iContact_getCountryList();
				if(!in_array($data['MailingCountry'], $valid_countries)) $errors[] = "Invalid country name.";
			}
		}
		
		if(empty($data['MailingState']) and in_array('MailingState', $required_fields)) $errors[] = "You didn't provide a province or state.";
		elseif(!empty($data['MailingState'])) {
			if(strlen($data['MailingState']) > 80) $errors[] = "That state/province name is too long.";
			elseif(!ucp_iContact_checkStringChars($data['MailingState'])) $errors[] = "State name contains invalid characters.";
		}
		
		if(empty($data['MailingCity']) and in_array('MailingCity', $required_fields)) $errors[] = "You didn't provide a city.";
		elseif(!empty($data['MailingCity'])) {
			if(strlen($data['MailingCity']) > 40) $errors[] = "That city name is too long.";
			elseif(!ucp_iContact_checkStringChars($data['MailingCity'])) $errors[] = "City name contains invalid characters.";
		}
		
		if(empty($data['MailingStreet']) and in_array('MailingStreet', $required_fields)) $errors[] = "You didn't provide your address.";
		elseif(!empty($data['MailingStreet'])){
			if(strlen($data['MailingStreet']) > 255) $errors[] = "Your address is too long.";
			elseif(!ucp_iContact_checkStringChars($data['MailingStreet'])) $errors[] = "Address contains invalid characters.";
		}
		
		if(empty($data['Phone']) and in_array('Phone', $required_fields)) $errors[] = "You didn't provide your phone number.";
		elseif(!empty($data['Phone'])) {
			if(strlen($data['Phone']) > 40) $errors[] = "That phone number is too long.";
			if(strlen($data['Phone']) < 7) $errors[] = "That phone number is too short.";
			elseif(!ucp_iContact_checkPhoneNumber($data['Phone'])) $errors[] = "Phone number contains invalid characters.";
		}
		
		if(empty($data['newsletters']) and in_array('newsletters', $required_fields)) $errors[] = "You didn't pick any subscriptions.";
		elseif(!empty($data['newsletters'])){
			$primary_mailings_list = ucp_iContact_getPrimaryMailings();
			foreach($primary_mailings_list as $a_newsletter){
				if(!in_array($a_newsletter, $primary_mailings_list)) $errors[] = $a_newsletter . " isn't a valid subscription.";
			}
		}
		
		if(empty($data['SignupVerificationCode']) and in_array('SignupVerificationCode', $required_fields)) $errors[] = "You didn't provide a verification code.";
		elseif(!empty($data['SignupVerificationCode'])){
			if(strlen($data['SignupVerificationCode']) > 20) $errors[] = 'That verification code was too long.';
			elseif(strlen($data['SignupVerificationCode']) < 20) $errors[] = 'That verification code was too short.';
			elseif(!ucp_iContact_checkStringChars($data['SignupVerificationCode'], false)) $errors[] = 'Invalid verification code - code should only contain numbers and letters.';
		}
		
		if(empty($data['token']) and in_array('token', $required_fields)) $errors[] = "You didn't provide a token code.";
		elseif(!empty($data['token'])){
			if(strlen($data['token']) > 32) $errors[] = 'That token code was too long.';
			elseif(strlen($data['token']) < 32) $errors[] = 'That token code was too short.';
			elseif(!ucp_iContact_checkStringChars($data['token'], false)) $errors[] = 'Invalid token code - code should only contain numbers and letters.';
		}
	}
	else{
		ucp_iContact_debugMsg('ucp_iContact_validateUserInfo(): No input or unsupported input!');
		return array('Validation error: No input or unsupported input!');
	}
	
	return $errors;
}


function ucp_iContact_getCountryList(){
	return array(
		'United States of America',
		'Aaland Islands',
		'Afghanistan',
		'Albania',
		'Algeria',
		'American Samoa',
		'Andorra',
		'Angola',
		'Anguilla',
		'Antigua And Barbuda',
		'Argentina',
		'Armenia',
		'Aruba',
		'Australia',
		'Austria',
		'Azerbaijan',
		'Bahamas',
		'Bahrain',
		'Bangladesh',
		'Barbados',
		'Belarus',
		'Belgium',
		'Belize',
		'Benin',
		'Bermuda',
		'Bhutan',
		'Bolivia',
		'Bosnia and Herzegovina',
		'Botswana',
		'Bouvet Island',
		'Brazil',
		'Brunei Darussalam',
		'Bulgaria',
		'Burkina Faso',
		'Burundi',
		'Cambodia',
		'Cameroon',
		'Canada',
		'Cape Verde',
		'Cayman Islands',
		'Central African Republic',
		'Chad',
		'Chile',
		'China',
		'Christmas Island',
		'Colombia',
		'Comoros',
		'Congo',
		'Cook Islands',
		'Costa Rica',
		"Cote D'Ivoire",
		'Croatia',
		'Cuba',
		'Curacao',
		'Cyprus',
		'Czech Republic',
		'Denmark',
		'Djibouti',
		'Dominica',
		'Dominican Republic',
		'East Timor',
		'Ecuador',
		'Egypt',
		'El Salvador',
		'Equatorial Guinea',
		'Eritrea',
		'Estonia',
		'Ethiopia',
		'Falkland Islands',
		'Faroe Islands',
		'Fiji',
		'Finland',
		'France',
		'French Guiana',
		'French Polynesia',
		'Gabon',
		'Gambia',
		'Georgia',
		'Germany',
		'Ghana',
		'Gibraltar',
		'Greece',
		'Greenland',
		'Grenada',
		'Guadeloupe',
		'Guam',
		'Guatemala',
		'Guernsey',
		'Guinea',
		'Guyana',
		'Haiti',
		'Honduras',
		'Hong Kong',
		'Hungary',
		'Iceland',
		'India',
		'Indonesia',
		'Iran',
		'Iraq',
		'Ireland',
		'Israel',
		'Italy',
		'Jamaica',
		'Japan',
		'Jersey (Channel Islands)',
		'Jordan',
		'Kazakhstan',
		'Kenya',
		'Kiribati',
		'Kosovo',
		'Kuwait',
		'Kyrgyzstan',
		"Lao People's Democratic Republic",
		'Latvia',
		'Lebanon',
		'Lesotho',
		'Liberia',
		'Libya',
		'Liechtenstein',
		'Lithuania',
		'Luxembourg',
		'Macau',
		'Macedonia',
		'Madagascar',
		'Malawi',
		'Malaysia',
		'Maldives',
		'Mali',
		'Malta',
		'Martinique',
		'Mauritania',
		'Mauritius',
		'Mayotte',
		'Mexico',
		'Moldova, Republic of',
		'Monaco',
		'Mongolia',
		'Montenegro',
		'Montserrat',
		'Morocco',
		'Mozambique',
		'Myanmar',
		'Namibia',
		'Nepal',
		'Netherlands',
		'Netherlands Antilles',
		'New Caledonia',
		'New Zealand',
		'Nicaragua',
		'Niger',
		'Nigeria',
		'Niue',
		'Norfolk Island',
		'North Korea',
		'Norway',
		'Oman',
		'Pakistan',
		'Palau',
		'Palestine',
		'Panama',
		'Papua New Guinea',
		'Paraguay',
		'Peru',
		'Philippines',
		'Pitcairn',
		'Poland',
		'Portugal',
		'Qatar',
		'Reunion',
		'Romania',
		'Russia',
		'Rwanda',
		'Saint Kitts and Nevis',
		'Saint Lucia',
		'Saint Vincent and the Grenadines',
		'Samoa (Independent)',
		'San Marino',
		'Saudi Arabia',
		'Senegal',
		'Serbia',
		'Seychelles',
		'Sierra Leone',
		'Singapore',
		'Sint Maarten',
		'Slovakia',
		'Slovenia',
		'Solomon Islands',
		'Somalia',
		'South Africa',
		'South Georgia and the South Sandwich Islands',
		'South Korea',
		'South Sudan',
		'Spain',
		'Sri Lanka',
		'Sudan',
		'Suriname',
		'Svalbard and Jan Mayen Islands',
		'Swaziland',
		'Sweden',
		'Switzerland',
		'Syria',
		'Taiwan',
		'Tajikistan',
		'Tanzania',
		'Thailand',
		'Togo',
		'Tonga',
		'Trinidad and Tobago',
		'Tunisia',
		'Turkey',
		'Turks & Caicos Islands',
		'Uganda',
		'Ukraine',
		'United Arab Emirates',
		'United Kingdom',
		'Uruguay',
		'Uzbekistan',
		'Vanuatu',
		'Vatican City State (Holy See)',
		'Venezuela',
		'Vietnam',
		'Virgin Islands (British)',
		'Virgin Islands (U.S.)',
		'Western Sahara',
		'Yemen',
		'Zambia',
		'Zimbabwe'
	);
}


function ucp_iContact_outputCountrySelectOptions($selected_country = false){
	$countries = ucp_iContact_getCountryList();
	if($selected_country and !in_array($selected_country, $countries)) $selected_country = false;
	?>
	<option value=""></option>
	<?php foreach($countries as $country): ?>
	<option value="<?php echo $country; ?>"<?php if($selected_country and $selected_country == $country) echo ' selected="selected"'; ?>><?php echo $country; ?></option>
	<?php endforeach;
}


function ucp_iContact_outputSalutationSelectOptions($selected_salutation = false){
	$salutation_field_metadata = ucp_iContact_getSFMetadata('Salutation');
	
	if(empty($salutation_field_metadata->picklistValues)){
		ucp_iContact_debugMsg('No metadata for Salutation field!');
		return null;
	}
	
	$option_html = '';
	
	$option_html .= '<option value=""></option>';
	
	foreach($salutation_field_metadata->picklistValues as $a_salutation_value){
		if(!empty($a_salutation_value->active)){
			$selected_attribute = '';
			if($selected_salutation and $selected_salutation === $a_salutation_value->value){
				$selected_attribute = ' selected="selected"';
			}
			
			$option_html .= '<option value="' . $a_salutation_value->value . '"' . $selected_attribute . '>' . $a_salutation_value->label . '</option>';
		}
	}
	
	return $option_html;
}


function ucp_iContact_outputSuffixSelectOptions($selected_suffix = false){
	$suffix_field_metadata = ucp_iContact_getSFMetadata('ucp_contact_suffix__c');
	
	if(empty($suffix_field_metadata->picklistValues)){
		ucp_iContact_debugMsg('No metadata for Suffix field!');
		return null;
	}
	
	$option_html = '';
	
	$option_html .= '<option value=""></option>';
	
	foreach($suffix_field_metadata->picklistValues as $a_suffix_value){
		if(!empty($a_suffix_value->active)){
			$selected_attribute = '';
			if($selected_suffix and $selected_suffix === $a_suffix_value->value){
				$selected_attribute = ' selected="selected"';
			}
			
			$option_html .= '<option value="' . $a_suffix_value->value . '"' . $selected_attribute . '>' . $a_suffix_value->label . '</option>';
		}
	}
	
	return $option_html;
}


function ucp_iContact_getCurrentURL(){
	if(!empty($_SERVER['HTTPS']) and $_SERVER['HTTPS'] === 'on') $protocol = "https://";
	else $protocol = 'http://';
	if(!empty($_SERVER['HTTP_HOST'])) $host = $_SERVER['HTTP_HOST'];
	else $host = $_SERVER['SERVER_NAME'];
	
	return $protocol . $host . $_SERVER['REQUEST_URI'];
}


/*
 * Strips slashes from POST data, which seem to be necessary on Firehost even though magic_quotes reports off
 */
function ucp_iContact_stripPOSTslashes(){
	foreach($_POST as $post_key => &$post_field){
		if(is_string($post_field)){
			$post_field = stripslashes($post_field);   // Even though Firehost doesn't have magic_quotes turned on, it may act as if it does
			if($post_key == 'Email') $post_field = trim($post_field);   // Extra whitespace at end of address was getting through validation and causing trouble
		}
		elseif(is_array($post_field)){   // go one level deeper if necessary
			foreach($post_field as &$post_array_field){
				if(is_string($post_array_field)) $post_array_field = stripslashes($post_array_field);
			}
		}
	}
	
	return true;
}


/*
 * Runs strings in POST data through htmlspecialchars(). Goes one level deeper into arrays if necessary.
 */
function ucp_iContact_sanitizePOSTForHTML(){
	foreach($_POST as &$post_field){
		if(is_string($post_field)) $post_field = htmlspecialchars($post_field);
		elseif(is_array($post_field)){   // go one level deeper if necessary
			foreach($post_field as &$post_array_field){
				if(is_string($post_array_field)) $post_array_field = htmlspecialchars($post_array_field);
			}
		}
	}
	
	return true;
}


function ucp_iContact_getStylesheet(){
	?>
	<style type="text/css">
		form.ucp-newsletter input:disabled{opacity: 0.7;};
		form.ucp-newsletter {width:500px;}
		form.ucp-newsletter div.contactfields{margin: 0 auto; max-width:500px; width: 400px;}
		form.ucp-newsletter .contactfields p{line-height:2; vertical-align:middle;}
		form.ucp-newsletter .contactfields p input{width:250px}
		form.ucp-newsletter .contactfields p select{width:260px}
		form.ucp-newsletter .contactfields span.label{display:inline-block; width:125px;}
		form.ucp-newsletter p.submit{width:100%; text-align:center;}
		form.ucp-newsletter input.submit{font-size: 16px;}
		div.newslettersbox, div.newsletter-errors {
			width: 400px;
			border: 1px solid rgb(204,204,204);
			background-color: rgb(248,248,248);
			padding: 3px 5px;
			margin: 10px auto;
		}
		div.newslettersbox > div {clear: both;}
		div.newslettersbox p.newsletter-desc {margin-left: 40px; font-style: italic;}
		label.newsletter, span.newsletter {font-weight:bold;}
		
		.ucp-newsletter-emailpluscode{
			width: 100%;
		}
		.ucp-newsletter-emailpluscode div{
			margin: 0 auto;
		}
		.ucp-newsletter-emailpluscode label{
			display: inline-block;
			width: 75px;
			padding-right: 5px;
			text-align: right;
			font-weight: bold;
		}
		.ucp-newsletter-emailpluscode input[type="text"], #ucp-newsletter-signup-verif input[type="email"]{
			width: 250px;
		}
		.ucp-newsletter-emailpluscode p.submit{width:100%; text-align:center;}
		.ucp-newsletter-emailpluscode input.submit{font-size: 16px;}
	</style>
	<?php
}


/*
 * Adapted from Greg Winiarski, from http://advent.squareonemd.co.uk/find-pages-using-given-wordpress-shortcode/
 */
function ucp_iContact_pages_with_shortcode($shortcode, $args = array()) {
	if(!shortcode_exists($shortcode)){
		ucp_iContact_debugMsg('ucp_iContact_pages_with_shortcode() was told to search for shortcode ['.$shortcode.'] that does not exist!');
		return null;
	}
	
	$pages = get_pages($args);
	$list = array();
	
	foreach($pages as $page) {
		if(has_shortcode($page->post_content, $shortcode)) {
			$list[] = $page;
		}
	}
	
	return $list;
}


/*
 * Returns the URL for the page containing the shortcode submitted as argument.
 * 
 * If there is more than one such page, URLs are sorted by:
 * 1) number of directories
 * 2) length
 * and the shortest (on the 1st count, with 2nd as tiebreaker) URL is returned
 */
function ucp_iContact_getURLForShortcode($shortcode = ''){
	function sortByStringLength($a, $b){
		return strlen($b)-strlen($a);
	}
	
	
	
	global $settings;
	$debug = $settings['debug'];
	
	if(empty($shortcode)){
		ucp_iContact_debugMsg('ucp_iContact_getURLForShortcode wasn\'t passed a shortcode, so it cannot return anything.');
		return false;
	}
	
	if(!empty($settings['url_for_shortcode_' . $shortcode])) return $settings['url_for_shortcode_' . $shortcode];
	
	$urls = array();
	
	$shortcode_search = ucp_iContact_pages_with_shortcode($shortcode);
	if(empty($shortcode_search)){
		ucp_iContact_debugMsg('ucp_iContact_getURLForShortcode() didn\'t find any pages containing shortcode ['.$shortcode.']');
		return false;
	}
	
	foreach($shortcode_search as $a_page){
		$urls[] = get_permalink($a_page->ID);
	}
	
	if(count($urls) > 1){
		if($debug === 'verbose'){
			echo '<p>Found more than one matching URL for shortcode ' . $shortcode . ', here they are:</p><pre>';
			print_r($urls);
			echo '</pre>';
		}
		
		$url_slash_count = array();
		
		foreach($urls as $key => $a_url){
			$url_slash_count[$key] = count(explode("/", $a_url));
		}
		unset($key, $a_url);
		
		$min_slashes = min($url_slash_count);
		
		$reduced_urls = array();
		foreach($urls as $key => $a_url){
			if($url_slash_count[$key] == $min_slashes) $reduced_urls[] = $a_url;
		}
		unset($key, $a_url);
		
		$urls = $reduced_urls;
		
		if($count($urls) > 1){
			usort($urls, 'sortByStringLength');
		}
	}
	
	return $urls[0];
}


function ucp_iContact_retryAFailedEmail($num_retries = 5){
	$fails = 0;
	for($t=0; $t < $num_retries; $t++){
		$result = ucp_iContact_sendEmail(-1);
		if($result === null) break;   // null means there isn't anything found in the queue
		if(!$result) $fails++;
		if($fails > 1) break;   // If more than one fails, it's probably us. Don't bang poor SwiftMailer's head against the wall
	}
}


function ucp_iContact_activation() {
	if(!wp_next_scheduled('ucp-iContact-retryEmail')){
		wp_schedule_event(time(), 'hourly', 'ucp-iContact-retryEmail');
	}
}


function ucp_iContact_deactivation() {
	wp_clear_scheduled_hook('ucp-iContact-retryEmail');
}


// Run queue retry on the WP Cron
if(function_exists('register_activation_hook')){
	add_action('ucp-iContact-retryEmail', 'ucp_iContact_retryAFailedEmail');
	
	register_activation_hook( __FILE__, 'ucp_iContact_activation' );
	
	register_deactivation_hook( __FILE__, 'ucp_iContact_deactivation');
}



// page modules
require_once(dirname(__FILE__).'/signup.php');
require_once(dirname(__FILE__).'/verifysignup.php');
require_once(dirname(__FILE__).'/editsub.php');

?>