<?php

function ucp_iContact_doSignupVerificationPage($atts){
	global $settings;
	$debug = $settings['debug'];
	
	ob_start();
	
	if($debug) echo '<strong>Debug mode on.</strong><br />';
	
	$show_form = true;
	$show_form_only = false;
	$stop_errors = array();
	
	ucp_iContact_getStylesheet();
	
	if(empty($_GET['Email']) and empty($_GET['SignupVerificationCode'])){
		$show_form_only = true;
	}
	elseif(empty($_GET['Email'])){
		$stop_errors[] = "You didn't provide your email address! Please enter that as well.";
	}
	elseif(empty($_GET['SignupVerificationCode'])){
		$signup_page_url = ucp_iContact_getURLForShortcode('icontact_signup');
		$stop_errors[] = 'You didn\'t include a code! Do you need to <a href="'.$signup_page_url.'">sign up</a>?';
	}
	else{   //have fields, will validate
		$stop_errors = ucp_iContact_validateUserInfo($_GET, array('Email', 'SignupVerificationCode'));
	}
	
	if(empty($stop_errors) and !$show_form_only){   // almost good, but let's see if this is an actual signup
		$code_correct_for_signup = ucp_iContact_isEmailInSignupTable($_GET['Email'], $_GET['SignupVerificationCode']);
		if($code_correct_for_signup){
			$show_form = false;
			ucp_iContact_completeSignup($_GET['Email'], $_GET['SignupVerificationCode']);
		}
		elseif($code_correct_for_signup === 0){
			$stop_errors[] = 'Sign-up was already completed for this address. You don\'t need to do anything further. Thanks for signing up!';
		}
		else{
			$code_correct_for_address_change = ucp_iContact_isEmailInAddressChangeTable($_GET['Email'], $_GET['SignupVerificationCode']);
			if($code_correct_for_address_change){
				ucp_iContact_completeAddressChange($_GET['Email'], $_GET['SignupVerificationCode']);
				$show_form = false;
			}
			elseif($code_correct_for_address_change === 0){
				$stop_errors[] = 'Verification was already completed for this address. You don\'t need to do anything further. Thanks!';
			}
			else $stop_errors[] = 'Either this email is not in our records, or the code is incorrect. If you are certain you have the correct code for a subscription made today, please contact <a href="mailto:support@ucp.org">support@ucp.org</a>.';
		}
	}
	
	if(!empty($stop_errors)){   // show errors
		?>
		<div class="newsletter-errors">
			<p><strong>There were problems submitting your request.</strong></p>
			<ul>
				<?php foreach($stop_errors as $an_error) echo '<li>' . $an_error . '</li>'; ?>
			</ul>
		</div>
		<?php
	}
	
	if($show_form){ ?>
		<form name="ucp-newsletter-signup-verif" id="ucp-newsletter-signup-verif" class="ucp-newsletter-emailpluscode" method="GET" action="<?php echo ucp_iContact_getCurrentURL(); ?>">
			<div>
				<p><label for="Email">Email</label> <input type="text" name="Email" id="Email" maxlength="80" value="<?php if(!empty($_GET['Email'])) echo $_GET['Email'] ?>" placeholder="name@domain.com" /></p>
				<p><label for="SignupVerificationCode">Code</label> <input type="text" name="SignupVerificationCode" id="SignupVerificationCode" maxlength="20" value="<?php if(!empty($_GET['SignupVerificationCode'])) echo $_GET['SignupVerificationCode'] ?>" /></p>
				<p class="submit"><input type="submit" id="verification-submit" class="submit" value="Confirm my Subscription" /></p>
			</div>
		</form>
	<?php }
	
	return ob_get_clean();
}

if(function_exists('add_shortcode')){
	add_shortcode('icontact_signup_verification', 'ucp_iContact_doSignupVerificationPage');
}


function ucp_iContact_completeSignup($email, $verif_code){
	global $settings;
	$debug = $settings['debug'];
	
	$salesforce_result = ucp_iContact_subscribeEmail($email);
	
	if($debug === 'verbose'){
		echo '<p><strong>ucp_iContact_completeSignup() got this return from ucp_iContact_subscribeEmail:</strong></p><pre>';
		print_r($salesforce_result);
		echo '</pre>';
	}
	
	if(!$salesforce_result[0]){
		echo '<p>Error! ' . $salesforce_result[1] . '<br/>Please report this error to <a href="mailto:support@ucp.org">support@ucp.org</a>.</p>';
		return false;
	}
	else{
		echo '<p>Success! You are now subscribed at '.$email.'.</p>';
		if(!empty($salesforce_result[1])) {
			$php_log_minor_errors = 'Salesforce reported minor errors to ucp_iContact_subscribeEmail(): ';
			foreach($salesforce_result[1] as $an_error){
				$php_log_minor_errors .= $an_error . "\n";
			}
			unset($an_error);
			ucp_iContact_debugMsg($php_log_minor_errors);
			
			echo '<div class="newsletter-errors"><p><strong>There were some minor errors signing you up. We successfully subscribed you, but we\'d still appreciate hearing about these errors</strong> at <a href="mailto:support@ucp.org">support@ucp.org</a>.</p><ul>';
			foreach($salesforce_result[1] as $an_error) echo '<li>' . $an_error . '</li>';
			echo '</ul></div>';
		}
		ucp_iContact_sendEditSubscriptionLinkByEmail($email, $salesforce_result[2][0]->id);
		return true;
	}
}


function ucp_iContact_completeAddressChange($email, $verif_code){
	global $settings;
	$debug = $settings['debug'];
	
	$salesforce_result = ucp_iContact_updateEmail($email, $verif_code);
	
	if($debug === 'verbose'){
		echo '<p><strong>ucp_iContact_completeAddressChange() got this return from ucp_iContact_updateEmail:</strong></p><pre>';
		print_r($salesforce_result);
		echo '</pre>';
	}
	
	if(!$salesforce_result[0]){
		$error_message = 'Error in ucp_iContact_completeAddressChange! ' . $salesforce_result[1];
		ucp_iContact_debugMsg($error_message);
		if(!$debug) echo '<p>' . $error_message . '<br/>Please report this error to <a href="mailto:support@ucp.org">support@ucp.org</a>.</p>';
		return false;
	}
	else{
		echo '<p>Success! Your email address has been changed to '.$email.'.</p>';
		if(!empty($salesforce_result[1])) {
			echo '<div class="newsletter-errors"><p><strong>There were some minor errors changing your address. We successfully changed your email address, but we\'d still appreciate hearing about these errors</strong> at <a href="mailto:support@ucp.org">support@ucp.org</a>.</p><ul>';
			foreach($salesforce_result[1] as $an_error) echo '<li>' . $an_error . '</li>';
			echo '</ul></div>';
		}
		ucp_iContact_sendEditSubscriptionLinkByEmail($email, $salesforce_result[2][0]->id);
		return true;
	}
}


function ucp_iContact_sendEditSubscriptionLinkByEmail($email, $sf_id){
	global $settings;
	
	$subscriptions_email_address = $settings['subscriptions_email_address'];
	
	$edit_sub_page_url = ucp_iContact_getURLForShortcode('icontact_sub_edit');
	if(empty($edit_sub_page_url)) $edit_sub_page_url = 'PLACEHOLDER';
	
	$sf_record = ucp_iContact_getSFContactByID($sf_id, array('Id', 'Email', 'FirstName', 'LastName', 'Email_subscriptions__c', 'email_sub_token__c'));
	
	if(!empty($sf_record)) $sf_record = $sf_record[0];
	else{
		ucp_iContact_debugMsg('iContact Sub error: no Contact record found for ID ' . $sf_id . ' when sending subscription confirmation to ' . $email);
		echo '<p>We tried to send you a confirmation email including instructions for managing your subscriptions(s) if you want to change them in the future. <strong>We were unable to email you due to an error connecting to our database.</strong> You can reach us at <a href="'.$subscriptions_email_address.'">'.$subscriptions_email_address.'</a>.';
		return false;
	}
	
	if($sf_record->fields->LastName === 'true') $sf_record->fields->FirstName = '';
	if($sf_record->fields->LastName === 'true') $sf_record->fields->LastName = '';
	
	if(!empty($sf_record->fields->FirstName) and !empty($sf_record->fields->LastName)) $full_name = $sf_record->fields->FirstName . ' ' . $sf_record->fields->LastName;
	elseif(empty($sf_record->fields->FirstName) and empty($sf_record->fields->LastName)) $full_name = '';
	elseif(!empty($sf_record->fields->FirstName)) $full_name = $sf_record->fields->FirstName;
	else $full_name = $sf_record->fields->LastName;
	
	$verif_code = $sf_record->fields->email_sub_token__c;
	
	$edit_sub_page_url_prefilled = $edit_sub_page_url . '?Email=' . rawurlencode($email) . '&token=' . rawurlencode($verif_code);
	
	$email_template_html = <<<EOT
<html><body>
<center><img src="http://ucplabs.org/mailassets/icontact_subs/UCPLogo.png" alt="United Cerebral Palsy logo" style="margin: 5 auto;" /></center>
<p>Thank you for subscribing to UCP newsletters.</p>
<p>If you would like to edit your subscriptions or other aspects of your profile in the future, please click on <a href="$edit_sub_page_url_prefilled">this link</a> - or, go to <a href="$edit_sub_page_url">$edit_sub_page_url</a> and input your email address and this code: <b>$verif_code</b></p>
<p>You can contact us at: <a href="$subscriptions_email_address">$subscriptions_email_address</a></p>
</body></html>
EOT;
	$email_template_plain = <<<EOT
Thank you for subscribing to UCP newsletters.

<p>If you would like to edit your subscriptions or other aspects of your profile in the future, please go to $edit_sub_page_url_prefilled - or, go to $edit_sub_page_url and input your email address and this code: $verif_code

You can contact us at: $subscriptions_email_address
EOT;
	
	$mailing_result = ucp_iContact_sendEmail(0, $email, $subject = 'Subscription confirmed', $email_template_html, $email_template_plain, $full_name);
	
	if(!empty($mailing_result)){
		echo '<p><strong>Thank you for subscribing to UCP newsletters.</strong> We\'ve sent you a confirmation email from '.$subscriptions_email_address.'. It includes instructions for managing your subscriptions(s) if you want to change them in the future.</p>';
	}
	else{
		echo '<p>We tried to send you a confirmation email including instructions for managing your subscriptions(s) if you want to change them in the future. <strong>We were unable to email you at this time.</strong> We\'ll try to reach you again later. If this continues to be a problem, you can reach us at <a href="'.$subscriptions_email_address.'">'.$subscriptions_email_address.'</a>.';
	}
}

?>