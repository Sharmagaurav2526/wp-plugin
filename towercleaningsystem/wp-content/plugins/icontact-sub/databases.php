<?php

/*
 * Sanitizes a string for SQL or SOQL input
 */
function ucp_iContact_sanitizeForMySQL($input, $like_statement = false){
	$output = str_replace("\\", "\\\\", $input);
	$output = str_replace("'", "\'", $input);
	$output = str_replace('"', '\"', $output);
	
	if($like_statement){
		$output = str_replace('_', '\_', $output);
		$output = str_replace('%', '\%', $output);
	}
	
	return $output;
}


/*
 * Sends necessary commands to recreate all tables with the proper structure for the icontact database, if they don't already exist.
 * Just requires the existence of an icontact database, and a user set in settings.php with the power to create tables in said database.
 */
function ucp_iContact_createSQLTables($sql_db){
	global $settings;
	$debug = $settings['debug'];
	
	if(!defined('UCP_ICONTACT_RAN_SQL_TABLE_CREATION')){
		$table_commands = array(
		"CREATE TABLE IF NOT EXISTS `sf_meta` (`timestamp` int(11) NOT NULL,
			`data_serialized` mediumtext NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1;",
		
		"CREATE TABLE IF NOT EXISTS `new_registries` (`Email` varchar(80) NOT NULL UNIQUE,
			`data_serialized` mediumtext NOT NULL,
			`time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`verifcode` varchar(20) NOT NULL,
			`existing_contact` varchar(18) NOT NULL,
			`completed` BOOLEAN NOT NULL DEFAULT FALSE,
			`notes` varchar(500) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1;",
		
		"CREATE TABLE IF NOT EXISTS `address_changes` (`sfid` varchar(18) NOT NULL UNIQUE,
			`old_email` varchar(80) NOT NULL,
			`new_email` varchar(80) NOT NULL,
			`verifcode` varchar(20) NOT NULL,
			`time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`completed` BOOLEAN NOT NULL DEFAULT FALSE,
			`notes` varchar(500) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1;",
		
		"CREATE TABLE IF NOT EXISTS `pending_email` (`ID` int NOT NULL UNIQUE AUTO_INCREMENT,
			`address` varchar(80) NOT NULL,
			`name` varchar(120) NOT NULL,
			`subject` varchar(120) NOT NULL,
			`htmlbody` TEXT NOT NULL,
			`textbody` TEXT NOT NULL,
			`sfid` varchar(18) NOT NULL,
			`Attempts` int(11) NOT NULL,
			`time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`last_attempt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`notes` varchar(500) NOT NULL,
			PRIMARY KEY (ID)) ENGINE=MyISAM DEFAULT CHARSET=latin1;",
		
		"CREATE TABLE IF NOT EXISTS `sent_email` (`ID` int NOT NULL,
			`address` varchar(80) NOT NULL,
			`name` varchar(120) NOT NULL,
			`subject` varchar(120) NOT NULL,
			`htmlbody` TEXT NOT NULL,
			`textbody` TEXT NOT NULL,
			`sfid` varchar(18) NOT NULL,
			`Attempts` int(11) NOT NULL,
			`time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`last_attempt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`notes` varchar(500) NOT NULL,
			`sortingID` int NOT NULL UNIQUE AUTO_INCREMENT,
			PRIMARY KEY (sortingID)) ENGINE=MyISAM DEFAULT CHARSET=latin1;",
		
		"CREATE TABLE IF NOT EXISTS `failed_email` (`ID` int NOT NULL,
			`address` varchar(80) NOT NULL,
			`name` varchar(120) NOT NULL,
			`subject` varchar(120) NOT NULL,
			`htmlbody` TEXT NOT NULL,
			`textbody` TEXT NOT NULL,
			`sfid` varchar(18) NOT NULL,
			`Attempts` int(11) NOT NULL,
			`time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`last_attempt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`notes` varchar(500) NOT NULL,
			`sortingID` int NOT NULL UNIQUE AUTO_INCREMENT,
			PRIMARY KEY (sortingID)) ENGINE=MyISAM DEFAULT CHARSET=latin1;"
		);
		
		try{
			foreach($table_commands as $table => $query){
				$res = $sql_db -> query($query);
				if($res === false){
					ucp_iContact_debugMsg('Error in ucp_iContact_createSQLTables(): Failure to create table '.$table);
					return false;
				}
			}
			unset($table, $query);
		}
		catch(Exception $e){
			echo $e->getMessage()."\n";
			return null;
		}
		
		define('UCP_ICONTACT_RAN_SQL_TABLE_CREATION', true);
		if($debug === 'verbose') echo '<br>ucp_iContact_createSQLTables() ran without error.<br>';
		return true;
	}
	else return true;
}


/*
 * Returns a connection to the icontact SQL database, or null if there's an error
 */
function ucp_iContact_SQLConnect($function_name = ''){
	global $settings;
	$debug = $settings['debug'];
	
	if(empty($function_name)) $function_name = 'ucp_iContact_SQLConnect()';
	
	$sql_db = new mysqli('localhost', $settings['sqluser'], $settings['sqlpassword'], $settings['sqldb']);
	if($sql_db->connect_errno){
		ucp_iContact_debugMsg('Error: ' . $function_name . ' ' . 'encountered MySQL connection error: ' . $sql_db->connect_error);
		if(empty($debug)) echo 'Error: encountered MySQL connection error';
		return null;
	}
	else{
		ucp_iContact_createSQLTables($sql_db);
		
		if(!defined('UCP_ICONTACT_RAN_TABLE_PRUNING')){
			define('UCP_ICONTACT_RAN_TABLE_PRUNING', false);
			ucp_iContact_cleanSQL('sent_email', $settings['data_lifespan'], 'SQL');
			ucp_iContact_cleanSQL('failed_email', $settings['data_lifespan'], 'SQL');
			if($debug) echo '<p>Ran database clean on sent_email and failed_email.</p>';
		}
		
		return $sql_db;
	}
}


/*
 * Clean up out of date entries in an icontact table. Must specify table and lifespan of entries (in hours).
 * Specify "unix" as third parameter if it is a unix timestamp instead of a SQL one.
 */
function ucp_iContact_cleanSQL($table, $lifespan, $timestamp_type = 'SQL'){
	global $settings;
	$debug = $settings['debug'];
	
	if(empty($table) or !isset($lifespan) or !is_int($lifespan)){
		ucp_iContact_debugMsg('Error: Missing or invalid parameter in ucp_iContact_cleanSQL()');
		return null;
	}
	
	if(strtolower($timestamp_type) === 'sql'){
		$query = "DELETE FROM ".$table." WHERE time < (NOW() - INTERVAL ".$lifespan." HOUR)";
		if($debug === 'verbose') echo '<p>ucp_iContact_cleanSQL is now using this query:</p><pre>' . $query . '</pre>';
	}
	elseif(strtolower($timestamp_type) === 'unix'){
		$now = time();
		$least_good_time = $now - $lifespan * 3600;
		$query = "DELETE FROM ".$table." WHERE timestamp < ".$least_good_time;
	}
	else return false;
	
	$sql_db = ucp_iContact_SQLConnect('ucp_iContact_cleanSQL()');
	if(!$sql_db) return null;
	
	if($debug === 'verbose'){
		echo '<p>ucp_iContact_cleanSQL is using query:<br />'.$query.'</p>';
	}
	
	try{
		$res = $sql_db -> query($query);
	}
	catch(Exception $e){
		ucp_iContact_debugMsg($e->getMessage());
		return null;
	}
	
	if($res === false){
		ucp_iContact_debugMsg('Error: ucp_iContact_cleanSQL() SQL query failed');
		$sql_db->close();
		return false;
	}
	else{
		$sql_db->close();
		return $res;
	}
}


function ucp_iContact_updateSFMetadata() {
	$fields_from_sf = sfDescribe('Contact');
	
	if(empty($fields_from_sf)) return false;
	
	$fields = array();
	$fields_to_get = array('Email_subscriptions__c', 'Salutation', 'ucp_contact_suffix__c');
	
	foreach($fields_from_sf as $field) {
		if(in_array($field->name, $fields_to_get)){
			$fields[$field->name] = $field;
		}
	}
	
	return $fields;
	
	return null;
}


/*
 *Clean up out of date metadata entries
 */
function ucp_iContact_cleanSFMetadataCache($lifespan = false){
	global $settings;
	
	if($lifespan === false) $lifespan = $settings['data_lifespan'];
	
	$res = ucp_iContact_cleanSQL('sf_meta', $lifespan, 'unix');
	
	if($res === false){
		ucp_iContact_debugMsg('Error: ucp_iContact_cleanSFMetadataCache() failed');
		return false;
	}
	else{
		return $res;
	}
}


function ucp_iContact_readSFMetadataCache(){
	$sql_db = ucp_iContact_SQLConnect('ucp_iContact_readSFMetadataCache()');
	if(!$sql_db) return null;
	
	$query = "SELECT timestamp, data_serialized FROM sf_meta ORDER BY timestamp DESC LIMIT 1";
	
	try{
		$res = $sql_db -> query($query);
	}
	catch(Exception $e){
		ucp_iContact_debugMsg($e->getMessage()."\n");
		return null;
	}
	
	$entry = array();
	
	while ($row = $res->fetch_assoc()) $entry = $row;
	
	return $entry;
}


/*
 * Use this function to request metadata on a specific Contact Field in Salesforce.
 *
 * The function and those it calls will handle looking through a cache in SQL, and calling out to Salesforce if the data is stale enough.
 * 
 * Defaults to returning the Email Subscriptions field if an SF field name is not specified as the argument.
 */
function ucp_iContact_getSFMetadata($field_name = 'Email_subscriptions__c') {
	global $settings;
	$debug = $settings['debug'];
	
	$metadata = null;
	
	if(defined("UCP_ICONTACT_SF_METADATA_CONTACT_OBJECT")){
		$metadata = unserialize(UCP_ICONTACT_SF_METADATA_CONTACT_OBJECT);
	}
	elseif($settings['live_metadata']){
		if($debug) echo '<br />ucp_iContact_getSFMetadata() is updating live from Salesforce<br />';
		$metadata = ucp_iContact_updateSFMetadata();
		if(!empty($metadata)){
			define("UCP_ICONTACT_SF_METADATA_CONTACT_OBJECT", serialize($metadata));
		}
		else{
			ucp_iContact_debugMsg("ucp_iContact_getSFMetaData() failed getting data from Salesforce.");
		}
	}
	else{
		$sql_entry = ucp_iContact_readSFMetadataCache();
		
		$now = time();
		
		if(!empty($sql_entry) and !empty($sql_entry['data_serialized'])) $metadata = unserialize($sql_entry['data_serialized']);
		
		if( empty($metadata) or ( ($now - $sql_entry['timestamp']) > ($settings['metadata_refresh_time'] * 3600) ) ){
			$old_metadata = $metadata;
			
			//check with Salesforce if it's been a while or if the current entry is missing data
			ucp_iContact_cleanSFMetadataCache();
			
			if($debug){
				if(!empty($metadata)) echo '<br />ucp_iContact_getSFMetadata() determined cache is old, updating from Salesforce<br />';
				else echo '<br />ucp_iContact_getSFMetadata() determined cache is empty, updating from Salesforce<br />';
			}
			
			$metadata = ucp_iContact_updateSFMetadata();
			
			if(empty($metadata)){
				if(!$debug) echo '<p><strong>Error: failed update metadata cache</strong></p>';
				ucp_iContact_debugMsg('Error: failed update metadata cache in ucp_iContact_getSFMetadata()');
			}
			else{
				$metadata_for_sql = ucp_iContact_sanitizeForMySQL(serialize($metadata));
				
				$sql_db = ucp_iContact_SQLConnect('ucp_iContact_getSFMetadata()');
				
				$query = "INSERT INTO sf_meta (timestamp, data_serialized) VALUES ('".time()."', '".$metadata_for_sql."')";
				try{
					$res = $sql_db -> query($query);
				}
				catch(Exception $e){
					ucp_iContact_debugMsg($e->getMessage());
					return null;
				}
				if($res === false){
					echo '<p><strong>Error: failed to insert updated metadata cache</strong></p>';
					ucp_iContact_debugMsg('ucp_iContact_getSFMetadata() failed to insert updated metadata cache! ' . $sql_db->error);
					$sql_db->close();
					return null;
				}
				
				$sql_db->close();
				
				$sql_entry = ucp_iContact_readSFMetadataCache();
				
				$metadata = unserialize($sql_entry['data_serialized']);
			}
			
			if(empty($metadata) and !empty($old_metadata)){
				ucp_iContact_debugMsg('ucp_iContact_getSFMetadata() got empty metadata back when trying to refresh the cache! However, cached info seems good, using that.');
				$metadata = $old_metadata;
			}
		}
	}
	
	if($metadata === false){
		echo '<br /><strong>Metadata read failed!</strong><br />';
		return null;
	}
	elseif(!empty($metadata)){
		if(!defined("UCP_ICONTACT_SF_METADATA_CONTACT_OBJECT") and !empty($sql_entry)){
			define("UCP_ICONTACT_SF_METADATA_CONTACT_OBJECT", $sql_entry['data_serialized']);
		}
		
		if(!empty($metadata[$field_name])){
			return $metadata[$field_name];
		}
		else{
			ucp_iContact_debugMsg('ucp_iContact_getSFMetadata(): Metadata present, but requested field "' . $field_name . '" not found!');
		}
	}
	else{
		echo '<br /><strong>Unknown error reading metadata!</strong><br />';
		return null;
	}
}


function ucp_iContact_getMailingLists(){
	$newsletters = ucp_iContact_getPrimaryMailings();
	$primary_newsletter_names = array();
	
	foreach($newsletters as $primary_newsletter){
		$primary_newsletter_names[] = $primary_newsletter->sf_name;
	}
	
	$mailing_list_field = ucp_iContact_getSFMetadata();
	if(empty($mailing_list_field)){
		return false;
	}
	
	foreach($mailing_list_field->picklistValues as $order => $pick_value){
		if(!in_array($pick_value->value, $primary_newsletter_names)){
			$other_newsletter = new ucp_mailingList($pick_value->value, $pick_value->label, false, false);
			$newsletters[] = $other_newsletter;
		}
		unset($other_newsletter);
	}
	unset($order, $pick_value);
	
	return $newsletters;
}


function ucp_iContact_constructSOQLFieldString($fields = array()){
	$first_field = true;
	$field_string = '';
	foreach($fields as $a_field){
		if($first_field) $first_field = false;
		else $field_string .= ', ';
		$field_string .= $a_field;
	}
	
	return $field_string;
}


function ucp_iContact_getSFContactByID($sf_id, $fields = array('Id', 'Email', 'FirstName', 'LastName', 'HasOptedOutOfEmail', 'Email_subscriptions__c', 'email_sub_token__c')){
	global $settings;
	$debug = $settings['debug'];
	
	$field_string = ucp_iContact_constructSOQLFieldString($fields);
	
	$query_by_id = "SELECT " . $field_string . " FROM Contact WHERE Id = '" . $sf_id . "'";
	if($debug) echo '<br />Using SOQL query: ' . $query_by_id . '<br />';
	$result = sfQuery($query_by_id);
	
	if($result === false){
		trigger_error('ucp_iContact_getSFContactByID received a failure from sfQuery()', E_USER_WARNING);
	}
	
	return $result;
}


function ucp_iContact_getSFContactByToken($token, $fields = array('Id', 'Email', 'FirstName', 'LastName', 'HasOptedOutOfEmail', 'Email_subscriptions__c', 'email_sub_token__c')){
	global $settings;
	$debug = $settings['debug'];
	
	$field_string = ucp_iContact_constructSOQLFieldString($fields);
	
	$query_by_token = "SELECT " . $field_string . " FROM Contact WHERE email_sub_token__c = '" . $token . "'";
	if($debug) echo '<br />Using SOQL query: ' . $query_by_token . '<br />';
	$result = sfQuery($query_by_token);
	
	if($result === false){
		trigger_error('ucp_iContact_getSFContactByToken received a failure from sfQuery()', E_USER_WARNING);
	}
	
	return $result;
}


function ucp_iContact_getSFContactByEmail($email, $fields = array('Id', 'Email', 'FirstName', 'LastName', 'HasOptedOutOfEmail', 'Email_subscriptions__c')){
	global $settings;
	$debug = $settings['debug'];
	
	$field_string = ucp_iContact_constructSOQLFieldString($fields);
	
	$query_by_email = "SELECT " . $field_string . " FROM Contact WHERE Email = '" . $email . "'";
	if($debug) echo '<br />Using SOQL query: ' . $query_by_email . '<br />';
	$result = sfQuery($query_by_email);
	
	if($result === false){
		trigger_error('ucp_iContact_getSFContactByEmail received a failure from sfQuery()', E_USER_WARNING);
	}
	
	return $result;
}


/*
 * Checks to see if there is a Contact with the provided email and token both
 * If not, returns false
 * If there is, returns the Salesforce record
 */
function ucp_iContact_verifySubscriberToken($email, $token, $fields = array('Id', 'Email', 'FirstName', 'LastName', 'HasOptedOutOfEmail', 'Email_subscriptions__c', 'email_sub_token__c')){
	$sf_result = ucp_iContact_getSFContactByToken($token, $fields);
	
	if(!is_array($sf_result) and empty($sf_result)){
		echo '<p><strong>Unknown error contacting database!</strong><br />Please let us know this happened at <a href="mailto:support@ucp.org?subject=Subscriber Token unknown database error">support@ucp.org</a>.</p>';
		return false;
	}
	elseif(count($sf_result) > 1){
		echo '<p><strong>Inconceivable! Found more than one token instance!</strong> Please let us know this happened at <a href="mailto:support@ucp.org?subject=Duplicate Token Problem">support@ucp.org</a>.</p>';
		return false;
	}
	elseif(count($sf_result) == 0){
		return false;
	}
	else{
		if( ($sf_result[0]->fields->email_sub_token__c === $token) and ($sf_result[0]->fields->Email === $email) ) return $sf_result[0];
		else return false;
	}
}


/*
 * Checks to see if there is already a sign-up submitted for an email address. Returns true if there is, false if there isn't
 */
function ucp_iContact_isEmailInSignupTable($email, $verifcode = false){
	global $settings;
	$debug = $settings['debug'];
	
	if(empty($email)) return null;
	
	ucp_iContact_cleanSQL('new_registries', $settings['submission_good_for'], 'sql');
	
	$sql_db = ucp_iContact_SQLConnect('ucp_iContact_isEmailInSignupTable()');
	
	$query = "SELECT Email, verifcode, time, completed FROM new_registries WHERE Email = '" . ucp_iContact_sanitizeForMySQL($email) . "'";
	
	try{
		$res = $sql_db -> query($query);
	}
	catch(Exception $e){
		ucp_iContact_debugMsg($e->getMessage());
		return null;
	}
	if($res === false){
		echo '<strong>Error: failed signup table check.</strong><br>';
		ucp_iContact_debugMsg($sql_db->error);
		return null;
	}
	
	$results = array();
	while ($row = $res->fetch_assoc()) {
		$results[] = $row;
	}
	
	$sql_db->close();
	
	if($debug === 'verbose'){
		echo '<br />ucp_iContact_isEmailInSignupTable got this result for ' . $email . ':<br />';
		print_r($results);
		echo '<br />';
	}
	
	if(empty($results)) return false;
	
	if(empty($verifcode)) return true;   // just wanted to know if it exists
	else{
		if($results[0]['verifcode'] === $verifcode){
			if($results[0]['completed']) return 0; // If already completed, we don't want to process it again
			else return true;
		}
		else return false;
	}
}


/*
 * Checks to see if there is already a sign-up submitted for an email address. Returns true if there is, false if there isn't
 */
function ucp_iContact_isEmailInAddressChangeTable($email, $verifcode = false){
	global $settings;
	$debug = $settings['debug'];
	
	if(empty($email)) return null;
	
	ucp_iContact_cleanSQL('address_changes', $settings['submission_good_for'], 'sql');
	
	$sql_db = ucp_iContact_SQLConnect('ucp_iContact_isEmailInAddressChangeTable()');
	
	$query = "SELECT sfid, new_email, verifcode, time, completed FROM address_changes WHERE new_email = '" . ucp_iContact_sanitizeForMySQL($email) . "'";
	
	try{
		$res = $sql_db -> query($query);
	}
	catch(Exception $e){
		echo $e->getMessage()."\n";
		return null;
	}
	if($res === false){
		echo '<strong>Error: failed address change table check.</strong><br>';
		ucp_iContact_debugMsg($sql_db->error);
		return null;
	}
	
	$results = array();
	while ($row = $res->fetch_assoc()) {
		$results[] = $row;
	}
	
	$sql_db->close();
	
	if($debug === 'verbose'){
		echo '<br />ucp_iContact_isEmailInAddressChangeTable got this result for ' . $email . ':<br />';
		print_r($results);
		echo '<br />';
	}
	
	if(empty($results)) return false;
	
	if(empty($verifcode)) return true;   // just wanted to know if it exists
	else{
		if($results[0]['verifcode'] === $verifcode){
			if($results[0]['completed']) return 0; // If already completed, we don't want to process it again
			else return true;
		}
		else return false;
	}
}


/*
 * Accepts $sf_id, $old_email, $new_email, and $verification_code and inserts them into the SQL table for email changes
 *
 * Returns false on failure or the verification code string on success
 */
function ucp_iContact_insertEmailChangeToSQL($sf_id, $old_email, $new_email, $verification_code){
	if(empty($sf_id) or empty($old_email) or empty($new_email) or empty($verification_code)) return false;
	
	$query = "INSERT INTO address_changes (sfid, old_email, new_email, verifcode) VALUES ('". ucp_iContact_sanitizeForMySQL($sf_id) ."', '" . ucp_iContact_sanitizeForMySQL($old_email) . "', '" . ucp_iContact_sanitizeForMySQL($new_email) . "', '" . ucp_iContact_sanitizeForMySQL($verification_code) . "')";
	
	$sql_db = ucp_iContact_SQLConnect('ucp_iContact_insertEmailChangeToSQL()');
	
	try{
		$res = $sql_db -> query($query);
	}
	catch(Exception $e){
		ucp_iContact_debugMsg($e->getMessage());
		return null;
	}
	if($res === false){
		echo '<strong>Error: failed submitting email change to database.</strong><br>';
		ucp_iContact_debugMsg($sql_db->error);
		$sql_db->close();
		return null;
	}
	
	$sql_db->close();
	
	return true;
}


/*
 * Accepts array $data containing all POST fields, and $existing_contact to indicate if there's one in Salesforce already
 *
 * Returns false on failure or the verification code string on success
 */
function ucp_iContact_insertSignupDataToSQL($data, $verification_code, $existing_contact = false){
	if(empty($data) or empty($verification_code)) return false;
	
	if(empty($existing_contact)) $existing_contact = 'false';
	
	$sql_db = ucp_iContact_SQLConnect('ucp_iContact_insertSignupDataToSQL()');
	
	$query = "INSERT INTO new_registries (Email, data_serialized, verifcode, existing_contact) VALUES ('". ucp_iContact_sanitizeForMySQL($data['Email']) ."', '" . ucp_iContact_sanitizeForMySQL(serialize($data)) . "', '" . ucp_iContact_sanitizeForMySQL($verification_code) . "', '" . ucp_iContact_sanitizeForMySQL($existing_contact) . "')";
	try{
		$res = $sql_db -> query($query);
	}
	catch(Exception $e){
		echo $e->getMessage()."\n";
		return null;
	}
	if($res === false){
		echo '<strong>Error: failed submitting signup to database.</strong><br>';
		ucp_iContact_debugMsg($sql_db->error);
		$sql_db->close();
		return null;
	}
	
	$sql_db->close();
	
	return true;
}


/*
 * Adds a signup into Salesforce as a Contact (or edits existing one). Returns an array of ($result-boolean, error-message)
 */
function ucp_iContact_subscribeEmail($email){
	
	/*
	 * Removes an entry in the SQL subscription signup table by email.
	 * Returns a string if encountering an error, or boolean true on success
	 */
	function updateSQLEntryAsProcessed($email){
		$update_query = "UPDATE new_registries SET completed = 1 WHERE Email = '" . ucp_iContact_sanitizeForMySQL($email) . "'";
		$sql_db = ucp_iContact_SQLConnect('ucp_iContact_subscribeEmail() -> updateSQLEntryAsProcessed');
		if(!$sql_db) return 'Error: Could not connect to holding table.';
		try{
			$res = $sql_db -> query($update_query);
		}
		catch(Exception $e){
			return "SQL exception: ".$e->getMessage()."\n";
		}
		$sql_db->close();
		if(empty($res)){
			return 'Error: could not mark subscription as completed in holding table.';
		}
		else return true;
	}
	
	
	
	global $settings;
	$debug = $settings['debug'];
	
	if(empty($email)) return null;
	
	$sql_db = ucp_iContact_SQLConnect('ucp_iContact_isEmailInSignupTable()');
	
	$query = "SELECT Email, data_serialized, existing_contact FROM new_registries WHERE Email = '" . ucp_iContact_sanitizeForMySQL($email) . "'";
	
	try{
		$res = $sql_db -> query($query);
	}
	catch(Exception $e){
		ucp_iContact_debugMsg($e->getMessage());
		return null;
	}
	if($res === false){
		echo '<strong>Error: failed signup table query.</strong><br>';
		ucp_iContact_debugMsg($sql_db->error);
		$sql_db->close();
		return null;
	}
	
	$results = array();
	while ($row = $res->fetch_assoc()) {
		$results[] = $row;
	}
	
	$sql_db->close();
	unset($res);
	
	$data = unserialize($results[0]['data_serialized']);
	if(!empty($results[0]['existing_contact']) and $results[0]['existing_contact'] !== 'false') $existing_contact = $results[0]['existing_contact'];
	else $existing_contact = false;
	
	if($debug === 'verbose'){
		echo '<br />ucp_iContact_subscribeEmail got this result from the registration table for ' . $email . ':<br /><pre>';
		print_r($data);
		echo 'Existing contact: ' . $existing_contact . '</pre><br />';
	}
	
	$minor_errors = array();
	
	if($existing_contact){   // we're doing an update to an existing contact
		$established_record = ucp_iContact_getSFContactByEmail($email, $fields = array('Id', 'Email', 'FirstName', 'LastName', 'ucp_contact_middle_name__c', 'Salutation', 'ucp_contact_suffix__c', 'HasOptedOutOfEmail', 'Email_subscriptions__c', 'MailingCountry', 'MailingState', 'MailingCity', 'MailingPostalCode', 'MailingStreet', 'Phone'));
		
		if($established_record === false){
			return array(false, 'Error contacting our database!');
		}
		
		if($debug === 'verbose'){
			echo '<br />ucp_iContact_subscribeEmail got this result from Salesforce for ' . $email . ':<br /><pre>';
			print_r($established_record);
			echo '</pre><br />';
		}
		
		if(empty($established_record)) return array(false, 'Existing record deleted!');
		elseif(count($established_record) > 1) return array(false, 'More than one existing entry for this email address!');
		else $established_record = $established_record[0];
		
		if($established_record->Id !== $existing_contact) return array(false, 'Existing record Id mismatch!');
		if($established_record->fields->HasOptedOutOfEmail === 'true') return array(false, 'Contact already opted out!');
		if(!empty($established_record->fields->Email_subscriptions__c) and $established_record->fields->Email_subscriptions__c !== 'true') return array(false, 'Contact already subscribed!');
		elseif($debug) echo '<strong>No previous newsletter subscriptions found.</strong>';
		
		$contact_update = new sObject;
		$contact_update->type = 'Contact';
		$contact_update->Id = $existing_contact;
		
		$update_fields = array();
		
		if( (!empty($data['FirstName'])) and (empty($established_record->fields->FirstName) or $established_record->fields->FirstName === 'true' or $established_record->fields->FirstName === 'Anon' or $established_record->fields->FirstName === 'Anonymous') )
			$update_fields['FirstName'] = $data['FirstName'];
		elseif(!empty($data['FirstName']) and $data['FirstName'] !== $established_record->fields->FirstName) $minor_errors[] = 'Could not update First Name. This is normal - please contact <a href="'.$settings['subscriptions_email_address'].'">'.$settings['subscriptions_email_address'].'</a> to change your name in our records.';
		
		if( (!empty($data['LastName'])) and (empty($established_record->fields->LastName) or $established_record->fields->LastName === 'true' or $established_record->fields->LastName === 'Anon' or $established_record->fields->LastName === 'Anonymous') )
			$update_fields['LastName'] = $data['LastName'];
		elseif(!empty($data['LastName']) and $data['LastName'] !== $established_record->fields->LastName) $minor_errors[] = 'Could not update Last Name. This is normal - please contact <a href="mailto:'.$settings['subscriptions_email_address'].'">'.$settings['subscriptions_email_address'].'</a> to change your name in our records.';
		
		if( (!empty($data['ucp_contact_middle_name__c'])) and (empty($established_record->fields->ucp_contact_middle_name__c) or $established_record->fields->ucp_contact_middle_name__c === 'true') )
			$update_fields['ucp_contact_middle_name__c'] = $data['ucp_contact_middle_name__c'];
		elseif(!empty($data['ucp_contact_middle_name__c']) and $data['ucp_contact_middle_name__c'] !== $established_record->fields->ucp_contact_middle_name__c) $minor_errors[] = 'Could not update Middle Name. This is normal - please contact <a href="'.$settings['subscriptions_email_address'].'">'.$settings['subscriptions_email_address'].'</a> to change your name in our records.';
		
		if( (!empty($data['Salutation'])) and (empty($established_record->fields->Salutation) or $established_record->fields->Salutation === 'true') )
			$update_fields['Salutation'] = $data['Salutation'];
		elseif(!empty($data['Salutation']) and $data['Salutation'] !== $established_record->fields->Salutation) $minor_errors[] = 'Could not update Prefix. This is normal - please contact <a href="'.$settings['subscriptions_email_address'].'">'.$settings['subscriptions_email_address'].'</a> to change your name in our records.';
		
		if( (!empty($data['ucp_contact_suffix__c'])) and (empty($established_record->fields->ucp_contact_suffix__c) or $established_record->fields->ucp_contact_suffix__c === 'true') )
			$update_fields['ucp_contact_suffix__c'] = $data['ucp_contact_suffix__c'];
		elseif(!empty($data['ucp_contact_suffix__c']) and $data['ucp_contact_suffix__c'] !== $established_record->fields->ucp_contact_suffix__c) $minor_errors[] = 'Could not update Suffix. This is normal - please contact <a href="'.$settings['subscriptions_email_address'].'">'.$settings['subscriptions_email_address'].'</a> to change your name in our records.';
		
		$auto_approved_fields = array('email_sub_organization__c', 'MailingCountry', 'MailingState', 'MailingCity', 'MailingPostalCode', 'MailingStreet', 'Phone');
		foreach($auto_approved_fields as $an_auto_approved_field){
			if(!empty($data[$an_auto_approved_field])) $update_fields[$an_auto_approved_field] = $data[$an_auto_approved_field];
		}
		
		$update_fields['Email_subscriptions__c'] = ucp_iContact_getSubscriptionString($data['newsletters']);
		
		$token = ucp_iContact_genNewSubToken();
		if(empty($token)) return array(false, 'Could not generate a new token!');
		$update_fields['email_sub_token__c'] = $token;
		
		$contact_update->fields = $update_fields;
		
		if($debug === 'verbose'){
			echo '<br />ucp_iContact_subscribeEmail prepared this update for Contact ' . $contact_update->Id . ':<br /><pre>';
			print_r($contact_update);
			echo '</pre><br />';
		}
		
		$update_result = sfUpdate(array($contact_update));
		
		if($debug === 'verbose' or ($debug and empty($update_result[0]->success)) ){
			echo '<br />ucp_iContact_subscribeEmail received this result when updating Contact ' . $contact_update->Id . ':<br /><pre>';
			print_r($update_result);
			echo '</pre><br />';
		}
		
		if(empty($update_result[0]->success)) return array(false, 'Record update error!', $update_result);
		else{   // success!
			$SQL_update_result = updateSQLEntryAsProcessed($email);
			if(is_string($SQL_update_result)) $minor_errors[] = $SQL_update_result;
			return array(true, $minor_errors, $update_result);
		}
	}
	else{   // we're creating a new contact
		$new_contact = new sObject;
		$new_contact->type = 'Contact';
		
		$new_contact->fields['Email'] = $data['Email'];
		if(!empty($data['LastName'])) $new_contact->fields['LastName'] = $data['LastName'];
		else $new_contact->fields['LastName'] = "Anonymous";
		
		$auto_approved_fields = array('FirstName', 'ucp_contact_middle_name__c', 'Salutation', 'ucp_contact_suffix__c', 'email_sub_organization__c', 'MailingCountry', 'MailingState', 'MailingCity', 'MailingPostalCode', 'MailingStreet', 'Phone');
		foreach($auto_approved_fields as $an_auto_approved_field){
			if(!empty($data[$an_auto_approved_field])) $new_contact->fields[$an_auto_approved_field] = $data[$an_auto_approved_field];
		}
		
		$new_contact->fields['Email_subscriptions__c'] = ucp_iContact_getSubscriptionString($data['newsletters']);
		
		$token = ucp_iContact_genNewSubToken();
		if(empty($token)) return array(false, 'Could not generate a new token!');
		$new_contact->fields['email_sub_token__c'] = $token;
		
		if($debug === 'verbose'){
			echo '<br />ucp_iContact_subscribeEmail prepared this new Contact:<br /><pre>';
			print_r($new_contact);
			echo '</pre><br />';
		}
		
		$upload_result = sfCreate(array($new_contact));
		
		if($debug === 'verbose' or ($debug and empty($upload_result[0]->success)) ){
			echo '<br />ucp_iContact_subscribeEmail received this result when creating a Contact:<br /><pre>';
			print_r($upload_result);
			echo '</pre><br />';
		}
		
		if(empty($upload_result[0]->success)) return array(false, 'Subscription upload failed!', $upload_result);
		elseif($upload_result[0]->success == 0) return array(false, 'Subscription upload failed!', $upload_result);
		
		$SQL_update_result = updateSQLEntryAsProcessed($email);
		if(is_string($SQL_update_result)) $minor_errors[] = $SQL_update_result;
		
		return array(true, $minor_errors, $upload_result);
	}
}


/*
 * Effects an address change held in SQL in Salesforce. Returns an array of ($result-boolean, error-message)
 */
function ucp_iContact_updateEmail($email, $verifcode){
	
	/*
	 * Removes an entry in the SQL address change table by SF id.
	 * Returns a string if encountering an error, or boolean true on success
	 */
	function updateSQLEntryAsProcessed($sf_id){
		$update_query = "UPDATE address_changes SET completed = 1 WHERE sfid = '" . ucp_iContact_sanitizeForMySQL($sf_id) . "'";
		$sql_db = ucp_iContact_SQLConnect('ucp_iContact_completeAddressChange -> updateSQLEntryAsProcessed');
		if(!$sql_db) return 'Error: Could not connect to holding table.';
		try{
			$res = $sql_db -> query($update_query);
		}
		catch(Exception $e){
			return "SQL exception: ".$e->getMessage()."\n";
		}
		$sql_db->close();
		if(empty($res)){
			return 'Error: could not mark address change as successful in holding table.';
		}
		else return true;
	}
	
	
	
	global $settings;
	$debug = $settings['debug'];
	
	if(empty($email)) return null;
	
	$sql_db = ucp_iContact_SQLConnect('ucp_iContact_completeAddressChange()');
	
	$query = "SELECT sfid, old_email, new_email FROM address_changes WHERE new_email = '" . ucp_iContact_sanitizeForMySQL($email) . "' AND verifcode = '" . ucp_iContact_sanitizeForMySQL($verifcode) . "'";
	
	try{
		$res = $sql_db -> query($query);
	}
	catch(Exception $e){
		ucp_iContact_debugMsg($e->getMessage());
		return null;
	}
	if($res === false){
		echo '<strong>Error: failed signup table query.</strong><br>';
		ucp_iContact_debugMsg($sql_db->error);
		$sql_db->close();
		return null;
	}
	
	$results = array();
	while ($row = $res->fetch_assoc()) {
		$results[] = $row;
	}
	
	$sql_db->close();
	unset($res);
	
	if($debug === 'verbose'){
		echo '<br />ucp_iContact_updateEmail got this result from the registration table for ' . $email . ':<br /><pre>';
		print_r($results);
		echo '</pre><br />';
	}
	
	$minor_errors = array();
	
	if(empty($results)) return array(false, 'Existing record deleted!');
	elseif(count($results) > 1) return array(false, 'More than one existing entry for this email address!');
	else $data = $results[0];
	
	$contact_update = new sObject;
	$contact_update->type = 'Contact';
	$contact_update->Id = $data['sfid'];
	
	$update_fields = array();
	$update_fields['Email'] = $data['new_email'];
	$contact_update->fields = $update_fields;
	
	if($debug === 'verbose'){
		echo '<br />ucp_iContact_updateEmail prepared this update for a Contact:<br /><pre>';
		print_r($contact_update);
		echo '</pre><br />';
	}
	
	$update_result = sfUpdate(array($contact_update));
	
	if($debug === 'verbose' or ($debug and empty($update_result[0]->success)) ){
		echo '<br />ucp_iContact_updateEmail received this result when updating Contact ' . $contact_update->Id . ':<br /><pre>';
		print_r($update_result);
		echo '</pre><br />';
	}
	
	if(empty($update_result[0]->success)) return array(false, 'Record update error!', $update_result);
	else{   // success!
		$SQL_update_result = updateSQLEntryAsProcessed($data['sfid']);
		if(is_string($SQL_update_result)) $minor_errors[] = $SQL_update_result;
		return array(true, $minor_errors, $update_result);
	}
}


function ucp_iContact_updateContact($id, $update_fields){
	global $settings;
	$debug = $settings['debug'];
	
	$contact_update = new sObject;
	$contact_update->type = 'Contact';
	$contact_update->Id = $id;
	$contact_update->fields = $update_fields;
	
	if($debug === 'verbose'){
		echo '<br />ucp_iContact_updateContact prepared this object:<br /><pre>';
		print_r($contact_update);
		echo '</pre><br />';
	}
	
	$update_result = sfUpdate(array($contact_update));
	
	if($debug === 'verbose' or ($debug and empty($update_result[0]->success)) ){
		echo '<br />ucp_iContact_updateContact received this result when updating Contact ' . $contact_update->Id . ':<br /><pre>';
		print_r($update_result);
		echo '</pre><br />';
	}
	
	if(empty($update_result[0]->success)) return false;
	else return true;
}


/*
 * Returns array of delayed/dropped email entries. The queue/SQL table must be specified in the call.
 * Remember, valid tables are:
 *           pending_email (hasn't been sent successfully yet, but system will keep trying)
 *           sent_email (there were one or more failures initially, but it has now been sent)
 *           failed_email (too many failures/over too long a time period, system has given up trying to send this one)
 * Each entry in the return is an array: (ID, time, last_attempt, address, name, subject, htmlbody, textbody, sfid, attempts, notes)
 */
function ucp_iContact_getEmailTableEntries($table){
	$valid_tables = array('pending_email', 'sent_email', 'failed_email');
	if(!in_array($table, $valid_tables)){
		ucp_iContact_debugMsg('Error: ucp_iContact_getEmailTableEntries() was asked to retrieve from an invalid table: ' . $table);
		return null;
	}
	
	$sql_db = ucp_iContact_SQLConnect('ucp_iContact_getEmailTableEntries()');
	if(!$sql_db) return null;
	
	$query = "SELECT ID, address, name, subject, htmlbody, textbody, sfid, Attempts, time, last_attempt, notes FROM ".$table." ORDER BY last_attempt, time";
	
	try{
		$res = $sql_db -> query($query);
	}
	catch(Exception $e){
		ucp_iContact_debugMsg($e->getMessage());
		return null;
	}
	
	$entries = array();
	
	while ($row = $res->fetch_assoc()) {
		$entries[] = array('ID' => $row['ID'],
						   'time' => $row['time'],
						   'last_attempt' => $row['last_attempt'],
						   'address' => $row['address'],
						   'name' => $row['name'],
						   'subject' => $row['subject'],
						   'htmlbody' => $row['htmlbody'],
						   'textbody' => $row['textbody'],
						   'sfid' => $row['sfid'],
						   'attempts' => $row['Attempts'],
						   'notes' => $row['notes']);
	}
	
	$sql_db->close();
	
	return $entries;
}


/*
 * Send an email. Is routed through a MySQL queue to be tried again if initial send fails.
 */
function ucp_iContact_sendEmail($queue_id = 0, $email = '', $subject = '', $html_body = '', $text_body = '', $name='', $sf_id = ''){
	
	if(!function_exists('logFirstSend')){
		function logFirstSend($table, $email, $name, $subject, $html_body, $text_body, $sf_id, $attempts = 1, $first_attempt = 'CURRENT_TIMESTAMP', $last_attempt = 'CURRENT_TIMESTAMP', $id = 0){
			$query = "INSERT INTO ".$table." (address, name, subject, htmlbody, textbody, sfid, Attempts, time, last_attempt";
			if(!empty($id)) $query .= ", ID";
			$query .= ") VALUES ('".ucp_iContact_sanitizeForMySQL($email)."', '".ucp_iContact_sanitizeForMySQL($name)."', '".ucp_iContact_sanitizeForMySQL($subject)."', '".ucp_iContact_sanitizeForMySQL($html_body)."', '".ucp_iContact_sanitizeForMySQL($text_body)."', '".$sf_id."', 1, '".$first_attempt."', '".$last_attempt."'";
			if(!empty($id)) $query .= ", " . $id;
			$query .= ")";
			
			$sql_db = ucp_iContact_SQLConnect('ucp_iContact_sendEmail -> logFirstSend');
			try{
				$res = $sql_db -> query($query);
			}
			catch(Exception $e){
				echo 'Error: SQL Exception logging mail!';
				ucp_iContact_debugMsg('SQL Exception logging mail!' . $e->getMessage()."   Query: <pre>".$query."</pre>");
				return null;
			}
			
			if($res === false){
				echo '<p><strong>Error: failed to log email send!</strong></p>';
				ucp_iContact_debugMsg('Failed to log email send! Query: <pre>'.htmlspecialchars($query).'</pre>Error:<pre>'.$sql_db->error.'</pre>');
				$sql_db->close();
				return null;
			}
			elseif($table === 'sent_email' and $id > 0){ //delete from pending_email
				$query = "DELETE FROM pending_email WHERE ID = " . $id;
				try{
					$res = $sql_db -> query($query);
				}
				catch(Exception $e){
					echo 'Error: SQL Exception deleting entry from pending queue!';
					ucp_iContact_debugMsg('SQL Exception deleting entry from pending queue! ' . $e->getMessage()."<br />Query: ".$query);
					$sql_db->close();
					return true;
				}
				
				if($res === false){
					ucp_iContact_debugMsg('Error: Unsuccessful query: could not delete entry from pending queue! Query:<pre>'.htmlspecialchars($query).'</pre> Error:<pre>'.$sql_db->error.'</pre>');
				}
			}
			
			$sql_db->close();
			return true;
		}
	}
	
	
	if(!function_exists('incrementAttempts')){
		function incrementAttempts($table, $queue_id){
			$update_query = "UPDATE ".$table." SET Attempts = (Attempts + 1), last_attempt = CURRENT_TIMESTAMP WHERE ID = '".$queue_id."'";
			
			$sql_db = ucp_iContact_SQLConnect('ucp_iContact_sendEmail -> incrementAttempts');
			
			try{
				$update_res = $sql_db -> query($update_query);
			}
			catch(Exception $e){
				ucp_iContact_debugMsg('SQL Exception incrementing email attempt count!' . $e->getMessage()."<br />Query: ".$query);
				return null;
			}
			if($res === false){
				ucp_iContact_debugMsg('Failed to log email send attempt! Query: '.$query.'</p>');
				$sql_db->close();
				return null;
			}
			
			$sql_db->close();
			
			return true;
		}
	}
	
	
	global $settings;
	$debug = $settings['debug'];
	
	if(empty($queue_id)){   // new message, first try
		if(empty($email) or empty($subject) or (empty($html_body) and empty($text_body))){
			ucp_iContact_debugMsg('ucp_iContact_sendEmail() was not given all necessary parameters for a new message!');
			return null;
		}
		
		$send_result = ucp_iContact_processEmail($email, $subject, $html_body, $text_body, $name);
		if(!empty($send_result)){   // success!
			logFirstSend('sent_email', $email, $name, $subject, $html_body, $text_body, $sf_id);
			return $send_result;
		}
		else{   // failure!
			logFirstSend('pending_email', $email, $name, $subject, $html_body, $text_body, $sf_id);
			return false;
		}
	}
	else{   // retry previously failed message
		if(!is_int($queue_id)){
			ucp_iContact_debugMsg('ucp_iContact_sendEmail() was given an invalid queue ID!');
			return null;
		}
		
		if($queue_id < 0){
			$query = "SELECT ID, address, name, subject, htmlbody, textbody, sfid, time, Attempts, last_attempt FROM pending_email ORDER BY last_attempt LIMIT 1";
		}
		else{
			$query = "SELECT ID, address, name, subject, htmlbody, textbody, sfid, time, Attempts, last_attempt FROM pending_email WHERE ID = " . $queue_id;
		}
		
		if($debug === 'verbose'){
			echo '<p>ucp_iContact_sendEmail() using query:<br />' . $query . '</p>';
		}
		
		$sql_db = ucp_iContact_SQLConnect('ucp_iContact_sendEmail()');
		if(!$sql_db) return null;
		try{
			$res = $sql_db -> query($query);
		}
		catch(Exception $e){
			ucp_iContact_debugMsg($e->getMessage());
			return null;
		}
		
		$entries = array();
		while ($row = $res->fetch_assoc()) {
			$entries[] = array('ID' => $row['ID'],
							   'address' => $row['address'],
							   'name' => $row['name'],
							   'subject' => $row['subject'],
							   'htmlbody' => $row['htmlbody'],
							   'textbody' => $row['textbody'],
							   'sfid' => $row['sfid'],
							   'time' => $row['time'],
							   'Attempts' => $row['Attempts'],
							   'last_attempt' => $row['last_attempt']
							  );
		}
		
		$sql_db->close();
		
		if(empty($entries)){
			if($queue_id > -1) ucp_iContact_debugMsg('Error in ucp_iContact_sendEmail: requested queue ID of '.$queue_id.' does not exist!');
			return null;
		}
		elseif(count($entries) > 1){
			if($queue_id > -1) ucp_iContact_debugMsg('Error in ucp_iContact_sendEmail: requested queue ID of '.$queue_id.' turned up more than one record! Inconceivable!');
			return false;
		}
		else $pending_email = $entries[0];
		
		if($debug === 'verbose'){
			echo '<p>The entry found by ucp_iContact_sendEmail():</p><pre>'.htmlspecialchars(print_r($pending_email, true)).'</pre><br />';
		}
		
		$send_result = ucp_iContact_processEmail($pending_email['address'], $pending_email['subject'], $pending_email['htmlbody'], $pending_email['textbody'], $pending_email['name']);
		
		if(empty($send_result)){   // if message failed again, update attempts data
			incrementAttempts('pending_email', $pending_email['ID']);
			return false;
		}
		else{   // message succeeded this time, move into sent_email
			logFirstSend('sent_email', $pending_email['address'], $pending_email['name'], $pending_email['subject'], $pending_email['htmlbody'], $pending_email['textbody'], $pending_email['sfid'], $pending_email['Attempts'], $pending_email['time'], $pending_email['last_attempt'], $pending_email['ID']);
			incrementAttempts('sent_email', $pending_email['ID']);
			return true;
		}
	}
}

?>