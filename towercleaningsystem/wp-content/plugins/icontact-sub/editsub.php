<?php

function ucp_iContact_doEditSubPage($atts){
	wp_enqueue_script('jquery');
	global $settings;
	$debug = $settings['debug'];
	
	ob_start();
	
	if($debug) echo '<strong>Debug mode on.</strong><br />';
	
	$show_form = true;
	$show_form_only = false;
	$stop_errors = array();
	
	ucp_iContact_getStylesheet();
	
	if(empty($_GET['Email']) and empty($_GET['token'])){
		$show_form_only = true;
	}
	elseif(empty($_GET['Email'])){
		$stop_errors[] = "You didn't provide your email address! Please enter that as well.";
	}
	elseif(empty($_GET['token'])){
		$stop_errors[] = 'You didn\'t include a code!';
	}
	else{   //have fields, will validate
		if(strpos($_GET['Email'], "@") === false) $_GET['Email'] = rawurldecode($_GET['Email']);   // Compensate for iContact's requirement that the % in escape characters also be escaped
		$stop_errors = ucp_iContact_validateUserInfo($_GET, array('Email', 'token'));
	}
	
	if(empty($stop_errors) and !$show_form_only){   // now let's verify with Salesforce
		$sf_fields_to_get = array('Id', 'Email', 'FirstName', 'LastName', 'ucp_contact_middle_name__c', 'Salutation', 'ucp_contact_suffix__c', 'HasOptedOutOfEmail', 'Email_subscriptions__c', 'email_sub_token__c', 'email_sub_organization__c', 'MailingCountry', 'MailingState', 'MailingCity', 'MailingPostalCode', 'MailingStreet', 'Phone');
		$sf_record = ucp_iContact_verifySubscriberToken($_GET['Email'], $_GET['token'], $sf_fields_to_get);
		if(!$sf_record){
			$stop_errors[] = 'Either this email is not in our records, or the token code is incorrect. If you followed a link provided in a recent email from us or are otherwise certain you have a correct token, please contact <a href="mailto:'.$settings['subscriptions_email_address'].'">'.$settings['subscriptions_email_address'].'</a>.';
		}
		elseif(empty($sf_record->fields->Email_subscriptions__c) or $sf_record->fields->Email_subscriptions__c === 'true') $stop_errors[] = 'This address is not currently subscribed to any newsletters. Please use the <a href="'.ucp_iContact_getURLForShortcode('icontact_signup').'">signup form</a> instead.';
		else{
			$show_form = false;
			
			ucp_iContact_doEditSubForm($sf_record);
		}
	}
	
	if(!empty($stop_errors)){   // show errors
		?>
		<div class="newsletter-errors">
			<p><strong>There were problems authenticating your subscription.</strong></p>
			<ul>
				<?php foreach($stop_errors as $an_error) echo '<li>' . $an_error . '</li>'; ?>
			</ul>
		</div>
		<?php
	}
	
	if($show_form){ ?>
		<form name="ucp-newsletter-edit-sub" id="ucp-newsletter-edit-sub" class="ucp-newsletter-emailpluscode" method="GET" action="<?php echo ucp_iContact_getCurrentURL(); ?>">
			<div>
				<p><label for="Email">Email</label> <input type="text" name="Email" id="Email" maxlength="80" value="<?php if(!empty($_GET['Email'])) echo $_GET['Email'] ?>" placeholder="name@domain.com" /></p>
				<p><label for="token">Code</label> <input type="text" name="token" id="newsletter-token" maxlength="32" value="<?php if(!empty($_GET['token'])) echo $_GET['token'] ?>" /></p>
				<p class="submit"><input type="submit" id="editsub-submit" class="submit" value="Edit my Subscriptions" /></p>
			</div>
		</form>
	<?php }
	
	return ob_get_clean();
}

if(function_exists('add_shortcode')){
	add_shortcode('icontact_sub_edit', 'ucp_iContact_doEditSubPage');
}


function ucp_iContact_doEditSubForm($sf_record){
	global $settings;
	$debug = $settings['debug'];
	
	$submission_made = false;
	$submission_validated = false;
	
	if(isset($_POST['formsubmitted'])){
		$submission_made = true;
		$existing_id = false;
		
		ucp_iContact_stripPOSTslashes();
		
		$locked_fields = array('Salutation', 'FirstName', 'ucp_contact_middle_name__c', 'LastName', 'ucp_contact_suffix__c');
		$editable_fields = array('Email', 'email_sub_organization__c', 'MailingCountry', 'MailingState', 'MailingCity', 'MailingPostalCode', 'MailingStreet', 'Phone');
		
		//Don't allow user edit of locked fields.
		foreach($locked_fields as $a_locked_field){
			if(isset($_POST[$a_locked_field])) unset($_POST[$a_locked_field]);
		}
		
		if(!empty($_POST['address1']) or !empty($_POST['address2'])){
			if(empty($_POST['address2'])) $_POST['MailingStreet'] = $_POST['address1'];
			elseif(empty($_POST['address1'])) $_POST['MailingStreet'] = "\n" . $_POST['address2'];
			else $_POST['MailingStreet'] = $_POST['address1'] . "\n" . $_POST['address2'];
		}
		
		// Validate input
		$stop_errors = ucp_iContact_validateUserInfo($_POST, array('Email'));
		
		if(empty($stop_errors)){ //everything's validated, so let's put our Contact update object together
			$submission_validated = true;
			ucp_iContact_sanitizePOSTForHTML();
			
			if($debug === 'verbose'){
				echo '<p>User input passed validation. Here is the POST:</p><pre>';
				print_r($_POST);
				echo '</pre>';
			}
			
			//Begin email update process if applicable
			$email_changed = false;
			if(!empty($_POST['Email']) and $_POST['Email'] !== $sf_record->fields->Email){
				$email_changed = true;
				
				$old_email = $sf_record->fields->Email;
				$new_email = $_POST['Email'];
				$verification_code = ucp_iContact_randomString(20);
				$sf_id = $sf_record->Id;
				
				$insert_result = ucp_iContact_insertEmailChangeToSQL($sf_id, $old_email, $new_email, $verification_code);
				if(!$insert_result) $stop_errors[] = 'Could not submit email address change to database. Your email address was not changed.';
				else{
					if(empty($sf_record->fields->FirstName) or $sf_record->fields->FirstName === 'true') $full_name = $sf_record->fields->LastName;
					else $full_name = $sf_record->fields->FirstName . ' ' . $sf_record->fields->LastName;
					
					$subscriptions_email_address = $settings['subscriptions_email_address'];
					
					$verification_page_url = ucp_iContact_getURLForShortcode('icontact_signup_verification');
					if(empty($verification_page_url)) $verification_page_url = 'PLACEHOLDER';
					
					$verification_page_url_prefilled = $verification_page_url . '?Email=' . rawurlencode($new_email) . '&SignupVerificationCode=' . rawurlencode($verification_code);
					
					$email_template_html = <<<EOT
<html><body>
<center><img src="http://ucplabs.org/mailassets/icontact_subs/UCPLogo.png" alt="United Cerebral Palsy logo" style="margin: 5 auto;" /></center>
<p>We received a request to change your email address from $old_email and instead send all your subscriptions to $new_email.</p>
<p>To confirm your request and change your email address, please visit <a href="$verification_page_url_prefilled">$verification_page_url_prefilled</a> - or, go to <a href="$verification_page_url">$verification_page_url</a> and input your new email address and this code: <b>$verification_code</b></p>
<p>If you did not request this change, you can ignore this email and the email address for this subscription will remain $old_email.</p>
<p>You may also contact us at: <a href="$subscriptions_email_address">$subscriptions_email_address</a></p>
</body></html>
EOT;
					$email_template_plain = <<<EOT
We received a request to change your email address from $old_email and instead send all your subscriptions to $new_email.

To confirm your request and change your email address, please visit $verification_page_url_prefilled - or, go to $verification_page_url_prefilled and input your new email address and this code: $verification_code

If you did not request this change, you can ignore this email and our email address for you will remain unchanged.

You may also contact us at: $subscriptions_email_address
EOT;
					
					$mailing_result = ucp_iContact_sendEmail(0, $new_email, $subject = 'Please confirm your email address change', $email_template_html, $email_template_plain, $full_name, $sf_id);
					
					if(!empty($mailing_result)){
						echo '<p>We\'ve sent you a verification email from '.$subscriptions_email_address.'. <strong>Please follow the instructions in this email to finalize your address change.</strong></p>';
					}
					else{
						echo '<p><strong>We were unable to email you at this time.</strong> We need you to confirm receipt of an email message at your new address before the change can be made. We\'ll try to reach you again later. If this continues to be a problem, you can reach us at <a href="'.$subscriptions_email_address.'">'.$subscriptions_email_address.'</a>.';
					}
				}
			}
			
			//Now update any other fields as necessary
			$update_fields = array();
			
			// Do a quick comparison on fields that don't need special handling
			$standard_fields = array('email_sub_organization__c', 'MailingCountry', 'MailingState', 'MailingCity', 'MailingPostalCode', 'Phone');
			foreach($standard_fields as $a_standard_field){
				if(isset($_POST[$a_standard_field]) and $_POST[$a_standard_field] !== htmlspecialchars($sf_record->fields->$a_standard_field) and !(empty($_POST[$a_standard_field]) and $sf_record->fields->$a_standard_field === 'true') ) $update_fields[$a_standard_field] = $_POST[$a_standard_field];
			}
			
			// Don't change the address if the only difference is newlines vs. commas
			if(isset($_POST['MailingStreet']) and ( ($_POST['MailingStreet'] !== htmlspecialchars($sf_record->fields->MailingStreet)) or (empty($_POST['MailingStreet']) and $sf_record->fields->MailingStreet !== 'true') ) ){
				$commas_replaced = str_replace(", ", "\n", $_POST['MailingStreet']);
				if($commas_replaced !== htmlspecialchars($sf_record->fields->MailingStreet)) $update_fields['MailingStreet'] = $_POST['MailingStreet'];
			}
			
			// Now test if subscriptions have changed
			$subscriptions_existing = $sf_record->fields->Email_subscriptions__c;
			
			if(!empty($_POST['newsletters'])) $subscriptions_submitted = ucp_iContact_getSubscriptionString($_POST['newsletters']);
			else $subscriptions_submitted = '';
			
			$subscriptions_existing = explode(";", $subscriptions_existing);
			$subscriptions_submitted = explode(";", $subscriptions_submitted);
			
			sort($subscriptions_existing);
			sort($subscriptions_submitted);
			
			if($subscriptions_existing !== $subscriptions_submitted){
				$update_fields['Email_subscriptions__c'] = ucp_iContact_getSubscriptionString($_POST['newsletters']);
			}
			
			if(empty($update_fields)){
				if(!$email_changed) $stop_errors[] = 'You did not change anything on the form, so no changes have been made.';
			}
			else{
				$update_result = ucp_iContact_updateContact($sf_record->Id, $update_fields);
				
				if(!$update_result) $stop_errors[] = 'Error submitting record update to database! Please try again.<br />If this problem persists, please contact <a href="mailto:support@ucp.org">support@ucp.org</a>.';
				elseif(!$email_changed) echo '<p><strong>We\'ve made the requested changes.</strong></p>';
				else echo '<p><strong>We\'ve already made your other requested changes.</strong></p>';
			}
		}
		
		// If there are errors, display them
		if(!empty($stop_errors)){
			?>
			<div class="newsletter-errors"><p><strong>There were problems submitting your request.</strong></p><ul>
			<?php
			foreach($stop_errors as $an_error) echo '<li>' . $an_error . '</li>';
			?>
			</ul></div>
			<?php
			if($debug === 'verbose'){
				echo '<br /><strong>POST</strong><br />';
				foreach($_POST as $post_name => $post_value){
					echo $post_name . ' => ' . $post_value . '<br />';
				}
				if(!empty($_POST['newsletters']) or true){
					echo '<br />';
					foreach($_POST['newsletters'] as $newsl_name => $newsl_value){
						echo $newsl_name . ' => ' . $newsl_value . '<br />';
					}
				}
			}
		}
	}
	
	if(!$submission_validated or !empty($stop_errors)){
		if(empty($_POST['address1']) and empty($_POST['address2'])){
			if(!empty($sf_record->fields->MailingStreet) and $sf_record->fields->MailingStreet !== 'true'){
				$address_array = explode("\n", $sf_record->fields->MailingStreet);
				if(!empty($address_array[0])) $address1_from_sf = $address_array[0];
				if(count($address_array) > 1){
					$address2_from_sf = '';
					for($i=1; $i < count($address_array); $i++){
						if($i > 1) $address2_from_sf .= ", ";
						$address2_from_sf .= $address_array[$i];
					}
				}
			}
		}
		
		$primary_newsletters = ucp_iContact_getPrimaryMailings();
		$primary_newsletter_names = array();
		$secondary_newsletters = array();
		$subscription_array = explode(";", $sf_record->fields->Email_subscriptions__c);
		foreach($primary_newsletters as $a_primary){
			$primary_newsletter_names[] = $a_primary->sf_name;
		}
		foreach($subscription_array as $a_subscription){
			if(!in_array($a_subscription, $primary_newsletter_names)) $secondary_newsletters[] = $a_subscription;
		}
		unset($a_subscription, $a_primary);
		
		$char_coding = ENT_QUOTES | ENT_HTML401;
		ucp_iContact_getStylesheet();
		?>
		
		<form name="ucp-newsletter-subedit" id="ucp-newsletter-subedit" class="ucp-newsletter" method="POST" action="<?php echo ucp_iContact_getCurrentURL(); ?>">
			<div class="contactfields">
				<input type="hidden" name="formsubmitted" value="true" />
				<p class="submit" id="oneClickUnsubContainer" style="display: none;"><input type="button" id="oneClickUnsub" class="submit" value="Unsubscribe Me from Everything" /></p>
				<p><span class="label"><label for="email">Email</label>*</span> <input type="email" name="Email" id="email" maxlength="80" value="<?php if(!empty($_POST['Email'])) echo $_POST['Email']; elseif($sf_record->fields->Email !== 'true') echo htmlspecialchars($sf_record->fields->Email); ?>" required="required" /></p>
				<p class="disabled"><span class="label"><label for="nameprefix">Prefix</label></span> <input type="text" name="Salutation" id="nameprefix" class="disabled" maxlength="40" value="<?php if($sf_record->fields->Salutation !== 'true') echo htmlspecialchars($sf_record->fields->Salutation); ?>" disabled="disabled" /></p>
				<p class="disabled"><span class="label"><label for="firstname">First Name</label></span> <input type="text" name="FirstName" id="firstname" class="disabled" maxlength="40" value="<?php echo htmlspecialchars($sf_record->fields->FirstName); ?>" disabled="disabled" /></p>
				<p class="disabled"><span class="label"><label for="middlename">Middle Name</label></span> <input type="text" name="ucp_contact_middle_name__c" id="middlename" class="disabled" maxlength="60" value="<?php if($sf_record->fields->ucp_contact_middle_name__c !== 'true') echo htmlspecialchars($sf_record->fields->ucp_contact_middle_name__c); ?>" disabled="disabled" /></p>
				<p class="disabled"><span class="label"><label for="lastname">Last Name</label></span> <input type="text" name="LastName" id="lastname" class="disabled" maxlength="80" value="<?php echo htmlspecialchars($sf_record->fields->LastName); ?>" disabled="disabled" /></p>
				<p class="disabled"><span class="label"><label for="namesuffix">Suffix</label></span> <input type="text" name="ucp_contact_suffix__c" id="namesuffix" class="disabled" maxlength="40" value="<?php if($sf_record->fields->ucp_contact_suffix__c !== 'true') echo htmlspecialchars($sf_record->fields->ucp_contact_suffix__c); ?>" disabled="disabled" /></p>
				<p><span class="label"><label for="email_sub_organization__c">Organization</label></span> <input type="text" name="email_sub_organization__c" id="email_sub_organization__c" maxlength="100" value="<?php if(!empty($_POST['email_sub_organization__c'])) echo $_POST['email_sub_organization__c']; elseif($sf_record->fields->email_sub_organization__c !== 'true') echo htmlspecialchars($sf_record->fields->email_sub_organization__c); ?>" /></p>
				<p>
					<span class="label"><label for="country">Country</label></span> <select name="MailingCountry" id="country" />
					<?php if(!empty($_POST['MailingCountry'])) $country_val = $_POST['MailingCountry'];
					else $country_val = $sf_record->fields->MailingCountry;
					ucp_iContact_outputCountrySelectOptions($country_val); ?>
					</select>
				</p>
				<p><span class="label"><label for="province">State/Province</label></span> <input type="text" name="MailingState" id="province" maxlength="80" value="<?php if(!empty($_POST['MailingState'])) echo $_POST['MailingState']; elseif($sf_record->fields->MailingState !== 'true') echo htmlspecialchars($sf_record->fields->MailingState); ?>" /></p>
				<p><span class="label"><label for="city">City</label></span> <input type="text" name="MailingCity" id="city" maxlength="40" value="<?php if(!empty($_POST['MailingCity'])) echo $_POST['MailingCity']; elseif($sf_record->fields->MailingCity !== 'true') echo htmlspecialchars($sf_record->fields->MailingCity);?>" /></p>
				<p><span class="label"><label for="postalcode">Zip/Postal Code</label></span> <input type="text" name="MailingPostalCode" id="postalcode" maxlength="20" value="<?php if(!empty($_POST['MailingPostalCode'])) echo $_POST['MailingPostalCode']; elseif($sf_record->fields->MailingPostalCode !== 'true') echo htmlspecialchars($sf_record->fields->MailingPostalCode); ?>" /></p>
				<p><span class="label"><label for="address1">Address</label></span> <input type="text" name="address1" id="address1" maxlength="255" value="<?php if(!empty($_POST['address1'])) echo $_POST['address1']; elseif(!empty($address1_from_sf)) echo htmlspecialchars($address1_from_sf); ?>"; /></p>
				<p><span class="label"><label for="address2">Address Line 2</label></span> <input type="text" name="address2" id="address2" maxlength="255" value="<?php if(!empty($_POST['address2'])) echo $_POST['address2']; elseif(!empty($address2_from_sf)) echo htmlspecialchars($address2_from_sf); ?>" /></p>
				<p><span class="label"><label for="phone">Phone</label></span> <input type="tel" name="Phone" id="phone" maxlength="40" value="<?php if(!empty($_POST['Phone'])) echo $_POST['Phone']; elseif($sf_record->fields->Phone !== 'true') echo htmlspecialchars($sf_record->fields->Phone); ?>" /></p>
				<p></p><strong>*required</strong></p>
			</div>
			<div class="newslettersbox">
				<div>
					<p style="float:left;">You can subscribe or unsubscribe to our newsletters here.</p>
					<p id="newsletters-toggle-container" style="float:right; display:none;"><input type="checkbox" name="newsletters-toggle" id="newsletters-toggle" value="newsletters-toggle" /> <label class="newsletter" for="newsletters-toggle">Everything</label></p>
				</div>
				<?php foreach($primary_newsletters as $a_newsletter):
					$friendly_name = preg_replace("/[^A-Za-z0-9]/", '', $a_newsletter->sf_name);
					if($a_newsletter->isIndie()): ?>
						<div>
							<input type="checkbox" id="indie-<?php echo $friendly_name; ?>" disabled="disabled" /> <label class="newsletter" for="indie-<?php echo $friendly_name; ?>">
							<?php if(!empty($a_newsletter->indie_link)): ?><a href="<?php echo $a_newsletter->indie_link; ?>" target="_blank"><?php endif; ?>
							<?php echo $a_newsletter->label; ?>
							<?php if(!empty($a_newsletter->indie_link)): ?></a><?php endif; ?>
							</label><br />
							<p class="newsletter-desc"><?php echo $a_newsletter->description; ?></p>
						</div>
					<?php elseif($a_newsletter->isActive() or strpos($sf_record->fields->Email_subscriptions__c, $a_newsletter->sf_name) !== false): ?>
						<div>
							<input type="checkbox" name="newsletters[]" id="newsletters-<?php echo $friendly_name; ?>" value="<?php echo $a_newsletter->sf_name; ?>"<?php if( (!$submission_made and strpos($sf_record->fields->Email_subscriptions__c, $a_newsletter->sf_name) !== false) or ($submission_made and !empty($_POST['newsletters']) and in_array($a_newsletter->sf_name, $_POST['newsletters'])) ) echo ' checked="checked"'; ?> /> <label class="newsletter" for="newsletters-<?php echo $friendly_name; ?>"><?php echo $a_newsletter->label; ?></label><br />
							<p class="newsletter-desc"><?php echo $a_newsletter->description; ?></p>
						</div>
					<?php else: ?>
						<div style="opacity: 0.75;">
							<input type="checkbox" id="unlinkednewsletter-<?php echo $friendly_name; ?>" disabled="disabled" /> <label class="newsletter" for="unlinkednewsletter-<?php echo $friendly_name; ?>"><?php echo $a_newsletter->label; ?></label><br />
							<p class="newsletter-desc"><?php echo $a_newsletter->description; ?></p>
						</div>
				<?php endif;
				endforeach; ?>
			</div>
			<?php if(!empty($secondary_newsletters)): ?>
			<div class="newslettersbox">
				<div>
					<p style="float:left;">These are additional interest groups you have been included in. You can remove yourself from them below if you wish.</p>
				</div>
				<?php   foreach($secondary_newsletters as $a_sec_newsletter){
							$friendly_name = preg_replace("/[^A-Za-z0-9]/", '', $a_sec_newsletter); ?>
							<div>
								<input type="checkbox" name="newsletters[]" id="newsletters-<?php echo $friendly_name; ?>" value="<?php echo $a_sec_newsletter; ?>"<?php if( (!$submission_made and strpos($sf_record->fields->Email_subscriptions__c, $a_sec_newsletter) !== false) or ($submission_made and !empty($_POST['newsletters']) and in_array($a_sec_newsletter, $_POST['newsletters'])) ) echo ' checked="checked"'; ?> /> <label class="newsletter" for="newsletters-<?php echo $friendly_name; ?>"><?php echo $a_sec_newsletter; ?></label><br />
							</div>
				<?php   } ?>
			</div>
			<?php endif; ?>
			<p class="submit"><input type="submit" name="newsletter-subedit-submit" id="newsletter-subedit-submit" class="submit" value="Make Changes" /></p>
		</form>
		
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('#newsletters-toggle-container').css('display', 'block');
				
				jQuery('#newsletters-toggle').change(function() {
					if(this.checked) {
						jQuery('input[name=newsletters\\[\\]').prop('checked', true);
					}
					else{
						jQuery('input[name=newsletters\\[\\]').prop('checked', false);
					}
				});
				
				jQuery('input[name=newsletters\\[\\]').change(function(){
					if(!this.checked) {
						jQuery('#newsletters-toggle').prop('checked', false);
					}
					else if(jQuery('input[name=newsletters\\[\\]').not(':checked').length == 0) {
						jQuery('#newsletters-toggle').prop('checked', true);
					}
				});
				
				jQuery('p.disabled').click(function() {
					alert('You cannot edit your name on this form. If you need to change your name in our database, please contact subscriptions@ucp.org');
				});
				
				jQuery('input[name=newsletters\\[\\]').change();   // Make sure Everything toggle is updated
				
				jQuery('#oneClickUnsub').click(function() {
					var user_confirm = confirm('This will unsubscribe you from ALL UCP publications!\n\nAre you sure you want to proceed? You can also cancel and unsubscribe from individual publications instead.');
					if (user_confirm == true) {
						jQuery('input[name=newsletters\\[\\]').prop('checked', false);
						jQuery('#ucp-newsletter-subedit').submit();
					}
				});
				
				jQuery('#oneClickUnsubContainer').css('display', 'block');
			});
		</script>
		<?php
	}
}

?>