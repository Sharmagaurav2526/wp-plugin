<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<style>
.col-xs-3{
    color:#000;
}
.pagination.justify-content-center
{
	float:right;
}
.heading
{
	text-align:center;
}
@media only screen and (max-width: 600px) {
 #p1, #p2, #p3, #p4
 {
 	width:100%;
 }
}

</style>
<div class="container">
    <h2>AUDIO PAGE</h2>
    
    <div class="row">
    	<!-- first row start -->

            
   
      
    <?php if ( have_posts() ) :
	while ( have_posts() ) : the_post(); ?>
        <div class="col-xs-3 col-half-offset" id="p2"><img src="<?php echo the_post_thumbnail_url('thumbnail'); ?>" />
        	<h4><?php the_title(); ?></h4>
        	<div class="tracks">
        		<span><i class="fas fa-download"></i><a href="#" data-toggle="modal" data-target="#myModal1">Download</a></span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i><a href="#" data-toggle="modal" data-target="#myModal-<?php echo get_the_ID(); ?>">Listen Now</a></span>
        	</div>
        </div>
         <div class="modal fade" id="myModal-<?php echo get_the_ID(); ?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Play Audio</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        <p>Post ID : <?php echo get_the_ID(); ?></p>
        <p>Category :<?php echo get_the_category(); ?> </p>

        <?php 
        $post_id = get_the_id();
        $taxonomy = 'audio_type';
        $audio_args = array(
            'order' => 'DESC'
        );
        $terms = wp_get_post_terms( $post_id, $taxonomy, $audio_args ); 
        foreach ($terms as $alldata) { 
          
          echo $alldata->term_id;
           }
        //echo "<pre>"; print_r($terms); echo "</pre>";

        ?>

        <?php 
         /*$id = get_the_id();
        $terms = get_the_terms( $id, 'audio_type' );
        
        foreach($terms as $term) {
            echo $term->cat_ID;   
        }*/

        ?>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
        <?php
          endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
         ?>
       <!--  <div class="col-xs-3 col-half-offset" id="p3"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p4"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div> -->
       	<!-- first row end-->

       	<!-- second row start-->
       	<!--<div class="col-xs-3" id="p1"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p2"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p3"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p4"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>-->
       	<!-- second row end-->

       	<!-- third row start-->
       	<!--<div class="col-xs-3" id="p1"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p2"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p3"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p4"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>-->
       	<!-- third row end-->

    </div>

    <!-- pagination start -->
    <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    <!-- pagination end -->  

</div>
	

<?php get_sidebar(); ?>
<?php get_footer(); ?>
