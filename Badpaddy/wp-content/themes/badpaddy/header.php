<!DOCTYPE html>
<html lang="en">
	  <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php wp_title(); ?></title>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
		 <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		  <link href="https://fonts.googleapis.com/css?family=Great+Vibes|Oswald:200,300,400,500,600,700" rel="stylesheet"> 

		  <!--<link href="<?php echo get_template_directory_uri(); ?>/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />-->

		<!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/circle.player.css">  --> 
<?php wp_head(); ?>
	  </head>

	<body>
		<header class="top-nav">
			<nav class="navbar navbar-inverse">
				<div class="container-fluid">
					
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>                        
							</button>
						</div>
						
						<div class="collapse navbar-collapse" id="myNavbar">
						
			            <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'nav navbar-nav navbar-left','container' => false ) ); ?>

							<!-- <?php //wp_nav_menu( array(  'container' => 'ul' , 'menu_class' => 'nav navbar-nav navbar-left') ); ?> -->
							<!-- <ul class="nav navbar-nav navbar-left">
								<li><a href="#">HOME</a></li>
								<li><a href="#">EVENTS</a></li>
								<li><a href="#">GALLERY</a></li>
								<li><a href="#">NEWS</a></li>
								<li><a href="#">ALBUMS</a></li>
								<li><a href="#">PAGES</a></li>
							</ul> -->
							
							<ul class="nav navbar-nav navbar-center">
								<li><a href="<?php echo home_url(); ?>"><?php the_field('header_logo' , 'option'); ?></a></li>
							</ul>

							<ul class="nav navbar-nav navbar-right">
								<!-- <span><?php //echo do_shortcode('[Social9_Share]');?></span> -->
                                 <?php if( have_rows('header_social_icons', 'option') ): 
								  while( have_rows('header_social_icons', 'option') ): the_row(); ?>
								<li><a href="<?php the_sub_field('icons_url' , 'option'); ?>"><i class="<?php the_sub_field('icon_class' , 'option'); ?>"></i></a></li>
								  <?php endwhile; 
								        endif; ?> 
								<!-- <li><a href="#"><span class="top-nav-icon fa fa-twitter"></span>55k</a></li>
								<li><a href="#"><span class="top-nav-icon fa fa-google-plus"></span>23k</a></li> -->
								<li><a href="<?php the_field('download_music_url' , 'option'); ?>" class="music-btn"><?php the_field('download_music' , 'option'); ?></a></li>
							</ul>
						</div>
						<a href="<?php echo home_url(); ?>" class="mob-logo">BADPADDY</a>
					 
				</div>
			</nav>
		</header>