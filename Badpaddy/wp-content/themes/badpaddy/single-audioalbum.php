<?php
get_header(); ?>
<style type="text/css">
.img-responsive.albumthumbnail {
    height: 300px;
    width: 100%;
    padding: 0px;
    object-fit: cover;
}
.albumcontentsection {
    padding: 0px;
}
.album_content {
    margin: 10px;
}
.counttracks {
    padding: 0px 10px 10px;
}
.total_audio_track i {
    margin-right: 1rem;
}
.album_content a:hover {
	text-decoration: none;
}
.playlink {
    float: right;
}
.playlink i {
    margin-right: 1rem;
}
.album_content i {
    color: #3399cc;
}
.album_content span {
    color: #000000;
}
.content_title h2 {
	padding: 10px;
}
.audio_desc {
    text-align: center;
}
.singleaudio {
	position: relative;
}
.overlay {
    position: absolute;
    width: 100%;
    left: 0px;
    top: 0px;
    height: 100%;
    background: rgba(1, 1, 1, 0.38);
}
.audiobtn {
	display: none;
}
.audiobtn.active {
	display: block;
}
.playpauseaudio {
    position: absolute;
    width: 100%;
    height: 100%;
}
.audiobtn {
    width: 32px;
    margin: 50% auto;
    cursor: pointer;
}
.badpady-single-song img.playicon { display: block !important; }
.badpady-single-song.active img.playicon { display: none !important; }
.badpady-single-song.active img.pauseicon { display: block !important; }

</style>

<div class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 content_title">
				<h2><?php the_title(); ?></h2>
			</div>
			<div class="col-sm-12">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>    
						<?php
							if( have_rows('upload_album_audios') ){
								$count = 0;
							    while ( have_rows('upload_album_audios') ) : the_row();
							    $image_artist = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
							    $image_artist = $image_artist['0'];
							    $audiothumbnail = get_sub_field('add_audio_thumbnail');
							    $defaultimg = get_stylesheet_directory_uri().'/images/default_album_art.png';

							    if( $audiothumbnail != '' ){
							    	$trackimg = $audiothumbnail;
							    }else if ( $audiothumbnail == '' ) {
							    	$trackimg = $image_artist;
							    }else if ( $audiothumbnail == '' && $image_artist =='' ) {
							    	$trackimg = $defaultimg;
							    }

						?>

						<div class="col-sm-3 albumcontentsection badpady-single-song">
							<div class="album_content">
								
								<div class="singleaudio">
									<div class="overlay"></div>

									<div class="playpauseaudio">
										<div class="playaudio audiobtn active" data-id="#playaudio_<?php echo $count; ?>">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/play-audio.png" class="img-responsiver playicon">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pause-audio.png" class="img-responsiver pauseicon" style="display:none;">
										</div>
										<div class="audiofiles" style="display:none;">
											<audio id="playaudio_<?php echo $count; ?>" controls="" preload="auto" autobuffer="">
											  <source class="firstaudiotype" src="<?php echo get_sub_field('upload_ogg_file'); ?>" type="audio/ogg">
											  <source class="secondaudiotype" src="<?php echo get_sub_field('upload_mp3_file'); ?>" type="audio/mpeg">
											</audio>
										</div>
									</div>
									<img src="<?php echo $trackimg; ?>" class="img-responsive albumthumbnail">


									<!--  -->
									<div class="audo_player_popular_section single_audio_player_section">
								 		<div class="latest_close_popular_audio single_audio_close" data-value="#playaudio_latest"><span>x</span></div>

								 		<div class="audio_player_popular_content">
								 			<audio id="playaudio_latest" controls="" preload="auto" autobuffer="">
												  <source class="firstaudiotype" src="<?php echo get_sub_field('upload_ogg_file'); ?>" type="audio/ogg">
												  <source class="secondaudiotype" src="<?php echo get_sub_field('upload_mp3_file'); ?>" type="audio/mpeg">

											</audio>
												<div class="audio_next_pre audio">
												<i class="material-icons previous">skip_previous</i>
												<i class="material-icons next">skip_next</i>
											</div>
											
								 		</div>
								 	</div>
									<!--  -->


								</div>

							</div>
							<div class="counttracks">
								<?php
									$audioname = get_sub_field('audio_name');
									$artistname = get_the_title();
									if($audioname == ''){
										$title = $artistname; 
									}else{
										$title = $audioname;
									}
								?>
								<span class="total_audio_track"><?php echo $title; ?></span>
								<span class="playlink">
									<i class="fa fa-download"></i><a href="<?php echo get_sub_field('upload_mp3_file'); ?>" download="download" target="_blank">Download</a>
								</span>
							</div>
						</div>

						<?php	    
							   $count++; endwhile;
							}else{
							    echo '<p class="notrack">No Track Found..</p>';
							}
					    ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

	//LATEST AUDIO - START

	jQuery(document).on("click", ".audio .next", function(event){

		event.preventDefault();
		
		jQuery(".badpady-single-song").removeClass("active");

		jQuery(".single_audio_player_section audio").trigger('pause');
		jQuery(".single_audio_player_section").hide(400);
		
		jQuery(this).closest(".badpady-single-song").next('.badpady-single-song').addClass("active");
	 	
	 	if ( !jQuery(this).closest(".badpady-single-song").next('.badpady-single-song').find(".single_audio_player_section").length) {
	 			
	 		
	 		alert('No next song in the album');

	 		/*jQuery(document).first('.badpady-single-song').find(".single_audio_player_section").show(400);
			
			var playing = false;
			playing = !playing;
			
			if (playing)
			{
				jQuery(document).first('.badpady-single-song').find(".single_audio_player_section audio").trigger('play');
		    }
		    else
		    {
				jQuery(document).first('.badpady-single-song').find(".single_audio_player_section").trigger('pause');
		    }*/

	 	} else {

			jQuery(this).closest(".badpady-single-song").next('.badpady-single-song').find(".single_audio_player_section").show(400);
			
			var playing = false;
			playing = !playing;
			
			if (playing)
			{
				jQuery(this).closest(".badpady-single-song").next(".badpady-single-song").find(".single_audio_player_section audio").trigger('play');
		    }
		    else
		    {
				jQuery(this).closest(".badpady-single-song").next(".badpady-single-song").find(".single_audio_player_section audio").trigger('pause');
		    }
	 	}
	});

	jQuery(document).on("click", ".audio .previous", function(event){

		event.preventDefault();

		jQuery(".badpady-single-song").removeClass("active");

		jQuery(".single_audio_player_section audio").trigger('pause');
		jQuery(".single_audio_player_section").hide(400);
		
		jQuery(this).closest(".badpady-single-song").prev('.badpady-single-song').addClass("active");
		jQuery(this).closest(".badpady-single-song").prev('.badpady-single-song').find(".single_audio_player_section").show(400);
			
			var playing = false;
			playing = !playing;
			
			if (playing)
			{
				jQuery(this).closest(".badpady-single-song").prev(".badpady-single-song").find(".single_audio_player_section audio").trigger('play');
		    }
		    else
		    {
				jQuery(this).closest(".badpady-single-song").prev(".badpady-single-song").find(".single_audio_player_section audio").trigger('pause');
		    }
	});

	jQuery(document).on("click", ".badpady-single-song .playicon", function(event){

		event.preventDefault();


		if (jQuery(this).closest(".badpady-single-song").hasClass("active")) {

		} 
		else 
		{
			jQuery(".badpady-single-song").removeClass("active");
			jQuery(this).closest(".badpady-single-song").addClass("active");
		}

		jQuery(this).hide();
		jQuery(this).parent().find('.pauseicon').show(400);

		jQuery(".single_audio_player_section audio").trigger('pause');
		jQuery(".single_audio_player_section").hide();


		jQuery(this).closest(".badpady-single-song").find(".single_audio_player_section").show(400);
		//jQuery(this).closest(".badpady-single-song").find(".playicon").hide(400);
		//jQuery(this).closest(".badpady-single-song").find(".pauseicon").show(400);
		
		var playing = false;
		playing = !playing;
		if (playing) {
			jQuery(this).closest(".badpady-single-song").find(".single_audio_player_section audio").trigger('play');
	    }
	    else {
			jQuery(this).closest(".badpady-single-song").find(".single_audio_player_section audio").trigger('pause');
	    }
	});

	jQuery(document).on("click", ".badpady-single-song .pauseicon", function(event){

		event.preventDefault();

		jQuery(this).closest(".badpady-single-song").removeClass("active");

		jQuery(".single_audio_player_section audio").trigger('pause');
		jQuery(".single_audio_player_section").hide();
		
		//jQuery(this).closest(".badpady-single-song").find(".playicon").show(400);
		//jQuery(this).closest(".badpady-single-song").find(".pauseicon").hide(400);
			
	});

	jQuery(".single_audio_close").on("click",function(event){
		event.preventDefault();

		jQuery(".badpady-single-song").removeClass("active");

		var playing = false;
		playing = !playing;
		if (playing) {
			jQuery(this).closest(".badpady-single-song").find(".single_audio_player_section audio").trigger('pause');
	    }
	    else {
			jQuery(this).closest(".badpady-single-song").find(".single_audio_player_section audio").trigger('pause');
	    }
	    jQuery(".single_audio_player_section").hide(400);
	});

</script>
<?php get_footer('pages'); ?>