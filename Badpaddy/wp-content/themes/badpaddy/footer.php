<footer class="footer-background">
				<div class="news">
					<!-- <span><input class="form-control form-subscribe" id="name" name="name" placeholder="subscribe newsletter" required="" type="text"></span>
					<a href="#"><i class="fa fa-arrow-right"></i></a> -->
					<?php echo do_shortcode('[newsletter_form button_label="Go"][newsletter_field name="email" placeholder="subscribe newsletter"][/newsletter_form]'); ?>
					<div class="news-border">
					</div>
				</div>
				<div class="footer-border">
					<div class="footer-links">
						<?php dynamic_sidebar('footer-1'); ?>
						<!-- <ul class="nav navbar-nav navbar-left">
							<li><a href="#">HOME</a></li>
							<li><a href="#">EVENTS</a></li>
							<li><a href="#">GALLERY</a></li>
							<li><a href="#">NEWS</a></li>
							<li><a href="#">ALBUMS</a></li>
							<li><a href="#">PAGES</a></li>
						</ul> -->
						<a href="<?php the_field('copy_write_url' , 'option'); ?>"><?php the_field('copy_write' , 'option'); ?></a>
					</div>
				</div>
					
			</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/carousal.js" type="text/javascript"></script>


<!-- <script src="<?php echo get_template_directory_uri(); ?>/js/audio.min.js" type="text/javascript"></script>
 -->

<!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.jplayer.min.js"></script>-->
<!-- <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.transform2d.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.grab.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/mod.csstransforms.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/circle.player.js"></script> -->

<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js" type="text/javascript"></script>

<!-- <script>
  audiojs.events.ready(function() {
    var as = audiojs.createAll();
  });
</script> -->

 <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Download</h4>
        </div>
        <div class="modal-body">
          
          <table>
          	<thead></thead>
          	<tbody class="iris_table-list-items">
          		<tr class="iris_table-item">
          			<td class="iris_table-data">SD360</td>
          			<td class="iris_table-data">640 × 360 / 397.765KB</td>
          			<td class="iris_table-data"><a data-fatal-attraction="container:clip|component:modal|keyword:download_sd540P" href="https://player.vimeo.com/play/1073821218?s=285150117_1534441019_9bb1474fe9a3d6b5e02a4980577774cb&amp;loc=external&amp;context=Vimeo%5CController%5CClipController.main&amp;download=1" download="samplevideo_1280x720_1mb_960x540.mp4"><span class="sc-csuQGl hlXsyG" format="secondary"><span class="sc-Rmtcm fRxrcj"><!-- react-text: 783 -->Download<!-- /react-text --></span><div class="sc-dVhcbM kprwJE" format="secondary"></div></span></a></td>
          		</tr>
          	</tbody>
          </table>
        
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
<style>
	.iris_table-item {
    height: 3.4375rem;
}
.iris_table-data:nth-last-of-type(n+3):first-child {
    width: 50%;
    font-weight: 700;
    color: #1a2e3b;
    text-align: left;
}
.iris_table-data {
    border-bottom: 1px solid #E3E8E9;
    vertical-align: initial;
}
.hlXsyG {
    display: inline-flex;
    position: relative;
    /* width: 100%; */
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: 700;
    text-align: center;
    vertical-align: middle;
    /* letter-spacing: 0.1px; */
    /* -webkit-box-align: center; */
    /* align-items: center; */
    /* -webkit-box-pack: center; */
    /* justify-content: center; */
    /* -webkit-font-smoothing: antialiased; */
    /* min-width: 4.25rem; */
    /* min-height: 2rem; */
    /* font-size: 0.875rem; */
    line-height: 2.14286;
    background-color: rgb(238, 241, 242);
    /* color: rgb(26, 46, 59); */
    /* outline: 0px; */
    margin: 0px 46px 0.5rem;
    /* border-width: 0.0625rem; */
    /* border-style: solid; */
    /* transition: all 0.1s ease-in-out; */
    /* padding: 0px 0.625rem; */
    /* border-radius: 0.1875rem; */
    border-color: rgb(238, 241, 242);
}
.modal-header .close {
    margin-top: -2px;
    width: 21%;
}
div#myModal1 {
    width: 100%;
    top: 10%;
}
</style>
<?php wp_footer(); ?>
	</body>
</html>
