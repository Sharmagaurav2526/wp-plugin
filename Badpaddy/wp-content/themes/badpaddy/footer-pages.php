
    <!---- Footer Area Starts ---->
    <footer class="footer pt-4">
        <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-xs-12 col-sm-4">
                <div class="footer-logo1">
                <a class="text-logo" href="<?php echo home_url(); ?>"><?php the_field('logo_footer_title' , 'option'); ?></a>
            </div>
            </div>
            
            <div class="col-sm-12 col-md-2 col-xs-12 col-sm-4">
                <div class="footer-head">
                    <h1><?php the_field('navigation_title' , 'option'); ?></h1>
                    <?php dynamic_sidebar('footer-1'); ?>
                    <!-- <ul>
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">ABOUT US</a></li>
                    <li><a href="#">VIDEO</a></li>
                    <li><a href="#">AUDIO</a></li>
                    <li><a href="#">NEWS/EVENTS</a></li>
                    <li><a href="#">CONTACT US</a></li>
                    </ul> -->
                </div>
            </div>
            
            <div class="col-sm-12 col-md-2 col-xs-12 col-sm-4">
                <div class="footer-head">
                    <h1><?php the_field('quick_link_title' , 'option'); ?></h1>
                    <?php wp_nav_menu( array(  'container' => 'ul' , 'menu_class' => 'footer-head') ); ?>
                    <!-- <ul>
                    <li><a href="#">PRIVACY POLICY</a></li>
                    <li><a href="#">DISCLAIMER</a></li>
                    <li><a href="#">TERM AND CONDITION</a></li>
                    <li><a href="#">BLOGS</a></li>
                    <li><a href="#">MUCH MORE</a></li>
                    </ul> -->
                </div>
            </div>
            
            <div class="col-sm-12 col-md-4 col-xs-12 col-sm-12">
                <div class="footer-head footer-2 ">
                    <h1><?php the_field('news_letter_title' , 'option'); ?></h1>
                    <p><?php the_field('news_letter_content' , 'option'); ?></p>
                      <?php echo do_shortcode('[newsletter_form button_label="GO"][newsletter_field name="email" placeholder="Email Address"][/newsletter_form]'); ?>
                    <!-- <form class="example" action="/action_page.php" style="margin:auto;max-width:300px">
                      <input type="text" placeholder="EMAIL ADDRESS" name="search2">
                      <button type="submit">SUBSCRIBE</button>
                    </form> -->
                </div>
            </div>
    </div>
            
            
            <div class="footerbootom">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-xs-12 col-lg-6">
                            <div class="copy_write-content">
                                <span><a href="<?php the_field('copy_write_url' , 'option'); ?>"><?php the_field('copy_write' , 'option'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-12 col-lg-6">
                            <ul class="footericon">
                                 <?php if( have_rows('header_social_icons', 'option') ): 
                                  while( have_rows('header_social_icons', 'option') ): the_row(); ?>
                                <li><a href="<?php the_sub_field('icons_url' , 'option'); ?>"><i class="<?php the_sub_field('icon_class' , 'option'); ?>"></i></a></li>
                                  <?php endwhile; 
                                        endif; ?> 
                                <!-- <li><a href="#"><span class="top-nav-icon fa fa-facebook"></span>32k</a></li>
                                <li><a href="#"><span class="top-nav-icon fa fa-twitter"></span>55k</a></li>
                                <li><a href="#"><span class="top-nav-icon fa fa-google-plus"></span>23k</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            
            
        </div>
    </footer>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/carousal.js" type="text/javascript"></script>


<!-- <script src="<?php echo get_template_directory_uri(); ?>/js/audio.min.js" type="text/javascript"></script>
 -->

<!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.jplayer.min.js"></script>-->
<!-- <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.transform2d.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.grab.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/mod.csstransforms.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/circle.player.js"></script> -->

<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js" type="text/javascript"></script>

<!-- <script>
  audiojs.events.ready(function() {
    var as = audiojs.createAll();
  });
</script> -->



<?php wp_footer('pages'); ?>
	</body>
</html>
