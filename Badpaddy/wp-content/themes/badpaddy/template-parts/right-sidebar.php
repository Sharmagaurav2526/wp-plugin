<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

	<div class="col-sm-4">
									<div class="recent-posts">
										<div class="row">
											<div class="span12">
									<?php echo do_shortcode("[display_search_form]");?>
											</div>
										</div>
                    <!-- <h4 class="text-left">
                    CATEGORY
                    </h4> -->

                    <div class="row-category">
                      
                    	<?php dynamic_sidebar( 'right-sidebar-1' ); ?>

                    <!-- <ul>
                    <li class="list-item news-category">
                    Lorem ipsum dolor sit amet
                    </li>
                    <li class="list-item news-category">
                    Consectetur adipiscing elit
                    </li>
                    <li class="list-item news-category">
                    Integer molestie lorem at massa
                    </li>
                    <li class="list-item news-category">
                    Facilisis in pretium nisl aliquet
                    </li>
                    <li class="list-item news-category">
                    Nulla volutpat aliquam velit
                    </li>
                    <li class="list-item news-category">
                    Faucibus porta lacus fringilla vel
                    </li>
                    <li class="list-item news-category">
                    Aenean sit amet erat nunc
                    </li>
                    <li class="list-item news-category">
                    Eget porttitor lorem
                    </li>
                    </ul> -->
                  </div>
                  <div class="recent-posts">
										<!-- <div class="row calcy">
											<h2>Recent Posts</h2> -->
<!-- <ul>
<?php
    $recent_posts = wp_get_recent_posts(array('post_type'=>'event'));
    foreach( $recent_posts as $recent ){
        echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
    }
?>
</ul> -->
                      <h4>RECENT EVENTS</h4>
                     	 <?php $recent_posts = wp_get_recent_posts(array('post_type'=>'event')); 
                       //print_r($recent_posts);  
                     	 		foreach ($recent_posts as $events) {?>
                     	 	<div class="row calcy">
											<div class="col-md-4">
                        <div class="recent_postsss">
                        
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $events["ID"] ), 'single-post-thumbnail' ); ?>
												<img src="<?=$image[0]?>" class="img-responsive">
											</div></div>
											<div class="col-md-8">
                        <div class="recent_postsss2">
											<?php 	echo '<span class="octo">'.$events["post_date"].'</span>';?>
											<?php	echo '<p class="event-news">' .$events["post_title"].'</p>';?>
											</div></div>
										</div>

                     	 			
                     	 	<?php	}
                     	  ?>
                     					<!--  <div class="row calcy">
                     					 	<h4>RECENT NEWS / EVENTS</h4>
											<div class="col-md-4">
												<img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img3.jpg" class="img-responsive">
											</div>
											<div class="col-md-8">
												<span class="octo">October- 11- 2015</span>
												<p class="event-news">Lorem Ipsum is simply dummy text  printing and typesettingm.</p>
											</div>
										</div> -->



										<!-- <div class="row tabtat">
											<div class="col-md-4">
												<img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img3.jpg" class="img-responsive">
											</div>
											<div class="col-md-8">
												<span class="octo">October- 11- 2015</span>
												<p class="event-news">Lorem Ipsum is simply dummy text  printing and typesettingm.</p>
											</div>
										</div> -->
										<!-- <div class="row piano">
											<div class="col-md-4">
												<img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img3.jpg" class="img-responsive">
											</div>
											<div class="col-md-8">
												<span class="octo">October- 11- 2015</span>
												<p class="event-news">Lorem Ipsum is simply dummy text  printing and typesettingm.</p>
											</div>
										</div> -->
                  </div>

                  </div>
                  <div class="recent-posts-news">
  
                      <h4>RECENT NEWS</h4>
                       <?php $recent_posts = wp_get_recent_posts(array('post_type'=>'post')); 
                       //print_r($recent_posts);  
                          foreach ($recent_posts as $news) {?>
                        <div class="row calcy1">
                      <div class="col-md-4">
                        
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $news["ID"] ), 'single-post-thumbnail' ); ?>
                        <img src="<?=$image[0]?>" class="img-responsive">
                      </div>
                      <div class="col-md-8">
                      <?php   echo '<span class="octo">'.$news["post_date"].'</span>';?>
                      <?php echo '<p class="event-news">' .$news["post_title"].'</p>';?>
                      </div>
                    </div>

                            
                        <?php }
                        ?>
                              
                  </div>
									</div>
								
 				</div>
 				
 			
