<?php
/**
 * Template Name:Video Page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<style type="text/css">
.videocontentsection {
    padding: 0px;
}
.video_content {
    position: relative;
	margin: 10px;
}
.video_overlay {
    position: absolute;
    width: 100%;
    top: 0px;
    left: 0px;
    height: 100%;
    background: rgba(1, 1, 1, 0.42);
}
.videooverlaycontent {
    position: absolute;
    width: 100%;
    left: 0px;
    height: 100%;
}
.videooverlaycontent .play_btn {
    width: 64px;
    margin: 0px auto;
    margin-top: 25%;
    cursor: pointer;
}
/*.video_popup_content {
    position: relative;
    padding-bottom: 56.25%;
    overflow: hidden;
    background: rgba(19, 17, 17, 0.9519607843137255);
}	
.video_popup_content iframe {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
.video_popup_section {
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0px;
    display: none;
}*/
.closevideo {
    position: absolute;
    width: 100%;
    left: 0px;
    top: 0px;
    z-index: 1;
}
.closevideo span {
    float: right;
    cursor: pointer;
    margin-right: 1rem;
    margin-top: 1rem;
}
button.btn.btn-default {
    width: 20%;
    bottom: 1px;
    right: 4%;
}
span.leftcnt {
    float: right;
    margin-right: 37px;
}
</style>
<?php
require_once( get_stylesheet_directory() . '/templates/download/YouTubeDownloader.php');
require_once( get_stylesheet_directory() . '/templates/download/VimeoDownloader.php');
require_once( get_stylesheet_directory() . '/templates/download/LinkHandler.php');
?>

<div class="banner-bgimage" style="background: url(<?php the_field('video_banner_image')?>);">
	<h3 class="video"><?php the_field('video_title')?></h3>
</div>


<!--video content section start here-->
<div class="container-fluid">
	<div class="container">
		<div class="row">
			<h3><?php the_title();?></h3>
			<?php
				// WP_Query arguments
				$args = array(
					'post_type'              => array( 'video' ),
					'posts_per_page'         => '12',
				);

				// The Query
				$query = new WP_Query( $args );

				// The Loop
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
						$query->the_post();
						$videoid = get_field('vimeo_video_id',get_the_ID());
						$videothums = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".$videoid.".php"));
						$videothums = $videothums[0]['thumbnail_large'];
				?>
					<div class="col-sm-6 videocontentsection">
						<div class="video_content">
							<div class="video_overlay"></div>
							<div class="videooverlaycontent">
								<div data-id="<?php echo $videoid; ?>" class="play_btn">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/play-button.png">
								</div>
							</div>	
							<img src="<?php echo $videothums; ?>" class="img-responsive">
						</div>
						<div class="video_info">
							<span class="leftcnt"><?php the_title(); ?></span>
							<span data-toggle="modal" data-target="#myModal_<?php echo $videoid; ?>" class="rightcnt"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/download-button.png" class="img-responsive"></span>
						</div>
					</div>

					<!--modal to show download section-->
					<!-- Modal -->
					<div id="myModal_<?php echo $videoid; ?>" class="modal fade" role="dialog">
					  <div class="modal-dialog">

					    <!-- Modal content-->
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title">Download Video</h4>
					      </div>
					      <div class="modal-body">
					      	<?php
						        $url = "https://vimeo.com/".$videoid;
								$handler = new LinkHandler();
								$downloader = $handler->getDownloader($url);
								$downloader->setUrl($url);
								if($downloader->hasVideo())
								{
								    $link = $downloader->getVideoDownloadLink();
								    //echo "<pre>"; print_r($link); echo "</pre>";
							?>
							<table class="table">
								<thead>
									<tr>
										<th scope="col">Quality</th>
										<th scope="col">Download Link</th>
									</tr>
								</thead>
								<tbody>
							<?php			    
									foreach ($link as $value) {
										$downloadlink = $value['url'];
										$downloadquality = $value['quality'];
							?>
								<tr>
									<td><?php echo $downloadquality; ?></td>
									<td><a class="btn" href="<?php echo $downloadlink; ?>" download="download"><i class="fa fa-download"></i> Download</a></td>
								</tr>
									
							<?php			
									}
							?>
								</tbody>
							</table>
							<?php		
								}
							?>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div>
					    </div>

					  </div>
					</div>
					<!--//modal to show download section-->
				<?php
					}
				} else {
					// no posts found
				}

				// Restore original Post Data
				wp_reset_postdata();
			?>
		</div>
	</div>
</div>
<p class="directoryurl" style="display:none;"><?php echo get_stylesheet_directory_uri(); ?></p>
<div class="video_popup_section">
	<div class="video_popup_content">
		
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".videooverlaycontent .play_btn").on("click",function(){
		var directoryurl = jQuery(".directoryurl").html();
		jQuery(".video_popup_section").show();
		var videoid = jQuery(this).attr("data-id");
		jQuery(".video_popup_section .video_popup_content").html('<div class="closevideo"><span><img src="'+directoryurl+'/images/cross.png" class="img-responsive"></span></div><iframe src="//player.vimeo.com/video/'+videoid+'?autoplay=1&loop=1&background=0&control=1" width="100%" height="500px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>')
	});
	jQuery(document).on("click",".closevideo span",function(){
		jQuery(".video_popup_section").hide();
		jQuery(".video_popup_section .video_popup_content").empty();
	});
});
</script>

<!--//video content section end here-->
	<!-- <div class="conatiner">
		<div class="col-md-12 ">
			<h1 class="our-video">OUR VIDEO</h1>
		</div>
			<div class="row m-0">
				<div class="col-md-6 mb-3">
					<div class=" ">
						<iframe width="560" height="300" src="https://www.youtube.com/embed/M-P4QBt-FWw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
					<div>
						<span><i class="fas fa-download"></i></span>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<iframe width="560" height="300" src="https://www.youtube.com/embed/j6sSQq7a_Po" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					<div>
						<span><i class="fas fa-download"></i></span>
					</div>
				</div>
			</div>
	</div> -->
<?php get_footer('pages'); ?>
