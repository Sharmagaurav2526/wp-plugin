<?php
/*
*Template Name:Album Template
*/
get_header(); ?>
<style type="text/css">
.img-responsive.albumthumbnail {
    height: 300px;
    width: 100%;
    padding: 0px;
    object-fit: cover;
}
.albumcontentsection {
    padding: 0px;
}
.album_content {
    margin: 10px;
}
.counttracks {
    padding: 10px 0px 0px;
}
.total_audio_track i {
    margin-right: 1rem;
}
.album_content a:hover {
	text-decoration: none;
}
.playlink {
    float: right;
}
.playlink i {
    margin-right: 1rem;
}
.album_content i {
    color: #3399cc;
}
.album_content span {
    color: #000000;
}
.content_title h2 {
	padding: 10px;
}
</style>
	<div class="banner-bgimage" style="background: url(<?php the_field('audio_banner_image')?>); ">
    <h3 class="audio"><?php the_field('audio_banner_title')?></h3>
</div>
<div class="container-fluid">

	<div class="container">
		<div class="row">
			<div class="col-sm-12 content_title">
				<h2>OUR AUDIOS</h2>
			</div>
			<div class="col-sm-12">
				<?php
					// WP_Query arguments
					$args = array(
						'post_type'              => array( 'audioalbum' ),
						'posts_per_page'         => '7',
					);

					// The Query
					$query = new WP_Query( $args );

					// The Loop
					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();
				?>
					<div class="col-sm-3 albumcontentsection">
						<div class="album_content">
							<a href="<?php echo get_permalink(); ?>">
								<?php if (has_post_thumbnail( get_the_ID() ) ){ ?>
								  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ); ?>
									<img src="<?php echo $image[0]; ?>" class="img-responsive albumthumbnail">
								<?php }else{ ?>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/default_album_art.png" class="img-responsive albumthumbnail">
								<?php } ?>

								<div class="counttracks">
									<span class="total_audio_track"><i class="fas fa-headphones"></i>
									    <?php
											if( have_rows('upload_album_audios') ){
												$count = 0;
											    while ( have_rows('upload_album_audios') ) : the_row();
											    $count++;
											    endwhile;
											    echo $count.' Track';
											}else{
											    echo '0 Track';
											}
									    ?>
									</span>
									<span class="playlink">
										<i class="fa fa-play"></i>Listen Now
									</span>
								</div>


							</a>
						</div>
					</div>
				<?php
						}
					} else {
						// no posts found
					}

					// Restore original Post Data
					wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</div>
<?php get_footer('pages'); ?>