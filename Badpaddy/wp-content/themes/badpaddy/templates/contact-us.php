


<style>

a.logo-text { /*412*/
    color: #fff;
    float: right;
    margin-top: 22%;
    text-decoration: none;
    border: 1px solid #fff;
    padding: 6px 18px;
    font-size: 22px;
    font-weight: 400;
}

</style>



<?php
/**
 * Template Name:Contact Us Page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="col-md-12 Banner" style="background-image: url('<?php the_field('contact_banner_image'); ?>');">
	<h3 class="text-center Banner-title" >
		<?php the_field('contact_title'); ?>
	</h3>
</div>
<div class="container-fluid">
	<div class="container">
	<div class="row">
		<div class="col-md-2">
		</div>
		<div class="col-md-8">
			<h3 class="text-left map-title">
				CONTACT US
			</h3>
			<div class="row">
				<div class="col-md-12">
				<?php echo do_shortcode('[gmap-embed id="172"]');?>
				<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3736489.7218514383!2d90.21589792292741!3d23.857125486636733!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1506502314230" width="100%" height="315" frameborder="0" style="border:0" allowfullscreen></iframe> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						 <?php if( have_rows('contact_us') ): ?>
                
                       <?php while ( have_rows('contact_us') ) : the_row(); ?>
						<div class="col-md-4">
							<div class="data">
						<?php the_sub_field('contact_icon'); ?>
						<p class="icon-info" align="center"><?php the_sub_field('contact_heading'); ?></p>
						<p class="icon-info" align="center"><?php the_sub_field('contact_content'); ?></p>
					    </div>
						</div>
						<!-- <div class="col-md-4">
							<div class="data">
						<i class="fas fa-map-marker-alt" style="font-size:36px"></i>
						<p class="icon-info" align="center">PERSONAL:+8799 879 0000</p>
						<p class="icon-info" align="center">OFFICES:0477 224 00 00</p>
					    </div>
						</div>
						<div class="col-md-4">
							<div class="data">
						<i class="fas fa-envelope" style="font-size:36px"></i>
						<p class="icon-info" align="center">PERSONAL:+8799 879 0000</p>
						<p class="icon-info" align="center">OFFICES:0477 224 00 00</p>
					</div>
						</div> -->
						<?php  endwhile; ?>

                       <?php endif; ?>
					</div>

				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
				<h3 class="text-center">
				CONTACT FORM
				</h3>

				<?php echo do_shortcode('[contact-form-7 id="182" title="Contact Page"]');?>
				 <!-- <form>
		          <div class="form-group col-md-6">
		            <input type="text" class="form-control" name="" value="" placeholder="Name" required="required">
		          </div>
		          <div class="form-group col-md-6">
		            <input type="email" class="form-control" name="" value="" placeholder="E-mail" required="required">
		          </div>
		          <div class="form-group col-md-6">
		            <input type="tel" class="form-control" name="" value="" placeholder="Phone" required="required">
		          </div>
		          <div class="form-group col-md-6">
		            <input type="text" class="form-control" name="" value="" placeholder="Website" required="required">
		          </div>
		          <div class="form-group col-md-12">
		            <textarea class="form-control" name="" placeholder="Message" required="required"></textarea>
		          </div>
		          <button style="margin-left:385px;" class="btn btn-default" type="submit" name="button">
		              <i class="fa fa-paper-plane" aria-hidden="true"></i> Submit
		          </button>
       			 </form> -->
				</div>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>
<?php get_footer('pages'); ?></div>