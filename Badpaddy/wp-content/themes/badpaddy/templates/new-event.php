<?php
/**
 * Template Name:Event Page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="container-fluid Banner-img">
 		<h3 class="text-center banner-event">NEWS/EVENTS</h3>
</div>
 <div class="container">
 	<div class="row">

 	
 		<div class="">	
 			<div class="">
        <div class="">
           <div class="col-sm-8 news-contentss">
              <h3>NEWS/EVENTS</h3>
              <?php // WP_Query arguments
                  $args = array(
                    'post_type'              => array( 'event' ),
                    'post_status'            => array( 'publish' ),
                    'nopaging'               => false,
                    'posts_per_page'         => '3',
                  );

                  // The Query
                  $events_news = new WP_Query( $args );
                  $get_author_id = get_the_author_meta('ID');
                  $get_author_gravatar = get_avatar_url($get_author_id, array('size' => 450));
                  //var_dump($get_author_gravatar);
                  // The Loop
                  if ( $events_news->have_posts() ) {
                    while ( $events_news->have_posts() ) {
                      $events_news->the_post();?>

                   <div class="row event-posts">
                <div class="col-sm-12 blogcontent round-image">
                <?php  echo '<img class="img-circle" src="'.$get_author_gravatar.'" alt="'.get_the_title().'" />';?>
                 <!--  <img src="<?php //echo esc_url( get_avatar_url( $author->ID ) ); ?>" class="img-circle"> -->
                   <div class="right-support-heading">
                  <span><?php the_title();?></span>
                  <p> <?php echo get_the_author(); ?>,  <?php echo get_the_date('d-m-Y'); ?></p>
                </div>
                </div>
              
                <div class="support-img-content">
                  <?php the_post_thumbnail('full')?>
                    <p><?php the_excerpt();?></p>
                    <div class="col-md-6 col-xs-9">
                     <p class="share-icon">SHARE:</p>
                      <ul class="social-icon-share">
                    <?php if( have_rows('soical_media') ): ?>
                    <?php while ( have_rows('soical_media') ) : the_row(); ?>
                        <li class="social-icon"><a href="<?php the_sub_field('soical_link'); ?>"><img src="<?php the_sub_field('soical_image'); ?>"></a></li>
                      <?php  endwhile; ?>
                     <?php endif; ?>

                      </ul>
                      </div>
                      <div class="col-md-6 col-xs-3">
                       <div class=" btn-more">
                    <!--   <button type="button" class="btn btn-default ">Read More</button> -->
                    <a href="<?php the_permalink(); ?>" class="btn btn-default">Read More</a>
                    </div>
                  </div>
                </div>
              
               </div>
                    


                  <?php }
                  } else {
                    // no posts found
                  }

                  // Restore original Post Data
                  wp_reset_postdata();?>

            </div>
            <?php
                echo get_template_part('template-parts/right', 'sidebar'); 
                ?>

                </div>
      </div>
  
   
  </div>
</div>
 			

<?php get_footer('pages'); ?>

