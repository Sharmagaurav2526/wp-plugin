<?php
/**
 * Template Name:News Page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="container-fluid Banner-img">
 		<h3 class="text-center banner-event"><?php the_title();?></h3>
</div>
 <div class="container">
 	<div class="row">

           <div class="col-sm-8 news-contentss">
              <h3><?php the_title();?></h3>
              <?php // WP_Query arguments
                  $args = array(
                    'post_type'              => array( 'post' ),
                    'post_status'            => array( 'publish' ),
                    'nopaging'               => false,
                    'posts_per_page'         => '3',
                  );

                  // The Query
                  $news = new WP_Query( $args );
                  $get_author_id = get_the_author_meta('ID');
                  $get_author_gravatar = get_avatar_url($get_author_id, array('size' => 450));
                  //var_dump($get_author_gravatar);
                  // The Loop
                  if ( $news->have_posts() ) {
                    while ( $news->have_posts() ) {
                      $news->the_post();?>

                   <div class="row event-posts">
                <div class="col-sm-12 blogcontent round-image">
                <?php  echo '<img class="img-circle" src="'.$get_author_gravatar.'" alt="'.get_the_title().'" />';?>
                 <!--  <img src="<?php //echo esc_url( get_avatar_url( $author->ID ) ); ?>" class="img-circle"> -->
                 <div class="right-support-heading">
                  <span><?php the_title();?></span>
                  <p> <?php echo get_the_author(); ?>,  <?php echo get_the_date('d-m-Y'); ?></p>
                </div>
                </div>
                
                 <div class="support-img-content">
                  <?php the_post_thumbnail('full')?>
                    <p><?php the_excerpt();?></p>
                    <div class="col-md-6 col-xs-9">
                     <p class="share-icon">SHARE:</p>
                      <ul class="social-icon-share">
                    <?php if( have_rows('soical_icon') ): ?>
                    <?php while ( have_rows('soical_icon') ) : the_row(); ?>
                        <li class="social-icon"><a href="<?php the_sub_field('soical_link'); ?>"><img src="<?php the_sub_field('soical_image'); ?>"></a></li>
                      <?php  endwhile; ?>
                     <?php endif; ?>

                      </ul>
                      </div>
                      <div class="col-md-6 col-xs-3">
                       <div class=" btn-more">
                    <!--   <button type="button" class="btn btn-default ">Read More</button> -->
                    <a href="<?php the_permalink(); ?>" class="btn btn-default">Read More</a>
                    </div>
                  </div>
                </div>
               </div>


                  <?php }
                  } else {
                    // no posts found
                  }

                  // Restore original Post Data
                  wp_reset_postdata();?>

             
               
              	<!-- <div class="event-posts">
              	<div class="col-sm-1 round-image">
              		<img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/profile.jpg" class="img-circle">
              	</div>
              	<div class="right-support-heading">
              		<span>Blog Heading Here</span>
              		<p>Posted by: admin,  JAN- 7- 2014</p>
              	</div>
              	<div class="support-img-content">
              		<img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/news-first.jpg" class="img-responsive">
              	</div>
              	<div class="support-content1">
              		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo</p>
              		<div class="row">
              			<div class="col-md-1">
              				<p class="share-icon">SHARE:</p>
              			</div>
              			<div class="col-md-8">
              				<ul>
              					<li class="social-icon"><a href="#"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/if_facebook_circle_294710.png"></a></li>
              					<li class="social-icon"><a href="#"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/if_twitter_circle_294709-1.png"></a></li>
              					<li class="social-icon"><a href="#"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/if_google_circle_294707-1.png"></a></li>
              				</ul>
              			</div>
              			<div class="col-md-3 btn-more">
              				<button type="button" class="btn btn-default ">Read More</button>
              			</div>
              		</div>
              	</div>
              </div> -->

            </div>
            <?php
                echo get_template_part('template-parts/right', 'sidebar'); 
                ?>

                </div>
      </div>

 			

<?php get_footer('pages'); ?>

