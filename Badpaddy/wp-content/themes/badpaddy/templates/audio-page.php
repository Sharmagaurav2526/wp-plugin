<?php
/**
 * Template Name:Audio Page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<style>
.col-xs-3{
    color:#000;
}
.pagination.justify-content-center
{
	float:right;
}
.heading
{
	text-align:center;
}
@media only screen and (max-width: 600px) {
 #p1, #p2, #p3, #p4
 {
 	width:100%;
 }
}

</style>

<div class="container">
    <h2>AUDIO PAGE</h2>
    
    <div class="row">
    	<!-- first row start -->
        <?php 
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        $counter = 0;
        $taxonomy = 'audio_type';
        $audio_args = array(
            'order' => 'DESC',
            'posts_per_page' => 2,
            'paged' => $paged
        );

        $terms = get_terms( $taxonomy, $audio_args );
        foreach ($terms as  $value) {    ?>   
              <div class="col-xs-3" id="p1">
               <!--  <img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" /> -->
               
               <img src="<?php echo z_taxonomy_image_url($value->term_id,'thumbnail'); ?>" />
            <h4><?php echo $value->name ?></h4>
            <div class="tracks">
                <span><i class="fas fa-headphones"></i><?php echo $value->count; ?> Tracks</span>&nbsp;<span>
                    <a href="<?php echo get_category_link( $value->term_id); ?>"><i class="fa fa-play" aria-hidden="true"></i>Listen Now</a></span>
            </div>
        </div>
     <?php   }
                    ?>
      
    
       <!--  <div class="col-xs-3 col-half-offset" id="p2"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p3"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p4"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div> -->
       	<!-- first row end-->

       	<!-- second row start-->
       	<!--<div class="col-xs-3" id="p1"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p2"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p3"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p4"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>-->
       	<!-- second row end-->

       	<!-- third row start-->
       	<!--<div class="col-xs-3" id="p1"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p2"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p3"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>
        <div class="col-xs-3 col-half-offset" id="p4"><img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/slider-img4-250x250.jpg" />
        	<h4>PARTY MUSIC</h4>
        	<div class="tracks">
        		<span><i class="fas fa-headphones"></i>6 Tracks</span>&nbsp;<span><i class="fa fa-play" aria-hidden="true"></i>Listen Now</span>
        	</div>
        </div>-->
       	<!-- third row end-->

    </div>

    <!-- pagination start -->
    <!-- Pagination -->

        <?php 
              if (function_exists("pagination")) {
                pagination($custom_query->max_num_pages);
              } 
            ?>


      <!-- <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul> -->
    <!-- pagination end -->  

</div>


 
<?php get_footer('pages'); ?>
