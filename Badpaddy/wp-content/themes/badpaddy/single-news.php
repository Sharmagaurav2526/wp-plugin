<?php

get_header(); 
?>

<div class="container-fluid">
<div class="row">

  <div class="col-md-2">
      
    </div>
    
    <div class="col-md-8">  
      <div class="row">
  <div class="col-md-12">

  <div class="col-md-8">
<?php 
 $get_author_id = get_the_author_meta('ID');
$get_author_gravatar = get_avatar_url($get_author_id, array('size' => 450));
if ( have_posts() ) :
  while ( have_posts() ) : the_post(); ?>
   <div class="event-posts">
                <div class="col-sm-1 round-image">
                 <?php  echo '<img class="img-circle" src="'.$get_author_gravatar.'" alt="'.get_the_title().'" />';?>
                </div>
                <div class="right-support-heading">
                  <span><?php the_title();?></span>
                  <p>Posted by: admin,  JAN- 7- 2014</p>
                </div>
                <div class="support-img-content">
                  <?php the_post_thumbnail('full')?>
                </div>
                <div class="support-content1">
                  <p><?php the_content();?></p>
                </div>
              </div>


  <?php endwhile;
else :
  echo wpautop( 'Sorry, no posts were found' );
endif;
?>
</div>

   <?php
    echo get_template_part('template-parts/right', 'sidebar'); 
  ?>

<div class="col-md-2">

</div>
</div>
</div>
</div>
</div>
</div>
<?php get_footer('pages'); ?>
