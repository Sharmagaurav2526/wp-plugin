<?php get_header(); ?>

<!-- <audio src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/SampleAudio_0.4mb.mp3" preload="auto" /></audio> -->
<style type="text/css">
.playpauseaudio {
    position: absolute;
	top: 0px;
	width: 100%;
	height: 100%;
	left: 0px;
	right: 0px;
	z-index: 1;
}
.featuredaudio {
	position: relative;
}
.playaudio {
    width: 32px;
    margin: 50% auto;
}
.play_video {
    position: absolute;
	top: 0px;
	width: 100%;
	height: 100%;
	left: 0px;
	right: 0px;
	z-index: 1;
}
.play_video_cnt {
    width: 32px;
    margin: 50% auto;
}
/*.video_popup_content {
    position: relative;
    padding-bottom: 56.25%;
    overflow: hidden;
    background: rgba(19, 17, 17, 0.9519607843137255);
}	
.video_popup_content iframe {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}*/
/*.video_popup_section {
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0px;
    display: none;
    z-index: 999;
}*/
.closevideo {
    position: absolute;
    width: 100%;
    left: 0px;
    top: 0px;
    z-index: 1;
}
.closevideo span {
    float: right;
    cursor: pointer;
    margin-right: 1rem;
    margin-top: 1rem;
}
.audo_player_popular_section {
	display: none;
    position: fixed;
    background: rgba(1, 1, 1, 0.57);
    width: 100%;
    height: 100vh;
    top: 0px;
    z-index: 999;
    left: 0px;
}

.audio_player_popular_content {
    margin: 5% auto 0% !important;
    width: 100%;
    text-align: center;
}

.close_popular_audio, .latest_close_popular_audio {
    margin-top: 5rem;
    float: right;
    margin-right: 4rem;
}
.close_popular_audio span, .latest_close_popular_audio span{
    border-radius: 50%;
    border: 1px solid #ffffff;
    padding: 2px 8px;
    background: #fff;
    font-weight: bold;
    cursor: pointer;
}
</style>
<p class="directoryurl" style="display:none;"><?php echo get_stylesheet_directory_uri(); ?></p>
<div class="video_popup_section">
	<div class="video_popup_content">
		
	</div>
</div>

<!-- <section class="banner-section">
			<div class="banner">
				<img src="<?php the_field('banner_image'); ?>" alt="#">
			</div>
				<div class="banner-title">
					<h1><?php the_field('banner_heading'); ?></h1>
					<p><?php the_field('banner_content'); ?></p>
					<a href="<?php the_field('banner_button_url'); ?>" class="discover-btn"><?php the_field('banner_button'); ?></a>
				</div>
		</section>
 -->
 <section class="banner-section1">
<div class="carousal-slide">

  <?php if( have_rows('banner_repeater','5') ): ?>
  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->

    <div class="carousel-inner">
    	<?php $counter = 0; ?>
    	 <?php while ( have_rows('banner_repeater','5') ) : the_row();         
    	  ?> 
      <div class="item <?php if($counter == "1"){ echo "active"; } ?> ">
        <img src="<?php the_sub_field('banner_image','5'); ?>" alt="Los Angeles" style="width:100%;">
        <div class="banner-title">
					<h1><?php the_sub_field('banner_heading', '5'); ?></h1>
					<p><?php the_sub_field('banner_content','5'); ?></p>
					<a href="<?php the_sub_field('banner_button_url'); ?>" class="discover-btn"><?php the_sub_field('banner_button','5'); ?> </a>
				</div>
      </div>

    <!--   <div class="item">
        <img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/banner.jpg" alt="Los Angeles" style="width:100%;">
        <div class="banner-title">
					<h1>Test</h1>
					<p>Test Content</p>
					<a href="" class="discover-btn">Test </a>
				</div>
      </div>

      <div class="item">
        <img src="http://badpaddy.mobilytedev.com/wp-content/uploads/2018/08/banner.jpg" alt="Los Angeles" style="width:100%;">
        <div class="banner-title">
					<h1>Test</h1>
					<p>Test Content</p>
					<a href="" class="discover-btn">Test </a>
				</div>
      </div> -->

    
    
        <?php $counter++;  endwhile; ?>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

<?php endif; ?>
</div>




</div>

</section>
		
<section class="featured-carousal">
	<div class="container-fluid">
		<div class="featured-releases">
			<h1>FEATURED RELEASES</h1>
			<!-- <a href="#" class="search-btn">Live Performances Rock<span><i class="fa fa-search"></i></span></a> -->
			<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
		</div>
		<div class="carousal">
			<ul id="flexiselDemo1">
				<?php 
					$args = array(
				        'post_type' => 'audioalbum',
				        'tax_query' => array(
				                    array(
				                        'taxonomy' => 'album_types',
				                        'field' => 'slug',
				                        'terms' => 'featured'
				                    )
				                )
				        );
					// Custom query.
					$query = new WP_Query( $args );
					// Check that we have query results.
					if ( $query->have_posts() ) {
					    // Start looping over the query results.
					    while ( $query->have_posts() ) {
				 
				        $query->the_post();
				        if (has_post_thumbnail( $post->ID ) ){
						  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
						}
				 		//the_title();
				?>
				    <li>
				    	<a href="<?php echo get_permalink(); ?>">
					    	<img src="<?php echo $image[0]; ?>" class="img-responsive"> 
				    		<div class="carousal-img-title">
				    			<h1><?php the_title() ?></h1>
						    	<p>
						    		<?php
										if( have_rows('upload_album_audios') ){
											$count = 0;
										    while ( have_rows('upload_album_audios') ) : the_row();
										    $count++;
										    endwhile;
										    echo 'FFEATURED RELEASES '.$count;
										}else{
										    echo 'FFEATURED RELEASES 0';
										}
								    ?>
								</p>
							</div>
						</a>
					</li>
				<?php
				    }
				}
				// Restore original post data.
				wp_reset_postdata();
				?> 
			</ul>
		</div>
	</div>
</section>	
		
		<div class="clearfix"></div>
		
		<section class="events-carousal">
			<div class="upcoming-events">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
							<?php
							// WP_Query arguments
							$args = array(
								'post_type'              => array( 'event' ),
								'post_status'            => array( 'publish' ),
								'order'                  => 'ASC',
								'posts_per_page'          => 1
							);

							// The Query
							$the_query = new WP_Query( $args );
							while ( $the_query->have_posts() ) : 
    						$the_query->the_post();
							?>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div class="event-img">
										<h1>UPCOMING <span>EVENTS</span></h1>
                                      <img src="<?php the_post_thumbnail_url(); ?>" />


										<!-- <img src="<?php echo get_template_directory_uri(); ?>//images/upcoming-events.jpg" alt="#"> -->
										<div class="details">
											<h1>13</h1>
											<h1>jan</h1>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div class="rocks-nation">
										<h1><?php  
                                             echo get_the_title();?></h1>
										<p>Location  :   <span><?php echo get_field('event_location',get_the_ID()); ?></span></p>
										<p>Date     :   <span><?php echo get_field('start_date',get_the_ID()); ?></span></p>
										<p>Time      :   <span><?php echo get_field('event_time',get_the_ID()); ?></span></p>
										<p>Price     :   <span><?php echo get_field('event_price',get_the_ID()); ?></span></p>
										<div class="ticket-btn">
										<a href="#" class="purchase-ticket">PURCHASE TICKET</a>
										</div>
									</div>
								</div>	
							</div>
						<?php endwhile; 
							wp_reset_postdata();
							?>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
							<div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<div class="carousal-heding">
										<h1>LATEST <span>AUDIO</span></h1>
										<div class="carousal">
											<ul id="flexiselDemo2">
											<?php
												// WP_Query arguments
												$args = array(
													'post_type'              => array( 'audioalbum' ),
													'post_status'            => array( 'publish' ),
													'order'                  => 'DESC',
													'posts_per_page'         => '3',
													
												);
                                              // The Query
												$the_query = new WP_Query( $args );
												while ( $the_query->have_posts() ) : 
					    						$the_query->the_post();

													if( have_rows('upload_album_audios') ){
														$count = 0;
													    while ( have_rows('upload_album_audios') ) : the_row();
													    if($count =='0'){
														    $image_artist = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
														    $image_artist = $image_artist['0'];
														    $audiothumbnail = get_sub_field('add_audio_thumbnail');
														    $defaultimg = get_stylesheet_directory_uri().'/images/default_album_art.png';

														    if( $audiothumbnail != '' ){
														    	$trackimg = $audiothumbnail;
														    }else if ( $audiothumbnail == '' ) {
														    	$trackimg = $image_artist;
														    }else if ( $audiothumbnail == '' && $image_artist =='' ) {
														    	$trackimg = $defaultimg;
														    }
												?>
														<li class="featuredaudio latest_audio_slider">
															<div class="overlay"></div>
															<div class="playpauseaudio">
																<div class="playaudio audiobtn active" data-id="#playaudio_<?php echo $count; ?>" onclick="playsound('playaudio_<?php echo $count; ?>')">
																	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/play-audio.png" class="img-responsiver playicon">
																	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pause-audio.png" class="img-responsiver pauseicon" style="display:none;">
																</div>

																<div class="audiofiles" style="display:none;">
																	<audio id="playaudio_<?php echo $count; ?>" controls="" preload="auto" autobuffer="">
																	  <source class="firstaudiotype" src="<?php echo get_sub_field('upload_ogg_file'); ?>" type="audio/ogg">
																	  <source class="secondaudiotype" src="<?php echo get_sub_field('upload_mp3_file'); ?>" type="audio/mpeg">
																	</audio>
																</div>
																
															</div>
															<img src="<?php echo $trackimg; ?>" class="img-responsive">


															<!--  -->
															<div class="audo_player_popular_section">
														 		<div class="latest_close_popular_audio" data-value="#playaudio_latest_<?php echo $count; ?>"><span>x</span></div>
														 		<div class="audio_player_popular_content">
														 			<audio id="playaudio_latest_<?php echo $count; ?>" controls="" preload="auto" autobuffer="">
																		  <source class="firstaudiotype" src="<?php echo get_sub_field('upload_ogg_file'); ?>" type="audio/ogg">
																		  <source class="secondaudiotype" src="<?php echo get_sub_field('upload_mp3_file'); ?>" type="audio/mpeg">
																	</audio>
														 		</div>
														 	</div>
															<!--  -->

														</li>
												<?php
														}	    
													$count++; endwhile;
													}else{
													    echo '<p class="notrack">No Track Found..</p>';
													}
											    ?>
												<?php endwhile; 
												wp_reset_postdata();
												?>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<div class="carousal-heding">
										<h1>LATEST <span>VIDEO</span></h1>
										<div class="carousal">
											<ul id="flexiselDemo3">
												<?php
												// WP_Query arguments
												$args = array(
													'post_type'              => array( 'video' ),
													'post_status'            => array( 'publish' ),
													'order'                  => 'DESC',
													'posts_per_page'         => '3',
													
												);
	                                              // The Query
													$the_query = new WP_Query( $args );
													while ( $the_query->have_posts() ) : 
						    						$the_query->the_post();
						    						$videoid = get_field('vimeo_video_id',get_the_ID());
													$videothums = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".$videoid.".php"));
													$videothums = $videothums[0]['thumbnail_large'];
												?>
												<li class="video_content featuredvideo">
													<div class="play_video">
														<div class="play_video_cnt playvideobtn" data-id="<?php echo $videoid; ?>" >
															<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/play-audio.png" class="img-responsiver playicon">
														</div>
													</div>
													<img src="<?php echo $videothums; ?>" claSS="img-responsive" style="min-height:250px;"/>
												</li>
												<?php endwhile; 
												wp_reset_postdata();
												?>
											</ul>

										</div>
									</div>
								</div>

								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<div class="carousal-heding">
										<h1>NEW <span>ARTISTS</span></h1>
										<div class="carousal">
											<ul id="flexiselDemo4">
												<?php
												// WP_Query arguments
												$args = array(
													'post_type'              => array( 'audioalbum' ),
													'post_status'            => array( 'publish' ),
													'order'                  => 'DESC',
													'posts_per_page'         => '1',
													
												);
                                              // The Query
												$the_query = new WP_Query( $args );
												while ( $the_query->have_posts() ) : 
					    						$the_query->the_post();

													if( have_rows('upload_album_audios') ){
														$count = 4;
													    while ( have_rows('upload_album_audios') ) : the_row();
														    $image_artist = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
														    $image_artist = $image_artist['0'];
														    $audiothumbnail = get_sub_field('add_audio_thumbnail');
														    $defaultimg = get_stylesheet_directory_uri().'/images/default_album_art.png';

														    if( $audiothumbnail != '' ){
														    	$trackimg = $audiothumbnail;
														    }else if ( $audiothumbnail == '' ) {
														    	$trackimg = $image_artist;
														    }else if ( $audiothumbnail == '' && $image_artist =='' ) {
														    	$trackimg = $defaultimg;
														    }
												?>
														<li class="featuredaudio">
															<div class="overlay"></div>
															<div class="playpauseaudio">
																<div class="playaudio audiobtn active" data-id="#playaudio_<?php echo $count; ?>" onclick="playsound('playaudio_<?php echo $count; ?>')">
																	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/play-audio.png" class="img-responsiver playicon">
																	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pause-audio.png" class="img-responsiver pauseicon" style="display:none;">
																</div>
																<div class="audiofiles" style="display:none;">
																	<audio id="playaudio_<?php echo $count; ?>" controls="" preload="auto" autobuffer="">
																	  <source class="firstaudiotype" src="<?php echo get_sub_field('upload_ogg_file'); ?>" type="audio/ogg">
																	  <source class="secondaudiotype" src="<?php echo get_sub_field('upload_mp3_file'); ?>" type="audio/mpeg">
																	</audio>
																</div>
															</div>
															<img src="<?php echo $trackimg; ?>" class="img-responsive">
														</li>
												<?php    
													$count++; endwhile;
													}else{
													    echo '<p class="notrack">No Track Found..</p>';
													}
											    ?>
												<?php endwhile; 
												wp_reset_postdata();
												?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="marketing-slider">
			<div class="carousal">	
				<ul id="flexiselDemo6">
					<?php 
					    if( have_rows('marketing_images') ):
					    while ( have_rows('marketing_images') ) : the_row();
					?>
					<li><img src="<?php the_sub_field('images'); ?>" /></li>
			     	<?php  endwhile;
                       else :   endif; ?>
					<!-- <li><img src="<?php echo get_template_directory_uri(); ?>/images/marketing-slider.jpg" /></li>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/marketing-slider.jpg" /></li>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/marketing-slider.jpg" /></li> -->
				</ul>
			</div>
		</section>
		<div class="clearfix"></div>
		<section class="news-carousals">
			<div class="carousal">	
				<ul id="flexiselDemo5">
					<?php
							// WP_Query arguments
							$args = array(
								'post_type'              => array( 'event' ),
								'post_status'            => array( 'publish' ),
								'order'                  => 'ASC',
								
							);

							// The Query
							$the_query = new WP_Query( $args );
							while ( $the_query->have_posts() ) : 
    						$the_query->the_post();
							?>
					<li><img src="<?php the_post_thumbnail_url(); ?>" />
                       
                        <div class="details">
											<a href="<?php the_permalink(); ?>"><h1><?php echo get_the_title();?></h1></a>
											<P><?php echo get_the_content();?></P>
										</div></li>
					
					<!-- <li><img src="<?php echo get_template_directory_uri(); ?>/images/news-cr2.jpg" /><div class="details">
											<h1>VIET CONG</h1>
											<P>Seitan Plaid Wes Anderson Pug Blue Bottle...</P>
										</div></li>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/news-cr3.jpg" /><div class="details">
											<h1>HAPPENING NOW: <span>FEBRUARY 8 TO FEBRUARY 14,2015</span></h1>
											<P>Seitan Plaid Wes Anderson Pug Blue Bottle...</P>
										</div></li>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/news-cr4.jpg" /><div class="details">
											<h1>DAFT PUNK - GET LUCKY</h1>
											<P>Seitan Plaid Wes Anderson Pug Blue Bottle...</P>
										</div></li> -->
										<?php endwhile; 
							wp_reset_postdata();
							?>
				</ul>
				<a href="#" class="learn-more-btn">LEARN MORE</a>
			</div>
		</section>
		
		
			<section class="songs-insta">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="popular-songs">
								<h1>POPULAR <span>SONGS</span></h1>
								<img src="<?php echo get_template_directory_uri(); ?>/images/you-tube-img.jpg">
								<div class="heart-dancing">
									<ul>
										<?php
											$args=array(
											    'posts_per_page' => 6, 
											    'post_type' => 'audio',
											    'meta_key' => 'post_views_count',
											    'orderby' => 'meta_value_num',
											    'order' => 'DESC'
											);
											 
											$query = new WP_Query($args);
											if( $query->have_posts()):
											$count = 0;	
											while( $query->have_posts()): $query->the_post();
										?>
											 <li class="popular_video_link">
											 	<a href="#"><?php echo get_the_title(); ?></a>
											 	<div class="audo_player_popular_section">
											 		<div class="close_popular_audio" data-value="#playaudio_popular_<?php echo $count; ?>"><span>x</span></div>
											 		<div class="audio_player_popular_content">
											 			<audio id="playaudio_popular_<?php echo $count; ?>" controls="" preload="auto" autobuffer="">
															  <source class="firstaudiotype" src="<?php echo get_field('ogg_audio'); ?>" type="audio/ogg">
															  <source class="secondaudiotype" src="<?php echo get_field('mp3_audio'); ?>" type="audio/mpeg">
														</audio>
											 		</div>
											 	</div>

											 </li>
									<?php
										$count++;
										endwhile;
										endif;
									?>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="instagram">
							<h1 class="insta">INSTAGRAM</h1>
							<?php echo do_shortcode('[instagram-feed]'); ?>
								<!-- <a href="#"><img src="<?php echo get_template_directory_uri(); ?>//images/insta1.jpg"></a>
								<a href="#"><img class="insta2-img" src="<?php echo get_template_directory_uri(); ?>//images/insta2.jpg"></a>
								<a href="#"><img src="<?php echo get_template_directory_uri(); ?>//images/insta3.jpg"></a> -->
							</div>
						</div>
					</div>
				</div>
			</section>
			<div class="clearfix"></div>
			<section class="form-footer" style="background-image: url('<?php echo get_field('contact_us_image', '5'); ?>');">

				<div class="contact-form-footer">
					<div id="contact" class="container-fluid bg-grey">
					  <h2 class="text-center"><?php the_field('contact_title','5'); ?></h2>
					  <div class="row">
					  	<?php echo do_shortcode('[contact-form-7 id="8" title="Contact form 1"]'); ?>
						<!-- 
						<div class="col-sm-12 slideanim slide">
						  <div class="row">
							<div class="col-sm-6 form-group">
							   <input class="form-control" id="email" name="email" placeholder="Your Email" required="" type="email">
							</div>
							<div class="col-sm-6 form-group">
							  <input class="form-control" id="name" name="name" placeholder="Subject" required="" type="text">
							</div>
						  </div>
						  <textarea class="form-control" id="message" name="message" placeholder="message" rows="5"></textarea><br>
						  <div class="row">
							<div class="col-sm-12 form-group">
							  <button class="btn btn-default pull-right" type="submit">Send Message</button>
							</div>
						  </div>
						</div>
					  </div> -->
					</div>
				</div>
			</div>
			</section>
			<p class="directoryurl" style="display:none;"><?php echo get_stylesheet_directory_uri(); ?></p>
								<div class="video_popup_section">
									<div class="video_popup_content">
										
									</div>
								</div>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".videooverlaycontent .play_btn").on("click",function(){
		var directoryurl = jQuery(".directoryurl").html();
		jQuery(".video_popup_section").show();
		//jQuery(".banner-section1 .featured-carousal .events-carousal .marketing-slider").hide();
		var videoid = jQuery(this).attr("data-id");
		jQuery(".video_popup_section .video_popup_content").html('<div class="closevideo"><span><img src="'+directoryurl+'/images/cross.png" class="img-responsive"></span></div><iframe src="//player.vimeo.com/video/'+videoid+'?autoplay=1&loop=1&background=0&control=1" width="100%" height="500px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>')
	});
	jQuery(document).on("click",".closevideo span",function(){
		jQuery(".video_popup_section").hide();
		//jQuery(".banner-section1 .featured-carousal .events-carousal .marketing-slider").hide();
		
		jQuery(".video_popup_section .video_popup_content").empty();
	});
});
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(".featuredaudio").each(function(){
			var playing = false;
			jQuery(this).find(".playpauseaudio .audiobtn").on("click",function(){

				jQuery(".featuredaudio").find(".playpauseaudio audio").trigger('pause');
		    	jQuery(".featuredaudio").find(".playpauseaudio .playicon").show();
		    	jQuery(".featuredaudio").find(".playpauseaudio .pauseicon").hide();

				playing = !playing;
				jQuery(this).toggleClass('on');
				if (playing) {
					jQuery(this).find(".playicon").hide();
					jQuery(this).find(".pauseicon").show();
					var dataid = jQuery(this).attr("data-id");
					jQuery(dataid).trigger('play');
			    }
			    else {
			    	jQuery(this).find(".playicon").show();
					jQuery(this).find(".pauseicon").hide();
			    	var dataid = jQuery(this).attr("data-id");
					jQuery(dataid).trigger('pause');
			    }
			});
		});

		jQuery(".popular_video_link a").on("click",function(event){
			event.preventDefault();
			var playing = false;
			jQuery(this).closest("li").find(".audo_player_popular_section").show();
			var audioid = jQuery(this).closest("li").find(".close_popular_audio").attr("data-value");
			playing = !playing;
			if (playing) {
				jQuery(audioid).trigger('play');
		    }
		    else {
				jQuery(audioid).trigger('pause');
		    }
		});
		
		//LATEST AUDIO - START
		jQuery(".latest_audio_slider .playaudio").on("click",function(event){
			event.preventDefault();
			var playing = false;
			jQuery(this).parent().parent().find(".audo_player_popular_section").show();
			var audioid = jQuery(this).parent().parent().find(".latest_close_popular_audio").attr("data-value");
			playing = !playing;
			if (playing) {
				jQuery(audioid).trigger('play');
		    }
		    else {
				jQuery(audioid).trigger('pause');
		    }
		});

		jQuery(".latest_close_popular_audio").on("click",function(event){
			event.preventDefault();
			var playing = false;
			var audioid = jQuery(this).attr("data-value");
			playing = !playing;
			if (playing) {
				jQuery(audioid).trigger('play');
		    }
		    else {
				jQuery(audioid).trigger('pause');
		    }
		    jQuery(".audo_player_popular_section").hide();
		});
		//LATEST AUDIO - END

		jQuery(".close_popular_audio").on("click",function(event){
			event.preventDefault();
			var playing = false;
			var audioid = jQuery(this).attr("data-value");
			playing = !playing;
			if (playing) {
				jQuery(audioid).trigger('play');
		    }
		    else {
				jQuery(audioid).trigger('pause');
		    }
		    jQuery(".audo_player_popular_section").hide();
		});

	});
</script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".featuredvideo .playvideobtn").on("click",function(){
		var directoryurl = jQuery(".directoryurl").html();
		jQuery(".video_popup_section").show();
		var videoid = jQuery(this).attr("data-id");
		jQuery(".video_popup_section .video_popup_content").html('<div class="closevideo"><span><img src="'+directoryurl+'/images/cross.png" class="img-responsive"></span></div><iframe src="https://player.vimeo.com/video/'+videoid+'?autoplay=1&loop=1&background=0&control=1" width="100%" height="500px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>')
	});
	jQuery(document).on("click",".closevideo span",function(){
		jQuery(".video_popup_section").hide();
		jQuery(".video_popup_section .video_popup_content").empty();
	});
});
</script>		
<?php get_footer('page'); ?>