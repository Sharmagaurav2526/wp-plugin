<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'investedtraveler');

/** MySQL database username */
define('DB_USER', 'investedtraveler');

/** MySQL database password */
define('DB_PASSWORD', 'nntNtdUYpmH9ZbOX');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OiDO<OG`.ED{Me^rPg-/Y`xN<uOhBS!)HuT&%$7&Bo[nwqQ)1g`v%[@Uqx-onGcL');
define('SECURE_AUTH_KEY',  '27@X.n%}m`ctJ5[;@gsyG=O?hVNe$ [[r[mo8,-;R7wx91lScDoKoMGU,R`~{<HN');
define('LOGGED_IN_KEY',    '9%/KR {GGB7kT4+o/0R8K|{G`LT%YoEsX WcM:V>|; ZsQ^-@aV1uM]6.7l.I>2Z');
define('NONCE_KEY',        '!NVu|C*&ux.Xb~#Sy:zyC{05gppSGOc`opjn,.x#!|FlKD4uCu3_s!9Wc*ERoC]L');
define('AUTH_SALT',        '~/F`w&,7KU$nSUk=?@ne8 :2(?+[EpGxQ.Y{KCO.0J3i2#n/(B:!djb;7;5/fzMn');
define('SECURE_AUTH_SALT', 'FN3P|vt*=xcRL%{~S1m}w|#22TZdM7eONn.N;SpLCwQ]$GWsf2}ajHP&[H0vH%ZA');
define('LOGGED_IN_SALT',   '1r1PDR7sJi aM1&MJ`R^~q`kd WH=+qZdEgilKaAxEhBb_|x8,OiOD:{>d LYC?2');
define('NONCE_SALT',       'sLmouc$0d>*]}`sTxIc!n{DqJt btmIdq`B.g0=Y!w=FBu_-w1m+ILEGEy/}7mU{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
