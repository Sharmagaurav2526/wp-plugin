<?php 
//  Function to register nav menus
function register_my_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' ),
      'header-menu' => __( 'Header Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

//  Function to register sidebars
add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );

     register_sidebar( array(
        'name' => __( 'Footer Sidebar', 'theme-slug' ),
        'id' => 'footer-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<ul class="nav navbar-nav navbar-left">',
      'after_widget'  => '</ul>',
      'before_title'  => '<h2 class="widgettitle">',
      'after_title'   => '</h2>',
    ) );
     register_sidebar( array(
        'name' => __( 'Footer Sidebar 1', 'theme-slug' ),
        'id' => 'footer-2',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<ul class="footer-head">',
  'after_widget'  => '</ul>',
  'before_title'  => '<h2 class="widgettitle">',
  'after_title'   => '</h2>',
    ) );
}

// function to register logo
function theme_prefix_setup() {
	
	add_theme_support( 'custom-logo', array(
		'height'      => 100,
		'width'       => 400,
		'flex-width' => true,
	) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' ); 

// To display featue image option in posts  
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 250, 250, true ); // default Featured Image dimensions (cropped)
 
    // additional image sizes
    // delete the next line if you do not need additional image sizes
    add_image_size( 'category-thumb', 300, 9999 ); // 300 pixels wide (and unlimited height)
 }
function setPostViews($postID) {
$countKey = 'post_views_count';
$count = get_post_meta($postID, $countKey, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $countKey);
        add_post_meta($postID, $countKey, '0');
    }else{
        $count++;
        update_post_meta($postID, $countKey, $count);
    }
}


function pagination($pages = '', $range = 4)
{   

     $showitems = ($range * 1)+1;  
     global $paged;
     if(empty($paged)) $paged = 1;
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
     if(1 != $pages)
     {
        // echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         echo '<ul class="pagination">';
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 //echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
                 echo ($paged == $i)? "<li class=\"current page-item\"><a class=\"page-link\">".$i."</a></li>":"<li class=\"page-item\"><a href='".get_pagenum_link($i)."' class=\"inactive page-link\">".$i."</a></li>";
             }
         }
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         //echo "</div>\n";
         echo "</ul>\n";

     }
}


   function shapeSpace_display_search_form() {

    $search_form = '<form class="navbar-form" role="search" method="get" id="searchform" action="href="'. esc_url(home_url('/')) .'">
                        <div class="input-group add-on">
                        <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
                        <div class="input-group-btn">
                        <button class="btn btn-info" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                        </div>
                        </form>';         

    return $search_form;
}
add_shortcode('display_search_form', 'shapeSpace_display_search_form');

register_sidebar( array(
    'name' => __( 'Right Sidebar', 'badpaddy' ),
    'id' => 'right-sidebar-1',
    'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'badpaddy' ),
    'before_widget' => '<div class="row widget %2$s" id="%1$s"><div class="span12">',
    'after_widget'  => '</div></div>',
    'before_title'  => '<h4 class="text-left">',
    'after_title'   => '</h4>',
    ) 
);

function badpaddy_display_search_form() {

    $search_form = '<form method="get" class="form-search form-horizontal" id="search-form-alt1" action="'. esc_url(home_url('/')) .'">
        <div class="input-append span12">
            <div class="span13">
            <input type="text" name="s" class="search-query" placeholder="Search...">
            <button type="submit" class="btn serch-box"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
        </div></div>
    </form>';         

    return $search_form;
}
add_shortcode('display_search_form', 'badpaddy_display_search_form');

/*--function to add theme options start--*/
function my_acf_options_page_settings( $settings )
{
    $settings['title'] = 'Theme Options';
    $settings['pages'] = array('Header','Footer');
    return $settings;    
}
add_filter('acf/options_page/settings', 'my_acf_options_page_settings');
/*--//function to add theme options end--*/


/**
 * Register a custom menu page.
 */
function wpdocs_register_my_custom_menu_page(){
    add_menu_page( 
        'Request RFP', //Page Title
        'Request RFP', //Menu Title
        'manage_options', 
        'request_ref', //menu_slug
        'request_ref', //page_callback_function
        '
dashicons-admin-post',
        6
    ); 
}
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
 
/**
 * Display a custom menu page
 */
function request_ref(){ ?>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <style type="text/css">
       table.table.table-striped th {
    font-size: 12px !important;
        }
        table.table.table-striped td {
            font-size: 12px !important;
        } 
        div#header h1 {
            font-size: 24px;
            margin-top: 20px;
         }
    </style>
    
    <div id="header">
        <h1>RFP Requests</h1>
    </div>

    <div id="rpf_admin" class="wrap">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">WHAT ARE YOU LOOKING TO DO?</th>
              <th scope="col">LIST OF OTHER,NOT OTHER'S</th>
              <th scope="col">TELL US MORE</th>
              <th scope="col">WHAT’S YOU BUDGET AND TIME FRAME?</th>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Email</th>
              <th scope="col">Phone Number</th>
              <th scope="col">Company</th>
              <th scope="col">Message</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
           <?php 

            global $wpdb;
            $results = $wpdb->get_results( "SELECT * FROM `wp_posts` WHERE post_type = 'rfp_requests' AND post_status = 
                'publish'" );
            
            //echo "<pre>"; print_r($results); echo "</pre>";

            foreach ($results as $result) {

            ?>
            <tr>
              <th scope="row"><?=$result->ID?></th>
              <td><?= get_post_meta( $result->ID, $key = 'rfp_user_looking_to_do', $single = true ); ?></td>
              <td><?= get_post_meta( $result->ID, $key = 'rfp_list_of_other_not_other', $single = true ); ?></td>
              <td><?= get_post_meta( $result->ID, $key = 'rfp_tell_us_more', $single = true ); ?></td>
              <td><?= get_post_meta( $result->ID, $key = 'rfp_budget_timeframe', $single = true ); ?></td>
              <td><?= get_post_meta( $result->ID, $key = 'rfp_first_name', $single = true ); ?></td>
              <td><?= get_post_meta( $result->ID, $key = 'rfp_last_name', $single = true ); ?></td>
              <td><?= get_post_meta( $result->ID, $key = 'rfp_email', $single = true ); ?></td>
              <td><?= get_post_meta( $result->ID, $key = 'rfp_pnumber', $single = true ); ?></td>
              <td><?= get_post_meta( $result->ID, $key = 'rfp_company_name', $single = true ); ?></td>
              <td><?= get_post_meta( $result->ID, $key = 'rfp_message', $single = true ); ?></td>
              <td><a href="<?=base_url('#')?>/delete/id/<?=$result['id'];?>"><i class="fa fa-eraser"></i></a>
              </td>
            </tr>
             
             <?php } ?>

          </tbody>
        </table>
    </div>
<?php }