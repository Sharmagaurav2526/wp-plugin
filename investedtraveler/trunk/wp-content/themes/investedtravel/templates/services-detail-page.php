<?php
/**
 * Template Name: Agency Services Details Page
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


      <div class="banner_who" style="background-image: url(<?php the_field('event_placement_banner');?>);">
        <div class="container">
          <div class="banner_content  banner_who-content text-center">
            <h1><?php the_field('event_placement_title');?></h1>
            <p><?php the_field('event_placement_content');?></p>            
          </div>
        </div>
      </div>
      <!-- End Banner Area -->
    

        <!-- start section Area -->
      <section class="who-we-sec">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="who-we-content">
                <h3><?php the_field('event_section_title');?></h3>
                <div class="hr"></div>
                <div class="hr2"></div>
                <h1 class="jumps"><?php the_field('event_section_content');?></p>
                <button type="button" class="btn btn-light btn-req-pro">Request for Proposal</button>
              </div>
            </div>
          
          </div>
        </div>
      </section>
      
      
      
         <!-- End section Area -->
      
      
         <!-- End section Area -->
<?php get_footer('pages'); ?>