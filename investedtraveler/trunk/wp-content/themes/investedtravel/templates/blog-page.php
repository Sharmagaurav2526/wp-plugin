<?php
/**
 * Template Name:Blog Page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */ 
get_header(); ?>
<section class="blog-banner">
	<div class="blog-banner-content">
		<h1>Recent News</h1>
		<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here',</p>
	</div>
</section>
<section class="container-fluid">
	<div class="container">
		<div class="blog-temp">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="row blog-part">
						<div class="thumb-img col-sm-12 col-xs-5 col-md-12 col-lg-5 no-paddings">
							<img src="http://investedtraveler.mobilytedev.com/wp-content/uploads/2018/08/blog-side.png">
						</div>
					
						<div class="thumb-content col-sm-12 col-md-12 col-lg-7 col-xs-7">
							<h1>There are many variations of passages of Lorem Ipsum available of Lorem Ipsum available</h1>
							<h6>Posted on july 2,2018</h6>
							<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,Contrary to popular belief, </p>
							<i class="fas fa-long-arrow-alt-right"></i>
						</div>
					</div>
					
					<div class="row blog-part">
						<div class="thumb-img col-sm-12 col-xs-5 col-md-12 col-lg-5 no-paddings">
							<img src="http://investedtraveler.mobilytedev.com/wp-content/uploads/2018/08/blog-side.png">
						</div>
					
						<div class="thumb-content col-sm-12 col-md-12 col-lg-7 col-xs-7">
							<h1>There are many variations of passages of Lorem Ipsum available of Lorem Ipsum available</h1>
							<h6>Posted on july 2,2018</h6>
							<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,Contrary to popular belief, </p>
							<i class="fas fa-long-arrow-alt-right"></i>
						</div>
					</div>
					
					<div class="row blog-part">
						<div class="thumb-img col-sm-12 col-xs-5 col-md-12 col-lg-5 no-paddings">
							<img src="http://investedtraveler.mobilytedev.com/wp-content/uploads/2018/08/blog-side.png">
						</div>
					
						<div class="thumb-content col-sm-12 col-md-12 col-lg-7 col-xs-7">
							<h1>There are many variations of passages of Lorem Ipsum available of Lorem Ipsum available</h1>
							<h6>Posted on july 2,2018</h6>
							<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,Contrary to popular belief, </p>
							<i class="fas fa-long-arrow-alt-right"></i>
						</div>
					</div>
					
					<div class="row blog-part">
						<div class="thumb-img col-sm-12 col-md-12 col-xs-5 col-lg-5 no-paddings">
							<img src="http://investedtraveler.mobilytedev.com/wp-content/uploads/2018/08/blog-side.png">
						</div>
					
						<div class="thumb-content col-sm-12 col-md-12 col-lg-7 col-xs-7">
							<h1>There are many variations of passages of Lorem Ipsum available of Lorem Ipsum available</h1>
							<h6>Posted on july 2,2018</h6>
							<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,Contrary to popular belief, </p>
							<i class="fas fa-long-arrow-alt-right"></i>
						</div>
					</div>
					
					<div class="row blog-part">
						<div class="thumb-img col-sm-12 col-xs-5 col-md-12 col-lg-5 no-paddings">
							<img src="http://investedtraveler.mobilytedev.com/wp-content/uploads/2018/08/blog-side.png">
						</div>
					
						<div class="thumb-content col-sm-12 col-md-12 col-lg-7 col-xs-7">
							<h1>There are many variations of passages of Lorem Ipsum available of Lorem Ipsum available</h1>
							<h6>Posted on july 2,2018</h6>
							<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,Contrary to popular belief, </p>
							<i class="fas fa-long-arrow-alt-right"></i>
						</div>
					</div>
					
					
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="blog-sidebar">
						<h1>SUBSCRIBE TO THE BLOG</h1>
						<div class="blog-hr">
						</div>
						<div class="blue-box">
							<h4>THE NEWSLETTER</h4>
							<P>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,Contrary to popular belief,</P>
							  <input type="text" class="form-control" id="usr" placeholder="Enter your email">
							  <button type="button" class="btn btn-follow">FOLLOW</button>
						</div>
					</div>
					
					
					<div class="blog-sidebar">
					<h1>RECENT POSTS</h1>
					<div class="blog-hr">
					</div>
					
					<div class="aside-post">
						<div class="row">
							<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 no-paddings">
								<div class="thumb-img">
									<img src="http://investedtraveler.mobilytedev.com/wp-content/uploads/2018/08/blog-side.png">
							</div>
							</div>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 no-padding">
								<div class="side-content">
									<h1>It has roots in a piece of classical Latin literature from 45 BC</h1>
									<h6>on january 9,2018</h6>
								</div>
							</div>
						</div>
					</div>
					
					<div class="aside-post">
						<div class="row">
							<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 no-paddings">
								<div class="thumb-img">
									<img src="http://investedtraveler.mobilytedev.com/wp-content/uploads/2018/08/blog-side.png">
							</div>
							</div>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 no-padding">
								<div class="side-content">
									<h1>It has roots in a piece of classical Latin literature from 45 BC</h1>
									<h6>on january 9,2018</h6>
								</div>
							</div>
						</div>
					</div>
					
					<div class="aside-post">
						<div class="row">
							<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 no-paddings">
								<div class="thumb-img">
									<img src="http://investedtraveler.mobilytedev.com/wp-content/uploads/2018/08/blog-side.png">
							</div>
							</div>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 no-padding">
								<div class="side-content">
									<h1>It has roots in a piece of classical Latin literature from 45 BC</h1>
									<h6>on january 9,2018</h6>
								</div>
							</div>
						</div>
					</div>
					
					<div class="aside-post">
						<div class="row">
							<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 no-paddings">
								<div class="thumb-img">
									<img src="http://investedtraveler.mobilytedev.com/wp-content/uploads/2018/08/blog-side.png">
							</div>
							</div>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 no-padding">
								<div class="side-content">
									<h1>It has roots in a piece of classical Latin literature from 45 BC</h1>
									<h6>on january 9,2018</h6>
								</div>
							</div>
						</div>
					</div>
					
					<div class="aside-post">
						<div class="row">
							<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 no-paddings">
								<div class="thumb-img">
									<img src="http://investedtraveler.mobilytedev.com/wp-content/uploads/2018/08/blog-side.png">
							</div>
							</div>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 no-padding">
								<div class="side-content">
									<h1>It has roots in a piece of classical Latin literature from 45 BC</h1>
									<h6>on january 9,2018</h6>
								</div>
							</div>
						</div>
					</div>
					
					
					<div class="topics">
						<ul>
							<li class="main-t">TOPICS</li>
							<li>Curated Experiences</li>
							<li>In The News</li>
							<li>Thought Leadership</li>
							<li>Uncategorized</li>
						</ul>
					</div>
					
					<ul class="topics follow">
						<li class="main-t">FOLLOW US</li>
					</ul>
					
					<ul class="social-icons">
						<li><a href="#"><i class="fab fa-twitter"></i></a></li>
						<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
						<li><a href="#"><i class="fab fa-instagram"></i></a></li>
						<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
					</ul>
					
				</div>
			</div>
		</div>
		
		
		</div>
		
</section>
			
<?php get_footer('pages'); ?>