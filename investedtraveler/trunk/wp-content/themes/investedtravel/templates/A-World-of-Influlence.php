<?php
/**
 * Template Name:A World of Influlence page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */ 
get_header(); ?>
<section class="influence-headertop" style="background-image: url(<?php the_field('influlence_banner_image')?>);">
    <div class="container">
        <h2><?php the_field('influlence_banner_title')?> </h2>
        <p>
            <?php the_field('influlence_banner_tagline')?>
        </p>
    </div>
</section>

<!-- End leadership team Area -->
<section class="influence-team">
    <div class="container">
        <h2><?php the_field('our_team_title')?></h2>
        <div class="hr"></div>
        <div class="hr2"></div>
        <h3><?php the_field('our_team_tagline')?></h3>
        <p><?php the_field('our_team_content')?>
        </p>
        <div class="row">
        	<?php if( have_rows('team_information') ): ?>
            <?php while ( have_rows('team_information') ) : the_row(); ?>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="team-inner">
                    <label><img src="<?php the_sub_field('team_image'); ?>"></label>
                    <h4><?php the_sub_field('team_name'); ?></h4>
                    <p><?php the_sub_field('team_description'); ?></p>
                    <!-- <ul>
                        <li><a href="<?php //the_sub_field('facebook_link'); ?>"><img src="<?php //the_sub_field('facebook_image'); ?>"> </a></li>
                        <li><a href="<?php //the_sub_field('twitter_link'); ?>"><img src="<?php //the_sub_field('twitter_image'); ?>"> </a></li>
                    </ul> -->
                </div>
            </div>
            <?php  endwhile; ?>
            <?php endif; ?>
            <!-- <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="team-inner">
                    <label><img src="<?php echo get_template_directory_uri(); ?>/images/team2.jpg"></label>
                    <h4>Crystal Sargent</h4>
                    <p>Founder and president Ambitious. Kalymons, Greece. Gratitude</p>
                    <ul>
                        <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/fbicon.png"> </a></li>
                        <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/vimages/linkicon.png"> </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="team-inner">
                    <label><img src="<?php echo get_template_directory_uri(); ?>/images/team3.jpg"></label>
                    <h4>Crystal Sargent</h4>
                    <p>Founder and president Ambitious. Kalymons, Greece. Gratitude</p>
                    <ul>
                        <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/fbicon.png"> </a></li>
                        <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/linkicon.png"> </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="team-inner">
                    <label><img src="<?php echo get_template_directory_uri(); ?>/images/team-default.jpg"></label>
                    <h4>Crystal Sargent</h4>
                    <p>Founder and president Ambitious. Kalymons, Greece. Gratitude</p>
                    <ul>
                        <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/fbicon.png"> </a></li>
                        <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/linkicon.png"> </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="team-inner">
                    <label><img src="<?php echo get_template_directory_uri(); ?>/images/team-default.jpg"></label>
                    <h4>Crystal Sargent</h4>
                    <p>Founder and president Ambitious. Kalymons, Greece. Gratitude</p>
                    <ul>
                        <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/fbicon.png"> </a></li>
                        <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/linkicon.png"> </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="team-inner">
                    <label><img src="<?php echo get_template_directory_uri(); ?>/images/team4.jpg"></label>
                    <h4>Crystal Sargent</h4>
                    <p>Founder and president Ambitious. Kalymons, Greece. Gratitude</p>
                    <ul>
                        <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/fbicon.png"> </a></li>
                        <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/linkicon.png"> </a></li>
                    </ul>
                </div>
            </div> -->
        </div>

    </div>
</section>
<!-- Our Corporate Profile -->
<section class="invest-hpycustomer">
    <div class="container">
        <h2><?php the_field('corporate_title');?></h2>
        <div class="hr"></div>
        <div class="hr2"></div>
        <h3><?php the_field('corportate_tagline');?></h3>
        <p><?php the_field('corporate_content');?></p>
         <h3><?php the_field('certification_tagline');?></h3>
        <div class="happycrousel owl-carousel owl-theme">
            
            <?php if( have_rows('corportate_repeater') ): $class_count = 0; ?>
            <?php while ( have_rows('corportate_repeater') ) : the_row(); ?>
            <div class="item ">

                <label><img class="img-<?=$class_count?>" src="<?php the_sub_field('corporate_image'); ?>"></label>
                <ul><?php the_sub_field('corporate_code');?>
                    <!-- <li>54112- Human Resources Consulting</li>
                    <li>541613 – Marketing Consulting</li>
                    <li>611430 – Professional Management</li>
                    <li>541820 – Public Relations Agency</li>
                    <li>514618 – Management Consulting</li>
                    <li>561599 – All Other Travel Arrangements</li>
                    <li>721110 – Hotel, Motel (excluding casino)</li>
                    <li>561920 - Convention and Trade Show</li>
                    <li>512110 - Motion Picture/ Video Production</li>
                    <li>624410 – Child care services</li> -->
                </ul>

            </div>

           <?php  $class_count++; endwhile; ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
</section>


<!-- end the section -->
<!-- About Invested Advisors Section -->
<section class="invest-hpycustomer">
    <div class="container">
        <h2><?php the_field('adout_adivisor_title');?></h2>
        <div class="hr"></div>
        <div class="hr2"></div>
        <h3><?php the_field('about_advisior_tagline');?></h3>
        <p><?php the_field('about_advisior_content');?></p>
        </div>
    </div>
</section>


<!-- end the section -->

<!--  Happy Customer Area -->
<section class="invest-hpycustomer">
    <div class="container">
        <h2><?php the_field('client_title');?></h2>
        <div class="hr"></div>
        <div class="hr2"></div>
        <h3><?php the_field('client_content');?></h3>

        <div class="happycrousel owl-carousel owl-theme">
        	<?php if( have_rows('happy_carousel') ):$class1_count = 0; ?>
            <?php while ( have_rows('happy_carousel') ) : the_row(); ?>
            <div class="item">
                <label><img  class1="img-<?=$class1_count?>" src="<?php the_sub_field('happy_carousel_image_1'); ?>"></label>
                <label><img class1="img-<?=$class1_count?>" src="<?php the_sub_field('happy_carousel_image_2'); ?>"></label>

            </div>
            <?php $class1_count++;  endwhile; ?>
            <?php endif; ?>
           <!--  <div class="item">
                <label><img src="<?php echo get_template_directory_uri(); ?>/images/californiaarmy.png"></label>
                <label> <img src="<?php echo get_template_directory_uri(); ?>/images/btigroth.png"></label>
                </h4>
            </div>
            <div class="item">
                <label><img src="<?php echo get_template_directory_uri(); ?>/images/culinary.png"></label>
                <label><img src="<?php echo get_template_directory_uri(); ?>/images/capstone.png"></label>
                </h4>
            </div>
            <div class="item">
                <label><img src="<?php echo get_template_directory_uri(); ?>/images/denovoscan.png"></label>
                <label> <img src="<?php echo get_template_directory_uri(); ?>/images/harland.png"></label>
                </h4>
            </div>

            <div class="item">
                <label><img src="<?php echo get_template_directory_uri(); ?>/images/anneseed.png"></label>
                <label><img src="<?php echo get_template_directory_uri(); ?>/images/sandiegocounty.png"></label>
                </h4>
            </div>
            <div class="item">
                <label><img src="<?php echo get_template_directory_uri(); ?>/images/californiaarmy.png"></label>
                <label><img src="<?php echo get_template_directory_uri(); ?>/images/btigroth.png"></label>
                </h4>
            </div> -->

        </div>
    </div>

</section>
<!-- Start our Giving Area -->

<section class="invest-giving">
    <div class="container">
        <div class="row ">
            <div class="col-md-7 ">
                <h2><?php the_field('giving_title');?></h2>
                <div class="hr"></div>
                <div class="hr2"></div>
                <h3><?php the_field('giving_tagline');?></h3>
                <p><?php the_field('giving_content');?></p>
            </div>
            <div class="col-md-5">
                <img src="<?php the_field('giving_image');?>">
            </div>
        </div>
    </div>
</section>

<!-- Start our Donation Area -->

<section class="invest-donation">
    <div class="container">
        <h2><?php the_field('donation_title');?> </h2>
        <div class="hr"></div>
        <div class="hr2"></div>
        <h3><?php the_field('donation_tagline');?></h3>
        <p><?php the_field('donation_content');?></p>
        <a href="<?php the_field('donation_link');?>"><?php the_field('donation_button');?> </a>
        <p><strong><?php the_field('foundation_heading');?> </strong></p>
        <ul class=" donation-text">
        	<?php if( have_rows('foundation_text') ): ?>
            <?php while ( have_rows('foundation_text') ) : the_row(); ?>
            <li class="">
                <h4><?php the_sub_field('foundation_title'); ?></h4>
                <p><?php the_sub_field('foundation_tagline'); ?><p>
            </li>
            <?php  endwhile; ?>
            <?php endif; ?>
        </ul>
        <p><?php the_field('foundation_content');?></p>
        <a href="<?php the_field('foundation_link');?>" class="makeapoin-btn"><?php the_field('foundation_button');?></a>
    </div>
</section>
<!--Start Blogs-->
<!-- <div class="blog-posts">
    <div class="container">
        <div class="row blog-posts-header">
            <div class="col-md-12">
                <h2>Blogs</h2>
                <div class="hr"></div>
               <div class="hr2"></div>
                <h3>Featured News</h3>
                <p>Get the most up to date news about programs and happenings.</p>
            </div>
        </div>

        <div class="row">

            <div class="resource-pod col-sm-4">
                <a href="http://investedadvisors.com/whats-your-game-plan-winning-government-contracts/">
                    <div class="resource-thumbnail-wrapper">
                        <div class="featured-img-blog" style="background-image: url('http://investedadvisors.com/wp-content/uploads/2018/07/Splashes-Restaurant.jpg')">
                            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                        </div>
                    </div>
                </a>

                <div class="resource-pod-content">
                    <p class="post-date">July 2, 2018</p>
                    <h3>What’s Your Hotel’s Game Plan: Winning With Government Contracts</h3>
                    <p></p>
                    <div class="at-above-post-homepage addthis_tool" data-url="http://investedadvisors.com/whats-your-game-plan-winning-government-contracts/"></div>
                    <p>INVESTED TRAVELER OFFERS WEBINAR-BASED SALES AND MARKETING TRAINING FOR HOTELS LOOKING TO ACCELERATE REVENUE WITH WINNING STRATEGIES FOR GOVERNMENT CONTRACTS &nbsp;Smart hotel revenue management practices leverage on-going federal purchasing and advantageous business concerns (San Diego, CA—July 10, 2018) Invested Traveler, a division of Invested Advisors, Inc., a California based B2B marketing and communications consulting firm,…</p>
                    <p><!-- <a class="excerpt-read-more btn btn-primary" href="http://investedadvisors.com/whats-your-game-plan-winning-government-contracts/" title="ReadWhat’s Your Hotel’s Game Plan: Winning With Government Contracts">Read More</a> -->
                        <!-- AddThis Advanced Settings above via filter on get_the_excerpt -->
                        <!-- AddThis Advanced Settings below via filter on get_the_excerpt -->
                        <!-- AddThis Advanced Settings generic via filter on get_the_excerpt -->
                        <!-- AddThis Share Buttons above via filter on get_the_excerpt -->
                        <!-- AddThis Share Buttons below via filter on get_the_excerpt -->
                  <!--   </p>
                    <div class="at-below-post-homepage addthis_tool" data-url="http://investedadvisors.com/whats-your-game-plan-winning-government-contracts/"></div>
                    <a class="world_model" href="#">Read More</a>
                </div>
            </div>

            <div class="resource-pod col-sm-4">
                <a href="http://investedadvisors.com/invested-advisors-facilitates-business-seminar-for-u-s-dot/">
                    <div class="resource-thumbnail-wrapper">
                        <div class="featured-img-blog" style="background-image: url('http://investedadvisors.com/wp-content/uploads/2018/04/iStock-671472916.jpg')">
                            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                        </div>
                    </div>
                </a>

                <div class="resource-pod-content">
                    <p class="post-date">April 21, 2018</p>
                    <h3>Invested Advisors Leads U.S. DOT and Sacramento Chamber Let’s Grow Together Seminar</h3>
                    <p></p>
                    <div class="at-above-post-homepage addthis_tool" data-url="http://investedadvisors.com/invested-advisors-facilitates-business-seminar-for-u-s-dot/"></div>
                    <p>Invested Advisors was invited by the U.S. DOT Small Business Transportation Resource Center and Sacramento Asian Pacific Chamber of Commerce to participate in its&nbsp;eight-week Business Development program, designed to educate business owners on critical aspects&nbsp;germane to&nbsp;building and growing a business. Isabella Argueta, Program Coordinator for the Sacramento Asian Pacific Chamber of Commerce said “the Chamber…</p>
                    <p> --><!-- <a class="excerpt-read-more btn btn-primary" href="http://investedadvisors.com/invested-advisors-facilitates-business-seminar-for-u-s-dot/" title="ReadInvested Advisors Leads U.S. DOT and Sacramento Chamber Let’s Grow Together Seminar">Read More</a> -->
                        <!-- AddThis Advanced Settings above via filter on get_the_excerpt -->
                        <!-- AddThis Advanced Settings below via filter on get_the_excerpt -->
                        <!-- AddThis Advanced Settings generic via filter on get_the_excerpt -->
                        <!-- AddThis Share Buttons above via filter on get_the_excerpt -->
             <!-- <a class="excerpt-read-more btn btn-primary" href="http://investedadvisors.com/invested-advisors-ceo-invited-to-roskilde-university-as-guest-lecturer-for-business-schools-entrepreneurs-program/" title="ReadInvested Advisors CEO Guest Lectures at Roskilde University’s Entrepreneurs Program">Read More</a> -->
                        <!-- AddThis Advanced Settings above via filter on get_the_excerpt -->
                        <!-- AddThis Advanced Settings below via filter on get_the_excerpt -->
                        <!-- AddThis Advanced Settings generic via filter on get_the_excerpt -->
                        <!-- AddThis Share Buttons above via filter on get_the_excerpt -->
                        <!-- AddThis Share Buttons below via filter on get_the_excerpt -->
             
<!--End the Section-->
<?php get_footer('pages'); ?>