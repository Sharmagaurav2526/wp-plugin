<?php
/**
 * Template Name:The How Page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */ 
get_header(); ?>

<section class="industry-headertop" style="background-image: url(<?php the_field('industry_banner_image');?>);">
    <div class="container">
        <h2><?php the_field('industry_experties_title');?></h2>
        <p><?php the_field('industries_experties_content');?> </p>
    </div>
</section>

<!-- Industry-service -->
<section class="industry-service">
    <div class="container">
          <!-- Start About Area -->
        <h2><?php the_field('about_text'); ?></h2>
        <div class="hr"></div>
        <div class="hr2"></div>
        <h3><?php the_field('about_tagline');?></h3>
        <p><?php the_field('about_context'); ?></p>
                    
            <!-- End About Area -->
        <h2><?php the_field('services_title');?></h2>
        <div class="hr"></div>
        <div class="hr2"></div>
        <h3><?php the_field('services_tagline');?></h3>
        <p><?php the_field('services_content');?></p>
        <strong><?php the_field('services_excerpt');?></strong>
        <h3><?php the_field('we_can_help_title');?></h3>
        <div class="row">
        	<?php if( have_rows('we_can_help') ): ?>
            <?php while ( have_rows('we_can_help') ) : the_row(); ?>
            <div class="serve col-sm-12 col-md-4 col-lg-3">
                <div class="indservice-inner">
                    <h4><?php the_sub_field('indservice_title'); ?></h4>
                    <img src="<?php the_sub_field('indservice_image'); ?>">
                    <ul>
                        <li><?php the_sub_field('indservice_content'); ?></li>
                    </ul>
                </div>
            </div>
            <?php  endwhile; ?>
            <?php endif; ?>
            <!-- <div class="serve col-sm-12 col-md-4 col-lg-3">
                <div class="indservice-inner">
                    <h4>Promote</h4>
                    <img src="images/serviceimg2.png">
                    <ul>
                        <li>Collateral materials</li>
                        <li>Video production</li>
                        <li>Executive communications</li>
                        <li>Social media to engage and connect</li>
                    </ul>
                </div>
            </div>
            <div class="serve col-sm-12 col-md-4 col-lg-3">
                <div class="indservice-inner">
                    <h4>THRIVE</h4>
                    <img src="images/serviceimg3.png">
                    <ul>
                        <li>Culture and mission alignment</li>
                        <li>Surveys and feedback </li>
                        <li>Press coverage that helps tell your story</li>
                        <li>Inspire great things and winning outcomes</li>
                    </ul>

                </div>
            </div>
            <div class="serve col-sm-12 col-md-4 col-lg-3">
                <div class="indservice-inner">
                    <h4>Save</h4>
                    <img src="images/serviceimg4.png">
                    <ul>
                        <li>The ultimate guide for online booking tours & travel activities. Save time & money and get the best of your holidays now!</li>

                    </ul>

                </div>
            </div> -->
        </div>

    </div>
</section>
<!-- Start Testimonials  -->
                
                <section class="testimonials">
                <div class="container">
                <h2>Testinomials </h2>
                    <div class="hr"></div>
                    <div class="hr2"></div>
                    <h3></h3>
                    <ul class=" testimonials-text">                
                    <li class="">
                        <p>This was my first client appreciation event I have attended. I was told before going that I would be impressed, but that was a huge understatement.  What an event!  I just had to tell you that the flower wall was absolutely brilliant.  My few clients who attended would not stop raving about it.  One of the clients I invited is a wholesale flower company and he was very impressed with the quality of flowers you had available for our clients.
                        <span>What a fantastic job!! –</span></p>
                    
                        </li>   
                                    <li class="">
                    
                    <p>I spoke with a number of employees enjoying the holiday celebration and you seemed to have earned the respect and support from all.  I must say that that kind of feedback speaks very highly to the reputation of your organization. You have certainly set an early “high bar” for all holiday parties to come.  – David, California<p>            
                        </li>   
                    
                    <li class="">
                    <p>The event was absolutely beautiful last night!  My colleague and I had the pleasure of attending and I’m remiss that I didn’t have the chance to introduce myself to you.  The area that your team utilized in the lobby was clean and everything seemed to run smoothly.  Thank you for your attention to detail. Again, the event was great; we especially loved the flower wall. Amber, San Diego<p>
                
                        </li>                           
                </ul>
                
                
                </div>
                </section>
<!-- End Testinomials -->

<!--  Happy People gallery Area -->
<section class="invest-hpypeople">
    <div class="container">
        <h2><?php the_field('gallery_title');?></h2>
        <div class="hr"></div>
        <div class="hr2"></div>
        <h3><?php the_field('gallery_tagline');?></h3>

    </div>
    <div class=" happypeople owl-carousel owl-theme">
    	<?php if( have_rows('gallery_repeater') ): ?>
            <?php while ( have_rows('gallery_repeater') ) : the_row(); ?>
        <div class="item">
            <img src="<?php the_sub_field('gallery_images'); ?>">

        </div>
        <?php  endwhile; ?>
            <?php endif; ?>
        <!-- <div class="item">
            <img src="images/happypeople2.jpg">

            </h4>
        </div>
        <div class="item">
            <img src="images/happypeople3.jpg">

            </h4>
        </div>
        <div class="item">
            <img src="images/happypeople4.jpg">

            </h4>
        </div>

        <div class="item">
            <img src="images/happypeople5.jpg">

            </h4>
        </div>
        <div class="item">
            <img src="images/happypeople2.jpg">

            </h4>
        </div>
         <div class="item">
            <img src="images/happypeople3.jpg">

            </h4>
        </div>
        <div class="item">
            <img src="images/happypeople4.jpg">

            </h4>
        </div>

        <div class="item">
            <img src="images/happypeople5.jpg">

            </h4>
        </div>
        <div class="item">
            <img src="images/happypeople2.jpg">

            </h4>
        </div> -->

    </div>

</section>
<!-- Start Request rfp-->

<!-- <section class="request-rfp" style="background-image: url(<?php //the_field('requset_backgroung_image');?>);">
    <div class="container">
        <h2><?php //the_field('request_title');?></h2>
        <a href="<?php //the_field('request_link');?>"><?php //the_field('request_button');?></a>

    </div>
</section> -->

<!-- Start request services Area -->

<section class="request-services">
    <div class="container">
        <h2><?php the_field('services_list_title');?></h2>
        <div class="hr"></div>
        <div class="hr2"></div>
        <h3><?php the_field('services_list_tagline');?></h3>

        <ul class=" rqservices-text">
        	<?php if( have_rows('services_list_repeater') ): ?>
            <?php while ( have_rows('services_list_repeater') ) : the_row(); ?>
            <li class="">
                <?php the_sub_field('services_list_content'); ?>
            </li>
            <?php  endwhile; ?>
            <?php endif; ?>
            <!-- <li class="">
                <ul>
                    <li>Event marketing</li>
                    <li>Event production</li>
                    <li>Food/beverage management</li>
                    <li>Entertainment/leisure activity coordination</li>
                    <li>Hotel and rooming list management</li>
                </ul>
            </li>
            <li class="">
                <ul>
                    <li>Event marketing</li>
                    <li>Event production</li>
                    <li>Food/beverage management</li>
                    <li>Entertainment/leisure activity coordination</li>
                    <li>Hotel and rooming list management</li>
                </ul>
            </li>
            <li class="">
                <ul>
                    <li>Event marketing</li>
                    <li>Event production</li>
                    <li>Food/beverage management</li>
                    <li>Entertainment/leisure activity coordination</li>
                    <li>Hotel and rooming list management</li>
                </ul>
            </li> -->

        </ul>
        <!-- <p><?php //the_field('services_list_excerpt');?></p>
        <a href="<?php //the_field('services_list_link');?>" class="makeapoin-btn"><?php the_field('services_list_button');?></a> -->
    </div>
</section>

<!-- End investedtraveler Area -->

<?php get_footer('pages'); ?>
