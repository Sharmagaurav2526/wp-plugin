<?php
/**
 * Template Name: Agency Services Page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */ 
get_header(); ?>

	
			
			<section class="industry-headertop"  style="background-image: url('<?php the_field('agency_banner_image')?>;">
			<div class="container">
			<h2><?php the_field('agency_banner_title')?></h2>
			<p><?php the_field('agency_banner_content')?> </p>			
			</div>
			</section>
			
			
	<section class="agenservice">
	    <div class="container">
	        <div class="row">
	            <div class="">
	                <h2><?php the_field('agency_section_title')?></h2>
	                <p class="p2"><?php the_field('agency_section_tagline')?> </p>
	                <!-- tabs -->
	                <div class="tabbabled tabs-left">
                          
	                    <ul class="col-md-4 nav nav-tabs">
	                    <?php
	                    	$activetab = $_GET['active'];
	                    ?>	
	                    <?php if( have_rows('services_section_repeater') ): $i = 0; ?>
                          <?php while ( have_rows('services_section_repeater') ) : the_row(); ?>

	                        <li><a class="<?php if($i == $activetab){ echo 'active'; } ?>" href="#services-tabs-<?php echo $i; ?>" data-toggle="tab"><img src="<?php the_sub_field('icon_image'); ?>"> <span><?php the_sub_field('tab_title'); ?> </span></a></li>

	                        <?php $i++; endwhile; ?>
                        <?php endif; ?>

	                        <!-- <li><a href="#dmddd2" data-toggle="tab"><img src="<?php echo get_template_directory_uri(); ?>/images/icon2.png"> <span>Schedule an Appointment</span></a></li>
	                        <li><a href="#dmddd3" data-toggle="tab"><img src="<?php echo get_template_directory_uri(); ?>/images/icon3.png"> <span>3rd Party Meeting Placement Services</span></a></li>
	                        <li><a href="#dmddd4" data-toggle="tab"><img src="<?php echo get_template_directory_uri(); ?>/images/icon4.png"> <span>Affinity and Group Travel</span></a></li>
	                        <li><a href="#dmddd5" data-toggle="tab"><img src="<?php echo get_template_directory_uri(); ?>/images/icon5.png"> <span>Hotel Services</span></a></li>
	                        <li><a href="#dmddd6" data-toggle="tab"><img src="<?php echo get_template_directory_uri(); ?>/images/icon6.png"> <span>You're Going Place Products and Wearables</span></a></li> -->
	                    
	                    </ul>

	                    <div class="col-md-8 tab-content">

	                    <?php if( have_rows('services_section_repeater') ): $j = 0; ?>
                          	<?php while ( have_rows('services_section_repeater') ) : the_row(); ?>

	                        <div class="tab-pane <?php if($j == $activetab){ echo 'active'; } ?>" id="services-tabs-<?=$j?>">
	                            <div class="">
	                                <h3>Agency Services</h3>
	                                <div class="hr"></div>
	                                <div class="hr2"></div>
	                                <h4><?php the_sub_field('service_text'); ?></h4>
	                               <p><?php the_sub_field('services_content'); ?></p>
	                                <!-- <h4>Journey Model of Workforce Engagement</h4>
	                                <p>At Invested Traveler, we help you straddle both sides of workforce engagement — recruitment and retention of top talent that translates into increased productivity, collaboration and better bottom line results.
	                                    <br>
	                                    <br> Research shows that workplace culture and engagement are key to the process but are also nebulous concepts that everyone is talking about but few have figured out how to deliver upon.
	                                    <br>
	                                    <br> At Invested Traveler, we offer a disciplined approach to help you reduce the time and cost of recruitment and provide solutions that make top talent want to work for your company and play a role in its long term success. Our customized meetings and incentives are designed to strengthen and enhance leadership capabilities, benefitting both the employee and company. We’ve coined this powerful solution the
	                                    <br>
	                                    <br><strong>Journey Model of Workforce Engagement. </strong>
	                                </p>
	                                <ul class="mddagnserv">
	                                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/agsserimg1.png"> Recruitment Fairs</li>
	                                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/agsserimg2.png"> Corporate Retreats</li>
	                                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/agsserimg3.png"> Sales Rallies</li>
	                                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/agsserimg4.png"> Incentive travel</li>
	                                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/agsserimg5.png"> Workshop Facilitation</li>

	                                </ul> -->


                  					<ul class="mddagnserv">
                  						
                  						<?php if( have_rows('services_list') ): $i = 0; ?>
                          					<?php while ( have_rows('services_list') ) : the_row(); ?>

	                                    <li><img src="<?php the_sub_field('icon'); ?>"> <?php the_sub_field('service_name'); ?></li>
	                                    
	                                     	<?php endwhile; ?>

                          				<?php endif; ?>
                                    </ul>
                                  <p><?php the_sub_field('services_tagline');?></p>
                                   <!-- <a href="<?php the_sub_field('service_link'); ?>"><button type="button" class="btn btn-info"><?php the_sub_field('service_button'); ?></button></a>  -->
	                            </div>
	                        </div>

	                        <?php $j++; endwhile; ?>
                          <?php endif; ?>

	                        <!-- <div class="tab-pane" id="dmddd2">
	                            <div class="">
	                                <h1>About Tab</h1>
	                                <p>because they are the most important for an encyclopedia to have, as determined by the community of participating editors. They may also be of interest to readers as an alternative to lists of overview articles.</p>
	                            </div>
	                        </div>

	                        <div class="tab-pane" id="dmddd3">
	                            <div class="">
	                                <h1>Services Tab</h1>
	                                <p>meant to identify articles which deserve editor attention because they are the most important for an encyclopedia to have, as determined by the community of participating editors. They may also be of interest to readers as an alternative to lists of overview articles.</p>
	                            </div>
	                        </div>

	                        <div class="tab-pane" id="dmddd4">
	                            <div class="">
	                                <h1>Contact Tab</h1>
	                                <p>deserve editor attention because they are the most important for an encyclopedia to have, as determined by the community of participating editors. They may also be of interest to readers as an alternative to lists of overview articles.</p>
	                            </div>
	                        </div>
	                        <div class="tab-pane" id="dmddd5">
	                            <div class="">
	                                <h1>Services Tab</h1>
	                                <p>meant to identify articles which deserve editor attention because they are the most important for an encyclopedia to have, as determined by the community of participating editors. They may also be of interest to readers as an alternative to lists of overview articles.</p>
	                            </div>
	                        </div>

	                        <div class="tab-pane" id="dmddd6">
	                            <div class="">
	                                <h1>Contact Tab</h1>
	                                <p>deserve editor attention because they are the most important for an encyclopedia to have, as determined by the community of participating editors. They may also be of interest to readers as an alternative to lists of overview articles.</p>
	                            </div>
	                        </div> -->
	                    </div>
	                </div>
	                <!-- /tabs -->
	            </div>
	        </div>
	    </div>
	</section>



			<!-- End investedtraveler Area -->
<?php get_footer('pages'); ?>