	<!DOCTYPE html>
	<html lang="en">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Site Title -->
		<title>Invested Traveler</title>
        
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Exo:300,400,600,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet"> 
		
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		
		<!-- Custom CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/investedtraveler.css">
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
        <?php wp_head(); ?>
		</head>
		
		
		<body>


			<!-- Start Header Area -->
			<header>
				<div class="container">						
					<nav class="navbar navbar-expand-lg navbar-light">
					  <a class="navbar-brand" href="<?php the_field('header_logo_link' , 'option'); ?>"><img src="<?php the_field('header_logo' , 'option'); ?>" alt="logo image"></a>
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>

					 <div class="Number"><?php the_field('telephone_number' , 'option'); ?></div>	

					  <div class="collapse navbar-collapse justify-content-end" id="navbarText">  
						
						<?php 
						
						$args = array( 
						  	'theme_location' => 'main-menu',       
						   	'menu' => 'Home',
						   	'menu_class'     => 'navbar-nav',
						);
						wp_nav_menu( $args ); 
						?>

						<!-- <ul class="navbar-nav">	
						  <li class="nav-item active">
							<a class="nav-link" href="who-we-serve.html">Who We Serve <span class="sr-only">(current)</span></a>
						  </li>
						  <li >
							<a class="nav-link" href="#">Agency Services</a>
						  </li>
						  <li class="nav-item">
							<a class="nav-link" href="#">Industry Expertise </a>
						  </li>
						  <li >
							<a class="nav-link" href="#">Request RFP</a>
						  </li>
						  <li class="nav-item">
							<a class="nav-link" href="#">A World of Influlence</a>
						  </li>		
						</ul> -->

					  </div>
					</nav>
				</div>
			</header>
			<!-- End Header Area -->