<section class=" investedtraveler_area">
    <div class="container-fluid p-0">
        <div class="row no-gutters">

            <div class="col-lg-12">
                <div class="mt-0 investedtraveler_title text-center">@investedtraveler</div>
            </div>
            <?php echo do_shortcode('[instagram-feed]'); ?>
          </div>
    </div>
</section>
<!-- Strat Footer Area -->

      <footer class="footer_area">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-lg-4">
              <img src="<?php the_field('footer_logo' , 'option'); ?>" alt="footer logo">
              <p><?php the_field('footer_content' , 'option'); ?></p>
              <div class="footer-social">
                <?php if( have_rows('footer_soical_icon', 'option') ): 
                  while( have_rows('footer_soical_icon', 'option') ): the_row(); ?>
                <a href="<?php the_sub_field('soical_link' , 'option'); ?>"><i class="<?php the_sub_field('soical_icon' , 'option'); ?>"></i></a>
               <!--  <a href="#"><i class="fab fa-google-plus-g"></i></a>
                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a> -->
                 <?php endwhile; 
                        endif; ?>
              </div>                          
            </div>
            <div class="col-md-3 col-lg-3">
              <h3>Quick Links</h3>
              <?php dynamic_sidebar('footer-1'); ?>
              <!-- <ul>
                <li><a href="">Who We Serve</a></li>
                <li><a href="">Agency Services</a></li>
                <li><a href="">Industry Expertise</a></li>
                <li><a href="">Request RFP</a></li>
                <li><a href="">A World of Influlence</a></li>
              </ul> -->
            </div>
            <div class="col-md-5 col-lg-5">
              <h3><?php the_field('testinomial_title' , 'option'); ?></h3>
              <p><?php the_field('testinomial_content' , 'option'); ?></p>
              <h4><?php the_field('author_name' , 'option'); ?></h4>
            </div>
            
            <div class="col-lg-12">             
              <div class="copyright text-center">
                <p><?php the_field('copyright_content' , 'option'); ?></p>
              </div>            
            </div>
          </div>
        </div>
      </footer>

      <!-- End Footer Area -->
<?php wp_footer(); ?>
      
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
  <script defer src="https://use.fontawesome.com/releases/v5.2.0/js/all.js" integrity="sha384-4oV5EgaV02iISL2ban6c/RmotsABqE4yZxZLcYMAdG7FAPsyHYAPpywE9PJo+Khy" crossorigin="anonymous"></script> 
  <script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>  
      
    <script>
            jQuery(document).ready(function() {
              jQuery('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 2,
                    nav: true
                  },
                  600: {
                    items: 3,
                    nav: true
                  },
                  1000: {
                    items: 4,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
              })
            })
          </script>  
	</body>
</html>

