<?php
//Adding shortcode [my_inc_shortcode]
add_shortcode("my_inc_shortcode", "my_shortcode_function_inc");

function my_shortcode_function_inc() {
	return "<h1>Hello Shortcodes in INC</h1>";
}