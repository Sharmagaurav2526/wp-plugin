<?php
/* Plugin Name: Mobilyte Plugin
Plugin URI: https://www.mobilyte.com
Description: Mobilyte Plugin for testing
Author: Mobilyte
Version: 1.0
Author URI: https://www.mobilyte.com */

//Include file in plugin
define( 'MY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

include( MY_PLUGIN_PATH . 'inc/mobilyte-functions.php');

//is_admin functionality
if ( is_admin() ) {
    //include_once( plugin_dir_path( __FILE__ ) . 'includes/admin-functions.php' );
} else {
    //include_once( plugin_dir_path( __FILE__ ) . 'includes/front-end-functions.php' );
}

//Add script and css in plugin 
function add_files() {
    wp_register_style('add_files', plugins_url('css/mobilyte-css.css',__FILE__ ));
    wp_enqueue_style('add_files');
    wp_register_script( 'add_files', plugins_url('js/mobilyte-jquery.js',__FILE__ ));
    wp_enqueue_script('add_files');
}

add_action( 'admin_init','add_files');

function fwds_slider_activation() {
	//die('activation hook');
}
    add_action('admin_menu', 'test_plugin_setup_menu');
	function test_plugin_setup_menu(){
	        add_menu_page( 'Test Plugin Page', 'Mobilyte Settings', 'manage_options', 'test-plugin', 'test_init' );
	        
	}
	 
	function test_init() { 
		echo "<h1>Hello Mobilytes</h1>";     
	 }

register_activation_hook(__FILE__, 'fwds_slider_activation');

//Plugin deactivation
function fwds_slider_deactivation() {
	//die('de activation hook');
}

register_deactivation_hook(__FILE__, 'fwds_slider_deactivation');

//Adding shortcode [my_shortcode]
add_shortcode("my_shortcode", "my_shortcode_function");

function my_shortcode_function() {
	return "<h1>Hello Shortcodes</h1>"; 
}

//Add advanced shortcode [mobilyte_widgets name="Nimesh" age="27"]
?>