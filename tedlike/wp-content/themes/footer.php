 <!-- Footer -->
    <footer class="footer-bs">
        <div class="container">
        
        <div class="row">
          <div class="footer-flex">
            <div class="footer-brand animated fadeInLeft">
              <img src="http://tedlike.mobilytedev.com/wp-content/uploads/2018/06/DAL-Logo-white.png" class="img-fluid" alt="LOGO"> 
              <?php //dynamic_sidebar('sidebar-2'); ?>              
              <ul id="menu-footer-nav" class="menu pages"><li id="menu-item-364" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-364"><a href="http://tedlike.mobilytedev.com/about-us/">About</a></li>
              <li id="menu-item-367" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-367"><a href="">Privacy Policy</a></li>
              <li id="menu-item-368" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-206 current_page_item menu-item-368"><a href="https://smartassistant.com" target="_blank">SMARTASSISTANT</a></li>
              <li id="menu-item-370" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-370"><a href="https://www.guided-selling.org" target="_blank">Guided Selling.org</a></li>
              <li id="menu-item-371" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-371"><a href="http://tedlike.mobilytedev.com/contact/">Contact</a></li>
              <!-- <li id="menu-item-1401" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1401"><a href="">Volunteer</a></li> -->
              </ul> 
              <?php //dynamic_sidebar('sidebar-3'); ?>              
            </div>
            
            <div class="footer-ns animated fadeInRight">
              <h4><?php the_field('newsletter_title' , 'option'); ?></h4>
              <p><?php the_field('newsletter_description' , 'option'); ?></p>
              <p><?php the_field('privacy' , 'option'); ?></p>
              <?php echo do_shortcode('[newsletter_form class="input-group"][newsletter_field name="email" class="form-control" placeholder="Enter your email"][/newsletter_form]'); ?>
            </div>
          </div>
        </div>
        
            <div class="row"> 
            
            <ul class="social-icons">
    <!-- <li>
    <i class="fab fa-facebook-f"></i>
    </li>
    

    <li>
    <i class="fab fa-youtube"></i>
    </li> 

    <li>
    <i class="fab fa-twitter"></i>
    </li>
    
    <li>
<i class="fab fa-instagram"></i>
    </li> -->
     <?php
    if( have_rows('social_icons' , 'option' ) ):
    while ( have_rows('social_icons' , 'option' ) ) : the_row(); ?>  
     <li><a href="<?php the_sub_field('social_url'); ?>" target="_blank"><i class="<?php the_sub_field('social_class'); ?>"
></i></a></li> 
  <?php 
 endwhile;
 else :
 endif;
   ?>  
    </ul>  
            </div>
        
        <div class="row"> 
        <div class="copyright"> 
    <div class="footer-last">
        <div class="footer-copyright"><?php the_field('copyright' , 'option'); ?></div>
        <div class="footer-twg float-right"><?php the_field('designed_by' , 'option'); ?> <a href="http://www.twg.io" target="_blank" rel="noopener"><img src="http://tedxtoronto.com/wp-content/themes/twg/images/twg.svg" alt="TWG" class="footer-twgLogo"></a></div>
      </div>
        
        </div>
        </div>
        
        </div>
    </footer>
    
  



    
<script src="//code.jquery.com/jquery-1.12.1.min.js"></script>

<?php wp_footer(); ?>

    <!-- Bootstrap core JavaScript -->
    <!-- <script src="<?php echo get_template_directory_uri(); ?>/vendor/jquery/jquery.min.js"></script> -->
    <script src="<?php echo get_template_directory_uri(); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/lightbox-plus-jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/mycustom.js"></script>
<script>
  jQuery(function() {
    jQuery(".video").click(function () {
      var theModal = jQuery(this).data("target"),
      videoSRC = jQuery(this).attr("data-video"),
      videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
      jQuery(theModal + ' iframe').attr('src', videoSRCauto);
      jQuery(theModal + ' button.close').click(function () {
        jQuery(theModal + ' iframe').attr('src', videoSRC);
      });
    });
  });
  </script>

<script type="text/javascript">
                var my_repeater_field_post_id = <?php echo $post->ID; ?>;
                console.log(my_repeater_field_post_id);
                //var my_repeater_field_offset = <?php //echo $number; ?>;
                console.log(my_repeater_field_offset);
                var my_repeater_field_nonce = '<?php echo wp_create_nonce('my_repeater_field_nonce'); ?>';
                console.log(my_repeater_field_nonce);
                var my_repeater_ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
                console.log(my_repeater_ajax_url);
                var my_repeater_more = true;

                function my_repeater_show_more() {
                      alert('hello');
                    // make ajax request
                    $.post(
                        my_repeater_ajax_url, {
                            // this is the AJAX action we set up in PHP
                            'action': 'my_repeater_show_more',
                            'post_id': my_repeater_field_post_id,
                            'offset': my_repeater_field_offset,
                            'nonce': my_repeater_field_nonce
                        },
                        function (json) {
                            // add content to container
                            // this ID must match the containter 
                            // you want to append content to
                            $('#my-repeater-list-id').append(json['content']);
                            // update offset
                            my_repeater_field_offset = json['offset'];
                            // see if there is more, if not then hide the more link
                            if (!json['more']) {
                                // this ID must match the id of the show more link
                                $('#my-repeater-show-more-link').css('display', 'none');
                            }
                            console.log(json);
                        },
                        'json'
                    );
                }

                //console.log(<?php //echo $total;?>);

            </script>

    <script type="text/javascript">
       ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
      jQuery(document).ready(function () {                
        jQuery('select.term_id').change(function () {
            
           var term_id =  jQuery( this ).val();
           let divRef = jQuery( this ).data( 'divref' );
           let year = jQuery( this ).data( 'year' );
           //alert(term_id);
            $.ajax({
                type: "POST",
                url: ajax_url,
                data: ({
                    action: 'generate',
                    id: term_id,
                    talk_year_tax: year
                }),
                success: function (msg) {
                 /* jQuery('#div1').html('');
                  alert(jQuery(msg).find('.cards-item').length);
            if (jQuery(msg).find('.cards-item').length > 0) {
            jQuery(msg).find(".cards-item").each(function() {
              
              //jQuery('#div1').append(jQuery(this));
             jQuery('#div1').append(jQuery(this)).find('.cards-item').animate({left: '0px', opacity: '1.0'}, "slow");
            });
                }*/
                    //console.log(divRef);
                    jQuery(divRef).html(msg).find('.cards-item,.col-md-3').each(function(){
                      
                      jQuery(this).animate({opacity: '1'}, "slow").nextAll().delay( 500 ).fadeIn( 400 );
                    })

                    
                    //setTimeout(function(){ jQuery( divRef ).html(msg).animate({left: '250px'}); }, 1000);
                    //setTimeout(function(){ jQuery( divRef ).html(msg)/*.animate({width: boxWidth })*/; }, 1500);
                    //setTimeout(function(){ jQuery( divRef ).html(msg)/*.animate({width: boxWidth })*/; }, 1500);
                    //setTimeout(function(){ jQuery( divRef ).html(msg).animate({width: boxWidth }); }, 1500);

                    //jQuery( divRef ).html(msg);
                    /*setTimeout(function(){ 
                $('#dvData').fadeOut();}, 2000);*/
          // $('.cards-item').on( 'click', function( e ){ 
          // jQuery(divRef).html(msg).find('cards-item').animate({left: '250px', opacity: '0.8'}, "slow");
          //   var t = $( this ).data( 'video-id' );
          //   var n = document.createElement("iframe");
          //     n.setAttribute("frameborder", "0"), n.setAttribute("allowfullscreen", ""), n.setAttribute("src", "https://www.youtube.com/embed/" + t + "?rel=0&showinfo=0&autoplay=1");
          //     $( this ).append( n );
          // });
          // jQuery(".cards-item").on('click',function(){
     //        let video_id = jQuery( this ).data( 'video-id' );
          //  jQuery("#"+video_id).addClass("cards-item expand--open");
          //  //jQuery(".cards-item").addClass("expand--open");
          // });
          jQuery(".card-close").on("click",function(){
            alert('inside function');
           jQuery(".cards-item").removeClass("cards-item expand--open");
           });
          
          
                }
            });
        });
        jQuery('select.term_id').trigger( 'change' );
});
    </script>
<!-- Script to add active class to selected year start here -->
<script type="text/javascript">
  jQuery(document).ready(function(event){
  jQuery('.my-active-class').click(function(event){
    jQuery('.my-active-class').removeClass("active");
    jQuery(this).addClass("active");
});
});
</script>
<!-- Script to add active class to selected year end here -->
</body>
</html>