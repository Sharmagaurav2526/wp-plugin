$(document).ready( function(){
        function i(e) {
            return void 0 === e.data("expand") ? e.closest("[data-expand]") : e
        }
function o(e, t) {
            var vurl = '';
            var urltype = $( e ).data( 'videourl' );
            if( urltype == 'youtube' ){
                vurl = "https://www.youtube.com/embed/" + t + "?rel=0&showinfo=0&autoplay=1";
            } else if( urltype == 'vimeo' ){
                vurl = "//player.vimeo.com/video/"+t+"?title=0&amp;byline=0&amp;portrait=0&amp;color=990033";
            } else if( urltype == 'slider' ){
                vurl = "//www.slideshare.net/slideshow/embed_code/key/"+t;
                //console.log( 'pending' );
            }
            var n = document.createElement("iframe");
            n.setAttribute("frameborder", "0"), n.setAttribute("allowfullscreen", ""), 
            n.setAttribute("src", vurl), e.append(n)
        }
        window.expander = {}, window.expander.speed = .7, window.expander.willOpen = window.expander.willOpen || function(e, t) {
            t()
        }, window.expander.didOpen = window.expander.didOpen || function(e) {}, window.expander.willClose = window.expander.willClose || function(e, t, n) {
            n()
        }, window.expander.didClose = window.expander.didClose || function(e, t, n) {
            n()
        },$("body").on("click", "[data-expand-close]", function() {
            var e, t, n = i($(this)),
                o = n.prev();
            n.hasClass("expand--open") && (e = n, t = o, window.expander.willClose(e, t, function() {
                e.css({
                    left: e.data("left"),
                    top: e.data("top"),
                    width: e.data("width"),
                    height: e.data("height"),
                    margin: e.data("margin")
                }), setTimeout(function() {
                    window.expander.didClose(e, t, function() {
                        e.removeClass("expand--open"), $("body").removeClass("body--lock"), e.remove()
                    })
                }, 1e3 * window.expander.speed)
            }))
        }), 
 $("body").on("click", "[data-expand-open]", function() {            
            var e = $(this);
            let vurltype = $(this).data( 'videourl' );
            if( vurltype &&  vurltype == 'slider' ){
                vurl = "//www.slideshare.net/slideshow/embed_code/key/"+$(this).data( 'video-id' );
                window.open( vurl, '_target' );
                return false;
            } else if( vurltype && vurltype == 'others' ){
                vurl = $(this).data( 'video-id' );
                window.open( vurl, '_target' );
                return false;
            }
            e.hasClass("expand--open") || function(e) {
                var t = e.outerWidth(),
                    n = e.outerHeight(),
                    o = e.offset().top - $(window).scrollTop(),
                    i = e.offset().left,
                    a = e.css("margin"),
                    r = parseInt(e.css("margin-top")),
                    s = parseInt(e.css("margin-left"));
                "height: " + e.outerHeight(!0) + "px;", "width: " + e.outerWidth(!0) + "px;";
                var c = $(".navsWrapper").outerHeight(),
                    d = e.clone();
                d.addClass("expand--open"), $("body").addClass("body--lock"), d.css({
                    top: o - r,
                    left: i - s,
                    width: t,
                    height: n
                }), d.data("top", o - r), d.data("left", i - s), d.data("width", t), d.data("height", n), d.data("margin", a), e.after(d), 
                window.expander.willOpen(d, function() {
                    setTimeout(function() {
                        d.css({
                            top: c + "px",
                            left: 0,
                            margin: 0,
                            width: "100vw",
                            height: "calc(100vh - " + c + "px)"
                        })
                    }, 100), setTimeout(function() {
                        window.expander.didOpen(d)
                    }, 1e3 * window.expander.speed)
                })
            }(e)
        }),
        window.expander.willOpen = function(e, t) {            
            (e = $(e)).hasClass("cards-item") ? function(e, t) {
                768 < $(window).width() && e.find(".card-image").css({
                    transform: "scale(1.05)",
                    transition: "none"
                });
                (new TimelineMax).to(e.find(".card-leftCorner"), .2, {
                    top: -35,
                    ease: Power2.easeOut
                }).to(e.find(".card-rightCorner"), .2, {
                    top: -40,
                    ease: Power2.easeOut
                }, "-=0.2").to(e.find(".card-fade"), .5, {
                    top: "-100%",
                    ease: Power2.easeOut
                }).to(e.find(".card-content"), .5, {
                    bottom: "100%",
                    ease: Power2.easeOut
                }, "-=0.5");
                setTimeout(t, 400)
            }(e, t) : e.hasClass("team-bioBox") ? function(e, t) {
                (new TimelineMax).to(e.find(".team-bioLeft"), .5, {
                    opacity: 0,
                    ease: Power2.easeOut
                }).to(e.find(".team-bioRight"), .5, {
                    opacity: 0,
                    ease: Power2.easeOut
                }, "-=0.5").to(e.find(".team-bioClose"), .5, {
                    opacity: 0,
                    ease: Power2.easeOut
                }, "-=0.5");
                setTimeout(t, 400)
            }(e, t) : t()
        }, window.expander.didOpen = function(e) {
            var t, n;
            if ((e = $(e)).hasClass("cards-item")) o(e.find(".card-video"), e.data("video-id")), n = e, (new TimelineMax).to(n.find(".card-video"), .3, {
                opacity: 1,
                ease: Power2.easeOut
            }).to(n.find(".card-close"), .3, {
                opacity: 1,
                ease: Power2.easeOut
            });
            else if (e.hasClass("team-bioBox")) {
                o(e.find(".team-bioVideo"), e.find(".team-bioPlay").data("video-id")), t = e, (new TimelineMax).to(t.find(".team-bioVideo"), .3, {
                    opacity: 1,
                    ease: Power2.easeOut
                }).to(t.find(".team-bioVideoClose"), .3, {
                    opacity: 1,
                    ease: Power2.easeOut
                })
            }
        }, window.expander.willClose = function(e, t, n) {            
            var o;
            (e = $(e)).hasClass("cards-item") ? function(e, t) {
                e.find(".card-image").css("transform", "scale(1)");
                new TimelineMax({
                    onComplete: t
                }).to(e.find(".card-close"), .3, {
                    opacity: 0,
                    ease: Power2.easeOut
                }).to(e.find(".card-video"), .3, {
                    opacity: 0,
                    ease: Power2.easeOut
                })
            }(e, n) : e.hasClass("team-bioBox") ? (o = e, new TimelineMax({
                onComplete: n
            }).to(o.find(".team-bioVideoClose"), .5, {
                opacity: 0,
                ease: Power2.easeOut
            }).to(o.find(".team-bioVideo"), .5, {
                opacity: 0,
                ease: Power2.easeOut
            })) : n()
        }, window.expander.didClose = function(e, t, n) {            
            var o, i;
            (e = $(e)).hasClass("cards-item") ? (i = e, new TimelineMax({
                onComplete: n
            }).to(i.find(".card-fade"), .5, {
                top: 0,
                ease: Power2.easeOut
            }).to(i.find(".card-content"), .5, {
                bottom: 0,
                ease: Power2.easeOut
            }, "-=0.5").to(i.find(".card-leftCorner"), .2, {
                top: 0,
                ease: Power2.easeOut
            }).to(i.find(".card-rightCorner"), .2, {
                top: 0,
                ease: Power2.easeOut
            }, "-=0.2")) : e.hasClass("team-bioBox") ? (o = e, new TimelineMax({
                onComplete: n
            }).to(o.find(".team-bioLeft"), .5, {
                opacity: 1,
                ease: Power2.easeOut
            }).to(o.find(".team-bioRight"), .5, {
                opacity: 1,
                ease: Power2.easeOut
            }, "-=0.5").to(o.find(".team-bioClose"), .5, {
                opacity: 1,
                ease: Power2.easeOut
            }, "-=0.5")) : n()
        } 
});

//  To add class in readmore in menu
jQuery(document).ready( function(){
    jQuery("ul li#menu-item-474").find('a').addClass("readmore");
});
