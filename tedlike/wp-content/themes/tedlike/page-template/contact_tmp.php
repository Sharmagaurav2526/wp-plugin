<?php 
// Template Name: Contact Page Template
get_header();
?>
<style type="text/css">
	
.headerBlock-subtitle {
    font-size: 18px;
    font-family: "Roboto Mono",monospace;
    text-transform: uppercase;
    color: #9b9b9b;
    font-weight: 500;
    margin-bottom: 50px;
}
.contactbottom input[type="submit"] {
    text-align: center;
    font-size: 16px;
    /* color: #d0021b; */
    font-family: 'Lato', sans-serif;
    font-weight: 400;
    /* max-width: 350px !important; */
    /* margin: auto; */
    display: block;
    /* background-color: #d0021b; */
    /* border: none; */
    /* color: #fff; */
    /* float: left; */
    font-family: "Roboto Mono",monospace;
    /* padding: 3px 17px; */
    background-color: #d0021b;
    border: none;
    color: #fff;
    font-family: "Roboto Mono",monospace;
    outline: none;
    transition: all 0.2s ease-in-out;
    float: left;
    border-radius: 0px;
}
.form-control {
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #e6e6e6;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.row.contactbottom {
    padding: 0px 0px 25px;
}
</style>
<div class="form-wrapper contactform-wrapper">	
    <section class="headerBlock">
       <div class="container">
      	 <div class="row">
           <div class="col-md-10 col-lg-8">
              <div class="headerBlock-content">
                <h1 class="h2 headerBlock-title"><?php the_field('contact_us_'); ?></h1>
                <div class="headerBlock-subtext">
                   <p><?php the_field('contact_us_description'); ?></p>
                </div>
             </div>
            </div>        
        </div>
      </div>
    </section>
    <div class="container ">
    		<div class="row contactbottom">
           <div class="col-md-10 col-lg-8">
            <?php echo do_shortcode('[contact-form-7 id="171" title="Contact form 1"]'); ?>
            </div>
    	  </div>
    </div>
</div>
<?php get_footer(); ?>