<?php 
// Template Name: About Page Template
get_header();
?>
<style type="text/css">
	

/*.banner_area {
    background: url('http://tedlike.mobilytedev.com/wp-content/uploads/2018/06/verne-ho-23881@2x-1024x285.png') no-repeat scroll center center;
        background-size: auto auto;
    background-size: cover;
    position: relative;
    min-height: 400px;
}*/
</style>



<div class="container-fluid">

<div class="row"><section class="pageHero Col-md-12">
  <div class="overlayimg"></div>
  <div class="container">
    <div class="pageHero-info">
      <h1 class="h2 pageHero-title"><?php the_field('about_header_title'); ?></h1>
      <p class="upcase mono pageHero-subtitle"><?php the_field('about_header_subtitle'); ?></p>
    </div>
  </div>
</section></div>

</div>

<!--     <section class="mater-section">
      <div class="container">
        <div class="row align-items-center">
      <div class="col-lg-7 order-lg-1">
            <div class="p-1">
              <img class="img-fluid " src="http://tedlike.mobilytedev.com/wp-content/uploads/2018/06/Video-BG-1-1024x607.png" alt="">
            </div>
          </div>
    <div class="col-lg-5 order-lg-2">
            <div class="p-1">
              <h4 class="display-5">What is TEDxToronto? </h4>
              <p>Going into our 10th year, TEDxToronto is Canada’s largest TEDx event, a platform for exceptional ideas, and a catalyst for profound change.</p>
            </div>
          </div>
        
          
        </div>
      </div>
    </section> -->


    <section class="textAndImage">
  <div class="container">
    <div class="textAndImage-overlap">
      <div class="textAndImage-overlapContent">
                        <div class="textAndImage-media" style="background-image: url(<?php the_field('about_middle_left_image') ?>)"></div>
                  <p class="textAndImage-caption"><!-- Jennifer Keesmat, TEDxToronto 2017 --></p>
                          <div class="textAndImage-wysiwyg">
                            <?php the_field('about_middle_left_description'); ?>
            <!-- <p>In addition to our annual, one-day conference, which draws a diverse and passionate array of speakers, performers, demos and audience members, TEDxToronto runs community programming throughout the year exploring issues with local relevance. Talks from the conference have been viewed millions of times around the globe and six of our speakers have been featured on TED.com, one of the world’s leading platforms for big ideas.</p>
<p>All TEDxToronto speakers and organizers are volunteers, committed to nurturing and amplifying the innovative ideas that we hope will make Toronto — and the world — a better place.</p> -->
          </div>
              </div>
      <div class="textAndImage-overlapText" data-scrollspeed=""><div class="scrollspeed" style="transform: translateY(-44.33px);">
        <div class="textAndImage-textarea circle0">
          <h3 class="h4">
            <?php the_field('about_middle_right_title'); ?>         </h3>
          <p>
            <?php the_field('about_middle_right_description'); ?>          </p>
                                <a class="button" href="<?php the_field('view_all_past_talks_link'); ?> " target="_blank"><?php the_field('view_all_past_talks_caption'); ?>  </a>
                  </div>
      </div></div>
    </div>
  </div>
</section>




<section class="textBlock" style="background-image: url(<?php the_field('about_bottom_image') ?>)">
  <div class="overlayimg"></div>
  <div class="textBlock-textGroup">
    <h4><?php the_field('about_bottom_title'); ?></h4>

            <p><?php the_field('about_bottom_description'); ?> </p>
    
            <a href="<?php the_field('view_all_link'); ?>" class="button"><?php the_field('view_all_caption'); ?></a>
      </div>
</section>




<!-- <section class="banner_area">
        <div class="container">
            <div class="banner_text_inner">
                <h4>A catalyst for profound change.</h4>
                <h5>Inspring Toronto since 2009.</h5>
            </div>
        </div>
    </section> -->






<?php get_footer(); ?>