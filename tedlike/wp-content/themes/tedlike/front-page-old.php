<?php 
/* template name: Posts by Category! */
get_header(); ?>
<style type="text/css">
/* Tabs*/
section {
    padding: 60px 0;
}

section .section-title {
    text-align: center;
    color: #007b5e;
    margin-bottom: 50px;
    text-transform: uppercase;
}
.boximg img {
    vertical-align: middle;
    width: 480px;
    height: 205px;
}
.navbar-brand img {
    margin-top: 25px;
    margin-right: 1px;
    margin-left: 30px !important;
}

#tabs{
	background: #007b5e;
    color: #eee;
}
#tabs h6.section-title{
    color: #eee;
}

#tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #f3f3f3;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 4px solid !important;
    font-size: 20px;
    font-weight: bold;
}
#tabs .nav-tabs .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #eee;
    font-size: 20px;
}
</style>
<style type="text/css">
    .bs-example{
    	margin: 20px;
    }
    .modal-content iframe{
        margin: 0 auto;
        display: block;
    }
</style>
<div class="container" style="width: 100%;">

<div class="tab-content custom-tab-content">

	<div class="year-bar">
	
	<!-- <ul>
	<li class="active">
	<a  href="#"> 2018
</a>
	</li>
	<li>
	<a href="#">2017</a>
	</li>
	<li>
	<a href="#">2016</a>
	
	</li>
	<li>
	<a href="#">2015</a>
	
	</li>
	</ul> -->
	<?php 
	$terms = get_terms([
    			'taxonomy' => 'talk_year_tax',
    			'hide_empty' => false,
					]);
            	//echo "<pre>"; print_r($terms);
		foreach ($terms as $termdata) { 

			?>
		       <?php //echo $termdata->term_id; ?>  
		       <?php //echo $termdata->name; ?>  
		<?php } ?>    	
	<ul class="nav nav-tabs custom-tab" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span class="link-lable"> 2018</span></a></li>
   <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><span class="link-lable"> 2017</span></a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><span class="link-lable"> 2016</span></a></li>
	
    <li role="presentation"><a href="#test5" aria-controls="test5" role="tab" data-toggle="tab"><span class="link-lable"> 2015</span></a></li>
  </ul>
	
	</div>
  

  <!-- Nav tabs -->
<!--   <ul class="nav nav-tabs custom-tab" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-home icon"></i><span class="link-lable"> 2018</span></a></li>
   <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-envelope"></i><span class="link-lable"> 2017</span></a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-cog"></i><span class="link-lable"> 2016</span></a></li>
	
    <li role="presentation"><a href="#test5" aria-controls="test5" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-user"></i><span class="link-lable"> 2015</span></a></li>
  </ul> -->

<div class="tab-content">

<!------ Content 2018 Start here ------>
  <div role="tabpanel" class="tab-pane fade in active" id="home">
	
<section id="about">
        <div class="container">
            
            <div class="row main">
                <div class="col-sm-7 wow fadeInLeft">
                    <div class="about-img">
                        <img class="img-responsive" src="http://tedxtoronto.com/wp-content/uploads/2015-1024x683.jpg" alt="">
                    </div>

                </div>
                <div class="col-sm-5" style="position: relative;">
                    <div class="center-cont">
                       
                    <div class="media service-box wow fadeInRight">
                        
                        <div class="media-body">
                            <h4 class="media-heading">Conference Talks 2018</h4>
                            <p>On October 22, 2018, TEDxToronto presented 14 big thinkers, three unbelievable performers and one captivating host, surrounding the thought-provoking theme: Thresholds. The 2015 conference marked several big “firsts” on the TEDx stage, including talks from a professional sex worker and an international hostage negotiator. It was as the most successful TEDxToronto conference to-date, reaching the #1 trending topic in Canada on Twitter, with a combined global audience of 6,000, both on-premise and online. Beyond conference day, TEDxToronto hosted inspiring partner events, such the Collaborator’s Breakfast and the Innovator’s Dinner, to further ignite Toronto’s “Ideas Worth Spreading”.</p>
                        </div>
                    </div>
                    

                    </div>
                </div>
            </div>
        </div>
    </section>



      <section id="services" >
        <div class="container outerpadding">
<div class="row">

  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2018</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2018</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2018</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2018</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
 </div>

</div>
    </section>
</div>


<!------ Content 2018 End here ------>



<!------ Content 2017 Start here ------>
 <div role="tabpanel" class="tab-pane fade" id="messages">
 	
 	<section id="about">
        <div class="container">
            
            <div class="row main">
            	<?php
            	/*$cats = get_categories();
            	echo "<pre>"; print_r($cats);*/
            	
            	 /*$args = array(
				'posts_per_page'   => 5,
				'offset'           => 0,
				'category'         => '',
				'category_name'    => '',
				'orderby'          => 'date',
				'order'            => 'DESC',
				'include'          => '',
				'exclude'          => '',
				'meta_key'         => '',
				'meta_value'       => '',
				'post_type'        => 'talk_year',
				'post_mime_type'   => '',
				'post_parent'      => '',
				'author'	   => '',
				'author_name'	   => '',
				'post_status'      => 'publish',
				'suppress_filters' => true,
				'fields'           => '',
			);
            
			$posts_array = get_posts( $args );
			echo "<pre>"; print_r($posts_array);
			$post_id = $posts_array['0']->ID;
			echo "<pre>"; print_r($post_id);
			$src = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'thumbnail_size' );
			$url = $src[0];
			echo "<pre>"; print_r($url);*/



			 ?>
                <div class="col-sm-7 wow fadeInLeft">
                    <div class="about-img">
                        <img class="img-responsive" src="http://tedxtoronto.com/wp-content/uploads/2015-1024x683.jpg" alt="">
                    </div>

                </div>
                <div class="col-sm-5" style="position: relative;">
                    <div class="center-cont">
                       
                    <div class="media service-box wow fadeInRight">
                        
                        <div class="media-body">
                            <h4 class="media-heading">Conference Talks 2017</h4>
                            <p>On October 22, 2017, TEDxToronto presented 14 big thinkers, three unbelievable performers and one captivating host, surrounding the thought-provoking theme: Thresholds. The 2015 conference marked several big “firsts” on the TEDx stage, including talks from a professional sex worker and an international hostage negotiator. It was as the most successful TEDxToronto conference to-date, reaching the #1 trending topic in Canada on Twitter, with a combined global audience of 6,000, both on-premise and online. Beyond conference day, TEDxToronto hosted inspiring partner events, such the Collaborator’s Breakfast and the Innovator’s Dinner, to further ignite Toronto’s “Ideas Worth Spreading”.</p>
                        </div>
                    </div>
                    

                    </div>
                </div>
            </div>
        </div>
    </section>


	  <section class="videos-section">
	     <div class="container">
        <div class="row">
	<div class="dropdown-filter">
	<i class="fas fa-sliders-h"></i>  Filter By

  <div>
  <select>
  <option value="volvo">Session </option>
  <option value="saab">Interview</option>
  <option value="opel">Presentation</option>
  <option value="audi">Pictures</option>
    <option value="audi"> Other</option>

</select>
  </div>
</div>
</div>

      <section id="services" >
       <div class="container outerpadding">
		<div class="row"> <!-- Start row -->
		  <?php $wpb_all_query = new WP_Query(array('post_type'=>'video_post', 'posts_per_page'=>10,'order'            => 'ASC')); 
		  		//echo "<pre>"; print_r($wpb_all_query);
		  ?> 
                      <?php if ( $wpb_all_query->have_posts() ) : ?>
                          <?php 
                  while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
		  <a href="#myModal" data-toggle="modal">
		  	<!-- <a id="video" video-url="https://www.youtube.com/watch?v=0wCC3aLXdOw"> -->
		  	<div class="col-lg-3">
		     <div class="panel">
		        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong><?php the_title(); ?></strong></div>
		        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
		        
		        <div class="boximg">
		        	<?php the_post_thumbnail();  ?>

		         <!-- <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive"> -->
		         <span class="label label-danger date">25 December 2017</span>
		         <span class="likebut glyphicon glyphicon-tag"></span>
		         </div>
         
         
		   <br>
		        <p class="pull-left"><?php the_content(); ?><br>
		           <!-- <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span> -->
		        </p>
		     
		        <!-- <span class="badge pull-right" style="background-color:teal">25</span> -->
		      </div>
		     </div>
		  </div> </a>


		
		  <?php endwhile; ?>
                  <?php wp_reset_postdata(); ?>
                     
                    <?php else : ?>
                        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
		  
		  <!--<div class="col-lg-3">
		     <div class="panel">
		        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
		        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
		        
		        <div class="boximg">
		         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
		         <span class="label label-danger date">25 December 2017</span>
		         <span class="likebut glyphicon glyphicon-tag"></span>
		         </div>
         
         
		   <br>
		        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
		           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
		        </p>
		     
		        <span class="badge pull-right" style="background-color:teal">25</span>
		      </div>
		     </div>
  		</div>
		  <div class="col-lg-3">
		     <div class="panel">
		        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
		        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
		        
		        <div class="boximg">
		         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
		         <span class="label label-danger date">25 December 2017</span>
		         <span class="likebut glyphicon glyphicon-tag"></span>
		         </div>
		         
		         
		   <br>
		        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
		           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
		        </p>
		     
		        <span class="badge pull-right" style="background-color:teal">25</span>
		      </div>
		     </div>
		  </div>
		  <div class="col-lg-3">
		     <div class="panel">
		        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
		        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
		        
		        <div class="boximg">
		         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
		         <span class="label label-danger date">25 December 2017</span>
		         <span class="likebut glyphicon glyphicon-tag"></span>
		         </div>
		         
		         
		   <br>
		        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
		           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
		               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
		        </p>
		     
		        <span class="badge pull-right" style="background-color:teal">25</span>
		      </div>
		     </div>
		  </div>-->
		 </div> <!-- End Row -->

</div>
    </section>


 </div>

<!------ Content 2017 End here ------>

<!------ Content 2016 Start here ------>
<div role="tabpanel" class="tab-pane fade" id="settings">
  	

<section id="about">
        <div class="container">
            
            <div class="row main">
                <div class="col-sm-7 wow fadeInLeft">
                    <div class="about-img">
                        <img class="img-responsive" src="http://tedxtoronto.com/wp-content/uploads/2015-1024x683.jpg" alt="">
                    </div>

                </div>
                <div class="col-sm-5" style="position: relative;">
                    <div class="center-cont">
                       
                    <div class="media service-box wow fadeInRight">
                        
                        <div class="media-body">
                            <h4 class="media-heading">Conference Talks 2016</h4>
                            <p>On October 22, 2016, TEDxToronto presented 14 big thinkers, three unbelievable performers and one captivating host, surrounding the thought-provoking theme: Thresholds. The 2015 conference marked several big “firsts” on the TEDx stage, including talks from a professional sex worker and an international hostage negotiator. It was as the most successful TEDxToronto conference to-date, reaching the #1 trending topic in Canada on Twitter, with a combined global audience of 6,000, both on-premise and online. Beyond conference day, TEDxToronto hosted inspiring partner events, such the Collaborator’s Breakfast and the Innovator’s Dinner, to further ignite Toronto’s “Ideas Worth Spreading”.</p>
                        </div>
                    </div>
                    

                    </div>
                </div>
            </div>
        </div>
    </section>



      <section id="services" >
        <div class="container outerpadding">
<div class="row">

  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2016</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2016</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2016</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2016</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
 </div>

</div>

    </section>



</div>


<!------ Content 2016 End here ------>


<!------ Content 2015 Start here ------>


<div role="tabpanel" class="tab-pane fade" id="test5">

<section id="about">
        <div class="container">
            
            <div class="row main">
                <div class="col-sm-7 wow fadeInLeft">
                    <div class="about-img">
                        <img class="img-responsive" src="http://tedxtoronto.com/wp-content/uploads/2015-1024x683.jpg" alt="">
                    </div>

                </div>
                <div class="col-sm-5" style="position: relative;">
                    <div class="center-cont">
                       
                    <div class="media service-box wow fadeInRight">
                        
                        <div class="media-body">
                            <h4 class="media-heading">Conference Talks 2015</h4>
                            <p>On October 22, 2015, TEDxToronto presented 14 big thinkers, three unbelievable performers and one captivating host, surrounding the thought-provoking theme: Thresholds. The 2015 conference marked several big “firsts” on the TEDx stage, including talks from a professional sex worker and an international hostage negotiator. It was as the most successful TEDxToronto conference to-date, reaching the #1 trending topic in Canada on Twitter, with a combined global audience of 6,000, both on-premise and online. Beyond conference day, TEDxToronto hosted inspiring partner events, such the Collaborator’s Breakfast and the Innovator’s Dinner, to further ignite Toronto’s “Ideas Worth Spreading”.</p>
                        </div>
                    </div>
                    

                    </div>
                </div>
            </div>
        </div>
    </section>



      <section id="services" >
       <div class="container outerpadding">
<div class="row">

  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2015</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2015</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2015</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
  <div class="col-lg-3">
     <div class="panel">
        <div class="panel-heading" style="background-color:teal;color:#fff;"><strong>Lorem Ipsum</strong></div>
        <div class="panel-body" style="background-color:#000;color:#fff; box-shadow:0 -12px 13px teal inset;">
        
        <div class="boximg">
         <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="img-responsive">
         <span class="label label-danger date">25 December 2015</span>
         <span class="likebut glyphicon glyphicon-tag"></span>
         </div>
         
         
   <br>
        <p class="pull-left">Lorem ipsum Lorem ipsum<br>
           <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
            <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
             <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
              <span class="glyphicon glyphicon-star" style="font-size:18px;"></span>
               <span class="glyphicon glyphicon-star-empty" style="font-size:18px;"></span>
        </p>
     
        <span class="badge pull-right" style="background-color:teal">25</span>
      </div>
     </div>
  </div>
 </div>

</div>
    </section>

</div> 

<!------ Content 2015 End here ------>

</div>



</div>
</div>
</div>
  <!-- <section id="about">
        <div class="container">
            
            <div class="row main">
                <div class="col-sm-7 wow fadeInLeft">
                    <div class="about-img">
                        <img class="img-responsive" src="http://tedxtoronto.com/wp-content/uploads/2015-1024x683.jpg" alt="">
                    </div>

                </div>
                <div class="col-sm-5" style="position: relative;">
                    <div class="center-cont">
                       
                    <div class="media service-box wow fadeInRight">
                        
                        <div class="media-body">
                            <h4 class="media-heading">Conference Talks 2015</h4>
                            <p>On October 22, 2015, TEDxToronto presented 14 big thinkers, three unbelievable performers and one captivating host, surrounding the thought-provoking theme: Thresholds. The 2015 conference marked several big “firsts” on the TEDx stage, including talks from a professional sex worker and an international hostage negotiator. It was as the most successful TEDxToronto conference to-date, reaching the #1 trending topic in Canada on Twitter, with a combined global audience of 6,000, both on-premise and online. Beyond conference day, TEDxToronto hosted inspiring partner events, such the Collaborator’s Breakfast and the Innovator’s Dinner, to further ignite Toronto’s “Ideas Worth Spreading”.</p>
                        </div>
                    </div>
                    

                    </div>
                </div>
            </div>
        </div>
    </section>



      <section id="services" >
        <div class="container">

<ul class="list-unstyled video-list-thumbs row">
	<li class="col-lg-4 col-sm-4 col-xs-6">
		<a href="#" title="Claudio Bravo, antes su debut con el Barça en la Liga">
			<img src="http://i.ytimg.com/vi/ZKOtE9DOwGE/mqdefault.jpg" alt="Barca" class="img-responsive" height="130px" />
			<h2>Claudio Bravo, antes su debut con el Barça en la Liga</h2>
			<span class="glyphicon glyphicon-play-circle"></span>
			<span class="duration">03:15</span>
		</a>
	</li>
	<li class="col-lg-4 col-sm-4 col-xs-6">
		<a href="#" title="Claudio Bravo, antes su debut con el Barça en la Liga">
			<img src="http://i.ytimg.com/vi/ZKOtE9DOwGE/mqdefault.jpg" alt="Barca" class="img-responsive" height="130px" />
			<h2>Claudio Bravo, antes su debut con el Barça en la Liga</h2>
			<span class="glyphicon glyphicon-play-circle"></span>
			<span class="duration">03:15</span>
		</a>
	</li>
	<li class="col-lg-4 col-sm-4 col-xs-6">
		<a href="#" title="Claudio Bravo, antes su debut con el Barça en la Liga">
			<img src="http://i.ytimg.com/vi/ZKOtE9DOwGE/mqdefault.jpg" alt="Barca" class="img-responsive" height="130px" />
			<h2>Claudio Bravo, antes su debut con el Barça en la Liga</h2>
			<span class="glyphicon glyphicon-play-circle"></span>
			<span class="duration">03:15</span>
		</a>
	</li>
	<li class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
		<a href="#" title="Claudio Bravo, antes su debut con el Barça en la Liga">
			<img src="http://i.ytimg.com/vi/ZKOtE9DOwGE/mqdefault.jpg" alt="Barca" class="img-responsive" height="130px" />
			<h2>Claudio Bravo, antes su debut con el Barça en la Liga</h2>
			<span class="glyphicon glyphicon-play-circle"></span>
			<span class="duration">03:15</span>
		</a>
	</li>
    <li class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
		<a href="#" title="Claudio Bravo, antes su debut con el Barça en la Liga">
			<img src="http://i.ytimg.com/vi/ZKOtE9DOwGE/mqdefault.jpg" alt="Barca" class="img-responsive" height="130px" />
			<h2>Claudio Bravo, antes su debut con el Barça en la Liga</h2>
			<span class="glyphicon glyphicon-play-circle"></span>
			<span class="duration">03:15</span>
		</a>
	</li>
</ul>
              
        </div>
    </section> -->


  <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">YouTube Video</h4>
                </div>
                <div class="modal-body">
                    <iframe id="cartoonVideo" width="560" height="315" src="//www.youtube.com/embed/YE7VzlLtp-4" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>


<?php get_footer();
?>