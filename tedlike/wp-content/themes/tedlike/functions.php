<?php 

//  To register a logo

$inc_path = TEMPLATEPATH . '/inc/';

function theme_prefix_setup() {
  
  add_theme_support( 'custom-logo', array(
    'height'      => 100,
    'max-width'       => 300,
    'flex-width' => true,
  ) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );

//  To register nav-menus in Wordpress 
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'top-menu' => __( 'Top Menu' ),
      'footer-menu' => (' Footer Menu ')
    )
  );
}
add_action( 'init', 'register_my_menus' );



add_filter( 'nav_menu_link_attributes', 'wpse156165_menu_add_class', 10, 3 );

function wpse156165_menu_add_class( $atts, $item, $args ) {
    $class = 'nav-link'; // or something based on $item
    $atts['class'] = $class;
    return $atts;
}



// To register a widget in wordpress
add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget'  => '</li>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Sidebar 2', 'theme-slug' ),
        'id' => 'sidebar-2', 
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget'  => '</li>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Sidebar 3', 'theme-slug' ),
        'id' => 'sidebar-3',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<ul class="menu pages"><li id="%1$s" class="menu-item menu-item-type-post_type menu-item-object-page">',
    'after_widget'  => '</li></ul>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Recent Sidebar', 'theme-slug' ),
        'id' => 'sidebar-4',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}  
//  End register custom widgets in Wordpress
//   For register  feature image in Wordpress 
add_theme_support( 'post-thumbnails' ); 
// End function here

function add_registration_trial( $user_id ) {
    if ($_POST['role']) {
        if($_POST['role'] == 'owner'){ 
           wp_update_user( array( 'ID' => $user_id, 'role' => 'owner' ) );
        // update_user_meta( $user_id, 'as_capabilities', 'a:1:{s:5:"owner";b:1;}');
        }
        else {
            wp_update_user( array( 'ID' => $user_id, 'role' => 'resident' ) );
        } 
        
    }
}

add_action( 'user_register', 'add_registration_trial', 10, 1 );


/*--======end walker menu code start========--*/
add_action("wp_ajax_nopriv_generate", "generate");
add_action("wp_ajax_generate", "generate");
function generate(){
    $count = $_POST['id'];
    $year = $_POST['talk_year_tax'];
    $args = array(
    'post_type' => 'video_post',
    'tax_query' => array(
        'relation' => 'AND',
        array(
            'taxonomy' => 'video_post_tax',
            'field' => 'term_id', //can be set to ID
            'terms' => array($count) //if field is ID you can reference by cat/term number
        ),
        array(
            'taxonomy' => 'talk_year_tax',
            'field' => 'term_id', //can be set to ID
            'terms' => array($year) //if field is ID you can reference by cat/term number
        )
    )
);
    //echo "<pre>"; print_r($args);

function get_vimeo_thumb($videoid, $size = "large", $return = false)
{
$imgid = $videoid;

$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$imgid.php"));

$the_thumb = '';

switch ($size) {
case 'small':
$the_thumb = $hash[0]['thumbnail_small'];
break;
case 'medium':
$the_thumb = $hash[0]['thumbnail_medium'];
break;
case 'large':
$the_thumb = $hash[0]['thumbnail_large'];
break;
}

if (!$return)
{
return $the_thumb;
}
else
{
return $the_thumb;
}
}
?> 
<div class="row1 my-img-class">
    <?php $wpb_all_query = new WP_Query($args);
            if ( $wpb_all_query->have_posts() ) : 
                while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); 
                    $postID = get_the_ID();
                    ?>


                     <?php 
                        $attachment_id = '';
                        $size = '';
                     if (get_field('pictures_show' )) { ?>
                         
                        <?php if( have_rows('pictures_show') ):
                          $total = count(get_field('pictures_show', 'option'));
                           $number = 80; // the number of rows to show
                           $count = 0;
                        while ( have_rows('pictures_show') ) : the_row(); ?>
                          <?php 
                          $attachment_id = get_sub_field('pictures');
                          $size = "thumbnail"; // (thumbnail, medium, large, full or custom size)
                          $imageurl = wp_get_attachment_image_src( $attachment_id, 'full' );
                          $image_title = $attachment->post_title;
                          /*echo "Attachment ID".$attachment_id.'</br>';
                          echo "Size".$size.'</br>';
                          echo "Image URL".$imageurl.'</br>';
                          echo "Image Title".$image_title.'</br>';*/
                          ?>
                          <div class="col-md-3 col-sm-4 col-xs-6 my-sub-img-class">
                          <a class="example-image-link" href="<?php the_sub_field('pictures'); ?>" data-lightbox="example-2" data-title="Optional caption.">
                              <img class="img-responsive" src="<?php the_sub_field('pictures'); ?>" />
                            </a>
                          </div>
                             <?php $count++;
                        if ($count == $number) {
                            // we've shown the number, break out of loop
                            break;
                        }
                         endwhile; ?> 
                         <?php /*echo do_shortcode('[ajax_load_more acf="true" acf_field_type="repeater" acf_field_name="pictures" scroll="false" images_loaded="true"]');*/ ?>
                              <!-- <a id="my-repeater-show-more-link" style="display: none;" href="javascript:void(0);" onclick="my_repeater_show_more();"<?php if ($total < $count) { ?> <?php  } ?>>Show More</a> -->
                             <?php else : endif;  }  ?>

    
               
                       <?php 
                        /*$img_url = '';
                        $video_id = '';
                        if(!get_field('conference_talks_videolink') == ""){
                            $str = get_field('conference_talks_videolink');
                            $res =  explode("/",$str);
                            $video_id =  end($res); 
                            $img_url = "https://img.youtube.com/vi/".$video_id."/0.jpg";
                         } elseif (!get_field('vimeo_link') == "") {
                            $str = get_field('vimeo_link');
                            $res = explode("/", $str);
                            end($res);
                            $video_id = prev($res);
                            $img_url = "https://i.vimeocdn.com/video/".$video_id."_640.jpg";
                         } elseif (!get_field('slide_share_link') == "") {
                            $video_id = '';
                            $img_url = the_post_thumbnail_url();                            
                         } */
                         ?>
                    


                     <?php 
                        $img_url = '';
                        $video_id = '';
                        if(!get_field('conference_talks_videolink') == ""){
                            $str = get_field('conference_talks_videolink');
                            $res =  explode("/",$str);
                            $video_id =  end($res); 
                            $img_url = "https://img.youtube.com/vi/".$video_id."/0.jpg";
                          ?>
                <div class="cards-item"                       
                      data-expand-open data-expand data-video-id="<?php echo $video_id; ?>">
                    <div class="card-container card-container--talk"  >
                      <div class="card-image" style="background-image: url(<?php echo $img_url; ?>);">
                      <!-- <img src="<?php echo $img_url; ?>" class="img-fluid"> -->
                    </div>
                      <!-- <div class="card-image" style="background-image: url(<?php echo $img_url; ?>);"></div> -->
                      <div class="card-leftCorner">
                        <div class="triangleWhite--right"></div>
                      </div>
                      <div class="card-fade"></div>                                
                      <div class="card-content">
                          <div class="card-meta">
                            <span class="u-bold"><?php echo get_field('conference_talks_title');//the_title(); ?></span>
                          </div>
                          <p class="card-title">
                            <?php echo get_field('conference_talks_subtitle');//the_title(); ?>
                          </p>
                      </div>
                      <div class="card-video" data-videourl="youtube" ></div>   
                      <div class="card-close" data-expand-close=""></div>
                    </div>
                </div>

                <?php } elseif(!get_field('vimeo_link') == ""){
                  $str = get_field('vimeo_link');
                  $res = explode("/", $str);
                      end($res); 
                   $video_id = prev($res);
                  // $img_url = "https://i.vimeocdn.com/video/".$video_id."_640.jpg";
                   $imgURL = get_vimeo_thumb($video_id, $size = "large", $return = false);
                      // $img_url = "https://i.vimeocdn.com/video/".$video_id.".webp?mw=960&mh=540";
                    ?>
                   <div class="cards-item"                    
                   data-expand-open data-expand data-video-id="<?php echo $video_id; ?>">
                    <div class="card-container card-container--talk"  >
                      <!-- <div class="card-image">
                      <img src="<?php echo $img_url; ?>" class="img-fluid"> </div> -->
                      <!-- <img src="https://img.youtube.com/vi/<?php echo $result; ?>/0.jpg" class="img-fluid"> -->
                       <div class="card-image" style="background-image: url(<?php echo $imgURL; ?>);"></div> 
                      <div class="card-leftCorner">
                        <div class="triangleWhite--right"></div>
                      </div>
                      <div class="card-fade"></div>                                
                      <div class="card-content">
                          <div class="card-meta">
                            <span class="u-bold"><?php echo get_field('conference_talks_title');//the_title(); ?></span>
                          </div>
                          <p class="card-title">
                            <?php echo get_field('conference_talks_subtitle');//the_title(); ?>
                          </p>
                      </div>
                      <div class="card-video" data-videourl="vimeo" ></div>   
                      <div class="card-close" data-expand-close=""></div>
                    </div>
                </div>
                <?php } elseif(!get_field('slide_share_link') == ""){
                        //$video_id = '';
                        $video_id = get_field('slide_share_link');
                            //$img_url = the_post_thumbnail_url();
                 ?>

                   <div class="cards-item"                    
                   data-expand-open data-expand data-video-id="<?php echo $video_id; ?>" data-videourl="slider" >
                    <div class="card-container card-container--talk"  >
                      <!-- <img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid"> -->
                      <div class="card-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div> 
                      <div class="card-leftCorner">
                        <div class="triangleWhite--right"></div>
                      </div>
                      <div class="card-fade"></div>                                
                      <div class="card-content">
                          <div class="card-meta">
                            <span class="u-bold"><?php echo get_field('conference_talks_title');//the_title(); ?></span>
                          </div>
                          <p class="card-title">
                            <?php echo get_field('conference_talks_subtitle');//the_title(); ?>
                          </p>
                      </div>
                      <div class="card-video" data-videourl="slider" ></div>   
                      <div class="card-close" data-expand-close=""></div>
                    </div>
                </div>

                <?php } elseif(!get_field('others_link') == ""){
                        //$video_id = '';
                        $video_id = get_field('others_link');
                            //$img_url = the_post_thumbnail_url();
                 ?>

                   <div class="cards-item"                    
                   data-expand-open data-expand data-video-id="<?php echo $video_id; ?>" data-videourl="others" >
                    <div class="card-container card-container--talk"  >
                      <!-- <img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid"> -->
                      <div class="card-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div> 
                      <div class="card-leftCorner">
                        <div class="triangleWhite--right"></div>
                      </div>
                      <div class="card-fade"></div>                                
                      <div class="card-content">
                          <div class="card-meta">
                            <span class="u-bold"><?php echo get_field('conference_talks_title');//the_title(); ?></span>
                          </div>
                          <p class="card-title">
                            <?php echo get_field('conference_talks_subtitle');//the_title(); ?>
                          </p>
                      </div>
                      <div class="card-video" data-videourl="others" ></div>   
                      <div class="card-close" data-expand-close=""></div>
                    </div>
                </div>


                <?php } ?>
                   
                    
          
          
        
          
          
          
          
          
          
            <?php endwhile;
                    wp_reset_postdata(); 
                else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
    </div> 
<?php die(); }






 ?>