<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php the_title(); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/custom.css" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity=
"sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Karla:400,400i,700,700i" rel="stylesheet">

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" 
integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous"> -->
    <!-- Custom styles for this template -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/one-page-wonder.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/lightbox.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/one-page-wonder.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/YouTubePopUp.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jssor.slider-27.2.0.min.js"></script>
    <script type="text/javascript">
        /*jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $Idle: 0,
              $SlideDuration: 5000,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 280,
              $Align: 0
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

           

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            
        };*/
    </script>
  <?php wp_head(); ?>

  </head>

  <body>

<div class="navsWrapper">   
   <!-- Top bar -->
    <div class="top-bar">    
    <div class="container-fluid">
    <div class="row">
    <div class="col-md-6"><p class="top-date-info"><?php the_field('top_address' , 'option'); ?></p></div>
    <div class="col-md-6"> 
    <ul class="social-icons">   
    <?php
    if( have_rows('social_icons' , 'option' ) ):
    while ( have_rows('social_icons' , 'option' ) ) : the_row(); ?>  
     <li><a href="<?php the_sub_field('social_url'); ?>" target="_blank"><i class="<?php the_sub_field('social_class'); ?>"
></i></a></li> 
  <?php 
 endwhile;
 else :
 endif;
   ?>  
    </ul>    
    </div>    
    </div>    
    </div>
    </div>
	
	<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark navbar-custom">
      <div class="container-fluid">
        <!-- <a class="navbar-brand" href="#"> <img src="http://tedlike.mobilytedev.com/wp-content/uploads/2018/06/DAL-Logo-white.png" class="img-fluid" 
alt="LOGO"> </a> -->
        <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
            $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            if ( has_custom_logo() ) {
                    echo '<a href="'.site_url().'"><img style="max-width: 220px !important;" src="'. esc_url( $logo[0] ) .'"></a>';
            } else {
                    echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
            } ?>
        <button class="navbar-toggler collapsed navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded=
"false" aria-label="Toggle navigation">
         <span> </span>
            <span> </span>
            <span> </span>
        </button>
        <div class="row mobnavv">
		<div class="collapse navbar-collapse" id="navbarResponsive">
            <?php   wp_nav_menu(array(
                    'theme_location' => 'header-menu',
                    'container' => 'ul',
                    'menu_class' => 'navbar-nav ml-auto nav-item ',
                    'echo' => true,
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'depth' => 0,
                    )
                  ); ?>
				  <!-- <ul class="social-icons mobsocil">     
     <li><a href="https://www.linkedin.com/company/smartassistant-smart-information-systems-?trk=biz-companies-cym" target="_blank"><i class="fab fa-linkedin"></i></a></li>
	 <li><a href="https://twitter.com/_GuidedSelling" target="_blank"><i class="fab fa-twitter"></i></a></li>
    <li><a href="https://www.facebook.com/guidedselling.org2014" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
	<li><a href="https://vimeo.com/smartassistant" target="_blank"><i class="fab fa-vimeo"></i></a></li>    
    </ul> -->
                  <!-- <ul class="navbar-nav ml-auto leranmore">
                  <li class="nav-item"><a class="readmore" href="https://smartassistant.com" target="_blank">Learn More: SMARTASSISTANT</a></li> 
					</ul> -->
          <!-- <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#">Past Conferences</a>
            </li> 
            <li class="nav-item">
              <a class="nav-link" href="https://live.digital-advice.org" target="_blank">Conference</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About</a>
            </li>
                <li class="nav-item">
              <a class="readmore" href="https://smartassistant.com" target="_blank">Learn More</a>
            </li>            
              
          </ul> -->
        </div>
		</div>
      </div>
    </nav>

</div>
    