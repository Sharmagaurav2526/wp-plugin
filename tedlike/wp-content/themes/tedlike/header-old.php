<!DOCTYPE html>
<html lang="en" style="margin-top: 0 !important;">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Tedlike</title>
	<!-- core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">    
    <link href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/owl.transitions.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/main.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/custom.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/video.popup.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php //echo get_template_directory_uri(); ?>/images/ico/icon.png">
    <?php wp_head(); ?>  
    <script>var ajaxurl= '<?php echo admin_url( 'admin-ajax.php' ) ?>'</script>  
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
 
</head><!--/head-->

<body id="home" class="homepage">

    
    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            
            <div class="top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 text-left">
                            <p>October 26, 2018 at Evergreen Brick Works</p>
                            <!-- <ul class="top-bar">
                                <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> </a></li>
                            </ul> -->
                        </div>
                        <div class="col-md-6 col-sm-4 text-right">
                            <ul class="social-icons">
                                  
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>                            
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container m-t-40">                
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>                   
                    <a class="navbar-brand" href="#"><img src="<?php echo home_url(); ?>/wp-content/uploads/2018/06/logo-ted.png" alt="logo"></a> 
                </div>
				<!-- http://tedlike.mobilytedev.com/wp-content/uploads/2018/06/logo-ted.png -->
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="scroll"><a href="#features">Past Conferences</a></li>
                        <li class="scroll"><a href="https://live.digital-advice.org" target="_blank">Conference</a></li>
                        <li class="scroll"><a href="#">About</a></li>
                                                                       
                    </ul>
                    <div class="learn-box">
                        <div class="learn-scroll">
                        <a href="https://smartassistant.com" target="_blank">Learn more</a>
                        </div>
                        </div>
                    <!-- <div class="siteNav-tickets">
                <a href="http://tedxtoronto.com/announcements/introducing-tedxtoronto-2018-theme-identity/">Learn More: IDENTITY</a>
              </div> -->
                    <?php //wp_nav_menu( array( 'theme_location' => 'header-menu' , 'container' => 'ul' , 'menu_class' => 'nav navbar-nav ') ); ?>
					<?php   /*wp_nav_menu(array(
                    'theme_location' => 'header-menu',
                    'container' => 'ul',
                    'menu_class' => 'nav navbar-nav ',
                    'echo' => true,
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'depth' => 0,
                    )
                  );*/ ?>

                    <?php  
                        //wp_nav_menu(array(  'menu' => 'Main Navigation',  'container_id' => 'cssmenu', 'walker' => new CSS_Menu_Maker_Walker(),'menu_class' => 'nav navbar-nav')); 
                    ?>
                </div>
                

            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->