<?php 

get_header(); ?>
<style>
  .u-bold {
    font-size: 13px;
}
a.video.card-section-link {
    position: absolute;
    width: 100%;
    top: 0px;
    left: 0px;
    right: 0px;
    height: 100%;
}
iframe{height: 77vh !important;}
</style>
  <style>
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }
    </style>
<div class="year-bar">
<ul class="nav nav-tabs custom-tab" role="tablist">
  <?php //$loop = new WP_Query( array( 'post_type' => 'candidates', 'posts_per_page' => -1 ) ); ?>
	<?php 
	/*$counter = 0;
	while ( $loop->have_posts() ) : $loop->the_post(); 
		$counter++;*/
	?>

	<?php 
	$counter = 0;
	/*$terms = get_terms([
	  'taxonomy' => 'talk_year_tax',
	  'hide_empty' => false,
    'order' => 'ASC'
	  ]);*/

$taxonomy = 'talk_year_tax';
$tax_args = array(
    'order' => 'DESC'
);
$terms = get_terms( $taxonomy, $tax_args );

		  //echo "<pre>"; print_r($terms);
foreach ($terms as $termdata) { 

 $counter++;
	?> 

<li role="presentation" class="<?=($counter == 2) ? 'active' : ''?> my-active-class"><a href="#home<?php echo $termdata->term_id; ?>" aria-controls="home" role="tab" data-toggle="tab"><span class="link-lable"> <?php echo $termdata->name; ?></span></a></li>
<?php //$counter=''; 
} ?>
<?php //endwhile; wp_reset_query(); ?>
<!-- <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><span class="link-lable"> 2017</span></a></li>
<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><span class="link-lable"> 2016</span></a></li>

<li role="presentation"><a href="#test5" aria-controls="test5" role="tab" data-toggle="tab"><span class="link-lable"> 2015</span></a></li> -->

</ul>
</div>


  <div class="tab-content">
    <?php 
        $counter = 0;
       /* $terms1 = get_terms([
          'taxonomy' => 'talk_year_tax',
          'hide_empty' => false,
          'order' => 'ASC'
          ]);*/
$taxonomy1 = 'talk_year_tax';
$tax_args1 = array(
    'order' => 'DESC'
);
$terms_ref = array();
$terms1 = get_terms( $taxonomy1, $tax_args1 );       
              //echo "<pre>"; print_r($terms1);
    foreach ($terms1 as $termdata1) { 
		$terms_ref[] = 'jssor_'.$termdata1->term_id.'_slider_init';
?> 
<!------ Content 2018 Start here ------>
  <div role="tabpanel" class="tab-pane fade <?=($counter == 1) ? 'active in' : ''?> show" id="home<?php echo $termdata1->term_id; ?>">
<div id="jssor_<?php echo $termdata1->term_id; ?>" 
		class="" 
		style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:100px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="http://localhost/doctedlike/trunk/wp-content/tedlike/images/spin.svg" />
        </div>
         <?php if( have_rows('slider_logo', $termdata1->taxonomy . '_' . $termdata1->term_id) ): ?>
    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:100px;overflow:hidden;">
           
        	<?php 
			 	  	while ( have_rows('slider_logo', $termdata1->taxonomy . '_' . $termdata1->term_id) ) : the_row(); 
			 	  		$image  = get_sub_field('slider_logo', $termdata1->taxonomy . '_' . $termdata1->term_id);		 	  		
			 	  		?>
      				<?php //$counter = $counter + 1; 
      				//$image = wp_get_attachment_image_src(get_sub_field('logos', $termdata1->taxonomy . '_' . $termdata1->term_id), 'full');


      				 ?>
            <div data-p="30">
                <img data-u="image" src="<?php the_sub_field('logos', $termdata1->taxonomy . '_' . $termdata1->term_id); ?>" />
            </div>
            <?php  endwhile; ?>
     		
    </div>
    <?php endif; ?>
</div>
    <section class="mater-section">
      <div class="container">
      <div class="row">	
		<div class="textAndImage-overlap">
			<?php   $imagePATH =   get_field('add_banner', $termdata1->taxonomy . '_' . $termdata1->term_id); ?>
			<?php  $videoPATH =  get_field('add_video', $termdata1->taxonomy . '_' . $termdata1->term_id) ?> 
            <?php if($imagePATH != ""){ ?>
			<div class="order-lg-1">			
				<img class="img-fluid " src="<?php echo get_field('add_banner', $termdata1->taxonomy . '_' . $termdata1->term_id); ?>" alt="">			
			</div> <?php } else {  ?>
                   <div class="order-lg-1">			
				<?php echo $videoPATH; ?>
			</div>
				<?php } ?>
			<div class="order-lg-2">			
				<h4 class="display-5"><?php echo get_field('add_custom_title', $termdata1->taxonomy . '_' . $termdata1->term_id); ?>
				</h4>
				<p><?php echo get_field('add_custom_description', $termdata1->taxonomy . '_' . $termdata1->term_id); ?></p>			
			</div>         
        </div>
        </div>		
      </div>
    </section>  
  <?php $terms = get_terms('video_post_tax'); 
        //$termId = $termdata->term_id; ?>  
	<section class="videos-section">						
				<div class="container">
					<div class="row">
						<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
							<div class="dropdown-filter">
								<p class="filterDropdown-pretitle">Filter By</p>
								<div class="filterDropdown-selectWrapper">
									<select name="term_id" 
											id="term_id"
											class="term_id" data-divref="#div<?= $counter; ?>" data-year="<?= $termdata1->term_id; ?>">
										<?php foreach ( $terms as $term ) { ?>
										<option value="<?php echo $term->term_id ?>"  ><?php echo $term->name ?></option>
										<?php } ?>

									</select>
								</div>
							</div>
						</form>
					</div>
				</div>			
				<div class="container">
					<div class="row">
						<div id="div<?= $counter; ?>"></div>
						
						<?php ///echo do_shortcode('[ajax_load_more container_type="div" post_type="video_post, talk_year" taxonomy="video_post_tax" taxonomy_terms="session, interview, presentation, pictures" taxonomy_operator="IN"]') ?>
					</div>
				</div>				
	</section>
</div>
<script type="text/javascript">
        jssor_<?= $termdata1->term_id; ?>_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $Idle: 0,
              $SlideDuration: 5000,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 280,
              $Align: 0
            };

			var jssor_<?= $termdata1->term_id; ?>_slider = new $JssorSlider$("jssor_<?= $termdata1->term_id; ?>", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_<?= $termdata1->term_id; ?>_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_<?= $termdata1->term_id; ?>_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
</script>
  <?php $counter++; } ?>

  <!------ Content 2018 Start here ------>
   </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<link href="http://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
	<script src="http://vjs.zencdn.net/4.12/video.js"></script>
	<script type="text/javascript">
		$( document ).ready( function(){
			<?php
				foreach ($terms_ref as $slider_ref) {
					echo $slider_ref.'();';					    
				}
			?>
		});
	</script>
<?php get_footer();?>